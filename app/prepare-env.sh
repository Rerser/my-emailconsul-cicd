#!/bin/bash

if [[ $NODE_ENV == 'staging' ]]; then
  echo 'prepare env for staging'
  rm .env.production
  cp .env.staging .env.production
fi
