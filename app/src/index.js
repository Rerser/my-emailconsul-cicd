import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from 'store/reducers';
import * as Sentry from '@sentry/browser';
import App from './App';

import 'utils/global';

if (process.env.NODE_ENV !== 'development') {
  Sentry.init({
    dsn:
      'https://7de55d9ed3814241b69b50e442e99e4f@o374497.ingest.sentry.io/5193587',
  });
}

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
