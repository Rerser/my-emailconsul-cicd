export const toastErrorConfig = {
  position: 'top-right',
  autoClose: false,
  hideProgressBar: true,
  closeOnClick: true,
  draggable: false,
};
