import messages_2_6_x from './v2.6x';
import messages_3_0_x from './v3.0.x';
import messages_3_1_x from './v3.1.x';
import messages_3_2_x from './v3.2.x';
import messages_3_3_x from './v3.3.x';

export default {
  ...messages_2_6_x,
  ...messages_3_0_x,
  ...messages_3_1_x,
  ...messages_3_2_x,
  ...messages_3_3_x,
};
