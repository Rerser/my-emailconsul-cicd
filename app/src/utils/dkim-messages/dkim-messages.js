export default {
  en: {
    'dkim.tag.translation.v': 'Version',
    'dkim.tag.translation.k': 'Key type',
    'dkim.tag.translation.p': 'Public key',
  },
  ru: {
    'dkim.tag.translation.v': 'Версия',
    'dkim.tag.translation.k': 'Тип ключа',
    'dkim.tag.translation.p': 'Публичный ключ',
  },
};
