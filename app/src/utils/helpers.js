import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import Moment from 'react-moment';
import moment from 'moment';
import {
  LOCALE,
  LOCAL_STORAGE_LOCALE_KEY,
  THREE_DAYS,
  MONTH,
  PAYMENT_CURRENCY,
  BILLING_PLAN_NAME,
  DELIVERABILITY_CONSULTING_PRICE_PER_HOUR,
  LIQPAY_URL,
  LOCAL_STORAGE_COUNTS_KEY,
  COUNTS_PERIOD_REQUEST,
  SEEDLISTIN_H_TO_PREFIX_EMAIL,
  DAY,
  DATE_GP_KEY_REGEXP,
  DATE_SNDS_DATE_KEY_REGEXP,
} from 'utils/constants';

export function convertDateTime(dateTime, format = "HH:mm:ss DD-MM-YYYY") {
  return Date.parse(dateTime) ? (
    <Moment date={dateTime} format={format} />
  ) : null;
}

export function convertDateTimeStr(dateTime, format = "HH:mm:ss DD-MM-YYYY") {
  return Date.parse(dateTime)
    ? moment(dateTime).format(format)
    : '';
}

export function validateEmail(email) {
  // eslint-disable-next-line
  const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
  
  return emailRegexp.test(email);
}

export function validateIpAddress(ipAddress) {
  const ipRegexp = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  
  if (ipRegexp.test(ipAddress)) {
    return true;
  }
  return false;
}

export function validateDomainAddress(address) {
  const domainRegexp = /^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/;

  if (domainRegexp.test(address)) {
    return true;
  }
  return false;
}

export function getQS(name) {
  if (window.URLSearchParams) {
    return new window.URLSearchParams(window.location.href.split('?')[1]).get(
      name
    );
  }
  let result = null;
  window.location.search
    .substr(1)
    .split('&')
    .forEach((item) => {
      const tmp = item.split('=');
      if (tmp[0] === name) {
        result = decodeURIComponent(tmp[1]);
      }
    });
  return result;
}

export function getLocale() {
  const localeFromLocalStorage = window.localStorage.getItem(
    LOCAL_STORAGE_LOCALE_KEY
  );

  if (localeFromLocalStorage && LOCALE[localeFromLocalStorage]) {
    return localeFromLocalStorage;
  }

  if (window?.navigator?.languages?.includes('ru')) {
    return LOCALE.ru;
  }

  return LOCALE.en;
}

export function getBillingPlanIsExpired(myUserBillingPlanData) {
  return (
    !myUserBillingPlanData?.timestamp_to ||
    Date.parse(new Date(myUserBillingPlanData?.timestamp_to)) < Date.now()
  );
}

export function getWillBeExpired(myUserBillingPlanData) {
  if (!myUserBillingPlanData) return false;

  const timeStampTo = Date.parse(new Date(myUserBillingPlanData.timestamp_to));
  const currentDate = Date.now();

  return timeStampTo - currentDate <= THREE_DAYS;
}

export function getDayExpired(myUserBillingPlanData) {
  if (!myUserBillingPlanData) return '';

  const timeStampTo = new Date(myUserBillingPlanData.timestamp_to);

  const day = timeStampTo.getDate();
  const month = timeStampTo.getMonth() + 1;
  const year = timeStampTo.getFullYear();

  return `${day < 10 ? `0${day}` : day}-${
    month < 10 ? `0${month}` : month
  }-${year}`;
}

export function setTimeStampFromDate({ timestamp_from, timestamp_to }) {
  if (!timestamp_from && !timestamp_to) return new Date();
  if (!timestamp_to) return new Date(timestamp_from);

  const timeStampFrom = Date.parse(new Date(timestamp_from));
  const timeStampTo = Date.parse(new Date(timestamp_to));
  const nextTimeStampFromDate =
    timeStampFrom > timeStampTo ? timeStampTo : timeStampFrom;

  return new Date(nextTimeStampFromDate);
}

export function setTimeStampToDate({ timestamp_from, renewal_left }) {
  if (!timestamp_from || !renewal_left) return new Date();

  const timeStampFrom = Date.parse(new Date(timestamp_from));
  const renewalLeft = Date.parse(new Date(renewal_left));
  const nextTimeStampToDate = timeStampFrom + (renewalLeft + 1) * MONTH;

  return new Date(nextTimeStampToDate);
}

export function setNextRenewalExpiredDate(timestamp_from) {
  const timeStampFrom = Date.parse(new Date(timestamp_from));
  const nextRenewalExpiredDate = timeStampFrom + MONTH;

  return new Date(nextRenewalExpiredDate);
}

export function getEmailAddressFromString(str) {
  const res = str.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
  if (res) {
    return res[0];
  } else {
    return '';
  }
}

export function useDebounce(value, delay) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);
    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}

// функция рекурсивного сравнения двух объектов
export function deepCompare(oneObj, twoObj) {
  const leftChain = [];
  const rightChain = [];

  function compare2Objects(x, y) {
    let p;

    // remember that NaN === NaN returns false
    // and isNaN(undefined) returns true
    if (
      isNaN(x) &&
      isNaN(y) &&
      typeof x === 'number' &&
      typeof y === 'number'
    ) {
      return true;
    }

    // Compare primitives and functions.
    // Check if both arguments link to the same object.
    // Especially useful on the step where we compare prototypes
    if (x === y) {
      return true;
    }

    // Works in case when functions are created in constructor.
    // Comparing dates is a common scenario. Another built-ins?
    // We can even handle functions passed across iframes
    if (
      (typeof x === 'function' && typeof y === 'function') ||
      (x instanceof Date && y instanceof Date) ||
      (x instanceof RegExp && y instanceof RegExp) ||
      (x instanceof String && y instanceof String) ||
      (x instanceof Number && y instanceof Number)
    ) {
      return x.toString() === y.toString();
    }

    // At last checking prototypes as good as we can
    // if (!(x instanceof Object && y instanceof Object)) {
    //   return false;
    // }

    // if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
    //   return false;
    // }

    // if (x.constructor !== y.constructor) {
    //   return false;
    // }

    // if (x.prototype !== y.prototype) {
    //   return false;
    // }

    // Check for infinitive linking loops
    if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
      return false;
    }

    // Quick checking of one object being a subset of another.
    for (p in y) {
      if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
        return false;
      }
      if (typeof y[p] !== typeof x[p]) {
        return false;
      }
    }

    for (p in x) {
      if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
        return false;
      }
      if (typeof y[p] !== typeof x[p]) {
        return false;
      }

      switch (typeof x[p]) {
        case 'object':
        case 'function':
          leftChain.push(x);
          rightChain.push(y);

          if (!compare2Objects(x[p], y[p])) {
            return false;
          }

          leftChain.pop();
          rightChain.pop();
          break;

        default:
          if (x[p] !== y[p]) {
            return false;
          }
          break;
      }
    }

    return true;
  }

  return compare2Objects(oneObj, twoObj);
}

// Regex мобильных устройств
const MobileDevicesRegEx = /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/i;
// функция определения мобильных устройств
export function getIsMobile() {
  return MobileDevicesRegEx.test(window.navigator.userAgent);
}

// для пагинации
export const getPaginal = ({ offset, limit, count }) => ({
  page: offset / limit,
  pages: count / limit,
  pageSize: limit,
});

export function usePagination({ data, action }) {
  const dispatch = useDispatch();

  const paginal = getPaginal(data);

  const handlePageSize = (page) => {
    dispatch(
      action({
        ...data,
        limit: parseInt(page) || 30,
        offset: 0,
      })
    );
  };

  const handlePage = (page) => {
    dispatch(
      action({
        ...data,
        offset: page * data.limit,
      })
    );
  };

  return { paginal, handlePageSize, handlePage };
}

export function getStatusError(error) {
  if (error) {
    const splittedError = error.toString().split(' ');
    const value = parseInt(splittedError[splittedError.length - 1]);
    return isNaN(value) ? -1 : value;
  }
  return -1;
}

export function getNameUserBillingPlan(name) {
  switch (name) {
    case BILLING_PLAN_NAME.JUNIOR:
      return 'STARTER';
    case BILLING_PLAN_NAME.MIDDLE:
      return 'STANDARD';
    default:
      return name;
  }
}

export function getConsultingPrice(paymentExchangeData, currency) {
  const usdExchangedData = paymentExchangeData.find(
    (data) => data.ccy === PAYMENT_CURRENCY.USD
  );
  const findedExchangedData = paymentExchangeData.find(
    (data) => data.ccy === currency
  );

  // TODO: DELIVERABILITY_CONSULTING_PRICE_PER_HOUR - значение надо будет брать с бекенда
  if (!findedExchangedData) return DELIVERABILITY_CONSULTING_PRICE_PER_HOUR;

  if (currency === PAYMENT_CURRENCY.UAH) {
    return DELIVERABILITY_CONSULTING_PRICE_PER_HOUR * usdExchangedData.buy;
  }

  if (currency === PAYMENT_CURRENCY.USD) {
    return DELIVERABILITY_CONSULTING_PRICE_PER_HOUR;
  }

  return (
    (DELIVERABILITY_CONSULTING_PRICE_PER_HOUR * usdExchangedData.buy) /
    findedExchangedData.buy
  );
}

export function getLiqpayPatmentLink(data, signature) {
  return `${LIQPAY_URL}?data=${data}&signature=${signature}`;
}

// функция чтения всех количеств запросов из localstorage
export function getAllCounts() {
  try {
    return JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_COUNTS_KEY));
  } catch (error) {
    console.error(error);
    return false;
  }
}

// функция проверки количества запросов из localstorage
function getAndCheckCounts({ type, data }) {
  // текущая дата
  const currentDate = Date.now();
  // данные из localstorage
  // если есть данные и время запросов не истекло (напр., за сутки)
  return data && currentDate - data.timeStart[type] <= COUNTS_PERIOD_REQUEST
    ? data
    : false;
}

// функция сохранения количества запросов в localstorage
// type - 'ip' | 'domain' | 'spf' | 'dkim' | 'dmarc' ...
// savedData - данные в local storage, напр.
//   {timeStart: {ip: 1611601674993}, count: {ip: 1}}
//   передается после чтения во временную переменную
// maxRequest - макс. количество запросов
export function setCounts({ type, savedData, maxRequest }) {
  if (!type) return;

  let allData;

  if (!savedData) {
    allData = getAllCounts(); // все данные из localstorage (check table)
  } else {
    allData = savedData;
  }

  const data = getAndCheckCounts({ type, data: allData }); // данные из localstorage по конкретному типу (check table)
  const currentDate = Date.now(); // текущая дата
  let timeStart = currentDate;
  let count = 0;
  let objLS = null;

  // если есть данные в LS по конкретному типу (check table)
  if (data) {
    // переписать все данные в новый объект
    objLS = { ...allData };
    // если время от предыдущих запросов истекло
    if (currentDate - timeStart >= COUNTS_PERIOD_REQUEST) {
      timeStart = currentDate;
      count = 0;
    } else if (data.count[type] >= maxRequest) {
      // если количество запросов превышено
      return;
    } else {
      timeStart = data.timeStart[type];
      count = data.count[type] + 1;
    }
    objLS.timeStart[type] = timeStart;
    objLS.count[type] = count;
  } else {
    // если нет данных по конкретному типу, но есть по другим типам
    if (allData) {
      // переписать оставшиеся значения и добавить новое
      objLS = {
        timeStart: {
          ...allData.timeStart,
          [type]: currentDate,
        },
        count: {
          ...allData.count,
          [type]: 1,
        },
      };
    } else {
      // если нет значений никаких типов созлать новое
      objLS = {
        timeStart: {
          [type]: currentDate,
        },
        count: {
          [type]: 0,
        },
      };
    }
  }
  // запишем в localstorage
  window.localStorage.setItem(LOCAL_STORAGE_COUNTS_KEY, JSON.stringify(objLS));
  return objLS;
}

export function getHToEmail(seedlistingId) {
  return `${seedlistingId}${SEEDLISTIN_H_TO_PREFIX_EMAIL}`;
}

export function copyDataFromElemToClipboard(domElem) {
  if (!domElem) return;

  /* Select the text field */
  domElem.select();
  domElem.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand('copy');
}

export function getDifferenceDays(dateFromISO, dateToISO) {
  const dateFrom = new Date(dateFromISO);
  const dateTo = new Date(dateToISO);

  return Math.abs(dateFrom - dateTo) / DAY + 1; // inclusive last
}

export function addNDayToDate(dateISO, nDay) {
  return new Date(Date.parse(dateISO) + nDay * DAY).toISOString();
}

export function getFormattedDaysYYYYMMDD(dateISO) {
  const date = new Date(dateISO);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return `${year}${month < 10 ? '0' + month : month}${day < 10 ? '0' + day : day}`
}

export function getFormattedDaysMMDD(dateISO) {
  const date = new Date(dateISO);
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return `${month < 10 ? '0' + month : month}/${day < 10 ? '0' + day : day}`
}

export function getFormattedDaysMMDDFromYYYYMMDD(dateYYYYMMDD) {
  const [,, month = '', day = '' ] = dateYYYYMMDD.match(DATE_GP_KEY_REGEXP);
  const numberMonth = Number(month);

  if (!month || !day || isNaN(numberMonth)) {
    return '';
  }
  return `${month}/${day}`;
}

export function getFormattedDaysMMDDFromDDMMYY(dateYYYYMMDD) {
  const [, month = '', day = ''] = dateYYYYMMDD.match(DATE_SNDS_DATE_KEY_REGEXP);
  const numberMonth = Number(month);

  if (!month || !day || isNaN(numberMonth)) {
    return '';
  }
  return `${month}/${day}`;
}

export function gotoExternalLink(link, targetBlank = false) {
  if (link) {
    // for modern browsers
    if (targetBlank) {
      const authWindow = window.open();

      if (authWindow) {
        authWindow.opener = null;
        authWindow.location.href = link;
        return;
      }
    }
    // for old browsers
    const a = document.createElement('A');

    a.href = link;
    if (targetBlank) {
      a.target = '_blank';
      a.rel = 'noopener noreferrer';
    }
    a.style = {
      position: 'absolute',
      visibility: 'hidden',
      opacity: '0',
    }
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

export function getNumWithRanks(number, countDigit = 3, divider = '') {
  const splittedNum = String(number).split('');
  const reversedNumberArr = splittedNum.reverse();
  const newNumberArr = [];

  for (let i = 0; i < reversedNumberArr.length; i++) {
    if (!(i % countDigit) && i > 0) {
      newNumberArr.push(divider);
    }
    newNumberArr.push(reversedNumberArr[i]);
  }

  return newNumberArr.reverse().join('');
}