export function decode(content) {
  let val;
  let raw;
  try {
    raw = content;
    val = window.atob(raw.replace(/\n/g, ''));
  } catch (e) {
    try {
      val = raw
        .replace(/=(..)/g, (v) =>
          String.fromCharCode(parseInt(v.replace('=', ''), 16))
        )
        .replace(/=\n/g, '')
        .replace(/=\r\n/g, '');
    } catch (ee) {
      val = ee.message;
    }
  }
  return val;
}

export function getBody(contentType, content) {
  let emailBody = content;

  if (content.includes('text/html')) {
    const htmlMatch = content.match('<html(.|\r|\n)*html>');
    if (htmlMatch && htmlMatch[0]) {
      emailBody = htmlMatch[0];
    }
  } else if (contentType) {
    if (contentType.includes('text/html')) {
      const htmlMatch = content.match('<html(.|\r|\n)*html>');
      if (htmlMatch && htmlMatch[0]) {
        emailBody = htmlMatch[0];
      }
    } else if (contentType.includes('multipart/alternative')) {
      const contentTypeHeaderMatch = contentType.match('boundary="(.*?)"');

      if (contentTypeHeaderMatch && contentTypeHeaderMatch[1]) {
        const boundary = contentTypeHeaderMatch[1];
        const bufferMatchOnBoundary = content.match(
          `--${boundary}(.|\r|\n)*${boundary}--`
        );

        if (bufferMatchOnBoundary && bufferMatchOnBoundary[0]) {
          emailBody = bufferMatchOnBoundary[0];
        }
      }
    }
  }
  return emailBody;
}
