export default {
  en: {
    'dmarc.tag.translation.v':
      'The DMARC version should always be “DMARC1”. Note: A wrong, or absent DMARC version tag would cause the entire record to be ignored.',
    'dmarc.tag.translation.p':
      'Policy applied to emails that fails the DMARC check. Authorized values: “none”, “quarantine”, or “reject”. “none” is used to collect feedback and gain visibility into email streams without impacting existing flows. “quarantine” allows Mail Receivers to treat email that fails the DMARC check as suspicious. Most of the time, they will end up in your SPAM folder. “reject” outright rejects all emails that fail the DMARC check.',
    'dmarc.tag.translation.adkim':
      'Specifies “Alignment Mode” for DKIM signatures. Authorized values: “r”, “s”. “r”, or “Relaxed Mode”, allows Authenticated DKIM d= domains that share a common Organizational Domain with an email’s “header-From:” domain to pass the DMARC check. “s”, or “Strict Mode” requires exact matching between the DKIM d= domain and an email’s “header-From:” domain.',
    'dmarc.tag.translation.aspf':
      'Specifies “Alignment Mode” for SPF. Authorized values: “r”, “s”. “r”, or “Relaxed Mode” allows SPF Authenticated domains that share a common Organizational Domain with an email’s “header-From:” domain to pass the DMARC check. “s”, or “Strict Mode” requires exact matching between the SPF domain and an email’s “header-From:” domain.',
    'dmarc.tag.translation.sp':
      'Policy to apply to email from a sub-domain of this DMARC record that fails the DMARC check. Authorized values: “none”, “quarantine”, or “reject”. This tag allows domain owners to explicitly publish a “wildcard” sub-domain policy.',
    'dmarc.tag.translation.fo':
      'Forensic reporting options. Authorized values: “0”, “1”, “d”, or “s”. “0” generates reports if all underlying authentication mechanisms fail to produce a DMARC pass result, “1” generates reports if any mechanisms fail, “d” generates reports if DKIM signature failed to verify, “s” generates reports if SPF failed.',
    'dmarc.tag.translation.ruf':
      'The list of URIs for receivers to send Forensic reports to. Note: This is not a list of email addresses, as DMARC requires a list of URIs of the form “mailto:address@example.org”.',
    'dmarc.tag.translation.rua':
      'The list of URIs for receivers to send XML feedback to. Note: This is not a list of email addresses, as DMARC requires a list of URIs of the form “mailto:address@example.org”.',
    'dmarc.tag.translation.rf':
      'The reporting format for individual Forensic reports. Authorized values: “afrf”, “iodef”.',
    'dmarc.tag.translation.pct':
      'The percentage tag tells receivers to only apply policy against email that fails the DMARC check x amount of the time. For example, “pct=25” tells receivers to apply the “p=” policy 25% of the time against email that fails the DMARC check. Note: The policy must be “quarantine” or “reject” for the percentage tag to be applied.',
    'dmarc.tag.translation.ri':
      'The reporting interval for how often you’d like to receive aggregate XML reports. You’ll most likely receive reports once a day regardless of this setting.',
  },
  ru: {
    'dmarc.tag.translation.v':
      'Версия DMARC всегда должна быть "DMARC1". Примечание. Неверный или отсутствующий тег версии DMARC приведет к игнорированию всей записи.',
    'dmarc.tag.translation.p':
      'Политика применяется к сообщениям, не прошедшим проверку DMARC. Авторизованные значения: "none", “quarantine” или “reject”. "None" используется для сбора отзывов и получения информации о потоках имейл, не влияя на существующие потоки. "Quarantine" позволяет получателям почты рассматривать имейл, который не проходит проверку DMARC, как подозрительный. В большинстве случаев они попадают в папку СПАМ. «Reject» полностью отклоняет все имейл, не прошедшие проверку DMARC.',
    'dmarc.tag.translation.adkim':
      'Задает "Режим выравнивания" для подписей DKIM. Разрешенные значения: "r", "s". "r", или "мягкий режим", позволяет аутентифицированным доменам DKIM d=, которые имеют общий организационный домен с доменом "header-From:" сообщения имейла, проходить проверку DMARC. "s" или "строгий режим" требует точного совпадения между доменом DKIM d= и доменом "header-From:" имейла.',
    'dmarc.tag.translation.aspf':
      'Задает "Режим выравнивания" для SPF. Разрешенные значения: "r", "s". "r" или "мягкий режим" позволяет доменам с SPF-аутентификацией, которые имеют общий организационный домен с доменом "header-From:" сообщения имейла, проходить проверку DMARC. "S" или "строгий режим" требует точного соответствия между доменом SPF и доменом "header-From:" имейла.',
    'dmarc.tag.translation.sp':
      'Политика, применяемая к имейлу из поддомена этой записи DMARC, которая не прошла проверку DMARC. Разрешенные значения: "none", "quarantine" или "reject". Этот тег позволяет владельцам доменов явно публиковать политику субдомена с "подстановочными знаками".',
    'dmarc.tag.translation.fo':
      'Возможности судебной отчетности. Разрешенные значения: "0", "1", "d" или "s". "0" генерирует отчеты, если все базовые механизмы аутентификации не дают результата прохождения DMARC, "1" генерирует отчеты, если какие-либо механизмы не работают, "d" генерирует отчеты, если не удалось проверить подпись DKIM, "s" генерирует отчеты, если SPF не удалось.',
    'dmarc.tag.translation.ruf':
      'Список URI для получателей, которым будут отправляться отчеты Forensic. Примечание. Это не список адресов имейл, так как DMARC требует список URI в форме "mailto:address@example.org".',
    'dmarc.tag.translation.rua':
      'Список URI, которым получатели будут отправлять обратную связь XML. Примечание. Это не список адресов имейл, так как DMARC требует список URI в форме "mailto:address@example.org".',
    'dmarc.tag.translation.rf':
      'Формат отчетов для индивидуальных судебных отчетов. Разрешенные значения: "afrf", "iodef".',
    'dmarc.tag.translation.pct':
      'Тег процента указывает получателям применять политику только к электронной почте, которая не проходит проверку DMARC x количество раз. Например, "pct=25" указывает получателям применять политику "p=" в 25% случаев к имейлу, не прошедшему проверку DMARC. Примечание: для применения процентного тега политика должна быть "quarantine" или "reject".',
    'dmarc.tag.translation.ri':
      'Интервал отчетности, определяющий, как часто вы хотите получать сводные отчеты в формате XML. Скорее всего, вы будете получать отчеты раз в день, независимо от этого параметра.',
  },
};
