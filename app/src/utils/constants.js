import timezones from 'utils/timezones.json';

export const MIN_SCREEN_WIDTH_BREAKPOINT = 960;
export const TABLETS_WIDTH_BREAKPOINT = 768;

export const LOCAL_STORAGE_TOKEN_KEY = 'emailconsul-token';
export const LOCAL_STORAGE_LOCALE_KEY = 'emailconsul-locale';

export const COLUMN_TABLE_MIN_WIDTH_PORTION = 0.5; // доля минимальной ширины от исходной ширины ячейки таблицы

// User
export const USER_ROLE = {
  USER: 'USER',
  ADMIN: 'ADMIN',
};

export const USER_ROLE_OPTIONS = [
  { value: USER_ROLE.USER, label: USER_ROLE.USER },
  { value: USER_ROLE.ADMIN, label: USER_ROLE.ADMIN },
];

// Billing plan
export const BILLING_PLAN_NAME = {
  JUNIOR: 'JUNIOR',
  MIDDLE: 'MIDDLE',
  SENIOR: 'SENIOR',
  ON_GO: 'ON_GO',
  NONE: 'NONE',
  EXPIRED: 'EXPIRED',
};

// Billing plan type
export const BILLING_PLAN_TYPE = {
  MONTHLY: 'MONTHLY',
  YEARLY: 'YEARLY',
};

export const HOST_LABEL = {
  GMAIL: 'Gmail',
  MAIL_RU: 'Mail.ru',
  RAMBLER: 'Rambler',
  YANDEX: 'Yandex',
  UKR_NET: 'Ukr.net',
  YAHOO: 'Yahoo',
  HOTMAIL: 'Hotmail',
  AOL: 'AOL',
  PRXY: 'prxy',
  SEZNAM: 'Seznam',
  NGS: 'ngs.ru',
};

export const LOCAL_STORAGE_HOSTS_SETTINGS = 'hosts_settings';

export const BILLING_PLAN_SEEDLISTING_MAX_EMAILS_PER_ITERATION = 25;

// listcleaning
export const LISTCLEANING_FILE_TYPE = {
  SYNTAX_NOT_VALID: 'SYNTAX_NOT_VALID',
  MX_NOT_VALID: 'MX_NOT_VALID',
  MX_AND_LOOKUP_NOT_VALID: 'MX_AND_LOOKUP_NOT_VALID',
  BLACKLIST: 'BLACKLIST',
  ROLE: 'ROLE',
  CLEAN: 'CLEAN',
};

// seedlisting
export const SEEDLISTING_STATUS = {
  ACTIVE: 'ACTIVE',
  DONE: 'DONE',
  FAILED: 'FAILED',
  TERMINATED: 'TERMINATED',
};

export const SEEDLISTING_EMAIL_STATUS = {
  WAITING: 'WAITING',
  INBOX: 'INBOX',
  SPAM: 'SPAM',
  NOT_RECEIVED: 'NOT_RECEIVED',
};

export const SEEDLISTING_WORKER_STATUS = {
  STARTING: 'STARTING',
  ACTIVE: 'ACTIVE',
  TERMINATED: 'TERMINATED',
  FAILED: 'FAILED',
};

export const AUTHENTICATION_NAMES = ['SPF', 'DKIM', 'DMARK'];

export const DKIM_SIGNATURE_HEADER = 'dkim-signature';
export const FROM_HEADER = 'from';
export const RETURN_PATH_HEADER = 'return-path';
export const MAIL_FROM_HEADER = 'mail-from';
export const ENVELOPE_FROM_HEADER = 'envelope-from';
export const MFROM_HEADER = 'mfrom';
export const HEADER_FROM_HEADER = 'header-from';

// auth
export const AUTH_ACTIVATION_RESULT_STATUS = {
  SUCCESS: 'SUCCESS',
  FAILED: 'FAILED',
};

// locale
export const LOCALE = {
  en: 'en',
  ru: 'ru',
};

// payment
export const PAYMENT_CURRENCY = {
  USD: 'USD',
  EUR: 'EUR',
  RUR: 'RUR',
  UAH: 'UAH',
};

export const CURRENCY_SIGN = {
  [PAYMENT_CURRENCY.USD]: '$',
  [PAYMENT_CURRENCY.EUR]: '€',
  [PAYMENT_CURRENCY.RUR]: '₽',
  [PAYMENT_CURRENCY.UAH]: '₴',
};

// hours
export const HOURS = {
  0: '12:00 AM',
  1: '1:00 AM',
  2: '2:00 AM',
  3: '3:00 AM',
  4: '4:00 AM',
  5: '5:00 AM',
  6: '6:00 AM',
  7: '7:00 AM',
  8: '8:00 AM',
  9: '9:00 AM',
  10: '10:00 AM',
  11: '11:00 AM',
  12: '12:00 PM',
  13: '1:00 PM',
  14: '2:00 PM',
  15: '3:00 PM',
  16: '4:00 PM',
  17: '5:00 PM',
  18: '6:00 PM',
  19: '7:00 PM',
  20: '8:00 PM',
  21: '9:00 PM',
  22: '10:00 PM',
  23: '11:00 PM',
};

export const TIMEZONES = timezones
  .sort((a, b) => a.utc_offset_sec - b.utc_offset_sec)
  .map((item, index) => ({ ...item, index }));

// ips
export const IP_STATUS = {
  WAITING: 'WAITING',
  IN_PROGRESS: 'IN_PROGRESS',
  UPDATED: 'UPDATED',
  FAILED: 'FAILED',
};

// domains
export const DOMAIN_STATUS = {
  WAITING: 'WAITING',
  IN_PROGRESS: 'IN_PROGRESS',
  UPDATED: 'UPDATED',
  FAILED: 'FAILED',
};

export const NEW_DOMAIN_MODE = {
  SINGLE: 'SINGLE',
  CSV: 'CSV',
};

export const NEW_IP_MODE = {
  SINGLE: 'SINGLE',
  NETMASK: 'NETMASK',
  CSV: 'CSV',
};

export const NEW_GP_DOMAIN_MODE = {
  SINGLE: 'SINGLE',
  CSV: 'CSV',
};

// Edit user billing plan
export const ESTABLISH_UPDATE_MODE = 'ESTABLISH_UPDATE_MODE';
export const SET_NEW_LIMITS_UPDATE_MODE = 'SET_NEW_LIMITS_UPDATE_MODE';
export const DISCARD_UPDATE_MODE = 'DISCARD_UPDATE_MODE';

export const CONTROL_ICON_COLOR = '#3F9FDF'; // цвет иконки плюс/минус
export const WARNING_ICON_COLOR = '#E7B416'; // цвет иконки предупреждения
export const INFO_ICON_COLOR = '#0177A4'; // цвет иконки примечания
export const ERROR_ICON_COLOR = '#E74C3C'; // цвет иконки ошибки

export const INFO_CONTAINER_STATUS = {
  error: 'error',
  warning: 'warning',
  info: 'info',
};

export const NOT_FOUND_ERROR_STATUS = 404;

export const HOUR = 3600 * 1000;
export const DAY = 24 * HOUR;
export const WEEK = 7 * DAY;
export const THREE_DAYS = DAY * 3;
export const MONTH = DAY * 30;

export const YESTERDAY = new Date(new Date(Date.now() - DAY * 2).setUTCHours(0, 0, 0, 0));
export const DAY_BEFORE_YESTERDAY = new Date(new Date(Date.now() - DAY * 2).setUTCHours(0, 0, 0, 0));
export const MINUS_30_DAYS_FROM_YESTERDAY = new Date(new Date(Date.now() - DAY - MONTH).setUTCHours(0, 0, 0, 0));

export const DELIVERABILITY_CONSULTING_PRICE_PER_HOUR = 50;
export const DELIVERABILITY_CONSULTING_MODE = 'DELIVERABILITY_CONSULTING';
export const SUPPORT_EMAIL = 'support@emailconsul.com';

export const LIQPAY_URL = 'https://www.liqpay.ua/api/3/checkout';

export const LOCAL_STORAGE_COUNTS_KEY = 'emailconsul-counts';
export const DELIVERABILITY_CONSULTING_TYPE = 'deliverabilityConsulting';
export const DELIVERABILITY_CONSULTING_MAX_REQUEST = 3;
export const COUNTS_PERIOD_REQUEST = DAY;

export const SEEDLISTING_CHECK_TYPE = {
  REF: 'REF',
  TAG: 'TAG',
  H_TO: 'H_TO',
};
export const SEEDLISTIN_H_TO_PREFIX_EMAIL = '@emailconsul.com';

const ONE_KILO_BYTE = 2 ** 10;
export const ONE_MEGA_BYTE = ONE_KILO_BYTE ** 2;

export const GP_REPUTATION_CATEGORY = {
  REPUTATION_CATEGORY_UNSPECIFIED: 'REPUTATION_CATEGORY_UNSPECIFIED',
  HIGH: 'HIGH',
  MEDIUM: 'MEDIUM',
  LOW: 'LOW',
  BAD: 'BAD',
};

export const GP_TABLE_SORT_BY = {
  NONE: 'NONE',
  AUTHENTICATION_AVERAGES: {
    SPF: 'AUTHENTICATION_AVERAGE_SPF',
    DKIM: 'AUTHENTICATION_AVERAGES_DKIM',
    DMARC: 'AUTHENTICATION_AVERAGES_DMARC',
  },
  IP_REPUTATION: {
    BAD: 'IP_REPUTATION_BAD',
    LOW: 'IP_REPUTATION_LOW',
    MEDIUM: 'IP_REPUTATION_MEDIUM',
    HIGH: 'IP_REPUTATION_HIGH',
  },
};

export const DEMO_USER_EMAIL = 'demo@test.com';

export const CHART_COLUMN_REPUTATION_COLORS = {
  [GP_REPUTATION_CATEGORY.BAD]: '#5b5b5b',
  [GP_REPUTATION_CATEGORY.LOW]: '#e74c3c',
  [GP_REPUTATION_CATEGORY.MEDIUM]: '#f39c12',
  [GP_REPUTATION_CATEGORY.HIGH]: '#2ecc71',
  [GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED]: '#aca8a8',
};

export const GP_SHOW_TABLE = {
  DOMAIN: 'DOMAIN',
  IP: 'IP',
};

export const DATE_GP_KEY_REGEXP = /(\d{4})(\d{2})(\d{2})/;

export const LANDING_FAQ_URL = `${process.env.REACT_APP_LANDING_URL}/faq`;

export const IP_DOMAINS_RBL_CHANGES_LOGS_STATUS = {
  BLACKLISTED: 'BLACKLISTED',
  RESOLVED: 'RESOLVED',
};

export const IP_DOMAINS_RBL_CHANGES_LOGS_COLORS = {
  [IP_DOMAINS_RBL_CHANGES_LOGS_STATUS.BLACKLISTED]: '#e74c3c',
  [IP_DOMAINS_RBL_CHANGES_LOGS_STATUS.RESOLVED]: '#2ecc71',
};

export const GP_DOMAINS_HISTORY_STATUS = {
  IN_PROGRESS: 'IN_PROGRESS',
  DONE: 'DONE',
  FAILED: 'FAILED',
}

export const GP_DAYS_LIMIT = 30;

export const HOVERED_GP_ITEMS = {
  none: 'none',
  row: 'row',
  badReputation: 'badReputation',
  lowReputation: 'lowReputation',
  mediumReputation: 'mediumReputation',
  highReputation: 'highReputation',
}

export const SNDS_INFO_MODE = {
  DASHBOARDS: 'DASHBOARDS',
  IP_DATA: 'IP_DATA',
  IP_STATUS: 'IP_STATUS',
}

export const USER_SNDS_CREDENTIALS_STATUS = {
  ACTIVE: 'ACTIVE',
  TERMINATED: 'TERMINATED',
  FAILED: 'FAILED',
};

export const SNDS_FILTER_RESULT = {
  GREEN: 'GREEN',
  YELLOW: 'YELLOW',
  RED: 'RED',
};

export const SNDS_IP_DATA_FILTER_COLORS = {
  [SNDS_FILTER_RESULT.GREEN]: '#2ecc71',
  [SNDS_FILTER_RESULT.YELLOW]: '#f39c12',
  [SNDS_FILTER_RESULT.RED]: '#e74c3c',
};

export const SNDS_DAYS_LIMIT = 30;

export const SNDS_MAX_CHOSE_USER_IPS = 33;
export const SNDS_DASHBOARDS_REQ_IPS_LIMIT = 1000;

export const ENTER_KEY_CODE = 13;

export const SNDS_HISTORY_STATUS = {
  IN_PROGRESS: 'IN_PROGRESS',
  DONE: 'DONE',
  FAILED: 'FAILED',
}

export const DATE_SNDS_DATE_KEY_REGEXP = /(\d{2})(\d{2})(\d{2})/;
