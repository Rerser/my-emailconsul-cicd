import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

import Layout from 'components/layout/Layout';
import Spinner from 'components/ui/spinner';

import {
  authTokenSelector,
  usersMeSelector,
  myUserBillingPlanDataSelector,
} from 'store/selectors';
import { BILLING_PLAN_NAME, USER_ROLE } from 'utils/constants';

const ProtectedRoute = ({
  exact = true,
  roles,
  path,
  component,
  children,
  authToken,
  usersMe,
  myUserBillingPlanData,
  ...props
}) => {
  if (authToken && (!usersMe || !myUserBillingPlanData)) {
    return <Spinner />;
  }

  return (
    <Route
      exact={exact}
      path={path}
      render={(...props) => {
        if (!authToken || !usersMe || !roles.includes(usersMe.role)) {
          return <Redirect to="/login" />;
        }

        if (
          authToken &&
          (!myUserBillingPlanData?.current_plan ||
            myUserBillingPlanData.current_plan === BILLING_PLAN_NAME.NONE) &&
          path !== '/user' &&
          usersMe.role !== USER_ROLE.ADMIN
        ) {
          return <Redirect to="/user/billing" />;
        }

        return (
          <Layout>
            <Route children={children} component={component} {...props} />
          </Layout>
        );
      }}
    />
  );
};

ProtectedRoute.defaultProps = {
  authToken: PropTypes.string,
  usersMe: PropTypes.object,
  roles: PropTypes.array,
};
ProtectedRoute.defaultProps = {
  authToken: null,
  usersMe: null,
  roles: [],
};

export default connect(
  (state) => ({
    authToken: authTokenSelector(state),
    usersMe: usersMeSelector(state),
    myUserBillingPlanData: myUserBillingPlanDataSelector(state),
  }),
  {}
)(ProtectedRoute);
