import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

import { authTokenSelector } from 'store/selectors';

const GuestRoute = ({ path, component, children, authToken, ...props }) => (
  <Route
    exact
    path={path}
    render={(...props) => {
      if (authToken) {
        return <Redirect to="/" />;
      }

      return <Route children={children} component={component} {...props} />;
    }}
  />
);

GuestRoute.defaultProps = {
  authToken: PropTypes.string,
};
GuestRoute.defaultProps = {
  authToken: null,
};

export default connect(
  (state) => ({
    authToken: authTokenSelector(state),
  }),
  {}
)(GuestRoute);
