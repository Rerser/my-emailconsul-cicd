import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import {
  getDmarcData,
  setDmarcData,
  setDmarcDataError
} from 'store/actions/dmarc';
import {
  dmarcDataIsLoadingSelector,
  dmarcDataSelector
} from 'store/selectors';

import withCheckUserBP from 'components/hocs/withCheckUserBP';
import DMARC from 'components/sections/seedlisting/seedlistingItem/components/seedlistingEmailInfo/components/emailInfo/components/emailAuthentication/components/DMARC';
import Button from 'components/ui/button/Button';
import InputText from 'components/ui/inputText';
import Spinner from 'components/ui/spinner';

import { validateDomainAddress } from 'utils/helpers';

import './dmarc-page.scss';

function DMARCPage() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const [domain, setDomain] = useState('');
  const [inputError, setInputError] = useState('');

  useEffect(() => {
    return () => {
      dispatch(setDmarcData(null));
      dispatch(setDmarcDataError(null));
    }
  }, [dispatch]);

  const dmarcData = useSelector(dmarcDataSelector);
  const dmarcDataIsLoading = useSelector(dmarcDataIsLoadingSelector);

  const handleChangeDomain = (value) => {
    if (inputError) {
      setInputError('');
    }
    setDomain(value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!inputError && validateDomainAddress(domain)) {
      setInputError('');
      return dispatch(getDmarcData(domain))
    }
    return setInputError(t('dmarc-page.input.domain.error.text'));
  }

  return (
    <div className="dmarc-page">
      <div className="dmarc-page__header">
        <h1 className="dmarc-page__title">
          {t('dmarc-page.title')}
        </h1>

        <form className="dmarc-page__header-right header-right" onSubmit={handleSubmit}>
          <InputText
            className={cn("header-right__input", {
              "header-right__input--error": inputError,
            })}
            placeholder={t('dmarc-page.input.domain.placeholder.text')}
            value={domain}
            onChange={(e) => handleChangeDomain(e.target.value)}
            errorText={inputError}
          />
          <Button
            type="submit"
            mode="purple"
            text={t('dmarc-page.btn.check.text')}
            className="header-right__btn"
            disabled={Boolean(!domain || inputError || dmarcDataIsLoading)}
          />
        </form>
      </div>
      <div className="dmarc-page__data-wrapper">
        {
          dmarcData
            ? <DMARC data={dmarcData} isLoading={dmarcDataIsLoading}/>
            : dmarcDataIsLoading
              ? <Spinner />
              : null
        }
      </div>
    </div>
  );
}

export default React.memo(withCheckUserBP(DMARCPage));
