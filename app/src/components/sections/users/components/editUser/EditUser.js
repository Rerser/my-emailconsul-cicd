import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import PropTypes from 'prop-types';

import { setEditedUser, handleEditedUser, saveEditedUser } from 'store/actions/users';
import { usersEditedUserSelector, usersEditedUserSavingIsLoadingSelector } from 'store/selectors';
import { USER_ROLE_OPTIONS } from 'utils/constants';

import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Select from 'components/ui/select';
import Checkbox from 'components/ui/checkbox';
import Button from 'components/ui/button';

const EditUser = (props) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const usersEditedUser = useSelector(usersEditedUserSelector);
  const usersEditedUserSavingIsLoading = useSelector(
    usersEditedUserSavingIsLoadingSelector
  );

  const handleChangeActiveUser = () => {
    dispatch(
      handleEditedUser({
        field: 'is_active',
        value: !usersEditedUser.is_active,
      })
    );
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(saveEditedUser(usersEditedUser));
  };

  return (
    <form className="c-slidebar-content" onSubmit={handleSubmit}>
      {props.title && (
        <h3 className="c-slidebar-content__title">
          {props.title}
        </h3>
      )}

      {usersEditedUserSavingIsLoading && <Spinner />}

      {!usersEditedUserSavingIsLoading && usersEditedUser && (
        <Fragment>
          {!usersEditedUser.id && (
            <Fragment>
              <div className="c-slidebar-content__row">
                <label className="c-slidebar-content__label">
                  <span className="c-slidebar-content__description">
                    {t('edit_user.email.label')}
                  </span>
                  <InputText
                    value={usersEditedUser.email}
                    onChange={(e) =>
                      dispatch(
                        handleEditedUser({
                          field: 'email',
                          value: e.target.value,
                        })
                      )
                    }
                  />
                </label>
              </div>

              <div className="c-slidebar-content__row">
                <label className="c-slidebar-content__label">
                  <span className="c-slidebar-content__description">
                    {t('edit_user.password.label')}
                  </span>
                  <InputText
                    value={usersEditedUser.password}
                    onChange={(e) =>
                      dispatch(
                        handleEditedUser({
                          field: 'password',
                          value: e.target.value,
                        })
                      )
                    }
                    type="password"
                  />
                </label>
              </div>
            </Fragment>
          )}

          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user.name.label')}
              </span>
              <InputText
                value={usersEditedUser.name}
                onChange={(e) =>
                  dispatch(
                    handleEditedUser({ field: 'name', value: e.target.value })
                  )
                }
              />
            </label>
          </div>

          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user.role.label')}
              </span>
              <Select
                placeholder={t('edit_user.select.role.placeholder')}
                value={usersEditedUser.role}
                options={USER_ROLE_OPTIONS}
                onChange={(value) =>
                  dispatch(handleEditedUser({ field: 'role', value }))
                }
              />
            </label>
          </div>

          <div className="c-slidebar-content__row c-slidebar-content__row--flex">
            <label className="c-slidebar-content__label c-slidebar-content__label--checkbox">
              <div
                className="c-slidebar-content__description"
                onClick={handleChangeActiveUser}
              >
                {t('edit_user.checkbox.active.title')}
              </div>
              <Checkbox
                checked={usersEditedUser.is_active}
                onChange={handleChangeActiveUser}
              />
            </label>
          </div>
        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {usersEditedUser && (
          <Button
            mode="purple"
            text={
              usersEditedUser.id
                ? t('edit_user.btn.update.text')
                : t('edit_user.btn.add.text')
            }
            className="c-slidebar-content__btn"
            disabled={usersEditedUserSavingIsLoading}
            type="submit"
          />
        )}

        <Button
          text={t('edit_user.btn.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setEditedUser(null))}
        />
      </div>
    </form>
  );
};

EditUser.propsTypes = {
  title: PropTypes.string,
};
EditUser.defaultProps = {
  title: '',
};

export default EditUser;
