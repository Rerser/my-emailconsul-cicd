import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { Link, useParams } from 'react-router-dom';

import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Pagination from 'components/ui/pagination';

import {
  handleUserLogsIsLoading,
  getUserLogs,
  setUserLogsDataPagination,
} from 'store/actions/userLogs';
import {
  userLogsSelector,
  userLogsIsLoadingSelector,
  userLogsPaginationSelector,
} from 'store/selectors';
import { usePagination, convertDateTime } from 'utils/helpers';

import './user-logs.scss';

function UserLogs() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const { id: userId } = useParams();

  const logs = useSelector(userLogsSelector);
  const logsIsLoading = useSelector(userLogsIsLoadingSelector);
  const logsPagination = useSelector(userLogsPaginationSelector);

  const tableData = useMemo(
    () =>
      logs.map((log) => ({
        ...log,
        created_at: convertDateTime(log.created_at, "HH:mm:ss DD-MM-YYYY"),
      })),
    [logs]
  );

  const { limit, offset } = logsPagination;

  useEffect(() => {
    dispatch(handleUserLogsIsLoading(true));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getUserLogs({ user_id: userId, withLoader: false }));
  }, [dispatch, userId, limit, offset]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: logsPagination,
    action: setUserLogsDataPagination,
  });

  if (logsIsLoading) return <Spinner />;

  return (
    <div className="user-logs">
      <div className="user-logs__header">
        <h1 className="user-logs__title">
          <Link to="/users">
            {t('user_logs.breadcrumps.text')}
          </Link>
        </h1>
      </div>
      <Pagination
        page={paginal.page}
        pages={paginal.pages}
        pageSize={paginal.pageSize}
        handlePageSize={handlePageSize}
        handlePage={handlePage}
        className="user-logs__pagination user-logs__pagination--top"
      />

      <Table
        columns={[
          {
            field: 'type',
            title: t('user_logs.table.type'),
            type: 'string',
            width: 130,
          },
          {
            field: 'created_at',
            title: t('user_logs.table.datetime'),
            type: 'string',
            width: 150,
          },
          {
            field: 'payload',
            title: t('user_logs.table.payload'),
            type: 'string',
            width: 800,
          },
        ]}
        data={tableData}
        statisticData={paginal}
        breakword
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="user-logs__pagination user-logs__pagination--bottom"
        />
      )}
    </div>
  );
}

export default React.memo(UserLogs);
