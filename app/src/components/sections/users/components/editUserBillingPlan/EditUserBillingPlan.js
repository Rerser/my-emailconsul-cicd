import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './edit_user_billing_plan.scss';

import {
  setEditedUserBillingPlanUserId,
  getUserBillingPlan,
  handleUserBillingPlanData,
  handleUserBillingPlanDataIps,
  updateUserBillingPlan,
  setUserBillingPlanData,
  handleUserBillingPlanDataListcleaning,
  handleUserBillingPlanDataSeedlisting,
  handleUserBillingPlanDataDomains,
} from 'store/actions/userBillingPlan';
import {
  editedUserBillingPlanUserIdSelector,
  userBillingPlanDataSelector,
  userBillingPlanGeneralSelector,
  userBillingPlanIsDataLoadingSelector,
} from 'store/selectors';

import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';
import Select from 'components/ui/select';

import {
  BILLING_PLAN_NAME,
  BILLING_PLAN_TYPE,
  DISCARD_UPDATE_MODE,
  ESTABLISH_UPDATE_MODE,
  SET_NEW_LIMITS_UPDATE_MODE,
} from 'utils/constants';
import { getNameUserBillingPlan } from 'utils/helpers';

const EditUser = (props) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const editedUserBillingPlanUserId = useSelector(
    editedUserBillingPlanUserIdSelector
  );
  const userBillingPlanData = useSelector(userBillingPlanDataSelector);
  const userBillingPlanIsDataLoading = useSelector(
    userBillingPlanIsDataLoadingSelector
  );
  const userBillingPlanGeneral = useSelector(userBillingPlanGeneralSelector);

  useEffect(() => {
    if (editedUserBillingPlanUserId) {
      dispatch(getUserBillingPlan(editedUserBillingPlanUserId));
    }
    return () => {
      dispatch(setUserBillingPlanData(null));
    };
  }, [dispatch, editedUserBillingPlanUserId]);

  const handleChangeCurrentPlan = (value) => {
    if (
      !!userBillingPlanGeneral &&
      value !== BILLING_PLAN_NAME.NONE &&
      value !== BILLING_PLAN_NAME.EXPIRED &&
      value !== BILLING_PLAN_NAME.ON_GO
    ) {
      dispatch(
        handleUserBillingPlanDataIps({
          field: 'ips_per_month',
          value: userBillingPlanGeneral.ips_per_month[value],
        })
      );
      dispatch(
        handleUserBillingPlanDataDomains({
          field: 'domains_per_month',
          value: userBillingPlanGeneral.domains_per_month[value],
        })
      );
      dispatch(
        handleUserBillingPlanDataSeedlisting({
          field: 'emails_available',
          value: userBillingPlanGeneral.seedlisting_emails_per_month[value],
        })
      );
      dispatch(
        handleUserBillingPlanDataListcleaning({
          field: 'emails_available',
          value: userBillingPlanGeneral.seedlisting_emails_per_month[value],
        })
      );
    }
    dispatch(handleUserBillingPlanData({ field: 'current_plan', value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (userBillingPlanData) {
      dispatch(
        updateUserBillingPlan({
          data: userBillingPlanData,
          mode: ESTABLISH_UPDATE_MODE,
        })
      );
    }
  };

  return (
    <form className="c-slidebar-content" onSubmit={handleSubmit}>
      {props.title && (
        <h3 className="c-slidebar-content__title">
          {props.title}
        </h3>
      )}

      {userBillingPlanIsDataLoading && <Spinner />}

      {!userBillingPlanIsDataLoading && userBillingPlanData && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user_billing.current_plan.label')}
              </span>
              <Select
                options={Object.keys(BILLING_PLAN_NAME)
                  .filter((key) => key !== BILLING_PLAN_NAME.ON_GO)
                  .map((key) => ({
                    value: BILLING_PLAN_NAME[key],
                    label: getNameUserBillingPlan(BILLING_PLAN_NAME[key]),
                    disabled:
                      key === BILLING_PLAN_NAME.NONE ||
                      key === BILLING_PLAN_NAME.EXPIRED,
                  }))}
                value={userBillingPlanData?.current_plan}
                onChange={handleChangeCurrentPlan}
                placeholder={t('edit_user_billing.select.placeholder')}
                resetBtnAvailable={false}
              />
            </label>
          </div>

          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user_billing.current_plan_type.label')}
              </span>
              <Select
                options={Object.keys(BILLING_PLAN_TYPE).map((key) => ({
                  value: BILLING_PLAN_TYPE[key],
                  label: BILLING_PLAN_TYPE[key],
                }))}
                value={userBillingPlanData?.current_plan_type}
                onChange={(value) =>
                  dispatch(
                    handleUserBillingPlanData({
                      field: 'current_plan_type',
                      value,
                    })
                  )
                }
                placeholder={t('edit_user_billing.select.placeholder')}
                resetBtnAvailable={false}
              />
            </label>
          </div>

          <div className="c-slidebar-content__div-line" />
          <div className="c-slidebar-content__row">
            <div className="c-slidebar-content__label">
              {t('edit_user_billing.date_from.label')}
            </div>
            <DatePicker
              dateFormat="MM/dd/yyyy"
              placeholderText="mm/dd/yyyy"
              selected={
                userBillingPlanData?.timestamp_from
                  ? new Date(userBillingPlanData.timestamp_from)
                  : new Date(Date.now())
              }
              onChange={(value) =>
                dispatch(
                  handleUserBillingPlanData({ field: 'timestamp_from', value })
                )
              }
              popperClassName="c-slidebar-content__datepicker c-slidebar-content__datepicker--popper"
              // maxDate={userBillingPlanData?.timestamp_to ? new Date(userBillingPlanData.timestamp_to) : null}
            />
          </div>
          <div className="c-slidebar-content__row">
            {userBillingPlanData?.timestamp_to && (
              <Fragment>
                <div className="c-slidebar-content__label">
                  {t('edit_user_billing.date_to.label')}
                </div>
                <DatePicker
                  dateFormat="MM/dd/yyyy"
                  placeholderText="mm/dd/yyyy"
                  selected={new Date(userBillingPlanData.timestamp_to)}
                  // minDate={userBillingPlanData?.timestamp_from ? new Date(userBillingPlanData.timestamp_from) : null}
                  disabled
                  className="c-slidebar-content__datepicker disabled"
                />
              </Fragment>
            )}
          </div>

          {(userBillingPlanData.hasOwnProperty('renewal_left') ||
            userBillingPlanData?.next_renewal_timestamp) && (
            <div className="c-slidebar-content__div-line" />
          )}
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              {userBillingPlanData.hasOwnProperty('renewal_left') && (
                <Fragment>
                  <span className="c-slidebar-content__description">
                    {t('edit_user_billing.renewal_left.label')}
                  </span>
                  <InputText
                    value={userBillingPlanData?.renewal_left}
                    disabled
                    className="c-slidebar-content__input disabled"
                  />
                </Fragment>
              )}
            </label>
          </div>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              {userBillingPlanData?.next_renewal_timestamp && (
                <Fragment>
                  <div className="c-slidebar-content__description">
                    {t('edit_user_billing.next_renewal_timestamp.label')}
                  </div>
                  <DatePicker
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    selected={
                      new Date(userBillingPlanData.next_renewal_timestamp)
                    }
                    disabled
                    className="c-slidebar-content__datepicker disabled"
                  />
                </Fragment>
              )}
            </label>
          </div>

          <div className="c-slidebar-content__div-line" />
          <div className="c-slidebar-content__subtitle">
            {t('edit_user_billing.content.subtitle.listcleaning')}
          </div>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user_billing.email_available.label')}
              </span>
              <InputText
                value={userBillingPlanData?.listcleaning?.emails_available || 0}
                onChange={(e) => {
                  if (!/^[\d]*$/.test(e.target.value)) {
                    return toast.error(
                      t(
                        'edit_user_billing.toast.number_of_checks.input.only_number_error'
                      )
                    );
                  }
                  dispatch(
                    handleUserBillingPlanDataListcleaning({
                      field: 'emails_available',
                      value: +e.target.value,
                    })
                  );
                }}
              />
            </label>
          </div>

          <div className="c-slidebar-content__div-line" />
          <div className="c-slidebar-content__subtitle">
            {t('edit_user_billing.content.subtitle.seedlisting')}
          </div>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user_billing.email_test_available.label')}
              </span>
              <InputText
                value={userBillingPlanData?.seedlisting?.emails_available || 0}
                onChange={(e) => {
                  if (!/^[\d]*$/.test(e.target.value)) {
                    return toast.error(
                      t(
                        'edit_user_billing.toast.number_of_checks.input.only_number_error'
                      )
                    );
                  }
                  dispatch(
                    handleUserBillingPlanDataSeedlisting({
                      field: 'emails_available',
                      value: +e.target.value,
                    })
                  );
                }}
              />
            </label>
          </div>

          <div className="c-slidebar-content__div-line" />
          <div className="c-slidebar-content__subtitle">
            {t('edit_user_billing.content.subtitle.ips')}
          </div>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user_billing.ips_test_available.label')}
              </span>
              <InputText
                value={userBillingPlanData?.ips?.ips_per_month || 0}
                onChange={(e) => {
                  if (!/^[\d]*$/.test(e.target.value)) {
                    return toast.error(
                      t(
                        'edit_user_billing.toast.number_of_checks.input.only_number_error'
                      )
                    );
                  }
                  dispatch(
                    handleUserBillingPlanDataIps({
                      field: 'ips_per_month',
                      value: e.target.value,
                    })
                  );
                }}
              />
            </label>
          </div>

          <div className="c-slidebar-content__div-line" />
          <div className="c-slidebar-content__subtitle">
            {t('edit_user_billing.content.subtitle.domains')}
          </div>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_user_billing.domains_test_available.label')}
              </span>
              <InputText
                value={userBillingPlanData?.domains?.domains_per_month || 0}
                onChange={(e) => {
                  if (!/^[\d]*$/.test(e.target.value)) {
                    return toast.error(
                      t(
                        'edit_user_billing.toast.number_of_checks.input.only_number_error'
                      )
                    );
                  }
                  dispatch(
                    handleUserBillingPlanDataDomains({
                      field: 'domains_per_month',
                      value: e.target.value,
                    })
                  );
                }}
              />
            </label>
          </div>
        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {!!userBillingPlanData && (
          <Fragment>
            <Button
              mode="purple"
              text={t('edit_user_billing.btn.establish.text')}
              className="c-slidebar-content__btn"
              disabled={
                userBillingPlanIsDataLoading ||
                userBillingPlanData.current_plan === BILLING_PLAN_NAME.NONE ||
                userBillingPlanData.current_plan === BILLING_PLAN_NAME.EXPIRED
              }
              type="submit"
            />

            <Button
              mode="purple"
              text={t('edit_user_billing.btn.set_new_limits.text')}
              className="c-slidebar-content__btn"
              disabled={
                userBillingPlanData.current_plan === BILLING_PLAN_NAME.NONE ||
                userBillingPlanData.current_plan === BILLING_PLAN_NAME.EXPIRED
              }
              onClick={() =>
                dispatch(
                  updateUserBillingPlan({
                    data: userBillingPlanData,
                    mode: SET_NEW_LIMITS_UPDATE_MODE,
                  })
                )
              }
            />
            <Button
              mode="red"
              text={t('edit_user_billing.btn.discard.text')}
              className="c-slidebar-content__btn fullrow"
              disabled={
                userBillingPlanData.current_plan === BILLING_PLAN_NAME.NONE ||
                userBillingPlanData.current_plan === BILLING_PLAN_NAME.EXPIRED
              }
              onClick={() =>
                dispatch(
                  updateUserBillingPlan({
                    data: userBillingPlanData,
                    mode: DISCARD_UPDATE_MODE,
                  })
                )
              }
            />
          </Fragment>
        )}
        <Button
          text={t('edit_user_billing.btn.close.text')}
          className="c-slidebar-content__btn fullrow nomargin"
          onClick={() => dispatch(setEditedUserBillingPlanUserId(false))}
        />
      </div>
    </form>
  );
};

EditUser.propsTypes = {
  title: PropTypes.string,
};
EditUser.defaultProps = {
  title: '',
};

export default EditUser;
