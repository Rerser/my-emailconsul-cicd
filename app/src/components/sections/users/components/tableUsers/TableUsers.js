import React, { useEffect, useMemo, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { useHistory } from 'react-router-dom';

import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Pagination from 'components/ui/pagination';

import { setSwitchedUser } from 'store/actions/auth';
import {
  getUsersData,
  setEditedUser,
  setUsersDataPagination,
  setUsersSorting,
  handleUsersDataIsLoading,
} from 'store/actions/users';
import { setEditedUserBillingPlanUserId } from 'store/actions/userBillingPlan';
import {
  usersDataSelector,
  usersMeSelector,
  usersPaginationSelector,
  usersIsDataLoadingSelector,
  usersSortingSelector,
} from 'store/selectors';

import { usePagination } from 'utils/helpers';

function TableUsers() {
  const dispatch = useDispatch();
  const t = useFormatMessage();
  const history = useHistory();

  const usersData = useSelector(usersDataSelector);
  const usersMe = useSelector(usersMeSelector);
  const usersIsDataLoading = useSelector(usersIsDataLoadingSelector);
  const usersSorting = useSelector(usersSortingSelector);

  const usersPagination = useSelector(usersPaginationSelector);

  const tableData = useMemo(
    () =>
      usersData.map((item) => ({
        ...item,
        cbsPayload: {
          isSwitchDisable:
            item.name === usersMe.name && item.email === usersMe.email,
        },
      })),
    [usersData, usersMe]
  );
  const isLoading = useMemo(() => usersIsDataLoading, [usersIsDataLoading]);

  const { limit, offset } = usersPagination;
  const { field: sortField, order: sortOrder } = usersSorting;

  useEffect(() => {
    dispatch(handleUsersDataIsLoading(true));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getUsersData({ withLoader: false }));
  }, [dispatch, limit, offset, sortField, sortOrder]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: usersPagination,
    action: setUsersDataPagination,
  });

  if (isLoading) return <Spinner />;

  return (
    <Fragment>
      <Pagination
        page={paginal.page}
        pages={paginal.pages}
        pageSize={paginal.pageSize}
        handlePageSize={handlePageSize}
        handlePage={handlePage}
        className="ips-list__pagination ips-list__pagination--top"
      />

      <Table
        columns={[
          {
            field: 'email',
            title: t('users.table.email'),
            type: 'string',
            width: 300,
          },
          {
            field: 'name',
            title: t('users.table.name'),
            type: 'string',
            width: 300,
          },
          {
            field: 'role',
            title: t('users.table.role'),
            type: 'string',
            width: 200,
          },
          {
            field: 'is_active',
            title: t('users.table.active'),
            type: 'boolean',
            width: 200,
          },
        ]}
        data={tableData}
        cbs={{
          onEdit: (data) => dispatch(setEditedUser(data)),
          onInfo: (item) => dispatch(setEditedUserBillingPlanUserId(item.id)),
          onSwitch: (item) => dispatch(setSwitchedUser(item)),
          onLogs: (data) => history.push(`/userlogs/${data.id}`),
        }}
        sorting={{
          ...usersSorting,
          sortFields: ['role', 'is_active'],
          isApiSorting: true,
        }}
        onSorting={(data) => dispatch(setUsersSorting(data))}
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ips-list__pagination ips-list__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(TableUsers);
