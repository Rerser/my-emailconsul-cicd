import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button/Button';
import SlideBar from 'components/ui/slideBar';
import Error404 from 'components/ui/error404';

import './users.scss';

import { setEditedUser } from 'store/actions/users';
import {
  getUserBillingPlanGeneral,
  setEditedUserBillingPlanUserId,
  setUserBillingPlanGeneral,
} from 'store/actions/userBillingPlan';
import { usersEditedUserSelector, editedUserBillingPlanUserIdSelector, usersErrorsSelector } from 'store/selectors';

import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import TableUsers from './components/tableUsers';
import EditUserBillingPlan from './components/editUserBillingPlan';
import EditUser from './components/editUser';

function Users() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const usersEditedUser = useSelector(usersEditedUserSelector);
  const editedUserBillingPlanUserId = useSelector(
    editedUserBillingPlanUserIdSelector
  );

  const usersErrors = useSelector(usersErrorsSelector);
  const { data: errorData } = usersErrors;

  useEffect(() => {
    dispatch(getUserBillingPlanGeneral());
    return () => {
      dispatch(setUserBillingPlanGeneral(null));
    };
  }, [dispatch]);

  const showSlider = useMemo(() => !!editedUserBillingPlanUserId, [
    editedUserBillingPlanUserId,
  ]);

  if (errorData?.status === NOT_FOUND_ERROR_STATUS) return <Error404 />;

  return (
    <div className="users">
      <div className="users__header">
        <h1 className="users__title">
          {t('users.header.text')}
        </h1>
        <Button
          mode="purple"
          text={t('users.btn.add_new_user.text')}
          className="users__btn"
          onClick={() => dispatch(setEditedUser({}))}
        />
      </div>

      <TableUsers />

      <SlideBar isOpen={!!usersEditedUser} onClose={() => dispatch(setEditedUser(null))}>
        <EditUser
          title={
            usersEditedUser && usersEditedUser.id
              ? t('users.slidebar.edituser.update')
              : t('users.slidebar.edituser.new')
          }
        />
      </SlideBar>

      <SlideBar isOpen={showSlider} onClose={() => dispatch(setEditedUserBillingPlanUserId(false))}>
        <EditUserBillingPlan title={t('users.slidebar.edituser_billing.title')} />
      </SlideBar>
    </div>
  );
}

export default Users;
