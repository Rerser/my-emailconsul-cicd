import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { cog, shoppingCart, question, signOut } from 'react-icons-kit/fa/';
import withConfirmationPopup from 'components/hocs/withConfirmationPopup';
import MenuLinks from 'components/ui/menuLinks';

import './user-layout-sidebar.scss';

import { authLogout } from 'store/actions/auth';

const UserLayoutSidebar = ({ showPopup }) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const menuItemList = [
    {
      to: '/user/settings',
      text: t('user_layout_sidebar.settings.link'),
      icon: cog,
    },
    {
      to: '/user/billing',
      text: t('user_layout_sidebar.billing.link'),
      icon: shoppingCart,
    },
    {
      to: '/user/help',
      text: t('user_layout_sidebar.help.link'),
      icon: question,
    },
    {
      func: () =>
        showPopup(t('confirmation_popup.title.logout'), () =>
          dispatch(authLogout())
        ),
      text: t('user_layout_sidebar.logout.link'),
      icon: signOut,
    },
  ];

  return (
    <div className="user-layout-sidebar">
      <MenuLinks menuItemList={menuItemList} vertical />
    </div>
  );
};

UserLayoutSidebar.defaultProps = {
  showPopup: () => {},
};
UserLayoutSidebar.propTypes = {
  showPopup: PropTypes.func,
};

export default React.memo(withConfirmationPopup(UserLayoutSidebar));
