import React from 'react';
import { Switch, Route, Redirect, useRouteMatch } from 'react-router-dom';
// import { ProtectedRoute } from 'components/routes';
import UserSettings from './sections/UserSettings';
import UserBilling from './sections/UserBilling';
import UserHelp from './sections/UserHelp';
import UserLayoutSidebar from './components/UserLayoutSidebar';

import './user-layout.scss';

const UserLayout = ({ propName }) => {
  const { path } = useRouteMatch();

  return (
    <div className="user-layout">
      <div className="user-layout__sidebar">
        <UserLayoutSidebar />
      </div>
      <div className="user-layout__content">
        <Switch>
          <Route path={`${path}/settings`} component={UserSettings} />
          <Route path={`${path}/billing`} component={UserBilling} />
          <Route path={`${path}/help`} component={UserHelp} />
          <Route
            path={path}
            exact
            render={() => <Redirect to={`${path}/settings`} />}
          />
        </Switch>
      </div>
    </div>
  );
};

export default React.memo(UserLayout);
