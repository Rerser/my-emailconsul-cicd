import React from 'react';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import './user-help.scss';
import { SUPPORT_EMAIL } from 'utils/constants';

const UserHelp = () => {
  const t = useFormatMessage();

  return (
    <div className="user-help">
      <div className="user-help__text">
        {t('user_help.support.txt')}
        {' '}
        <a href={`mailto:${SUPPORT_EMAIL}`}>
          {SUPPORT_EMAIL}
        </a>
      </div>
    </div>
  );
};

export default React.memo(UserHelp);
