import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { useSelector } from 'react-redux';
import { myUserBillingPlanDataSelector } from 'store/selectors';
import Button from 'components/ui/button/Button';
import { getBillingPlanIsExpired } from 'utils/helpers';

import cn from 'classnames';
import './billing-plank.scss';

import { DELIVERABILITY_CONSULTING_MODE } from 'utils/constants';

const BillingPlank = ({
  mode,
  type,
  title,
  price,
  description,
  onSubmit,
  isCurrent,
  disabledButton,
  captionButton,
}) => {
  const t = useFormatMessage();

  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  const isConsulting =
    mode === DELIVERABILITY_CONSULTING_MODE && !type && !isCurrent;
  const isCurrentPlanNotExpired =
    mode === myUserBillingPlanData.current_plan && !billingPlanIsExpired;

  const planButtonCaption =
    isCurrentPlanNotExpired && isCurrent
      ? t('user_billing.plank.update.btn.text')
      : t('user_billing.plank.choose.btn.text');

  return (
    <div className={cn('billing-plank', mode, { blured: !isCurrent })}>
      {!isConsulting && (
        <div
          className={cn('billing-plank__current-plan', {
            transparent: !isCurrent,
          })}
        >
          {t('user_billing.plank.current.text')}
        </div>
      )}
      <div className="billing-plank__title">
        {title}
      </div>
      {isConsulting ? (
        <div className="billing-plank__consulting">
          <span className="billing-plank__consulting-text">
            {t('user_billing.plank.consulting.price.from.text')}
            {' '}
          </span>
          <span
            className={cn('billing-plank__consulting-text price', {
              consulting: isConsulting,
            })}
          >
            {price}
          </span>
          <span className="billing-plank__consulting-text">
            {' '}
            {t('user_billing.plank.consulting.price.per_hour.text')}
          </span>
        </div>
      ) : (
        <div className="billing-plank__price">
          {price}
        </div>
      )}
      <div className="billing-plank__type">
        {type}
      </div>
      <div className="billing-plank__description">
        {description}
      </div>
      <div className="billing-plank__btn">
        <Button
          mode="purple"
          text={captionButton || planButtonCaption}
          className="seedlisting-workers__submit"
          onClick={() => onSubmit(mode)}
          disabled={disabledButton}
        />
      </div>
    </div>
  );
};

BillingPlank.defaultProps = {
  title: '',
  description: '',
  captionButton: '',
  disabledButton: false,
};
BillingPlank.propTypes = {
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  onSubmit: PropTypes.func.isRequired,
  captionButton: PropTypes.string,
  disabledButton: PropTypes.bool,
};

export default React.memo(BillingPlank);
