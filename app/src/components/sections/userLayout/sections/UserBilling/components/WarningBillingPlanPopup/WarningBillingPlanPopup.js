import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Portal from 'components/ui/portal';
import Button from 'components/ui/button';

import './warning-billing-plan-popup.scss';
import {
  myUserBillingPlanDataSelector,
  selectedPaymentBillingPlanSelector,
} from 'store/selectors';
import { getNameUserBillingPlan } from 'utils/helpers';
import { SUPPORT_EMAIL } from 'utils/constants';

const WarningBillingPlanPopup = ({ onSumbit, onClose }) => {
  const t = useFormatMessage();

  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const selectedPaymentBillingPlan = useSelector(
    selectedPaymentBillingPlanSelector
  );

  const { current_plan = '', seedlisting = {} } = myUserBillingPlanData;

  return (
    <Portal>
      <div className="warning-billing-plan-popup">
        <div className="warning-billing-plan-popup__title">
          {t('user_billing.warning-billing-plan-popup.message.title.text')}
        </div>

        <div className="warning-billing-plan-popup__message message">
          <div className="message__row">
            <span className="message__text">
              {t(
                'user_billing.warning-billing-plan-popup.message.curresnt-plan-exist.text'
              )}
            </span>
            <span className="message__text message__text_bold">
              {getNameUserBillingPlan(current_plan)}
              .
            </span>
          </div>
          <div className="message__row">
            <span className="message__text">
              {t(
                'user_billing.warning-billing-plan-popup.message.plan-reset.text'
              )}
            </span>
            <span className="message__text">
              {'('}
            </span>
            <span className="message__text">
              {t(
                'user_billing.warning-billing-plan-popup.message.seedlisting-emails-available.text'
              )}
            </span>
            <span className="message__text message__text_bold">
              {seedlisting?.emails_available}
            </span>
            <span className="message__text">
              {').'}
            </span>
          </div>
          <div className="message__row">
            <span className="message__text">
              {t(
                'user_billing.warning-billing-plan-popup.message.plan-new-confirm.text'
              )}
            </span>
            <span className="message__text message__text_bold">
              {getNameUserBillingPlan(selectedPaymentBillingPlan)}
              ?
              {' '}
            </span>
          </div>
          <div className="message__row">
            <span className="message__text">
              {t(
                'user_billing.warning-billing-plan-popup.message.questions-email.text'
              )}
            </span>
            <a href={`mailto:${SUPPORT_EMAIL}`}>
              {SUPPORT_EMAIL}
            </a>
          </div>
        </div>

        <div className="warning-billing-plan-popup__footer">
          <Button
            mode="light"
            text={t('user_billing.on_go_payment_params.btn.close')}
            onClick={onClose}
            type="button"
          />
          <Button
            mode="purple"
            text={t('user_billing.on_go_payment_params.btn.submit')}
            onClick={onSumbit}
            type="button"
          />
        </div>
      </div>
    </Portal>
  );
};

WarningBillingPlanPopup.defaultProps = {};
WarningBillingPlanPopup.propTypes = {
  onSumbit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default React.memo(WarningBillingPlanPopup);
