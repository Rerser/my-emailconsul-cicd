import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Portal from 'components/ui/portal';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import './billing-method-popup.scss';

import {
  getLiqpayNewPaymentData,
  setLiqpayNewPaymentData,
  handleWireTransferPaymentDataIsSended,
  sendWireTransferNewPaymentData,
} from 'store/actions/payment';
import {
  // liqpayNewPaymentDataSelector,
  // liqpayNewPaymentDataIsLoadingSelector,
  selectedPaymentBillingPlanSelector,
  selectedPaymentBillingPlanTypeSelector,
  selectedPaymentCurrencySelector,
  onGoPaymentParamsSelector,
  wireTransferPaymentDataIsSendSelector,
  wireTransferPaymentDataIsSendedSelector,
} from 'store/selectors';
import {
  BILLING_PLAN_NAME,
  BILLING_PLAN_TYPE,
  DELIVERABILITY_CONSULTING_MODE,
} from 'utils/constants';
// import { getLiqpayPatmentLink } from 'utils/helpers';

const ConfirmationPopup = ({ onSumbit, onClose }) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  // const liqpayNewPaymentData = useSelector(liqpayNewPaymentDataSelector);
  // const liqpayNewPaymentDataIsLoading = useSelector(liqpayNewPaymentDataIsLoadingSelector);
  const selectedPaymentBillingPlan = useSelector(
    selectedPaymentBillingPlanSelector
  );
  const selectedPaymentBillingPlanType = useSelector(
    selectedPaymentBillingPlanTypeSelector
  );
  const selectedPaymentCurrency = useSelector(selectedPaymentCurrencySelector);
  const onGoPaymentParams = useSelector(onGoPaymentParamsSelector);
  const wireTransferPaymentDataIsSend = useSelector(
    wireTransferPaymentDataIsSendSelector
  );
  const wireTransferPaymentDataIsSended = useSelector(
    wireTransferPaymentDataIsSendedSelector
  );

  // const isNotConsultingBillingPlanType = selectedPaymentBillingPlan !== DELIVERABILITY_CONSULTING_MODE;
  const isShowPayWireTransferButton =
    selectedPaymentBillingPlanType !== BILLING_PLAN_TYPE.MONTHLY ||
    (selectedPaymentBillingPlanType === BILLING_PLAN_TYPE.MONTHLY &&
      selectedPaymentBillingPlan === DELIVERABILITY_CONSULTING_MODE);

  useEffect(() => {
    if (wireTransferPaymentDataIsSended) {
      onClose();
    }
    dispatch(handleWireTransferPaymentDataIsSended(false));
  }, [dispatch, wireTransferPaymentDataIsSended, onClose]);

  useEffect(() => {
    if (selectedPaymentBillingPlan !== DELIVERABILITY_CONSULTING_MODE) {
      if (selectedPaymentBillingPlan === BILLING_PLAN_NAME.ON_GO) {
        dispatch(
          getLiqpayNewPaymentData({
            billingPlanName: selectedPaymentBillingPlan,
            currency: selectedPaymentCurrency,
            seedlistingEmailsAvailable:
              onGoPaymentParams?.seedlistingEmailsAvailable,
            billingPlanType: selectedPaymentBillingPlanType,
          })
        );
      } else {
        dispatch(
          getLiqpayNewPaymentData({
            billingPlanName: selectedPaymentBillingPlan,
            currency: selectedPaymentCurrency,
            billingPlanType: selectedPaymentBillingPlanType,
          })
        );
      }
    }
    return () => {
      dispatch(setLiqpayNewPaymentData(null));
    };
  }, [
    dispatch,
    selectedPaymentBillingPlan,
    selectedPaymentCurrency,
    onGoPaymentParams,
    selectedPaymentBillingPlanType,
  ]);

  return (
    <Portal>
      <div className="billing-method-popup">
        <div className="billing-method-popup__text">
          {t('user_billing.billing_method_popup.text')}
        </div>

        {/* {isNotConsultingBillingPlanType && liqpayNewPaymentDataIsLoading && <Spinner />}
          {isNotConsultingBillingPlanType && !liqpayNewPaymentDataIsLoading && liqpayNewPaymentData && <a
            href={getLiqpayPatmentLink(liqpayNewPaymentData.data, liqpayNewPaymentData.signature)}
            className="billing-method-popup__liqpay-link"
            target="_blank"
            rel="noopener noreferrer"
          >
            {t('user_billing.billing_method_popup.paypal.btn')}
          </a>} */}

        {isShowPayWireTransferButton && (
          <div className="billing-method-popup__wire-transfer">
            {wireTransferPaymentDataIsSend ? (
              <Spinner />
            ) : (
              <Button
                mode="blue"
                text={t('user_billing.billing_method_popup.wire-transfer.btn')}
                onClick={() =>
                  dispatch(
                    sendWireTransferNewPaymentData({
                      billingPlanName: selectedPaymentBillingPlan,
                      currency: selectedPaymentCurrency,
                      seedlistingEmailsAvailable:
                        onGoPaymentParams?.seedlistingEmailsAvailable,
                      billingPlanType: selectedPaymentBillingPlanType,
                    })
                  )
                }
                type="button"
              />
            )}
          </div>
        )}
        <div className="billing-method-popup__footer">
          <Button
            mode="light"
            text={t('user_billing.billing_method_popup.close.btn')}
            onClick={onClose}
            type="button"
          />
        </div>
      </div>
    </Portal>
  );
};

ConfirmationPopup.defaultProps = {};
ConfirmationPopup.propTypes = {
  onSumbit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ConfirmationPopup;
