import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { toast } from 'react-toastify';
import Portal from 'components/ui/portal';
import Button from 'components/ui/button';
import InputText from 'components/ui/inputText';

import './on-go-payment-params.scss';

import { setOnGoPaymentParams } from 'store/actions/payment';
import {
  onGoPaymentParamsSelector,
  onGoCalculatedPricePerSeedlistingEmailsAvailableSelector,
  selectedPaymentCurrencySelector,
} from 'store/selectors';
import { CURRENCY_SIGN } from 'utils/constants';

const OnGoPaymentParams = ({ onSumbit, onClose }) => {
  const t = useFormatMessage();
  const dispatch = useDispatch();

  const onGoPaymentParams = useSelector(onGoPaymentParamsSelector);
  const onGoCalculatedPricePerSeedlistingEmailsAvailable = useSelector(
    onGoCalculatedPricePerSeedlistingEmailsAvailableSelector
  );
  const selectedPaymentCurrency = useSelector(selectedPaymentCurrencySelector);

  useEffect(() => {
    // set 1 by default
    dispatch(
      setOnGoPaymentParams({
        seedlistingEmailsAvailable: 1,
      })
    );
  }, [dispatch]);

  return (
    <Portal>
      <div className="on-go-payment-params">
        <div className="on-go-payment-params__text">
          {t('user_billing.on_go_payment_params.number_of_checks.title')}
        </div>

        <InputText
          value={onGoPaymentParams?.seedlistingEmailsAvailable}
          onChange={(e) => {
            if (!/^[\d]*$/.test(e.target.value)) {
              return toast.error(
                t(
                  'user_billing.on_go_payment_params.number_of_checks.input.only_number_error'
                )
              );
            }
            dispatch(
              setOnGoPaymentParams({
                seedlistingEmailsAvailable: e.target.value,
              })
            );
          }}
          placeholder="Number of checks"
          className="on-go-payment-params__input"
        />

        <div className="on-go-payment-params__price agpp-price">
          <div className="agpp-price__label">
            {t('user_billing.on_go_payment_params.price.title')}
          </div>
          <div className="agpp-price__value">
            {Math.ceil(onGoCalculatedPricePerSeedlistingEmailsAvailable)}
            {CURRENCY_SIGN[selectedPaymentCurrency]}
          </div>
        </div>

        <div className="on-go-payment-params__footer">
          <Button
            mode="light"
            text={t('user_billing.on_go_payment_params.btn.close')}
            onClick={onClose}
            type="button"
          />
          <Button
            mode="purple"
            text={t('user_billing.on_go_payment_params.btn.submit')}
            onClick={onSumbit}
            type="button"
          />
        </div>
      </div>
    </Portal>
  );
};

OnGoPaymentParams.defaultProps = {};
OnGoPaymentParams.propTypes = {
  onSumbit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default OnGoPaymentParams;
