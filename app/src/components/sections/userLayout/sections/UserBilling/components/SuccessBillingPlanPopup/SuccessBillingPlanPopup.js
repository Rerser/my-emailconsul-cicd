import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Portal from 'components/ui/portal';
import Button from 'components/ui/button';

import { handleShowSuccessPopup } from 'store/actions/payment';
import './success-billing-plan-popup.scss';

const SuccessBillingPlanPopup = ({ text }) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  return (
    <Portal>
      <div className="success-billing-plan-popup">
        <div className="success-billing-plan-popup__title">
          {t('user_billing.success-billing-plan-popup.message.title.text')}
        </div>

        <div className="success-billing-plan-popup__message">
          {text}
        </div>

        <div className="success-billing-plan-popup__footer">
          <Button
            mode="purple"
            text={t('user_billing.success-billing-plan-popup.message.btn.caption')}
            onClick={() => dispatch(handleShowSuccessPopup(false))}
            type="button"
          />
        </div>
      </div>
    </Portal>
  );
};

SuccessBillingPlanPopup.defaultProps = {
  text: '',
};
SuccessBillingPlanPopup.propTypes = {
  text: PropTypes.string,
};

export default React.memo(SuccessBillingPlanPopup);
