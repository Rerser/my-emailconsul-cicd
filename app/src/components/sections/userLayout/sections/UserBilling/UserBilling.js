import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
// import OnGoPaymentParams from './components/OnGoPaymentParams';
import Select from 'components/ui/select';
import Spinner from 'components/ui/spinner';
import { Icon } from 'react-icons-kit';
import { ccVisa } from 'react-icons-kit/fa/ccVisa';
import { ccMastercard } from 'react-icons-kit/fa/ccMastercard';

import './user-billing.scss';

import { getUserBillingPlanGeneral } from 'store/actions/userBillingPlan';
import {
  setSelectedPaymentBillingPlan,
  setSelectedPaymentCurrency,
  getExchangeData,
  setSelectedPaymentBillingPlanType,
  handleShowWarningPopup,
  sendDeliverabilityConsultingData,
  // handleOnGoPaymentParamsIsEdit,
} from 'store/actions/payment';
import {
  selectedPaymentBillingPlanSelector,
  selectedPaymentCurrencySelector,
  paymentExchangeDataSelector,
  paymentExchangeDataIsLoadingSelector,
  // onGoPaymentParamsIsEditSelector,
  showWarningPopupSelector,
  showSuccessPopupSelector,
  userBillingPlanGeneralSelector,
  userBillingPlanGeneralIsLoadingSelector,
  billingPlanGeneralPricesSelector,
  myUserBillingPlanDataSelector,
  selectedPaymentBillingPlanTypeSelector,
  usersMeSelector,
  deliverabilityConsultingDataIsSendingSelector,
} from 'store/selectors';

import {
  BILLING_PLAN_NAME,
  BILLING_PLAN_TYPE,
  PAYMENT_CURRENCY,
  CURRENCY_SIGN,
  DELIVERABILITY_CONSULTING_MODE,
} from 'utils/constants';
import {
  convertDateTime,
  getBillingPlanIsExpired,
  getConsultingPrice,
  getNameUserBillingPlan,
} from 'utils/helpers';
import SuccessBillingPlanPopup from './components/SuccessBillingPlanPopup';
import WarningBillingPlanPopup from './components/WarningBillingPlanPopup';
import BillingMethodPopup from './components/BillingMethodPopup';
import BillingPlank from './components/BillingPlank';

const UserBilling = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const selectedPaymentBillingPlan = useSelector(
    selectedPaymentBillingPlanSelector
  );
  const selectedPaymentCurrency = useSelector(selectedPaymentCurrencySelector);
  const paymentExchangeData = useSelector(paymentExchangeDataSelector);
  const paymentExchangeDataIsLoading = useSelector(
    paymentExchangeDataIsLoadingSelector
  );
  // const onGoPaymentParamsIsEdit = useSelector(onGoPaymentParamsIsEditSelector);
  const showWarningPopup = useSelector(showWarningPopupSelector);
  const showSuccessPopup = useSelector(showSuccessPopupSelector);
  const userBillingPlanGeneral = useSelector(userBillingPlanGeneralSelector);
  const userBillingPlanGeneralIsLoading = useSelector(
    userBillingPlanGeneralIsLoadingSelector
  );
  const billingPlanGeneralPrices = useSelector(
    billingPlanGeneralPricesSelector
  );
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const selectedPaymentBillingPlanType = useSelector(
    selectedPaymentBillingPlanTypeSelector
  );
  const me = useSelector(usersMeSelector);
  const deliverabilityConsultingDataIsSending = useSelector(
    deliverabilityConsultingDataIsSendingSelector
  );

  const { seedlisting_emails_per_month = {}, emails_per_iteration = {} } = userBillingPlanGeneral || {};
  const { data: calculatedPrices } = billingPlanGeneralPrices || {};

  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  useEffect(() => {
    dispatch(getUserBillingPlanGeneral());
  }, [dispatch]);

  useEffect(() => {
    if (!paymentExchangeData) {
      dispatch(getExchangeData());
    }
  }, [selectedPaymentCurrency, paymentExchangeData, dispatch]);

  const handleBlank = (mode) => {
    dispatch(setSelectedPaymentBillingPlan(mode));
    if (
      !!myUserBillingPlanData &&
      !billingPlanIsExpired &&
      mode !== BILLING_PLAN_NAME.NONE &&
      mode !== DELIVERABILITY_CONSULTING_MODE
    ) {
      dispatch(handleShowWarningPopup(true));
    }
  };

  if (
    userBillingPlanGeneralIsLoading ||
    !userBillingPlanGeneral ||
    paymentExchangeDataIsLoading ||
    !paymentExchangeData
  )
    return <Spinner />;

  function getPopupLayout() {
    if (showWarningPopup) {
      return (
        <WarningBillingPlanPopup
          onSumbit={() => dispatch(handleShowWarningPopup(false))}
          onClose={() => {
            dispatch(handleShowWarningPopup(false));
            dispatch(setSelectedPaymentBillingPlan(null));
          }}
        />
      );
    }

    if (showSuccessPopup) {
      return (
        <SuccessBillingPlanPopup
          text={t(
            'user_billing.success-billing-plan-popup.message.description.text'
          )}
        />
      );
    }

    return (
      <BillingMethodPopup
        onSumbit={() => true}
        onClose={() => dispatch(setSelectedPaymentBillingPlan(null))}
      />
    );
  }

  return (
    <section className="user-billing">
      <section className="user-billing__content">
        <section className="user-billing__currency-type bcurrency-type">
          {myUserBillingPlanData && (
            <div className="bcurrency-type__currentplan currentplan">
              <div className="currentplan__name">
                <span className="currentplan__text description">
                  {t('user_billing.plank.current.name.text')}
                  :
                </span>
                {myUserBillingPlanData.current_plan ? (
                  <span className="currentplan__text">
                    {getNameUserBillingPlan(myUserBillingPlanData.current_plan)}
                  </span>
                ) : (
                  <span className="currentplan__text currentplan__text--error">
                    {t('user_billing.plank.no_current_plan.text')}
                  </span>
                )}
              </div>
              {myUserBillingPlanData.current_plan && (
                <div className="currentplan__type">
                  <span className="currentplan__text description">
                    {t('user_billing.plank.current.type.text')}
                    {':'}
                  </span>
                  <span className="currentplan__text">
                    {myUserBillingPlanData.current_plan_type}
                  </span>
                </div>
              )}
              {myUserBillingPlanData.current_plan && !!myUserBillingPlanData.next_renewal_timestamp && (
                <div className="currentplan__next-renewal">
                  <span className="currentplan__text description">
                    {t('user_billing.plank.current.next_renewal_date.text')}
                    {':'}
                  </span>
                  <span className="currentplan__text">
                    {convertDateTime(myUserBillingPlanData.next_renewal_timestamp, "DD-MM-YYYY")}
                  </span>
                </div>
              )}
              {myUserBillingPlanData.current_plan && (
                <div className="currentplan__expiration">
                  <span className="currentplan__text description">
                    {t('user_billing.plank.current.expiration_date.text')}
                    {':'}
                  </span>
                  <span className="currentplan__text">
                    {convertDateTime(myUserBillingPlanData.timestamp_to, "DD-MM-YYYY")}
                  </span>
                </div>
              )}
            </div>
          )}
          <div className="bcurrency-type__selects">
            <div className="bcurrency-type__select">
              <div className="bcurrency-type__label">
                {t('user_billing.plank.currency.text')}
                :
              </div>
              <Select
                value={selectedPaymentCurrency}
                options={[
                  { value: PAYMENT_CURRENCY.USD, label: PAYMENT_CURRENCY.USD },
                  { value: PAYMENT_CURRENCY.EUR, label: PAYMENT_CURRENCY.EUR },
                  { value: PAYMENT_CURRENCY.RUR, label: PAYMENT_CURRENCY.RUR },
                  { value: PAYMENT_CURRENCY.UAH, label: PAYMENT_CURRENCY.UAH },
                ]}
                onChange={(val) => dispatch(setSelectedPaymentCurrency(val))}
                resetBtnAvailable={false}
                className="bcurrency-type__fixwidth"
              />
            </div>
            <div className="bcurrency-type__select">
              <div className="bcurrency-type__label">
                {t('user_billing.plank.type.text')}
                :
              </div>
              <Select
                // options={Object.keys(BILLING_PLAN_TYPE).map((key) => ({
                //   value: BILLING_PLAN_TYPE[key],
                //   label: BILLING_PLAN_TYPE[key],
                // }))}
                options={[
                  {
                    value: BILLING_PLAN_TYPE.YEARLY,
                    label: BILLING_PLAN_TYPE.YEARLY,
                  },
                ]}
                value={selectedPaymentBillingPlanType}
                onChange={(value) =>
                  dispatch(setSelectedPaymentBillingPlanType(value))
                }
                placeholder={t('edit_user_billing.select.placeholder')}
                resetBtnAvailable={false}
                className="bcurrency-type__fixwidth"
              />
            </div>
          </div>
        </section>

        <section className="user-billing__planks">
          <BillingPlank
            mode={BILLING_PLAN_NAME.JUNIOR}
            isCurrent={
              myUserBillingPlanData?.current_plan_type ===
                selectedPaymentBillingPlanType &&
              myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.JUNIOR
            }
            type={selectedPaymentBillingPlanType}
            title={t('user_billing.plank.junior.title')}
            price={`${Math.ceil(
              calculatedPrices[selectedPaymentBillingPlanType][
                BILLING_PLAN_NAME.JUNIOR
              ]
            )}${CURRENCY_SIGN[selectedPaymentCurrency]}`}
            description={
              <div>
                <div className="billing-plank__text">
                  <b>
                    {emails_per_iteration[BILLING_PLAN_NAME.JUNIOR]}
                  </b>
                  {' '}
                  {t('user_billing.plank.junior.checks_per_test.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  <b>
                    {seedlisting_emails_per_month[BILLING_PLAN_NAME.JUNIOR]}
                  </b>
                  {' '}
                  {t('user_billing.plank.junior.checks_per_month.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.junior.up_to_ips.text-1')}
                  <b>
                    {t('user_billing.plank.junior.up_to_ips.text-2')}
                  </b>
                  {t('user_billing.plank.junior.up_to_ips.text-3')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.junior.ip_report.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.junior.check_spam.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.junior.authentication.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.junior.content_preview.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.junior.ui_dashboard.text')}
                </div>
              </div>
            }
            onSubmit={handleBlank}
          />
          <BillingPlank
            mode={BILLING_PLAN_NAME.MIDDLE}
            isCurrent={
              myUserBillingPlanData?.current_plan_type ===
                selectedPaymentBillingPlanType &&
              myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.MIDDLE
            }
            type={selectedPaymentBillingPlanType}
            title={t('user_billing.plank.middle.title')}
            price={`${Math.ceil(
              calculatedPrices[selectedPaymentBillingPlanType][
                BILLING_PLAN_NAME.MIDDLE
              ]
            )}${CURRENCY_SIGN[selectedPaymentCurrency]}`}
            description={
              <div>
                <div className="billing-plank__text">
                  <b>
                    {emails_per_iteration[BILLING_PLAN_NAME.MIDDLE]}
                  </b>
                  {' '}
                  {t('user_billing.plank.middle.checks_per_test.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  <b>
                    {seedlisting_emails_per_month[BILLING_PLAN_NAME.MIDDLE]}
                  </b>
                  {' '}
                  {t('user_billing.plank.middle.checks_per_month.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.middle.up_to_ips.text-1')}
                  <b>
                    {t('user_billing.plank.middle.up_to_ips.text-2')}
                  </b>
                  {t('user_billing.plank.middle.up_to_ips.text-3')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.middle.ip_report.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.middle.check_spam.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.middle.authentication.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.middle.content_preview.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.middle.ui_dashboard.text')}
                </div>
              </div>
            }
            onSubmit={handleBlank}
          />
          <BillingPlank
            mode={BILLING_PLAN_NAME.SENIOR}
            type={selectedPaymentBillingPlanType}
            isCurrent={
              myUserBillingPlanData?.current_plan_type ===
                selectedPaymentBillingPlanType &&
              myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.SENIOR
            }
            title={t('user_billing.plank.senior.title')}
            price={`${Math.ceil(
              calculatedPrices[selectedPaymentBillingPlanType][
                BILLING_PLAN_NAME.SENIOR
              ]
            )}${CURRENCY_SIGN[selectedPaymentCurrency]}`}
            description={
              <div>
                <div className="billing-plank__text">
                  <b>
                    {emails_per_iteration[BILLING_PLAN_NAME.SENIOR]}
                  </b>
                  {' '}
                  {t('user_billing.plank.senior.checks_per_test.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  <b>
                    {seedlisting_emails_per_month[BILLING_PLAN_NAME.SENIOR]}
                  </b>
                  {' '}
                  {t('user_billing.plank.senior.checks_per_month.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.senior.up_to_ips.text-1')}
                  <b>
                    {t('user_billing.plank.senior.up_to_ips.text-2')}
                  </b>
                  {t('user_billing.plank.senior.up_to_ips.text-3')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.senior.ip_report.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.senior.check_spam.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.senior.authentication.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.senior.content_preview.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.senior.ui_dashboard.text')}
                </div>
              </div>
            }
            onSubmit={handleBlank}
          />
          <BillingPlank
            mode={DELIVERABILITY_CONSULTING_MODE}
            type={null}
            isCurrent={false}
            title={t('user_billing.plank.consulting.title')}
            price={`${Math.ceil(
              getConsultingPrice(paymentExchangeData, selectedPaymentCurrency)
            )}${CURRENCY_SIGN[selectedPaymentCurrency]}`}
            description={
              <div>
                <div className="billing-plank__text">
                  {t(
                    'user_billing.plank.consulting.description.consulting.text'
                  )}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.consulting.description.audit.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t('user_billing.plank.consulting.description.ips.text')}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t(
                    'user_billing.plank.consulting.description.ip_warning.text'
                  )}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t(
                    'user_billing.plank.consulting.description.unblock_processes.text'
                  )}
                  <br />
                </div>
                <div className="billing-plank__text">
                  {t(
                    'user_billing.plank.consulting.description.email_phone_availability.text'
                  )}
                </div>
              </div>
            }
            onSubmit={() =>
              dispatch(sendDeliverabilityConsultingData({ email: me.email }))
            }
            captionButton={t('user_billing.plank.consulting.button.caption')}
            disabledButton={deliverabilityConsultingDataIsSending}
          />
          {/* <BillingPlank
            mode={BILLING_PLAN_NAME.ON_GO}
            title={t('user_billing.plank.on_go.title')}
            price={`${Math.ceil(calculatedPrices[BILLING_PLAN_NAME.ON_GO])}${CURRENCY_SIGN[selectedPaymentCurrency]}`}
            description={<div>
              <b>{emails_per_iteration[BILLING_PLAN_NAME.ON_GO]}</b> {t('user_billing.plank.on_go.checks_per_test.text')} <br/>
              <b>{t('user_billing.plank.on_go.checks_per_month.value')}</b> {t('user_billing.plank.on_go.checks_per_month.text')}
            </div>}
            onSubmit={() => dispatch(handleOnGoPaymentParamsIsEdit(true))}
          /> */}

          {(!!selectedPaymentBillingPlan || showSuccessPopup) &&
            getPopupLayout()}

          {/* {onGoPaymentParamsIsEdit && <OnGoPaymentParams
            onSumbit={() => { dispatch(handleOnGoPaymentParamsIsEdit(false)); handleBlank(BILLING_PLAN_NAME.ON_GO); }}
            onClose={() => dispatch(handleOnGoPaymentParamsIsEdit(false))}
          />} */}
        </section>
      </section>

      <footer className="user-billing__footer ubfooter">
        <div className="ubfooter__icons">
          <div className="ubfooter__icon">
            <Icon icon={ccVisa} size="100%" />
          </div>
          <div className="ubfooter__icon">
            <Icon icon={ccMastercard} size="100%" />
          </div>
        </div>
      </footer>
    </section>
  );
};

export default React.memo(UserBilling);
