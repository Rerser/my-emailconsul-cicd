import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import './user-settings.scss';

import {
  authSetCurrentPassword,
  authSetNewPassword,
  authSetConfirmNewPassword,
  authChangePassword,
} from 'store/actions/auth';
import {
  authCurrentPasswordSelector,
  authNewPasswordSelector,
  authConfirmNewPasswordSelector,
  authIsLoadingSelector,
  usersMeSelector,
} from 'store/selectors';

const UserSettings = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const authCurrentPassword = useSelector(authCurrentPasswordSelector);
  const authNewPassword = useSelector(authNewPasswordSelector);
  const authConfirmNewPassword = useSelector(authConfirmNewPasswordSelector);
  const isLoading = useSelector(authIsLoadingSelector);
  const me = useSelector(usersMeSelector);

  const { email } = me || {};

  const onSubmit = (e) => {
    e.preventDefault();

    dispatch(
      authChangePassword({
        currentPasword: authCurrentPassword,
        newPassword: authNewPassword,
        confirmNewPassword: authConfirmNewPassword,
      })
    );
  };

  return (
    <section className="user-settings">
      <section className="user-settings__section ussection">
        <h3 className="ussection__title">
          {t('user_settings.general.title')}
        </h3>
        <div className="ussection__row usrow">
          <div className="usrow__label">
            {t('user_settings.email.label')}
            :
          </div>
          <InputText value={email} disabled />
        </div>
      </section>
      <section className="user-settings__section ussection">
        {isLoading && <Spinner />}
        {!isLoading && (
          <form onSubmit={onSubmit}>
            <h3 className="ussection__title">
              {t('user_settings.password.title')}
            </h3>
            <div className="ussection__row usrow">
              <div className="usrow__label">
                {t('user_settings.current_password.label')}
                :
              </div>
              <InputText
                value={authCurrentPassword}
                onChange={(e) =>
                  dispatch(authSetCurrentPassword(e.target.value))
                }
                type="password"
                required
              />
            </div>
            <div className="ussection__row usrow">
              <div className="usrow__label">
                {t('user_settings.new_password.label')}
                :
              </div>
              <InputText
                value={authNewPassword}
                onChange={(e) => dispatch(authSetNewPassword(e.target.value))}
                type="password"
                required
              />
            </div>
            <div className="ussection__row usrow">
              <div className="usrow__label">
                {t('user_settings.confirm_new_password.label')}
                :
              </div>
              <InputText
                value={authConfirmNewPassword}
                onChange={(e) =>
                  dispatch(authSetConfirmNewPassword(e.target.value))
                }
                type="password"
                required
              />
            </div>
            <Button
              mode="purple"
              text={t('user_settings.password.submit.txt')}
              type="submit"
            />
          </form>
        )}
      </section>
    </section>
  );
};

export default React.memo(UserSettings);
