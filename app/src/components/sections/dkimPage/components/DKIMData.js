import React, { Fragment } from 'react';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import InfoContainer from 'components/ui/infoContainer';
import Table from 'components/ui/table';

import { INFO_CONTAINER_STATUS } from 'utils/constants';

import './dkim-data.scss';

const DKIMData = ({ data }) => {
  const t = useFormatMessage();

  const { result = '', warnings = [], errors = [] } = data;
  let dkimRecords = result;

  if (!dkimRecords) {
    dkimRecords = [];
  } else if (!Array.isArray(dkimRecords)) {
    dkimRecords = [dkimRecords];
  }

  const dkimLayout = dkimRecords.length
    ? dkimRecords.map((dkimRecord, i) => {
        const splittedData = dkimRecord.split(';').map(item => item.trim());

        const tableData = splittedData.map(sData => {
          const [tag, value] = sData.split('=');

          const tagLayout = (
            <span>
              {tag}
            </span>
          );
          const nameLayout = (
            <span>
              {value}
            </span>
          );
          const valueLayout = (
            <span>
              {t(`dkim.tag.translation.${tag}`)}
            </span>
          );

          return {
            tagLayout,
            nameLayout,
            valueLayout,
          }
        });

        return (
          <Fragment key={`dkimRecord-${i}`}>
            {
              i >= 1 && (<div className="dkim-data__separate"/>)
            }
            <div className="dkim-data__description">
              {t('dkim-page.signature.description.text')}
              <span className={cn("dkim-data__status", {
                "dkim-data__status--valid": !errors.length,
                "dkim-data__status--no-valid": errors.length,
              })}>
                {' '}
                {!errors.length
                  ? t('dkim-page.is-valid.text')
                  : t('dkim-page.no-valid.text')
                }
              </span>
            </div>
            <div className="dkim-data__result">
              {splittedData.map((item, index) => (
                <div className="dkim-data__value" key={`dkim-data-signature-${item}-${index}`}>
                  {index !== item.length - 1 ? `${item};` : item}
                </div>
              ))}
            </div>

            <div className="dkim-data__table">
              <Table
                columns={[
                  {
                    field: 'tagLayout',
                    title: t('dkim-page.table.header.tag.text'),
                    type: 'string',
                    width: 100,
                  },
                  {
                    field: 'nameLayout',
                    title: t('dkim-page.table.header.name.text'),
                    type: 'string',
                    width: 500,
                  },
                  {
                    field: 'valueLayout',
                    title: t('dkim-page.table.header.value.text'),
                    type: 'string',
                    width: 200,
                  },
                ]}
                data={tableData}
              />
            </div>

            {
              warnings.map((warning, i) => (
                <div className="dkim-data__warning-container" key={`dkim-warning-${warning}-${i}`}>
                  <InfoContainer text={warning} status={INFO_CONTAINER_STATUS.info} />
                </div>
              ))
            }
            {
              errors.map((error, i) => (
                <div className="dkim-data__error-container" key={`dkim-error-${error}-${i}`}>
                  <InfoContainer text={error} status={INFO_CONTAINER_STATUS.error} />
                </div>
              ))
            }
          </Fragment>
        )}) : (
          <InfoContainer
            text={t('dkim-page.no-data.warning.text')}
            key="DKIM warning"
            status={INFO_CONTAINER_STATUS.warning}
          />
        );

  return (
    <div className="dkim-data">
      {dkimLayout}
    </div>
  );
};

export default React.memo(DKIMData);
