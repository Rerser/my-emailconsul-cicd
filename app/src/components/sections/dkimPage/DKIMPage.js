import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import {
  getDkimData,
  setDkimData,
  setDkimDataError
} from 'store/actions/dkim';
import {
  dkimDataIsLoadingSelector,
  dkimDataSelector
} from 'store/selectors';

import withCheckUserBP from 'components/hocs/withCheckUserBP';
import Button from 'components/ui/button/Button';
import InputText from 'components/ui/inputText';
import Spinner from 'components/ui/spinner';
import DKIMData from './components/DKIMData';

import { validateDomainAddress } from 'utils/helpers';

import './dkim-page.scss';

function DKIMPage() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const [domain, setDomain] = useState('');
  const [selector, setSelector] = useState('');
  const [inputDomainError, setInputDomainError] = useState('');
  const [inputSelectorError, setInputSelectorError] = useState('');

  useEffect(() => {
    return () => {
      dispatch(setDkimData(null));
      dispatch(setDkimDataError(null));
    }
  }, [dispatch]);

  const dkimData = useSelector(dkimDataSelector);
  const dkimDataIsLoading = useSelector(dkimDataIsLoadingSelector);

  const handleChangeDomain = (value) => {
    if (inputDomainError) {
      setInputDomainError('');
    }
    setDomain(value);
  }

  const handleChangeSelector = (value) => {
    if (inputSelectorError) {
      setInputSelectorError('');
    }
    setSelector(value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!inputDomainError && !inputSelectorError && validateDomainAddress(domain) && selector.length) {
      setInputDomainError('');
      setInputSelectorError('');
      return dispatch(getDkimData(domain, selector));
    }
    if (inputDomainError) {
      return setInputDomainError(t('dkim-page.input.domain.error.text'));
    }
    if (inputSelectorError) {
      return setInputDomainError(t('dkim-page.input.selector.error.text'));
    }
  }

  return (
    <div className="dkim-page">
      <div className="dkim-page__header">
        <h1 className="dkim-page__title">
          {t('dkim-page.title')}
        </h1>

        <form className="dkim-page__header-right header-right" onSubmit={handleSubmit}>
          <InputText
            className={cn("header-right__input", {
              "header-right__input--error": inputDomainError,
            })}
            placeholder={t('dkim-page.input.domain.placeholder.text')}
            value={domain}
            onChange={(e) => handleChangeDomain(e.target.value)}
            errorText={inputDomainError}
          />
          <InputText
            className={cn("header-right__input", {
              "header-right__input--error": inputSelectorError,
            })}
            placeholder={t('dkim-page.input.selector.placeholder.text')}
            value={selector}
            onChange={(e) => handleChangeSelector(e.target.value)}
            errorText={inputSelectorError}
          />
          <Button
            type="submit"
            mode="purple"
            text={t('dkim-page.btn.check.text')}
            className="header-right__btn"
            disabled={Boolean(
              !domain || 
              !selector || 
              inputDomainError || 
              inputSelectorError || 
              dkimDataIsLoading
            )}
          />
        </form>
      </div>
      <div className="spf-page__data-wrapper">
        {
          dkimData
            ? <DKIMData data={dkimData} isLoading={dkimDataIsLoading}/>
            : dkimDataIsLoading
              ? <Spinner />
              : null
        }
      </div>
    </div>
  );
}

export default React.memo(withCheckUserBP(DKIMPage));
