import React, { Fragment, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import PropTypes from 'prop-types';

import InputFile from 'components/ui/inputFile';
import Button from 'components/ui/button/Button';
import Spinner from 'components/ui/spinner/Spinner';
import ConfirmationPopup from 'components/ui/confirmationPopup';

import {
  setNewListCleaning,
  handleNewListCleaningFile,
  runListcleaning,
} from 'store/actions/listcleaning';
import {
  newListCleaningSelector,
  newListCleaningFileIsUploadingSelector,
} from 'store/selectors';

const NewListCleaning = (props) => {
  const [isShowConfimationPopup, setIsShowConfirmationPopup] = useState(false);
  const hanleConfirmationPopup = () => {
    dispatch(runListcleaning(newListCleaning));
    setIsShowConfirmationPopup(false);
  };

  const dispatch = useDispatch();
  const t = useFormatMessage();

  const newListCleaning = useSelector(newListCleaningSelector);
  const newListCleaningFileIsUploading = useSelector(
    newListCleaningFileIsUploadingSelector
  );

  return (
    <div className="c-slidebar-content">
      {props.title && (
        <h3 className="c-slidebar-content__title">
          {props.title}
        </h3>
      )}

      {newListCleaningFileIsUploading && <Spinner />}

      {!newListCleaningFileIsUploading && (
        <Fragment>
          {newListCleaning && (
            <Fragment>
              <div className="c-slidebar-content__row">
                <div className="c-slidebar-content__label">
                  File to run (.csv):
                  {' '}
                </div>

                {!newListCleaning.file && (
                  <InputFile
                    onUpload={(data) =>
                      dispatch(handleNewListCleaningFile(data.files[0]))
                    }
                  />
                )}
                {newListCleaning.file && (
                  <div className="c-slidebar-content__file-row">
                    <div className="c-slidebar-content__file-label">
                      {newListCleaning.file.name}
                    </div>
                    <div
                      className="c-slidebar-content__file-delete"
                      onClick={() => dispatch(handleNewListCleaningFile(null))}
                    />
                  </div>
                )}
              </div>
            </Fragment>
          )}

          <div className="c-slidebar-content__btns">
            {newListCleaning && (
              <Button
                mode="purple"
                text={t('new.listcleaning.btn.run')}
                className="c-slidebar-content__btn"
                disabled={!newListCleaning.file}
                onClick={() => setIsShowConfirmationPopup(true)}
              />
            )}

            <Button
              text={t('new.listcleaning.btn.close')}
              className="c-slidebar-content__btn"
              onClick={() => dispatch(setNewListCleaning(null))}
            />
          </div>
        </Fragment>
      )}

      {isShowConfimationPopup && (
        <ConfirmationPopup
          onSumbit={hanleConfirmationPopup}
          onClose={() => setIsShowConfirmationPopup(false)}
        />
      )}
    </div>
  );
};

NewListCleaning.propsTypes = {
  title: PropTypes.string,
};
NewListCleaning.defaultProps = {
  title: '',
};

export default NewListCleaning;
