import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Button from 'components/ui/button/Button';
import SlideBar from 'components/ui/slideBar';

import './listcleaning.scss';

import {
  getListcleaningData,
  hanleListcleaningDataIsLoading,
  setNewListCleaning,
  terminateListCleaning,
} from 'store/actions/listcleaning';
import {
  getMyUserBillingPlan,
  handleMyUserBillingPlanIsDataLoading,
} from 'store/actions/myUserBillingPlan';
import {
  listcleaningDataSelector,
  listcleaningIsDataLoadingSelector,
  newListCleaningSelector,
  myUserBillingPlanDataSelector,
  myUserBillingPlanIsDataLoadingSelector,
} from 'store/selectors';
import { LISTCLEANING_FILE_TYPE } from 'utils/constants';
import NewListCleaning from './components/newListCleaning';

const API_URL = process.env.REACT_APP_API_URL;

function Listcleaning() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const listcleaningData = useSelector(listcleaningDataSelector);
  const listcleaningIsDataLoading = useSelector(
    listcleaningIsDataLoadingSelector
  );
  const newListCleaning = useSelector(newListCleaningSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const myUserBillingPlanIsDataLoading = useSelector(
    myUserBillingPlanIsDataLoadingSelector
  );

  const listcleaningEmailAvailable =
    myUserBillingPlanData?.listcleaning?.emails_available || 0;

  useEffect(() => {
    dispatch(hanleListcleaningDataIsLoading(true));
    dispatch(getListcleaningData());

    dispatch(handleMyUserBillingPlanIsDataLoading(true));
    dispatch(getMyUserBillingPlan());

    const interval = setInterval(() => {
      dispatch(getListcleaningData({ withLoader: false }));
      dispatch(getMyUserBillingPlan({ withLoader: false }));
    }, 5000);

    return () => clearInterval(interval);
  }, [dispatch]);

  if (listcleaningIsDataLoading || myUserBillingPlanIsDataLoading)
    return <Spinner />;

  const tableData = listcleaningData.map((item) => ({
    ...item,
    generated_lists:
      item.status === 'DONE' ? (
        <div>
          <div>
            <a
              href={`${API_URL}/listcleaning/${item.id}/${LISTCLEANING_FILE_TYPE.CLEAN}`}
            >
              clean
            </a>
          </div>
          <div>
            <a
              href={`${API_URL}/listcleaning/${item.id}/${LISTCLEANING_FILE_TYPE.SYNTAX_NOT_VALID}`}
            >
              syntax no valid
            </a>
          </div>
          <div>
            <a
              href={`${API_URL}/listcleaning/${item.id}/${LISTCLEANING_FILE_TYPE.MX_NOT_VALID}`}
            >
              mx not valid
            </a>
          </div>
          <div>
            <a
              href={`${API_URL}/listcleaning/${item.id}/${LISTCLEANING_FILE_TYPE.MX_AND_LOOKUP_NOT_VALID}`}
            >
              mx and lookup not valid
            </a>
          </div>
          <div>
            <a
              href={`${API_URL}/listcleaning/${item.id}/${LISTCLEANING_FILE_TYPE.BLACKLIST}`}
            >
              blacklist
            </a>
          </div>
          <div>
            <a
              href={`${API_URL}/listcleaning/${item.id}/${LISTCLEANING_FILE_TYPE.ROLE}`}
            >
              role
            </a>
          </div>
        </div>
      ) : (
        false
      ),
  }));

  return (
    <div className="listcleaning">
      <div className="listcleaning__header">
        <h1 className="listcleaning__title">
          {t('listcleaning.header.text')}
        </h1>

        <div className="listcleaning__header-right">
          <Button
            mode="purple"
            text={t('listcleaning.btn.start.text')}
            className="listcleaning__btn"
            onClick={() => dispatch(setNewListCleaning({}))}
            disabled={!listcleaningEmailAvailable}
          />
          <div className="email-available">
            <span
              className={cn('email-available__count', {
                danger: listcleaningEmailAvailable <= 100,
                warning:
                  listcleaningEmailAvailable > 100 &&
                  listcleaningEmailAvailable < 1000,
                ok: listcleaningEmailAvailable >= 1000,
              })}
            >
              {listcleaningEmailAvailable}
            </span>
            {' '}
            {t('listcleaning.span.check_available.text')}
          </div>
        </div>
      </div>

      {
        // <InputText
        //   placeholder='search'
        //   className='listcleaning__text-input'
        //   value={searchQuery}
        //   onChange={handleChangeSearchQuery}
        // />
      }

      <Table
        columns={[
          {
            field: 'source_file_name',
            title: t('listcleaning.table.name'),
            type: 'string',
            width: 300,
          },
          {
            field: 'updated_at',
            title: t('listcleaning.table.created'),
            type: 'datetime',
            width: 200,
          },
          {
            field: 'emails_passed',
            title: t('listcleaning.table.emails_passed'),
            type: 'number',
            width: 200,
          },
          {
            field: 'generated_lists',
            title: t('listcleaning.table.generated_lists'),
            width: 200,
          },
          {
            field: 'status',
            title: t('listcleaning.table.status'),
            type: 'string',
            width: 200,
          },
        ]}
        data={tableData}
        cbs={{
          onTerminate: (data) => dispatch(terminateListCleaning(data)),
        }}
        // sorting={props.filesSorting}
        // onSorting={props.setFilesSort}
      />

      <SlideBar
        isOpen={!!newListCleaning}
        onClose={() => dispatch(setNewListCleaning(null))}
      >
        <NewListCleaning title={t('listcleaning.new.title')} />
      </SlideBar>
    </div>
  );
}

export default Listcleaning;
