import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner/Spinner';
import SlideBar from 'components/ui/slideBar';
import Error404 from 'components/ui/error404';
import HangingBtn from 'components/ui/hangingBtn';

import {
  handleIpsDataFilter,
  setIsOnNewIp,
  setIpsAvailableCount,
  setIpsData,
  setNewIpsFile,
  setResponseNewIpsUploadedFile,
} from 'store/actions/ips';
import {
  handleIpsReportIsOnEdit,
  getIpsReportData,
} from 'store/actions/ipsReport';
import { getNetmasksData } from 'store/actions/netmasks';
import {
  ipsIsOnNewIpSelector,
  ipsReportIsOnEditSelector,
  myUserBillingPlanIsDataLoadingSelector,
  ipsErrorsSelector,
  netmasksErrorsSelector,
  // usersMeSelector,
  // usersMeIsLoadingSelector,
} from 'store/selectors';

import { LANDING_FAQ_URL, NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import TableIpList from './components/TableIpList';
import IpsReportSettings from './components/IpsReportSettings';
import AddIp from './components/AddIp';
import IpsListHeader from './components/IpsListHeader';

import './ips-list.scss';

function IpsList() {
  const t = useFormatMessage();
  const dispatch = useDispatch();

  const ipsIsOnNewIp = useSelector(ipsIsOnNewIpSelector);
  const ipsReportIsOnEdit = useSelector(ipsReportIsOnEditSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);

  const ipsErrors = useSelector(ipsErrorsSelector);
  const netmasksErrors = useSelector(netmasksErrorsSelector);

  // const usersMe = useSelector(usersMeSelector);
  // const usersMeIsLoading = useSelector(usersMeIsLoadingSelector);

  useEffect(() => {
    dispatch(getIpsReportData());
    dispatch(getNetmasksData({ withLoader: false }));
    return () => {
      dispatch(handleIpsDataFilter({ field: 'ip', value: '' }));
      dispatch(handleIpsDataFilter({ field: 'netmask', value: '' }));
      dispatch(setIpsAvailableCount(0));
      dispatch(setIpsData({ data:[] }));
    };
  }, [dispatch]);

  const showIpsReportSlider = useMemo(() => !!ipsReportIsOnEdit, [
    ipsReportIsOnEdit,
  ]);
  const showNewIpSlider = useMemo(() => !!ipsIsOnNewIp, [ipsIsOnNewIp]);

  if (myUserBillingPlanIsDataLoading /* || usersMeIsLoading */)
    return <Spinner />;

  if (
    ipsErrors?.data?.status === NOT_FOUND_ERROR_STATUS ||
    netmasksErrors?.data?.status === NOT_FOUND_ERROR_STATUS
  )
    return <Error404 />;

  return (
    <div className="ips-list">
      <IpsListHeader />

      <TableIpList />

      <SlideBar
        isOpen={showIpsReportSlider}
        onClose={() => dispatch(handleIpsReportIsOnEdit(false))}
        className="ips-list__report-settings"
      >
        <IpsReportSettings />
      </SlideBar>

      <SlideBar
        isOpen={showNewIpSlider}
        onClose={() => {
          dispatch(setIsOnNewIp(false));
          dispatch(setResponseNewIpsUploadedFile(null));
          dispatch(setNewIpsFile(null));
        }}
        className="ips-list__new-ip"
      >
        <AddIp />
      </SlideBar>

      <HangingBtn 
        tooltipText={t('seedlisting_list.hanging-btn.tootip.text')}
        href={`${LANDING_FAQ_URL}/index.html?faqItem=ipDomainMonitoringFaq`}
      />
    </div>
  );
}

export default React.memo(IpsList);
