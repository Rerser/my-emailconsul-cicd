import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Chart from 'react-google-charts';

import Spinner from 'components/ui/spinner';

import {
  setIpRblChangesLogsPeriodFrom,
  setIpRblChangesLogsPeriodTo,
} from 'store/actions/ips';
import {
  ipsIpRblChangesLogPeriodFromSelector,
  ipsIpRblChangesLogPeriodToSelector,
} from 'store/selectors';

import {
  IP_DOMAINS_RBL_CHANGES_LOGS_STATUS,
  IP_DOMAINS_RBL_CHANGES_LOGS_COLORS,
} from 'utils/constants';

import './ip-graph.scss';

const IpGraph = ({ data }) => {
  const t = useFormatMessage();
  const dispatch = useDispatch();

  const ipsIpRblChangesLogPeriodFrom = useSelector(ipsIpRblChangesLogPeriodFromSelector);
  const ipsIpRblChangesLogPeriodTo = useSelector(ipsIpRblChangesLogPeriodToSelector);

  const dateFrom = Date.parse(ipsIpRblChangesLogPeriodFrom);
  const dateTo = Date.parse(ipsIpRblChangesLogPeriodTo);

  const MIN_DATE = Math.min(...data.map((d) => Date.parse(d.created_at)));
  const MAX_DATE = Math.max(...data.map((d) => Date.parse(d.created_at)));

  useEffect(() => {
    if (data?.length) {
      if(!ipsIpRblChangesLogPeriodFrom && !ipsIpRblChangesLogPeriodTo) {
        if (MIN_DATE && isFinite(MIN_DATE)) {
          dispatch(setIpRblChangesLogsPeriodFrom(new Date(MIN_DATE).toISOString()));
        }
        if (MIN_DATE === MAX_DATE) {
          dispatch(setIpRblChangesLogsPeriodTo(new Date().toISOString()));
        } else if (MAX_DATE < Date.now()) {
          dispatch(setIpRblChangesLogsPeriodTo(new Date().toISOString()));
        } else {
          if (MAX_DATE && isFinite(MAX_DATE)) {
            dispatch(setIpRblChangesLogsPeriodTo(new Date(MAX_DATE).toISOString()));
          }
        }
      }
    }
  }, [dispatch, ipsIpRblChangesLogPeriodFrom, ipsIpRblChangesLogPeriodTo, data, MIN_DATE, MAX_DATE]);

  /**
   * converting the data to the form:
   * {
   *    [host]: [
   *      sortable list by created_at
   *      {
   *        status: string,
   *        createdParsed: date (ms),
   *      },
   *      ...
   *    ],
   *    ...
   * }
   */
  const modifiedData = data.reduce(
    (pred, curr) => ({
      ...pred,
      [curr.host]: [
        ...(pred[curr.host] ?? []),
        {
          status: curr.status,
          createdParsed: Date.parse(curr.created_at),
        },
      ].sort((a, b) => a.createdParsed - b.createdParsed),
    }),
    {}
  );

  /**
   * dateFrom
   */
  let modifiedDataWithDateRange = Object.keys(modifiedData).reduce((pred, host) => {
    const ranges = modifiedData[host];

    if (dateFrom <= MIN_DATE) {
      pred = {
        ...pred,
        [host]: ranges,
      };
    } else if (dateFrom > MIN_DATE && dateFrom < MAX_DATE) {
      // находим item с датой, большей, чем (следующий от) dateFrom
      const foundIndexDate = ranges.findIndex((item) => item.createdParsed > dateFrom);

      // если найден такой элемент
      if (foundIndexDate > -1) {
        // найдем предыдущий элемент и возьмем его статус
        const predIndex = foundIndexDate - 1;

        if (predIndex > -1) {
          pred = {
            ...pred,
            [host]: [
              {
                status: ranges[predIndex].status,
                createdParsed: dateFrom,
              },
              ...ranges.slice(foundIndexDate),
            ],
          };
        } else {
          // если нет предыдущего
          pred = {
            ...pred,
            [host]: [
              ...ranges
            ],
          };
        }
      } else {
        // если в массиве нет элемента, большего, чем dateFrom, то
        // возьмем статус последнего
        const lastIndex = ranges.length - 1;

        pred = {
          ...pred,
          [host]: [
            {
              status: ranges[lastIndex].status,
              createdParsed: dateFrom,
            },
          ],
        };
      }
    } else if (dateFrom >= MAX_DATE) {
      // возьмем статус последнего
      const lastIndex = ranges.length - 1;

      pred = {
        ...pred,
        [host]: [
          {
            status: ranges[lastIndex].status,
            createdParsed: dateFrom,
          },
        ],
      };
    }

    return pred;
  }, {});

  /**
   * dateTo
   */
  modifiedDataWithDateRange = Object.keys(modifiedDataWithDateRange).reduce((pred, host) => {
    const ranges = modifiedDataWithDateRange[host];
    const firstItem = ranges[0];

    if (dateTo <= MIN_DATE) {
      pred = {
        ...pred,
        [host]: [firstItem],
      };
    } else if (dateTo > MIN_DATE && dateTo < MAX_DATE) {
      // находим item с датой, большей, чем (следующий от) dateTo
      const foundIndexDate = ranges.findIndex((item) => item.createdParsed > dateTo);

      // если найден такой элемент
      if (foundIndexDate > -1) {
        pred = {
          ...pred,
          [host]: [...ranges.slice(0, foundIndexDate)],
        };
      } else {
        pred = {
          ...pred,
          [host]: ranges,
        };
      }
    } else {
      pred = {
        ...pred,
        [host]: ranges,
      };
    }

    return pred;
  }, {});

  /**
   * convert to chart data:
   * [
   *  [host, status, date start, date end],
   *  ...
   * ]
   */
  const chartData = Object.keys(modifiedDataWithDateRange).reduce((pred, host) => {
    const statusArr = modifiedDataWithDateRange[host];

    if (statusArr.length) {
      const calculatedArr = [];

      for (let i = 0; i < statusArr.length; i++) {
        const start = new Date(statusArr[i].createdParsed);
        const end = new Date(statusArr[i + 1]?.createdParsed || ipsIpRblChangesLogPeriodTo);
        let endDate = 0; // зависит от start и end

        if (start <= end) {
          endDate = new Date(statusArr[i + 1]?.createdParsed || new Date(ipsIpRblChangesLogPeriodTo).toISOString());
        } else {
          endDate = new Date(start);
        }
        if (Date.parse(endDate) > Date.now()) {
          endDate = new Date();
        }
        calculatedArr.push([
          host,                // 0 -> Row label
          statusArr[i].status, // 1 -> Bar label
          start,               // 2 -> Start Date
          endDate,             // 3 -> End Date
        ]);
      }
      pred = [...pred, ...calculatedArr];
    }
    return pred;
  }, []);

  if (!data.length ||
    (dateFrom < MIN_DATE && dateTo < MIN_DATE) ||
    (dateFrom > Date.now() && dateTo > Date.now())
  ) {
    return (
      <span>
        {t('ips_logs.graph.no-blacklisted-data.text')}
      </span>
    );
  }

  // массив данных для графика отсортирован по дате,
  // если статус первого элемента в chartData === BLACKLISTED,
  // то первый элемент массива цветов будет === цвету BLACKLISTED,
  // и наоборот, если первый === RESOLVED, то первый элемент в массиве цветов
  // будет равен цвету RESOLVED
  const baseColors = [
    IP_DOMAINS_RBL_CHANGES_LOGS_COLORS[IP_DOMAINS_RBL_CHANGES_LOGS_STATUS.BLACKLISTED],
    IP_DOMAINS_RBL_CHANGES_LOGS_COLORS[IP_DOMAINS_RBL_CHANGES_LOGS_STATUS.RESOLVED],
  ]
  const barColors = chartData.length && chartData[0].length >= 1 &&
    chartData[0][1] === IP_DOMAINS_RBL_CHANGES_LOGS_STATUS.BLACKLISTED 
      ? baseColors 
      : baseColors.reverse();

  return (
    <div className="c-slidebar-content chosen-ip-graph">
      {ipsIpRblChangesLogPeriodFrom && ipsIpRblChangesLogPeriodTo && ipsIpRblChangesLogPeriodFrom < ipsIpRblChangesLogPeriodTo ? (
        <Chart
          chartType="Timeline"
          loader={<Spinner />}
          data={[
            [
              { type: 'string', id: 'Host' },     // 0 -> Row label
              { type: 'string', id: 'Status' },   // 1 -> Bar label
              { type: 'date', id: 'StartDate' },  // 2 -> Start Date
              { type: 'date', id: 'EndDate' },    // 3 -> End Date
            ],
            ...chartData,
          ]}
          options={{
            width: '100%',
            height: '600',
            legend: { position: 'none' },
            timeline: {
              rowLabelStyle: {
                fontSize: 16,
              },
              barLabelStyle: { fontSize: 16 },
              colorByRowLabel: false,
              avoidOverlappingGridLines: true,
              showBarLabels: true,
            },
            hAxis: {
              format: 'dd.MM.yy \nHH:MM',
              minValue: new Date(dateFrom),
              maxValue: new Date(dateTo),
            },
            colors: barColors,
          }}
        />
      ) : (
        t('ips_logs.graph.no-date.text')
      )}
    </div>
  );
};

export default React.memo(IpGraph);
