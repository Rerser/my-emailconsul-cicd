import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { ru, enUS } from 'date-fns/locale';

import DatePickerRange from 'components/ui/datePickerRange';

import {
  localeCurrentSelector,
  ipsIpRblChangesLogPeriodFromSelector,
  ipsIpRblChangesLogPeriodToSelector,
} from 'store/selectors';
import {
  setIpRblChangesLogsPeriodFrom,
  setIpRblChangesLogsPeriodTo,
} from 'store/actions/ips';

import { DAY, LOCALE } from 'utils/constants';

function IpDatesRange({ disabled }) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const ipsIpRblChangesLogPeriodFrom = useSelector(ipsIpRblChangesLogPeriodFromSelector);
  const ipsIpRblChangesLogPeriodTo = useSelector(ipsIpRblChangesLogPeriodToSelector);

  const handleChangeDateFrom = (date) => {
    if (date) {
      dispatch(setIpRblChangesLogsPeriodFrom(new Date(date).toISOString()));
    } else {
      dispatch(setIpRblChangesLogsPeriodFrom(''));
    }
    if (date >= new Date(ipsIpRblChangesLogPeriodTo)) {
      dispatch(setIpRblChangesLogsPeriodTo(''));
    }
  };

  const handleChangeDateTo = (date) => {
    if (date) {
      dispatch(setIpRblChangesLogsPeriodTo(new Date(date).toISOString()));
    } else {
      dispatch(setIpRblChangesLogsPeriodTo(''));
    }
  };

  return (
    <div className="ip-list__dates-range">
      <DatePickerRange 
        locale={locale === LOCALE.ru ? ru : enUS}

        labelInputFrom={t('ips_logs.dates_range.date_from.text')}
        labelInputTo={t('ips_logs.dates_range.date_to.text')}

        dateFormat="DD-MM-yyyy HH:mm"
        dateFrom={ipsIpRblChangesLogPeriodFrom}
        dateTo={ipsIpRblChangesLogPeriodTo}
        handleChangeDateFrom={handleChangeDateFrom}
        handleChangeDateTo={handleChangeDateTo}

        hasTimeSelect
        timeFormat='HH:mm'
        timeIntervals={15}

        maxDate={new Date(Date.now() - DAY).toISOString()}

        disabled={disabled}
        disabledTo={!ipsIpRblChangesLogPeriodFrom || disabled}
        requiredDateTo={!disabled && !ipsIpRblChangesLogPeriodTo}
      />
    </div>
  );
}

export default React.memo(IpDatesRange);
