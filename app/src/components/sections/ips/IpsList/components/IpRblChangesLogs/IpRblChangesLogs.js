import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Spinner from 'components/ui/spinner';
import Error404 from 'components/ui/error404';
import IpDatesRange from './components/IpDatesRange';
import IpGraph from './components/IpGraph';

import {
  ipsErrorsSelector,
  ipsIpRblChangesLogIsLoadingSelector,
  ipsIpRblChangesLogSelector,
  usersMeSelector
} from 'store/selectors';
import {
  getIpRblChangesLogs,
  setIpRblChangesLogs,
  setIpRblChangesLogsPeriodFrom,
  setIpRblChangesLogsPeriodTo
} from 'store/actions/ips';

import {
  DEMO_USER_EMAIL,
  NOT_FOUND_ERROR_STATUS,
} from 'utils/constants';

import './ip-rbl-changes-log.scss';

const IpRblChangesLogs = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const { id } = useParams();

  const ipsIpRblChangesLogIsLoading = useSelector(ipsIpRblChangesLogIsLoadingSelector);
  const ipsIpRblChangesLog = useSelector(ipsIpRblChangesLogSelector);
  const ipsErrors = useSelector(ipsErrorsSelector);

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  useEffect(() => {
    if (id) {
      dispatch(getIpRblChangesLogs({ id }));
    }
    return () => {
      dispatch(setIpRblChangesLogsPeriodFrom(''));
      dispatch(setIpRblChangesLogsPeriodTo(''));
      dispatch(setIpRblChangesLogs([]));}
  }, [dispatch, id]);

  if (ipsErrors?.ipRblChangesLogs?.status === NOT_FOUND_ERROR_STATUS) {
    return <Error404 />;
  }

  return (
    <div className="ip-rblchangeslogs">
      <div className="ip-rblchangeslogs__header">
        <h1 className="ip-rblchangeslogs__title">
          <Link to="/ips">
            {t('ips_logs.title.ip-info.text')}
          </Link>
          {' > '}
          <span
            className={cn('ip-rblchangeslogs__breadcrumbs-current', {
              'ip-rblchangeslogs__breadcrumbs-current--blured': meEmail === DEMO_USER_EMAIL,
            })}
          >
            {ipsIpRblChangesLog?.ip ?? ''}
          </span>
        </h1>
      </div>

      <div className="ip-rblchangeslogs__filter">
        <IpDatesRange disabled={!ipsIpRblChangesLog?.data?.length || false} />
      </div>

      {ipsIpRblChangesLogIsLoading ? <Spinner /> : <IpGraph data={ipsIpRblChangesLog?.data || []} />}
    </div>
  );
};

export default React.memo(IpRblChangesLogs);
