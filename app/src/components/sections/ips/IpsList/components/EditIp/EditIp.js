import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import { setEditedIp, handleEditedIp, saveEditedIp } from 'store/actions/ips';
import {
  ipsEditedIpSelector,
  ipsEditedIpIsLoadingSelector,
} from 'store/selectors';

import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';

import './c-edit-ip.scss';

const EditIp = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const editedIp = useSelector(ipsEditedIpSelector);
  const editedIpIsLoading = useSelector(ipsEditedIpIsLoadingSelector);

  return (
    <form className="c-slidebar-content c-edit-ip">
      <h3 className="c-slidebar-content__title">
        {editedIp.id
          ? t('edit_ip.edited_update.ip')
          : t('edit_ip.edited_add.ip')}
        {' '}
        {t('edit_ip.header.ip')}
      </h3>

      {editedIpIsLoading && <Spinner />}

      {!editedIpIsLoading && editedIp && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('edit_ip.slidebar.description.ip')}
              </span>
              <InputText
                value={editedIp.ip}
                onChange={(e) =>
                  dispatch(
                    handleEditedIp({ field: 'ip', value: e.target.value })
                  )
                }
              />
            </label>
          </div>
        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {editedIp && (
          <Button
            mode="purple"
            text={
              editedIp.id ? t('edit_ip.btn.update.ip') : t('edit_ip.btn.add.ip')
            }
            className="c-slidebar-content__btn"
            disabled={editedIpIsLoading}
            onClick={() => dispatch(saveEditedIp(editedIp))}
          />
        )}

        <Button
          text={t('edit_ip.btn.close.ip')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setEditedIp(null))}
        />
      </div>
    </form>
  );
};

export default React.memo(EditIp);
