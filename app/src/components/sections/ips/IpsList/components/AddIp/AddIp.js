import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { basic_trashcan } from 'react-icons-kit/linea/basic_trashcan';
import Icon from 'react-icons-kit';

import {
  setIsOnNewIp,
  handleNewIps,
  handleNewNetmask,
  handleNewIpMode,
  removeNewIp,
  saveNewNetmask,
  saveNewIps,
  handelSetNewIpsFile,
  handelUploadNewIpsFile,
} from 'store/actions/ips';
import {
  myUserBillingPlanDataSelector,
  ipsNewIpsSelector,
  ipsNewNetmaskSelector,
  ipsNewIpModeSelector,
  ipsNewIpIsLoadingSelector,
  ipsDataPaginationSelector,
  ipsNewIpsFileSelector,
  ipsNewIpsFileIsUploadingSelector,
  ipsResponseNewIpsUploadedFileSelector
} from 'store/selectors';

import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';
import Tabs from 'components/ui/tabs';
import InputFile from 'components/ui/inputFile';

import { NEW_IP_MODE } from 'utils/constants';

import './c-add-ip.scss';
import cn from 'classnames';

const AddIp = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const newIps = useSelector(ipsNewIpsSelector);
  const netmask = useSelector(ipsNewNetmaskSelector);
  const ipsNewIpMode = useSelector(ipsNewIpModeSelector);
  const ipsNewIpIsLoading = useSelector(ipsNewIpIsLoadingSelector);
  const ipsDataPagination = useSelector(ipsDataPaginationSelector);
  const ipsNewIpsFile = useSelector(ipsNewIpsFileSelector);
  const ipsNewIpsFileIsUploading = useSelector(ipsNewIpsFileIsUploadingSelector);
  const ipsResponseNewIpsUploadedFile = useSelector(ipsResponseNewIpsUploadedFileSelector);

  const { ips = 0 } = myUserBillingPlanData;
  const { count = 0 } = ipsDataPagination;
  const ipsAvailable = ips?.ips_per_month - count || 0;

  const handleDeleteNewIp = (e, index) => {
    e.preventDefault();
    if (newIps.length === 1) {
      dispatch(handleNewIps({ value: '', index: 0 }));
    } else {
      dispatch(removeNewIp({ index }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (ipsNewIpMode === NEW_IP_MODE.SINGLE) {
      if (!ipsNewIpIsLoading && newIps.every((ip) => ip)) {
        dispatch(saveNewIps({ newIps }));
      }
    } else if (!ipsNewIpIsLoading && netmask) {
      dispatch(saveNewNetmask({ netmask }));
    }
  };

  return (
    <form className="c-slidebar-content c-add-ip" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('add_ip.header.text')}
      </h3>

      {ipsNewIpIsLoading && <Spinner />}

      {!ipsNewIpIsLoading && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <Tabs
              value={ipsNewIpMode}
              options={[
                {
                  value: NEW_IP_MODE.SINGLE,
                  label: t('add_ip.tabs.single_ip.label'),
                },
                {
                  value: NEW_IP_MODE.NETMASK,
                  label: t('add_ip.tabs.netmask.label'),
                },
                {
                  value: NEW_IP_MODE.CSV,
                  label: t('add_ip.tabs.csv.label'),
                },
              ]}
              onChange={(e) => dispatch(handleNewIpMode(e.target.value))}
            />
          </div>

          {ipsNewIpMode === NEW_IP_MODE.SINGLE && (
            <div className="c-slidebar-content__row">
              <label className="c-slidebar-content__label">
                <span className="c-slidebar-content__description">
                  {t('add_ip.ip.label')}
                </span>
                {newIps.map((ip, index) => (
                  <div key={index} className="c-add-ip__ip-row ip-row">
                    <InputText
                      value={ip}
                      onChange={(e) =>
                        dispatch(handleNewIps({ value: e.target.value, index }))
                      }
                      className="ip-row__input"
                    />
                    <button
                      type="button"
                      className="ip-row__remove-btn"
                      onClick={(e) => handleDeleteNewIp(e, index)}
                    >
                      <Icon icon={basic_trashcan} />
                    </button>
                  </div>
                ))}
                <Button
                  text={t('add_ip.btn.add.text')}
                  onClick={() =>
                    dispatch(
                      handleNewIps({ value: null, index: newIps.length })
                    )
                  }
                  disabled={newIps.length > ipsAvailable - 1}
                />
              </label>
            </div>
          )}

          {ipsNewIpMode === NEW_IP_MODE.NETMASK && (
            <div className="c-slidebar-content__row">
              <label className="c-slidebar-content__label">
                <span className="c-slidebar-content__description">
                  {t('add_ip.netmask.label')}
                </span>
                <InputText
                  value={netmask}
                  onChange={(e) => dispatch(handleNewNetmask(e.target.value))}
                />
              </label>
            </div>
          )}

          {ipsNewIpMode === NEW_IP_MODE.CSV && (
            <Fragment>
              {ipsNewIpsFileIsUploading ? (
                <Spinner />
              ) : (
                <Fragment>
                  {ipsResponseNewIpsUploadedFile && (
                    <div className="c-add-ip__result-uploaded-container">
                      <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                        <div className="c-add-ip__uploaded-result">
                          {t('add_ip.result.uploaded.added.text')}
                          {' '}
                          <span className={cn("c-add-ip__quantity", {
                            'c-add-ip__quantity--success': ipsResponseNewIpsUploadedFile.added > 0,
                            'c-add-ip__quantity--failed': ipsResponseNewIpsUploadedFile.added === 0,
                          })}>
                            {ipsResponseNewIpsUploadedFile.added}
                          </span>
                        </div>
                      </div>
                      {!!ipsResponseNewIpsUploadedFile.not_added && (
                        <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                          <div className="c-add-ip__uploaded-result">
                            {t('add_ip.result.uploaded.has_not_added.text')}
                            {' '}
                            <span className={cn("c-add-ip__quantity", {                              
                              'c-add-ip__quantity--success': ipsResponseNewIpsUploadedFile.not_processed === 0,                              
                              'c-add-ip__quantity--failed': ipsResponseNewIpsUploadedFile.not_processed > 0,
                            })}>
                              {ipsResponseNewIpsUploadedFile.not_added}
                            </span>
                          </div>
                        </div>
                      )}
                      {!!ipsResponseNewIpsUploadedFile.not_valid.length && (
                        <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                          <div className="c-add-ip__uploaded-result">
                            {t('add_ip.result.uploaded.not_valid.text')}
                            {' '}
                            <span className="c-add-ip__quantity c-add-gp-domain__quantity--failed">
                              {ipsResponseNewIpsUploadedFile.not_valid.length}
                            </span>
                            {!!ipsResponseNewIpsUploadedFile.not_valid.length && 
                              ipsResponseNewIpsUploadedFile.not_valid.map((ip, i) => (
                                <div key={`not-valid-ips-${i}`} className="c-add-ip__uploaded">
                                  {ip}
                                </div>
                              ))
                            }
                          </div>
                        </div>
                      )}
                      </div>
                  )}
                
                  <div className="c-slidebar-content__label">
                    {!ipsNewIpsFile && (
                      <Fragment>
                        <span>
                          {t('add_ip.drag_file.warning.text')}
                        </span>
                        <InputFile
                          labelNodeElement={
                            <div className="c-add-ip__dragfile-container">
                              {t('add_ip.drag_file.import.text')}
                            </div>
                          }
                          onUpload={(data) => dispatch(handelSetNewIpsFile(data.files[0]))}
                        />
                      </Fragment>
                    )}
                    {ipsNewIpsFile && (
                      <div className="c-slidebar-content__file-row">
                        <div className="c-slidebar-content__file-label">
                          {ipsNewIpsFile.name}
                        </div>
                        <div
                          className="c-slidebar-content__file-delete"
                          onClick={() => dispatch(handelSetNewIpsFile(null))}
                        >
                          <Icon icon={basic_trashcan} />
                        </div>
                      </div>
                    )}
                  </div>
                </Fragment>
              )}
            </Fragment>
          )}
        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {ipsNewIpMode === NEW_IP_MODE.SINGLE && (
          <Button
            mode="purple"
            text={t('add_ip.btn.save.text')}
            className="c-slidebar-content__btn"
            disabled={ipsNewIpIsLoading || newIps.some((ip) => !ip)}
            type="submit"
          />
        )}

        {ipsNewIpMode === NEW_IP_MODE.NETMASK && (
          <Button
            mode="purple"
            text={t('add_ip.btn.save.text')}
            className="c-slidebar-content__btn"
            disabled={ipsNewIpIsLoading || !netmask}
            type="submit"
          />
        )}

        {ipsNewIpMode === NEW_IP_MODE.CSV && (
          <Button
            mode="purple"
            text={t('add_ip.btn.import.text')}
            className="c-slidebar-content__btn"
            disabled={!ipsNewIpsFile || ipsNewIpsFileIsUploading}
            type="button"
            onClick={() => dispatch(handelUploadNewIpsFile())}
          />
        )}

        <Button
          text={t('add_ip.btn.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setIsOnNewIp(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(AddIp);
