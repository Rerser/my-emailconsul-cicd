import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Button from 'components/ui/button/Button';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import {
  ipsReportDataSelector,
  myUserBillingPlanDataSelector,
  ipsAvailableCountSelector,
  ipsAvailableCountIsLoadingSelector,
} from 'store/selectors';
import { getIpsAvailableCount, setIsOnNewIp } from 'store/actions/ips';
import { getBillingPlanIsExpired } from 'utils/helpers';
import { handleIpsReportIsOnEdit } from 'store/actions/ipsReport';

import { BILLING_PLAN_NAME } from 'utils/constants';

import history from 'utils/history';
import cn from 'classnames';
import './ips-list-header.scss';
import Spinner from 'components/ui/spinner';

function IpsListHeader() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const ipsReport = useSelector(ipsReportDataSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const ipsAvailable = useSelector(ipsAvailableCountSelector);
  const ipsAvailableIsLoading = useSelector(ipsAvailableCountIsLoadingSelector);

  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  const isNotAllowedToRequest =
    myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.NONE ||
    !myUserBillingPlanData?.ips?.ips_per_month ||
    billingPlanIsExpired;

  useEffect(() => {
    if (!isNotAllowedToRequest) {
      dispatch(getIpsAvailableCount());
    }
  }, [dispatch, isNotAllowedToRequest]);

  return (
    <div className="ips-list-header">
      <h1 className="ips-list__title">
        {t('ips_list.title.text')}
      </h1>

      <div className="ips-list__header-right header-right">
        {!ipsAvailableIsLoading && (billingPlanIsExpired || ipsAvailable <= 0) && (
          <div className="header-right__upgrade-plan">
            <Button
              mode="blue"
              text={t('seedlisting_list.update_plan.btn')}
              className="ips-list__btn"
              onClick={() => history.push('/user/billing')}
            />
          </div>
        )}
        <div className="header-right__config-report">
          <Button
            mode="purple"
            text={t('ips_list.daily_report.btn')}
            className="ips-list__btn"
            onClick={() => dispatch(handleIpsReportIsOnEdit(true))}
            disabled={
              billingPlanIsExpired /* || (seedlistingEmailsAvailable <= 0) */
            }
          />
          <div className="ips-report-status">
            {billingPlanIsExpired && (
              <div className="ips-report-status__value danger">
                {t('ips_list.billing_plan.exipe_warning_text')}
              </div>
            )}
            {!billingPlanIsExpired && (
              <Fragment>
                <span
                  className={cn('ips-report-status__value', {
                    danger: !ipsReport?.is_active,
                    ok: ipsReport?.is_active,
                  })}
                >
                  {ipsReport?.is_active
                    ? t('ips_list.ipsreport.status.on')
                    : t('ips_list.ipsreport.status.off')}
                </span>
              </Fragment>
            )}
          </div>
        </div>
        <div className="header-right__new-ip">
          <Button
            mode="purple"
            text={t('ips_list.add_ip.btn')}
            className="ips-list__btn"
            onClick={() => dispatch(setIsOnNewIp(true))}
            disabled={billingPlanIsExpired || ipsAvailable <= 0}
          />
          {ipsAvailableIsLoading ? (
            <Spinner />
          ) : (
            <div className="ips-available">
              {billingPlanIsExpired && (
                <div className="ips-available__count danger">
                  {t('ips_list.billing_plan.exipe_warning_text')}
                </div>
              )}
              {!billingPlanIsExpired && (
                <Fragment>
                  <span
                    className={cn('ips-available__count', {
                      danger: ipsAvailable <= 0,
                      warning: ipsAvailable > 0 && ipsAvailable < 25,
                      ok: ipsAvailable >= 25,
                    })}
                  >
                    {ipsAvailable}
                  </span>
                  <span>
                    {' '}
                    {t('ips_list.billing_plan.checks_available_text')}
                  </span>
                </Fragment>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default React.memo(IpsListHeader);
