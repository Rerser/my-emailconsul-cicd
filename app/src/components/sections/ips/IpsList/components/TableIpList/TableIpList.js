import React, { Fragment, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Table from 'components/ui/table';
import Pagination from 'components/ui/pagination';
import Select from 'components/ui/select';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button/Button';
import Spinner from 'components/ui/spinner/Spinner';

import {
  getIpsData,
  deleteIp,
  refreshIp,
  setIpsDataPagination,
  handleIpsDataFilter,
  handleIpsDataSorting,
  deleteNetmask,
  handleIpsDataIsLoading,
  deleteIpsFromFilters,
} from 'store/actions/ips';
import { getNetmasksData } from 'store/actions/netmasks';
import {
  ipsIsDataLoadingSelector,
  ipsDataSelector,
  ipsDataPaginationSelector,
  ipsDataFilterSelector,
  ipsDataSortingSelector,
  netmasksDataSelector,
  netmasksIsDataLoadingSelector,
  usersMeSelector,
} from 'store/selectors';

import { DEMO_USER_EMAIL, IP_STATUS } from 'utils/constants';
import { useDebounce, usePagination } from 'utils/helpers';
import { setCallbackConfirmationPopup } from 'store/actions/confirmationPopup';

function TableIpList() {
  const dispatch = useDispatch();
  const t = useFormatMessage();
  const history = useHistory();

  const ipsData = useSelector(ipsDataSelector);
  const ipsIsDataLoading = useSelector(ipsIsDataLoadingSelector);

  const ipsDataSorting = useSelector(ipsDataSortingSelector);
  const ipsDataFilter = useSelector(ipsDataFilterSelector);

  const netmasksData = useSelector(netmasksDataSelector);
  const netmasksIsDataLoading = useSelector(netmasksIsDataLoadingSelector);

  const ipsDataPagination = useSelector(ipsDataPaginationSelector);

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  const { limit, offset } = ipsDataPagination;
  const { netmask, ip: filterIp } = ipsDataFilter;
  const { field: sortField, order: sortOrder } = ipsDataSorting;

  useEffect(() => {
    dispatch(handleIpsDataIsLoading(true));
  }, [dispatch]);

  const debouncedFilterIp = useDebounce(filterIp, 1000);

  const tableData = useMemo(
    () =>
      ipsData.map((item) => {
        const { ip, dns_lookup_result = [], rbl_result = [], status } = item;

        const rdns = dns_lookup_result.map(({ rDNS, addresses }, i) => (
          <div
            key={`${ip}-${rDNS}`}
            className={cn('rdns-row', {
              warning: !addresses.includes(ip),
            })}
          >
            {rDNS}
          </div>
        ));

        const rblFiltered = new Set();

        rbl_result.forEach(({ url }) => rblFiltered.add(url));

        const rbl = [...rblFiltered].map((url, i) => (
          <div key={`rbl-value-ip-${i}`} className="rbl-row">
            <a
              href={url}
              target="_blank"
              rel="noopener noreferrer"
              className="rbl-row__link"
            >
              {url}
            </a>
          </div>
        ));

        const isUpdating =
          status === IP_STATUS.WAITING || status === IP_STATUS.IN_PROGRESS;

        const cbsPayload = {
          isUpdating,
          isUpdateDisable: isUpdating,
        };

        return {
          ...item,
          rdns,
          rbl_result: rbl,
          cbsPayload,
        };
      }),
    [ipsData]
  );

  useEffect(() => {
    dispatch(getIpsData({ withLoader: false }));

    const interval = setInterval(() => {
      dispatch(getIpsData({ withLoader: false }));
      dispatch(getNetmasksData({ withLoader: false }));
    }, 5000);

    return () => clearInterval(interval);
  }, [
    dispatch,
    limit,
    offset,
    netmask,
    sortField,
    sortOrder,
    debouncedFilterIp,
  ]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: ipsDataPagination,
    action: setIpsDataPagination,
  });

  if (ipsIsDataLoading) return <Spinner />;

  return (
    <Fragment>
      <div className="ips-list__pagination ipl-table-panel">
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ips-list__pagination ips-list__pagination--top"
        />

        <div className="ipl-table-panel__filter itp-filter">
          <div className="itp-filter__item itp-filter__ip-filter">
            <div className="itp-filter__label">
              {t('ips_list.filter_by_ip.lable')}
            </div>
            <InputText
              value={filterIp}
              onChange={(e) =>
                dispatch(
                  handleIpsDataFilter({ field: 'ip', value: e.target.value })
                )
              }
              placeholder={t('ips_list.filter_by_ip.placeholder')}
            />
          </div>

          {!!netmask && (
            <Button
              mode="red"
              text={t('ips_list.remove_netmask.btn.text')}
              className="ips-list__btn itp-filter__rm-netmask"
              onClick={() => {
                dispatch(
                  setCallbackConfirmationPopup(() =>
                    dispatch(deleteNetmask(netmask))
                  )
                );
              }}
            />
          )}

          <div className="itp-filter__item itp-filter__netmask">
            <div className="itp-filter__label">
              {t('ips_list.filter_by_netmask.lable')}
            </div>
            <Select
              value={netmask}
              options={netmasksData.map((value) => ({ value, label: value }))}
              onChange={(value) =>
                dispatch(handleIpsDataFilter({ field: 'netmask', value }))
              }
              disabledValue={null}
              placeholder={
                netmasksIsDataLoading
                  ? t('ips_list.filter_by_netmask.select.loading')
                  : t('ips_list.filter_by_netmask.select.select')
              }
              className="itp-filter__netmask"
              optionPlaceholderDisabled={false}
            />
          </div>
        </div>
      </div>

      <Table
        columns={[
          {
            field: 'ip',
            title: t('ips_list.table.header.ip.text'),
            type: 'string',
            width: 200,
          },
          {
            field: 'rdns',
            title: t('ips_list.table.header.rdns.text'),
            width: 400,
          },
          {
            field: 'rbl_result',
            title: t('ips_list.table.header.blacklist.text'),
            width: 400,
          },
        ]}
        data={tableData}
        cbs={{
          onUpdate: (item) => dispatch(refreshIp(item)),
          onDelete: (item) => dispatch(deleteIp(item)),
        }}
        sorting={{
          ...ipsDataSorting,
          sortFields: ['ip', 'rbl_result'],
          isApiSorting: true,
        }}
        onSorting={(data) => dispatch(handleIpsDataSorting(data))}
        headerCbs={{
          dump: (data) => false,
          onDelete: (data) => tableData.length ? dispatch(deleteIpsFromFilters()) : false,
        }}
        bluredColumns={meEmail === DEMO_USER_EMAIL ? ['ip', 'rdns'] : []}
        onRowClick={(item) => history.push(`/ips/${item.id}/rbl-logs`)}
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ips-list__pagination ips-list__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(TableIpList);
