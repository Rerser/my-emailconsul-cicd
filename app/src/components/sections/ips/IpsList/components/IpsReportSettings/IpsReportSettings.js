import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import { handleIpsReportIsOnEdit, handleIpsReportData, saveIpsReportData } from 'store/actions/ipsReport';
import { ipsReportDataSelector, ipsReportIsDataLoadingSelector } from 'store/selectors';

import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Select from 'components/ui/select';
import Checkbox from 'components/ui/checkbox';
import Button from 'components/ui/button';

import './ips-report-settings.scss';

import { HOURS, TIMEZONES } from 'utils/constants';

const IpsReportSettings = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();
  const ipsReport = useSelector(ipsReportDataSelector);
  const isLoading = useSelector(ipsReportIsDataLoadingSelector);

  const { emails = [] } = ipsReport;

  const handleChangeIpsReportData = () => {
    dispatch(
      handleIpsReportData({ field: 'is_active', value: !ipsReport?.is_active })
    );
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(saveIpsReportData(ipsReport));
  };

  return (
    <form className="c-slidebar-content c-edit-ip" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('ips_report_settings.form.header')}
      </h3>

      {isLoading && <Spinner />}

      {!isLoading && ipsReport && (
        <Fragment>
          <div className="c-slidebar-content__row c-slidebar-content__row--flex">
            <label className="c-slidebar-content__label c-slidebar-content__label--checkbox">
              <span
                className="c-slidebar-content__description"
                onClick={handleChangeIpsReportData}
              >
                {t('ips_report_settings.label.sendchecbox.text')}
              </span>
              <Checkbox
                checked={ipsReport?.is_active}
                onChange={handleChangeIpsReportData}
              />
            </label>
          </div>

          <div
            className={cn('c-slidebar-content__row', {
              'c-slidebar-content__disabled': !ipsReport?.is_active,
            })}
          >
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('ips_report_settings.label.email.text')}
              </span>
              <InputText
                value={emails[0]}
                onChange={(e) =>
                  dispatch(
                    handleIpsReportData({
                      field: 'emails',
                      value: [e.target.value],
                    })
                  )
                }
              />
            </label>
          </div>

          <div
            className={cn(
              'c-slidebar-content__row c-slidebar-content__row--2col',
              {                
                'c-slidebar-content__disabled': !ipsReport?.is_active,
              }
            )}
          >
            <div>
              <Select
                value={ipsReport?.hours}
                options={Object.keys(HOURS).map((hkey) => ({
                  value: hkey,
                  label: HOURS[hkey],
                }))}
                onChange={(value) =>
                  dispatch(handleIpsReportData({ field: 'hours', value }))
                }
                placeholder={t('ips_report_settings.select.hours.text')}
                disabledValue={null}
              />
            </div>
            <div>
              <Select
                value={ipsReport?.timezone_index}
                options={TIMEZONES.map(({ zone, utc_offset, index }) => ({
                  value: index,
                  label: `${utc_offset} ${zone}`,
                }))}
                onChange={(value) =>
                  dispatch(
                    handleIpsReportData({ field: 'timezone_index', value })
                  )
                }
                placeholder={t('ips_report_settings.select.offset.text')}
                disabledValue={null}
              />
            </div>
          </div>
        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {ipsReport && (
          <Button
            mode="purple"
            text={t('ips_report_settings.btn.save.text')}
            className="c-slidebar-content__btn"
            disabled={isLoading}
            onClick={() => dispatch(saveIpsReportData(ipsReport))}
          />
        )}

        <Button
          text={t('ips_report_settings.btn.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(handleIpsReportIsOnEdit(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(IpsReportSettings);
