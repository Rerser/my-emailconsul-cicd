import React, { Fragment, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button/Button';
import SlideBar from 'components/ui/slideBar';
import Spinner from 'components/ui/spinner/Spinner';
import HangingBtn from 'components/ui/hangingBtn';

import './seedlisting-list.scss';

import {
  setSeedlistingShowedInfo,
  setNewSeedlisting,
  saveHostsSettings,
  setSeedlistingData,
} from 'store/actions/seedlisting';
import {
  newSeedlistingSelector,
  seedlistingShowedInfoSelector,
  myUserBillingPlanIsDataLoadingSelector,
  myUserBillingPlanDataSelector,
} from 'store/selectors';
import history from 'utils/history';
import { getBillingPlanIsExpired } from 'utils/helpers';
import { LANDING_FAQ_URL } from 'utils/constants';
import TableSeedlistingList from './components/tableSeedlistingList';
import SeedlistingInfo from './components/seedlistingInfo';
import NewSeedlisting from './components/newSeedlisting';

function SeedlistingList() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const newSeedlisting = useSelector(newSeedlistingSelector);
  const seedlistingShowedInfo = useSelector(seedlistingShowedInfoSelector);

  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);

  const seedlistingEmailsAvailable = myUserBillingPlanData?.seedlisting?.emails_available || 0;
  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  const showNewSeedlistingSlider = useMemo(() => !!newSeedlisting, [
    newSeedlisting,
  ]);
  const showSeedlistingInfoSlider = useMemo(() => !!seedlistingShowedInfo, [
    seedlistingShowedInfo,
  ]);

  useEffect(() => () => dispatch(setSeedlistingData({ data: [] })), [dispatch]);

  if (myUserBillingPlanIsDataLoading) return <Spinner />;

  return (
    <div className="seedlisting-list">
      <div className="seedlisting-list__header">
        <h1 className="seedlisting-list__title">
          {t('seedlisting_list.title')}
        </h1>

        <div className="seedlisting-list__header-right header-right">
          {(billingPlanIsExpired || seedlistingEmailsAvailable <= 0) && (
            <div className="header-right__upgrade-plan">
              <Button
                mode="blue"
                text={t('seedlisting_list.update_plan.btn')}
                className="seedlisting-list__btn"
                onClick={() => history.push('/user/billing')}
              />
            </div>
          )}
          <div className="header-right__new-seedlisting">
            <Button
              mode="purple"
              text={t('seedlisting_list.new.btn')}
              className="seedlisting-list__btn"
              onClick={() => dispatch(setNewSeedlisting({}))}
              disabled={billingPlanIsExpired || seedlistingEmailsAvailable <= 0}
            />
            <div className="email-available">
              {billingPlanIsExpired && (
                <div className="email-available__count danger">
                  {t('seedlisting_list.billing_plan.exipe_warning_text')}
                </div>
              )}
              {!billingPlanIsExpired && (
                <Fragment>
                  <span
                    className={cn('email-available__count', {
                      danger: seedlistingEmailsAvailable <= 0,
                      warning: seedlistingEmailsAvailable > 0 && seedlistingEmailsAvailable < 25,
                      ok: seedlistingEmailsAvailable >= 25,
                    })}
                  >
                    {seedlistingEmailsAvailable}
                  </span>
                  <span>
                    {' '}
                    {t('seedlisting_list.billing_plan.checks_available_text')}
                  </span>
                </Fragment>
              )}
            </div>
          </div>
        </div>
      </div>

      <TableSeedlistingList />

      <SlideBar
        isOpen={showNewSeedlistingSlider}
        onClose={() => {
          dispatch(saveHostsSettings()); // сохраняем хосты в localStorage при закрытии
          dispatch(setNewSeedlisting(null));
        }}
        className="seedlisting-list__new-seedlisting"
      >
        <NewSeedlisting />
      </SlideBar>

      <SlideBar
        isOpen={showSeedlistingInfoSlider}
        onClose={() => dispatch(setSeedlistingShowedInfo(null))}
        className="seedlisting-list__showed-info-slidebar"
      >
        <SeedlistingInfo />
      </SlideBar>

      <HangingBtn 
        tooltipText={t('seedlisting_list.hanging-btn.tootip.text')}
        href={`${LANDING_FAQ_URL}/index.html?faqItem=seedlistingFaq`}
      />
    </div>
  );
}

export default React.memo(SeedlistingList);
