import React, { useEffect, useMemo, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Pagination from 'components/ui/pagination';

import {
  seedlistingDataSelector,
  seedlistingDataPaginationSelector,
  seedlistingIsDataLoadingSelector,
  usersMeSelector,
  authIsSwitchedUserModeSelector,
} from 'store/selectors';
import {
  getSeedlistingData,
  setSeedlistingDataPagination,
  getSeedlistingShowedInfo,
  terminateSeedlisting,
  handleSeedlistingDataIsLoading,
} from 'store/actions/seedlisting';

import history from 'utils/history';
import {
  USER_ROLE,
  SEEDLISTING_STATUS,
  SEEDLISTING_EMAIL_STATUS,
  DEMO_USER_EMAIL,
} from 'utils/constants';
import { usePagination } from 'utils/helpers';

function TableSeedlistingList() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const seedlistingData = useSelector(seedlistingDataSelector);
  const seedlistingIsDataLoading = useSelector(
    seedlistingIsDataLoadingSelector
  );
  const seedlistingDataPagination = useSelector(
    seedlistingDataPaginationSelector
  );
  const usersMe = useSelector(usersMeSelector);
  const isSwitchedUserMode = useSelector(authIsSwitchedUserModeSelector);
  const me = useSelector(usersMeSelector);

  const { limit, offset } = seedlistingDataPagination;
  const { email: meEmail } = me;

  useEffect(() => {
    dispatch(handleSeedlistingDataIsLoading(true));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getSeedlistingData({ withLoader: false }));

    const interval = setInterval(() => {
      dispatch(getSeedlistingData({ withLoader: false }));
    }, 5000);

    return () => clearInterval(interval);
  }, [dispatch, limit, offset /* , sortField, sortOrder */]);

  const tableData = useMemo(
    () =>
      seedlistingData.map((seedlisting) => {
        const item = { ...seedlisting };
        // get statuses
        const { status_aggregation: statusAgregation = {}, status = '' } = item;
        let sum = 0;
        let waiting = 0;
        let inbox = 0;
        let spam = 0;
        let notReceived = 0;

        Object.keys(statusAgregation).forEach((key) => {
          sum += statusAgregation[key];
          key === SEEDLISTING_EMAIL_STATUS.WAITING &&
            (waiting = statusAgregation[key]);
          key === SEEDLISTING_EMAIL_STATUS.INBOX &&
            (inbox = statusAgregation[key]);
          key === SEEDLISTING_EMAIL_STATUS.SPAM &&
            (spam = statusAgregation[key]);
          key === SEEDLISTING_EMAIL_STATUS.NOT_RECEIVED &&
            (notReceived = statusAgregation[key]);
        });

        let statusesIpPercent = {};
        if (sum > 0) {
          const inboxPercent = parseInt((inbox / sum) * 100);
          const spamPercent = parseInt((spam / sum) * 100);
          const unreceivedPercent = parseInt((notReceived / sum) * 100);
          const progressPercent = 100 - parseInt((waiting / sum) * 100);

          statusesIpPercent = {
            progress:
              status !== SEEDLISTING_STATUS.TERMINATED &&
              status !== SEEDLISTING_STATUS.DONE &&
              progressPercent === 100
                ? 99
                : progressPercent,
            inbox: inboxPercent,
            spam: spamPercent,
            notReceived: unreceivedPercent,
          };
        }

        // colorize percents only if  seedlisting test is not active
        if (item.status !== SEEDLISTING_STATUS.ACTIVE) {
          for (const [key, value] of Object.entries(statusesIpPercent)) {
            let clname = 'default';
            if (key === 'inbox') {
              if (value >= 70) clname = 'ok';
              if (value >= 50 && value < 70) clname = 'warning';
              if (value < 50) clname = 'danger';
            }
            if (key === 'spam') {
              if (value <= 30) clname = 'ok';
              if (value <= 50 && value > 30) clname = 'warning';
              if (value > 50) clname = 'danger';
            }
            if (key === 'notReceived') {
              if (value < 10) clname = 'ok';
              if (value >= 10 && value < 30) clname = 'warning';
              if (value >= 30) clname = 'danger';
            }

            statusesIpPercent[key] = (
              <span className={cn(['rez-status', `rez-status--${clname}`])}>
                {value}
                %
              </span>
            );
          }
        } else {
          for (const [key, value] of Object.entries(statusesIpPercent)) {
            statusesIpPercent[key] = <span>
              {value}
              %
            </span>;
          }
        }

        // get done
        if (item.status !== SEEDLISTING_STATUS.ACTIVE) {
          item.done = item.updated_at;
        }

        const cbsPayload = {
          isInfoDisable:
            item?.status === SEEDLISTING_STATUS.DONE ||
            item?.status === SEEDLISTING_STATUS.TERMINATED,
          isTerminateDisable:
            item?.status === SEEDLISTING_STATUS.DONE ||
            item?.status === SEEDLISTING_STATUS.TERMINATED,
        };

        return {
          ...item,
          ...statusesIpPercent,
          cbsPayload,
        };
      }),
    [seedlistingData]
  );

  const onRowClick = (item) => {
    history.push(`/seedlisting/${item.id}`);
  };

  const tableCbs = {
    onInfo: (data) => dispatch(getSeedlistingShowedInfo(data)),
  };

  if (
    usersMe.role === USER_ROLE.ADMIN ||
    (usersMe.role === USER_ROLE.USER && isSwitchedUserMode)
  ) {
    tableCbs.onTerminate = (data) => dispatch(terminateSeedlisting(data));
  }

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: seedlistingDataPagination,
    action: setSeedlistingDataPagination,
  });

  if (seedlistingIsDataLoading) return <Spinner />;

  return (
    <Fragment>
      <Pagination
        page={paginal.page}
        pages={paginal.pages}
        pageSize={paginal.pageSize}
        handlePageSize={handlePageSize}
        handlePage={handlePage}
        className="ips-list__pagination ips-list__pagination--top"
      />

      <Table
        columns={[
          {
            field: 'title',
            title: t('seedlisting_list.table.title'),
            type: 'string',
            width: 200,
          },
          {
            field: 'created_at',
            title: t('seedlisting_list.table.created_at'),
            type: 'datetime',
            width: 200,
          },
          {
            field: 'done',
            title: t('seedlisting_list.table.done'),
            type: 'datetime',
            width: 200,
          },
          {
            field: 'status',
            title: t('seedlisting_list.table.status'),
            type: 'string',
            width: 200,
          },
          {
            field: 'inbox',
            title: t('seedlisting_list.table.inbox'),
            type: 'string',
            width: 100,
          },
          {
            field: 'spam',
            title: t('seedlisting_list.table.spam'),
            type: 'string',
            width: 100,
          },
          {
            field: 'notReceived',
            title: t('seedlisting_list.table.not_received'),
            type: 'string',
            width: 100,
          },
          {
            field: 'progress',
            title: t('seedlisting_list.table.progress'),
            type: 'string',
            width: 100,
          },
        ]}
        data={tableData}
        cbs={tableCbs}
        // sorting={props.filesSorting}
        // onSorting={props.setFilesSort}
        onRowClick={onRowClick}
        resizabled
        bluredColumns={meEmail === DEMO_USER_EMAIL ? ['title'] : []}
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ips-list__pagination ips-list__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(TableSeedlistingList);
