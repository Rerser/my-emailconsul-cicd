import React, { Fragment, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button/Button';
import Spinner from 'components/ui/spinner/Spinner';
import cn from 'classnames';

import './seedlisting-info.scss';

import {
  setSeedlistingShowedInfo,
  exportSeedlistingEmails,
} from 'store/actions/seedlisting';
import {
  newSeedlistingIsCreatingSelector,
  seedlistingShowedInfoSelector,
} from 'store/selectors';
import { SEEDLISTING_CHECK_TYPE } from 'utils/constants';
import { copyDataFromElemToClipboard, getHToEmail } from 'utils/helpers';
import SeedlistingEmails from './components/seedlistingEmails';

const wrapTagIntoHtml = (tag) =>
  `<div style="height:0px;opacity:0;">${tag}</div>`;

const SeedlistingInfo = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const newSeedlistingIsCreating = useSelector(
    newSeedlistingIsCreatingSelector
  );
  const seedlistingShowedInfo = useSelector(seedlistingShowedInfoSelector);

  const [isShowEmails, setIsShowEmails] = useState(false);

  const { tag = '', check_type = '', email_ref_addr = '', id = '' } =
    seedlistingShowedInfo || {};

  const tagRef = useRef(null);

  function getShowedEmails() {
    switch (check_type) {
      case SEEDLISTING_CHECK_TYPE.REF:
        return [
          {
            email: email_ref_addr,
            id: 'email_ref_addr',
          },
          ...seedlistingShowedInfo?.emails,
        ];
      case SEEDLISTING_CHECK_TYPE.H_TO:
        return [
          {
            email: getHToEmail(id),
            id: 'email_h-to_addr',
          },
          ...seedlistingShowedInfo?.emails,
        ];

      default:
        return seedlistingShowedInfo?.emails;
    }
  }

  return (
    <form className="c-slidebar-content seedlisting-info">
      <h3 className="c-slidebar-content__title">
        {t('seedlisting_info.title')}
      </h3>

      {newSeedlistingIsCreating && <Spinner />}

      {!newSeedlistingIsCreating && seedlistingShowedInfo && (
        <Fragment>
          {tagRef && check_type === SEEDLISTING_CHECK_TYPE.TAG && (
            <Fragment>
              <div className="c-slidebar-content__row">
                <label className="c-slidebar-content__label">
                  <span className="c-slidebar-content__description">
                    {t('seedlisting_info.tag.label')}
                  </span>
                  <input
                    ref={tagRef}
                    value={wrapTagIntoHtml(tag)}
                    readOnly
                    className="seedlisting-info__tag"
                  />
                </label>
              </div>

              <div className="c-slidebar-content__row">
                <Button
                  mode="purple"
                  text={t('seedlisting_info.tag.copy.btn')}
                  onClick={() => copyDataFromElemToClipboard(tagRef.current)}
                />
              </div>
            </Fragment>
          )}
          {email_ref_addr && check_type === SEEDLISTING_CHECK_TYPE.REF && (
            <div className="c-slidebar-content__row">
              <span className="seedlisting-info__description">
                {t('seedlisting_info.ref.description.1')}
                <b>
                  {email_ref_addr}
                </b>
                {t('seedlisting_info.ref.description.2')}
              </span>
            </div>
          )}
          {id && check_type === SEEDLISTING_CHECK_TYPE.H_TO && (
            <div className="c-slidebar-content__row">
              <span>
                {t('seedlisting_info.h_to.description.1')}
                <b>
                  {id}
                  {t('seedlisting_info.h_to.description.2')}
                </b>
                {t('seedlisting_info.h_to.description.3')}
              </span>
            </div>
          )}
          <div className="c-slidebar-content__div-line" />
          <div className="seedlisting-info__sub-row">
            <h4 className="c-slidebar-content__subtitle">
              {t('seedlisting_info.emails.title')}
            </h4>
            <Button
              mode="blue"
              text={t('seedlisting_info.emails.export.btn')}
              className="c-slidebar-content__btn c-slidebar-content__btn--export"
              onClick={() =>
                dispatch(exportSeedlistingEmails(seedlistingShowedInfo))
              }
            />
          </div>

          <div className="c-slidebar-content__row">
            <Button
              mode="blue"
              text={
                isShowEmails
                  ? t('seedlisting_info.emails.hide_emails.btn')
                  : t('seedlisting_info.emails.show_emails.btn')
              }
              className="c-slidebar-content__btn--fullwidth"
              onClick={() => setIsShowEmails(!isShowEmails)}
            />
          </div>

          <div
            className={cn('seedlisting-info__emailslist', {
              showed: isShowEmails,
            })}
          >
            <SeedlistingEmails emails={getShowedEmails()} />
          </div>

          <div className="c-slidebar-content__btns">
            <Button
              text={t('seedlisting_info.close.btn')}
              className="c-slidebar-content__btn"
              onClick={() => dispatch(setSeedlistingShowedInfo(null))}
            />
          </div>
        </Fragment>
      )}
    </form>
  );
};

export default React.memo(SeedlistingInfo);
