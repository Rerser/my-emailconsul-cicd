import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button/Button';

import './seedlisting-emails.scss';
import { copyDataFromElemToClipboard } from 'utils/helpers';

const SeedlistingEmails = ({ emails }) => {
  const t = useFormatMessage();

  return (
    <ul className="seedlisting-emails">
      {emails.map((emailRow) => (
        <li className="seedlisting-emails__row" key={emailRow.id}>
          <input
            id={emailRow.id}
            value={emailRow.email}
            readOnly
            className="seedlisting-emails__email"
          />
          <Button
            mode="purple"
            text={t('seedlisting_emails.email.copy.btn')}
            onClick={() => copyDataFromElemToClipboard(document.getElementById(emailRow.id))}
            className="seedlisting-emails__btn"
          />
        </li>
      ))}
    </ul>
  );
};

SeedlistingEmails.defaultProps = {
  emails: [],
};
SeedlistingEmails.propTypes = {
  emails: PropTypes.arrayOf(PropTypes.object),
};

export default React.memo(SeedlistingEmails);
