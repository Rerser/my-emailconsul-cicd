import React, { Fragment, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import InputText from 'components/ui/inputText';
import InputRange from 'components/ui/inputRange';
import Checkbox from 'components/ui/checkbox';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import './new-seedlisting.scss';

import {
  setNewSeedlisting,
  handleNewSeedlisting,
  runSeedlisting,
  saveHostsSettings,
  updateHostsSettings,
} from 'store/actions/seedlisting';
import { getUserBillingPlanGeneralByPlan } from 'store/actions/userBillingPlan';
import { getSeedlistingWorkersHosts } from 'store/actions/seedlistingWorkers';
import {
  newSeedlistingIsCreatingSelector,
  newSeedlistingSelector,
  newSeedlistingIsCheckedInfoSelector,
  myUserBillingPlanDataSelector,
  myUserBillingPlanIsDataLoadingSelector,
  userBillingPlanHostsSelector,
  userBillingPlanMaxEmailsPerIterationSelector,
  userBillingPlanGeneralByPlanIsLoadingSelector,
  seedlistingWorkersHostsSelector,
  seedlistingWorkersHostsIsLoadingSelector,
} from 'store/selectors';
import {
  HOST_LABEL,
  LOCAL_STORAGE_HOSTS_SETTINGS,
  BILLING_PLAN_SEEDLISTING_MAX_EMAILS_PER_ITERATION,
  SEEDLISTING_CHECK_TYPE,
} from 'utils/constants';
import history from 'utils/history';

import { getBillingPlanIsExpired } from 'utils/helpers';
import Select from 'components/ui/select';

const DEFAULT_CHECK_TYPE = SEEDLISTING_CHECK_TYPE.REF;

const NewSeedlisting = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const newSeedlistingIsCreating = useSelector(newSeedlistingIsCreatingSelector);
  const newSeedlisting = useSelector(newSeedlistingSelector);
  const newSeedlistingIsCheckedInfo = useSelector(newSeedlistingIsCheckedInfoSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);
  const userBillingPlanHosts = useSelector(userBillingPlanHostsSelector);
  const userBillingPlanMaxEmailsPerIteration = useSelector(userBillingPlanMaxEmailsPerIterationSelector);
  const userBillingPlanGeneralByPlanIsLoading = useSelector(userBillingPlanGeneralByPlanIsLoadingSelector);
  const seedlistingWorkersHosts = useSelector(seedlistingWorkersHostsSelector);
  const seedlistingWorkersHostsIsLoading = useSelector(seedlistingWorkersHostsIsLoadingSelector);

  const [emailsRemains, setEmailsRemains] = useState(null);

  useEffect(() => {
    dispatch(getUserBillingPlanGeneralByPlan());
    dispatch(getSeedlistingWorkersHosts());
    dispatch(handleNewSeedlisting({ field: 'check_type', value: DEFAULT_CHECK_TYPE }));
  }, [dispatch]);

  useEffect(() => {
    if (userBillingPlanMaxEmailsPerIteration && userBillingPlanHosts?.length) {
      dispatch(
        updateHostsSettings({
          avaiableNum: userBillingPlanMaxEmailsPerIteration,
          avaiableHosts: userBillingPlanHosts,
        })
      );
    }
  }, [dispatch, userBillingPlanMaxEmailsPerIteration, userBillingPlanHosts]);

  useEffect(() => {
    if (userBillingPlanHosts && seedlistingWorkersHosts && userBillingPlanMaxEmailsPerIteration) {
      // загружаем хосты из localStorage
      let hostsSettings = null;
      try {
        hostsSettings = JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_HOSTS_SETTINGS));
      } catch (error) {
        console.error(error);
      } finally {
        const initSeedlistingHosts = hostsSettings || [
          ...userBillingPlanHosts.map((h) => ({
            value: h,
            emails_per_iteration: userBillingPlanMaxEmailsPerIteration,
            selected: true,
          })),
          ...seedlistingWorkersHosts
            .filter((h) => !userBillingPlanHosts.includes(h))
            .map((h) => ({
              value: h,
              emails_per_iteration: userBillingPlanMaxEmailsPerIteration,
              selected: false,
              disabled: true,
            })),
        ];
        dispatch(
          handleNewSeedlisting({ field: 'hosts', value: initSeedlistingHosts })
        );
      }
    }
  }, [userBillingPlanHosts, seedlistingWorkersHosts, userBillingPlanMaxEmailsPerIteration, dispatch]);

  const handleHostCbx = (host) => {
    const hosts = [...newSeedlisting.hosts];
    const newHost = hosts.find((h) => h.value === host.value);
    newHost.selected = !newHost.selected;
    dispatch(handleNewSeedlisting({ field: 'hosts', value: hosts }));
  };

  const handleHostEmailPerIteration = (host, value) => {
    const hosts = [...newSeedlisting.hosts];
    const newHost = hosts.find((h) => h.value === host.value);
    if (value > userBillingPlanMaxEmailsPerIteration) {
      value = userBillingPlanMaxEmailsPerIteration;
    }
    newHost.emails_per_iteration = value;
    dispatch(handleNewSeedlisting({ field: 'hosts', value: hosts }));
  };

  const handleCreate = () => {
    dispatch(
      runSeedlisting({
        title: newSeedlisting.title,
        hosts: newSeedlisting.hosts
          .filter((h) => !h.disabled && h.selected)
          .map((h) => ({
            host_name: h.value,
            emails_per_iteration: h.emails_per_iteration,
          })),
        check_type: newSeedlisting.check_type,
      })
    );
    dispatch(saveHostsSettings());
  };

  const handleCheckAll = () => {
    const hosts = newSeedlisting?.hosts;
    if (hosts !== null && hosts !== undefined) {
      hosts.forEach((h) => {
        // если checkboxAll в состоянии partly
        if (isCheckedPartly) {
          // выделить оставшиеся невыделенные хосты
          if (!h.selected) {
            !h.disabled && handleHostCbx(h);
          }
        } else {
          // иначе поменять статусы selected всех хостов на противоположные
          !h.disabled && handleHostCbx(h);
        }
      });
    }
  };

  const emailsAvailable = myUserBillingPlanData?.seedlisting?.emails_available;
  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  const { isCheckedAll, isCheckedPartly } = newSeedlistingIsCheckedInfo;

  useEffect(() => {
    const hosts = newSeedlisting?.hosts?.filter((h) => !h.disabled && h.selected);
    if (!!emailsAvailable && !!hosts) {
      const emailsPerIteration = hosts.reduce(
        (acum, h) => acum + h.emails_per_iteration,
        0
      );
      setEmailsRemains(emailsAvailable - emailsPerIteration);
    } else {
      setEmailsRemains(t('new.seedlisting_worker.emails_remain.text'));
    }
  }, [newSeedlisting, emailsAvailable, t]);

  const handleSubmit = (e) => {
    e.preventDefault();
    handleCreate();
  };

  function getCheckTypeDescription(type) {
    switch (type) {
      case SEEDLISTING_CHECK_TYPE.TAG:
        return t('new_seedlisting.check_type.tag.description.text');
      case SEEDLISTING_CHECK_TYPE.H_TO:
        return t('new_seedlisting.check_type.h_to.description.text');
      case SEEDLISTING_CHECK_TYPE.REF:
        return t('new_seedlisting.check_type.ref.description.text');
      default:
        return '';
    }
  }

  function getCheckTypeLabel(type) {
    switch (type) {
      case SEEDLISTING_CHECK_TYPE.TAG:
        return t('new_seedlisting.check_type.tag.label');
      case SEEDLISTING_CHECK_TYPE.H_TO:
        return t('new_seedlisting.check_type.h_to.label');
      case SEEDLISTING_CHECK_TYPE.REF:
        return t('new_seedlisting.check_type.ref.label');
      default:
        return '';
    }
  }

  return (
    <form className="c-slidebar-content new-seedlisting" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('new_seedlisting.title')}
      </h3>

      {newSeedlistingIsCreating && <Spinner />}

      {!newSeedlistingIsCreating && !!newSeedlisting && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('new_seedlisting.title.label')}
              </span>
              <InputText
                value={newSeedlisting?.title}
                onChange={(e) =>
                  dispatch(
                    handleNewSeedlisting({
                      field: 'title',
                      value: e.target.value,
                    })
                  )
                }
              />
            </label>
          </div>

          {!!newSeedlisting.check_type && (
            <Fragment>
              <div className="c-slidebar-content__div-line" />
              <div className="new-seedlisting__check-type">
                <h4 className="c-slidebar-content__subtitle">
                  {t('new_seedlisting.check_type.title')}
                </h4>
                <Select
                  value={newSeedlisting.check_type}
                  options={Object.keys(SEEDLISTING_CHECK_TYPE).map((value) => ({
                    value: SEEDLISTING_CHECK_TYPE[value],
                    label: getCheckTypeLabel(value),
                  }))}
                  onChange={(value) => dispatch(handleNewSeedlisting({ field: 'check_type', value }))}
                  className="new-seedlisting__select"
                  optionPlaceholderDisabled
                  resetBtnAvailable={false}
                />
                <div className="new-seedlisting__description-type">
                  {getCheckTypeDescription(newSeedlisting.check_type)}
                </div>
              </div>
            </Fragment>
          )}

          <div className="c-slidebar-content__div-line" />
          <div className="new-seedlisting__sub-row new-seedlisting__sub-row--hosts">
            <h4 className="c-slidebar-content__subtitle">
              {t('new_seedlisting.hosts.title')}
            </h4>
            <div className="new-seedlisting__email-available email-available">
              <span className="email-available__title">
                {t('new_seedlisting.billing_plan.check_remains.title')}
              </span>
              <span
                className={cn('email-available__value', {
                  danger: emailsRemains <= 0,
                  warning: emailsRemains > 0 && emailsRemains < 25,
                  ok: emailsRemains >= 25,
                })}
              >
                {emailsRemains}
              </span>
            </div>
          </div>

          <div className="new-seedlisting__hosts">
            {userBillingPlanGeneralByPlanIsLoading &&
              seedlistingWorkersHostsIsLoading &&
              myUserBillingPlanIsDataLoading && <Spinner />}

            {!userBillingPlanGeneralByPlanIsLoading && !seedlistingWorkersHostsIsLoading && !!newSeedlisting?.hosts && (
              <Fragment>
                <ul className="s-hosts">
                  <li className="s-hosts__row">
                    <div className="s-hosts__title">
                      {t('new_seedlisting.mbps.title')}
                    </div>
                    <div className="s-hosts__eperit">
                      {t('new_seedlisting.email_per_test.title')}
                    </div>
                    <div className="s-hosts__range range-title">
                      <div className="range-title__value">
                        {1}
                      </div>
                      <div className="range-title__value">
                        {BILLING_PLAN_SEEDLISTING_MAX_EMAILS_PER_ITERATION}
                      </div>
                      {userBillingPlanMaxEmailsPerIteration < BILLING_PLAN_SEEDLISTING_MAX_EMAILS_PER_ITERATION && (
                        <div
                          className="range-title__value range-title__value--current"
                          style={{
                            left: `${
                              (100 / BILLING_PLAN_SEEDLISTING_MAX_EMAILS_PER_ITERATION) *
                              userBillingPlanMaxEmailsPerIteration
                            }%`,
                          }}
                        >
                          {userBillingPlanMaxEmailsPerIteration}
                        </div>
                      )}
                    </div>
                    <Checkbox
                      onChange={handleCheckAll}
                      checked={isCheckedAll}
                      partly={isCheckedPartly}
                      className="s-hosts__cbx"
                    />
                  </li>

                  {newSeedlisting?.hosts.map((host) => (
                    <li
                      key={host.value}
                      className={cn('s-hosts__row', {
                        disabled: host.disabled,
                      })}
                    >
                      <h5 className="s-hosts__title">
                        {HOST_LABEL[host.value] || host.value}
                      </h5>
                      <div className="s-hosts__eperit s-hosts__eperit--value">
                        {host.emails_per_iteration}
                      </div>
                      <InputRange
                        onChange={(e) => handleHostEmailPerIteration(host, +e.target.value)}
                        min={1}
                        max={BILLING_PLAN_SEEDLISTING_MAX_EMAILS_PER_ITERATION}
                        value={host.emails_per_iteration}
                        className="s-hosts__range"
                      />
                      <Checkbox
                        onChange={() => handleHostCbx(host)}
                        checked={host.selected}
                        className="s-hosts__cbx"
                        // disabled={host.disabled}
                      />
                    </li>
                  ))}
                </ul>
              </Fragment>
            )}
          </div>

          <div className="c-slidebar-content__btns">
            <Button
              text={t('new_seedlisting.close.btn')}
              className="c-slidebar-content__btn"
              onClick={() => {
                dispatch(saveHostsSettings());
                dispatch(setNewSeedlisting(null));
              }}
            />
            <Button
              text={t('new_seedlisting.create.btn')}
              className="c-slidebar-content__btn"
              mode="purple"
              type="submit"
              disabled={
                !newSeedlisting?.title || !newSeedlisting?.hosts?.length || emailsRemains < 0 || billingPlanIsExpired
              }
            />
          </div>
          {(emailsRemains < 0 || billingPlanIsExpired) && (
            <Button
              text={t('seedlisting_list.update_plan.btn')}
              mode="blue"
              onClick={() => history.push('/user/billing')}
            />
          )}
        </Fragment>
      )}
    </form>
  );
};

export default React.memo(NewSeedlisting);
