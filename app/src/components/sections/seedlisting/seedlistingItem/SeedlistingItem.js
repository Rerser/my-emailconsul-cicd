import React, { useEffect } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
// import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Button from 'components/ui/button/Button';
import SlideBar from 'components/ui/slideBar';
import SeedlistingInfo from 'components/sections/seedlisting/seedlistingList/components/seedlistingInfo';
import Error404 from 'components/ui/error404';

import './seedlisting-item.scss';

import { terminateSeedlisting, setSeedlistingShowedInfo } from 'store/actions/seedlisting';
import {
  addSeedlistingShareableLink,
  getSeedlistingItemData,
  getSeedlistingShareableLinkData,
  setSeedlistingItemData,
  setSeedlistingItemEmailInfoId,
  setSeedlistingItemShareableLinkIsOn,
} from 'store/actions/seedlistingItem';

import { setCallbackConfirmationPopup } from 'store/actions/confirmationPopup';
import {
  seedlistingItemDataSelector,
  seedlistingItemIsDataLoadingSelector,
  seedlistingShowedInfoSelector,
  seedlistingItemErrorsSelector,
  authIsSwitchedUserModeSelector,
  usersMeSelector,
  seedlistingItemShareableLinkDataSelector,
  seedlistingItemShareableLinkDataIsLoadingSelector,
  seedlistingItemIsOnShareableLinkSelector,
} from 'store/selectors';

import { SEEDLISTING_STATUS, NOT_FOUND_ERROR_STATUS, USER_ROLE } from 'utils/constants';
import SIStats from './components/SIStats';
import SeedlistingItemTable from './components/SeedlistingItemTable';
import SeedlistingShareableLink from './components/SeedlistingShareableLink';

function SeedlistingItem() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const t = useFormatMessage();

  const seedlistingItemData = useSelector(seedlistingItemDataSelector);
  const seedlistingItemIsDataLoading = useSelector(seedlistingItemIsDataLoadingSelector);
  const seedlistingShowedInfo = useSelector(seedlistingShowedInfoSelector);
  const seedlistingItemErrors = useSelector(seedlistingItemErrorsSelector);
  const isSwitchedUserMode = useSelector(authIsSwitchedUserModeSelector);
  const seedlistingItemIsOnShareableLink = useSelector(seedlistingItemIsOnShareableLinkSelector);
  const seedlistingItemShareableLinkData = useSelector(seedlistingItemShareableLinkDataSelector);
  const seedlistingItemShareableLinkDataIsLoading = useSelector(seedlistingItemShareableLinkDataIsLoadingSelector);

  const usersMe = useSelector(usersMeSelector);

  const { title, emails = [], status, id: seedlistingId } = seedlistingItemData || {};
  const { data: errorData, emailInfoData: errorEmailInfoData } = seedlistingItemErrors;

  useEffect(() => () => dispatch(setSeedlistingItemData(null)), [dispatch]);

  useEffect(() => {
    if (seedlistingId) {
      dispatch(getSeedlistingShareableLinkData(seedlistingId));
    }
  }, [dispatch, seedlistingId]);

  useEffect(() => {
    if (!status) {
      dispatch(getSeedlistingItemData({ id }));
    }

    const interval = setInterval(() => {
      if (status === SEEDLISTING_STATUS.ACTIVE) {
        dispatch(getSeedlistingItemData({ id, withLoader: false }));
      } else if (status) {
        clearInterval(interval);
      }
    }, 5000);

    return () => clearInterval(interval);
  }, [id, dispatch, status]);

  if (seedlistingItemIsDataLoading) return <Spinner />;

  if (errorData?.status === NOT_FOUND_ERROR_STATUS || errorEmailInfoData?.status === NOT_FOUND_ERROR_STATUS)
    return <Error404 />;

  return (
    <div className="seedlisting-item">
      <div className="seedlisting-item__header">
        <h1 className="seedlisting-item__title">
          <Link to="/seedlisting">
            {t('seedlisting_item.title')}
          </Link>
          <span className="seedlisting-item__breadcrumbs-current">
            {' > '}
            {title}
          </span>
        </h1>

        <div className="seedlisting-item__header-right">
          {seedlistingItemShareableLinkDataIsLoading 
            ? <Spinner /> 
            : seedlistingItemShareableLinkData ? (
              <Button
                mode="blue"
                text={t('seedlisting_item.shareable_link.btn')}
                className="seedlisting-item__btn"
                onClick={() => dispatch(setSeedlistingItemShareableLinkIsOn(true))}
              />
            ) : (
              <Button
                mode="blue"
                text={t('seedlisting_item.shareable_link.create.btn')}
                className="seedlisting-item__btn"
                onClick={() => dispatch(addSeedlistingShareableLink())}
              />
            )}

          {status !== SEEDLISTING_STATUS.TERMINATED && status !== SEEDLISTING_STATUS.DONE &&
          (usersMe.role === USER_ROLE.ADMIN || (usersMe.role === USER_ROLE.USER && isSwitchedUserMode)) ? (
            <Button
              mode="red"
              text={t('seedlisting_item.stop.btn')}
              className="seedlisting-item__btn"
              onClick={() => dispatch(setCallbackConfirmationPopup(() => dispatch(terminateSeedlisting({ id }))))}
              disabled={!true}
            />
          ) : null}

          {status !== SEEDLISTING_STATUS.TERMINATED && status !== SEEDLISTING_STATUS.DONE && (
            <Button
              mode="purple"
              text={t('seedlisting_item.show_info.btn')}
              className="seedlisting-item__btn"
              onClick={() => dispatch(setSeedlistingShowedInfo(seedlistingItemData))}
              disabled={!true}
            />
          )}
        </div>
      </div>

      {
        // <Table
        //   columns={[
        //     { field: 'email', title: t('seedlisting_item.table.email'), type: 'string', width: 200 },
        //     { field: 'sended_timestamp', title: t('seedlisting_item.table.sended_at'), type: 'datetime', width: 200 },
        //     { field: 'received_timestamp', title: t('seedlisting_item.table.received_at'), type: 'datetime', width: 200 },
        //     { field: 'from', title: t('seedlisting_item.table.from'), type: 'string', width: 200 },
        //     { field: 'subject', title: t('seedlisting_item.table.subject'), type: 'string', width: 200 },
        //     { field: 'ip', title: t('seedlisting_item.table.ip'), type: 'string', width: 200 },
        //     { field: 'status', title: t('seedlisting_item.table.status'), type: 'string', width: 200 }
        //   ]}
        //   data={emails}
        //   onRowClick={item => dispatch(setSeedlistingItemEmailInfoId(item.id))}
        // />
      }

      <SIStats data={emails} status={status} />
      <SeedlistingItemTable
        data={emails}
        onRowClick={(item) => {
          history.push(`/seedlisting/${id}/${item.id}`);
          dispatch(setSeedlistingItemEmailInfoId(item.id));
        }}
      />

      <SlideBar
        isOpen={!!seedlistingShowedInfo}
        onClose={() => dispatch(setSeedlistingShowedInfo(null))}
        className="seedlisting-list__showed-info-slidebar"
      >
        <SeedlistingInfo />
      </SlideBar>

      <SlideBar
        isOpen={seedlistingItemIsOnShareableLink}
        onClose={() => dispatch(setSeedlistingItemShareableLinkIsOn(false))}
        className="seedlisting-list__showed-info-slidebar shareable-link"
      >
        <SeedlistingShareableLink />
      </SlideBar>
    </div>
  );
}

export default SeedlistingItem;
