import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import SiTableSection from './components/SITableSection';

import './seedlisting-item-table.scss';

const SeedlistingItemTable = ({ data, onRowClick }) => {
  const t = useFormatMessage();

  const hosts = new Set();

  data.forEach((item) => hosts.add(item.host));

  const parsedData = [];

  for (const host of hosts) {
    const hostData = {
      host: host || t('seedlisting_item_table.hosts.other.text'),
      emails: data.filter((item) => item.host === host),
    };
    parsedData.push(hostData);
  }

  return (
    <div className="seedlisting-item-table">
      {parsedData.map((item) => (
        <SiTableSection key={item.host} data={item} onRowClick={onRowClick} />
      ))}
    </div>
  );
};

SeedlistingItemTable.defaultProps = {
  data: [],
};
SeedlistingItemTable.propTypes = {
  data: PropTypes.array,
  onRowClick: PropTypes.func.isRequired,
};

export default React.memo(SeedlistingItemTable);
