import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import cn from 'classnames';
import { DEMO_USER_EMAIL, HOST_LABEL, SEEDLISTING_EMAIL_STATUS } from 'utils/constants';
import { convertDateTime } from 'utils/helpers';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { usersMeSelector } from 'store/selectors';

import './si-table-section.scss';

const SiTableSection = ({ data, onRowClick }) => {
  const t = useFormatMessage();

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  const { host = t('stshstats.label.host.other.text'), emails = [] } = data;

  const [isWrapped, setIsWrapped] = useState(true);

  const emailsLength = emails.length;
  let inbox = 0;
  let spam = 0;
  let unreceived = 0;
  let waiting = 0;

  emails.forEach((item) => {
    item.status === SEEDLISTING_EMAIL_STATUS.INBOX && inbox++;
    item.status === SEEDLISTING_EMAIL_STATUS.SPAM && spam++;
    item.status === SEEDLISTING_EMAIL_STATUS.NOT_RECEIVED && unreceived++;
    item.status === SEEDLISTING_EMAIL_STATUS.WAITING && waiting++;
  });

  const inboxPercent = parseInt((100 / emailsLength) * inbox);
  const spamPercent = parseInt((100 / emailsLength) * spam);
  const unreceivedPercent = parseInt((100 / emailsLength) * unreceived);
  const progressPercent = parseInt(
    100 - parseInt((waiting / emailsLength) * 100)
  );

  const colorStatus = (status) => {
    switch (status) {
      case 'INBOX':
        return 'green';
      case 'SPAM':
        return 'red';
      case 'NOT_RECEIVED':
        return 'yellow';
      default:
        return 'default';
    }
  };

  return (
    <section className="si-table-section">
      <div className="si-table-section__header stsheader" onClick={() => setIsWrapped(!isWrapped)}>
        <h2 className="stsheader__host-name">
          {HOST_LABEL[host] || host}
        </h2>
        <ul className="stsheader__stats">
          <li className="stshstats">
            <span className="stshstats__label">
              {t('stshstats.label.inbox.text')}
              :
              {' '}
            </span>
            <span className="stshstats__value">
              {inboxPercent}
              %
            </span>
          </li>
          <li className="stshstats">
            <span className="stshstats__label">
              {t('stshstats.label.spam.text')}
              :
              {' '}
            </span>
            <span className="stshstats__value">
              {spamPercent}
              %
            </span>
          </li>
          <li className="stshstats">
            <span className="stshstats__label">
              {t('stshstats.label.unreceived.text')}
              :
              {' '}
            </span>
            <span className="stshstats__value">
              {unreceivedPercent}
              %
            </span>
          </li>
          <li className="stshstats">
            <span className="stshstats__label">
              {t('stshstats.label.progress.text')}
              :
              {' '}
            </span>
            <span className="stshstats__value">
              {progressPercent}
              %
            </span>
          </li>
        </ul>
      </div>
      {!isWrapped && (
        <div className="si-table-section__data-table sis-data-table">
          <ul className="sis-data-table__header sisdtheader">
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.email.lable')}
            </li>
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.sended_at.lable')}
            </li>
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.received_at.lable')}
            </li>
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.from.lable')}
            </li>
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.subject.lable')}
            </li>
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.ip.lable')}
            </li>
            <li className="sisdtheader__cell sis-data-table__cell">
              {t('sisdatatable.cell.status.lable')}
            </li>
          </ul>
          <div className="sis-data-table__data">
            {emails.map((item) => (
              <ul
                key={item.id}
                className={cn('sis-data-table__row email-row', {
                  disabled:
                    item.status === SEEDLISTING_EMAIL_STATUS.NOT_RECEIVED,
                  waiting: item.status === SEEDLISTING_EMAIL_STATUS.WAITING,
                })}
                onClick={() => {
                  if (
                    item.status !== SEEDLISTING_EMAIL_STATUS.NOT_RECEIVED &&
                    item.status !== SEEDLISTING_EMAIL_STATUS.WAITING
                  ) {
                    onRowClick(item);
                  }
                }}
              >
                <li className="sis-data-table__cell">
                  <span className={cn("sis-data-table__cell-content", {
                    'sis-data-table__cell-content--blured': meEmail === DEMO_USER_EMAIL
                  })}>
                    {item.email}
                  </span>
                </li>
                <li className="sis-data-table__cell">
                  <span className="sis-data-table__cell-content">
                    {convertDateTime(item.sended_timestamp, "HH:mm:ss DD-MM-YYYY")}
                  </span>
                </li>
                <li className="sis-data-table__cell">
                  <span className="sis-data-table__cell-content">
                    {convertDateTime(item.received_timestamp, "HH:mm:ss DD-MM-YYYY")}
                  </span>
                </li>
                <li className="sis-data-table__cell">
                  <span className={cn("sis-data-table__cell-content", {
                    'sis-data-table__cell-content--blured': meEmail === DEMO_USER_EMAIL
                  })}>
                    {item.from}
                  </span>
                </li>
                <li className="sis-data-table__cell">
                  <span className={cn("sis-data-table__cell-content", {
                    'sis-data-table__cell-content--blured': meEmail === DEMO_USER_EMAIL
                  })}>
                    {item.subject}
                  </span>
                </li>
                <li className="sis-data-table__cell">
                  <span className={cn("sis-data-table__cell-content", {
                    'sis-data-table__cell-content--blured': meEmail === DEMO_USER_EMAIL
                  })}>
                    {item.ip}
                  </span>
                </li>
                <li
                  className={cn('sis-data-table__cell', {
                    [`sis-data-table__cell--${colorStatus(item.status)}`]: colorStatus(item.status),
                  })}
                >
                  <span className="sis-data-table__cell-content">
                    {item.status}
                  </span>
                </li>
              </ul>
            ))}
          </div>
        </div>
      )}
    </section>
  );
};

// SiTableSection.defaultProps = {
// };
SiTableSection.propTypes = {
  data: PropTypes.object.isRequired,
  onRowClick: PropTypes.func.isRequired,
};

export default React.memo(SiTableSection);
