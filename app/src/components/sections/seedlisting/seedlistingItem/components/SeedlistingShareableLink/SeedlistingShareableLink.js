import React, { Fragment, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';
import {
  seedlistingItemShareableLinkDataIsLoadingSelector,
  seedlistingItemShareableLinkDataSelector,
} from 'store/selectors';
import { deleteSeedlistingShareableLink, setSeedlistingItemShareableLinkIsOn } from 'store/actions/seedlistingItem';
import { convertDateTime, copyDataFromElemToClipboard } from 'utils/helpers';

import './seedlisting-shareable-link.scss';

const SeedlistingShareableLink = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const tagLinkRef = useRef(null);

  useEffect(() => {
    if (tagLinkRef?.current) {
      const textarea = tagLinkRef.current;
      textarea.style.height = textarea.scrollHeight + 'px';
    }
  }, [tagLinkRef])

  const seedlistingItemShareableLinkData = useSelector(seedlistingItemShareableLinkDataSelector);
  const seedlistingItemShareableLinkDataIsLoading = useSelector(seedlistingItemShareableLinkDataIsLoadingSelector);

  const { uid = '', timestamp_to = '' } = seedlistingItemShareableLinkData || {};

  const SEEDLISTING_ITEM_SHAREABLE_LINK = process.env.REACT_APP_SEEDLISTING_ITEM_SHAREABLE_LINK;
  const shareableLink = `${SEEDLISTING_ITEM_SHAREABLE_LINK}?uid=${uid}`;

  return (
    <div className="c-slidebar-content">
      <h3 className="c-slidebar-content__title">
        {t('seedlisting_item.shareable_link.slider.title')}
      </h3>

      {seedlistingItemShareableLinkDataIsLoading 
        ? ( 
          <Spinner />
        ) : (
          <Fragment>
            <div className="c-slidebar-content__row">
              <label className="c-slidebar-content__label">
                <span className="c-slidebar-content__description">
                  {t('seedlisting_item.shareable_link.slider.link.description')}
                </span>
                <textarea ref={tagLinkRef} value={shareableLink} readOnly className="shareable-link__link" />
              </label>
            </div>
            <div className="c-slidebar-content__row shareable-link__text-row">
              <span className="shareable-link__expired">
                {t('seedlisting_item.shareable_link.slider.expired_at.text')}
                {': '}
                <span className="shareable-link__expired-date">
                  {convertDateTime(timestamp_to, "HH:mm:ss DD-MM-YYYY")}
                </span>
              </span>
            </div>
          </Fragment>
      )}
      <div className="c-slidebar-content__btns c-slidebar-content__btns--column c-slidebar-content__btns--nomargin_top">
        {!seedlistingItemShareableLinkDataIsLoading && (
          <div className="c-slidebar-content__btns c-slidebar-content__btns--nomargin_top">
            <Button
              mode="purple"
              text={t('seedlisting_item.shareable_link.slider.copy.btn')}
              className="c-slidebar-content__btn"
              onClick={() => copyDataFromElemToClipboard(tagLinkRef.current)}
            />
            <Button
              mode="red"
              text={t('seedlisting_item.shareable_link.slider.delete.btn')}
              className="c-slidebar-content__btn"
              onClick={() => dispatch(deleteSeedlistingShareableLink())}
            />
          </div>
        )}

        <Button
          text={t('seedlisting_item.shareable_link.slider.close.btn')}
          className="c-slidebar-content__btn c-slidebar-content__btn--fullwidth nomargin"
          onClick={() => dispatch(setSeedlistingItemShareableLinkIsOn(false))}
        />
      </div>
    </div>
  );
};

export default React.memo(SeedlistingShareableLink);
