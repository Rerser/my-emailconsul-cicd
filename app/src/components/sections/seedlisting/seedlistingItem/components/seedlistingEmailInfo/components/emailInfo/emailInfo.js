import React, { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import { Icon } from 'react-icons-kit';
import { informationCircled } from 'react-icons-kit/ionicons/informationCircled';

import Spinner from 'components/ui/spinner/Spinner';
import StatusIcon from 'components/ui/statusIcon/StatusIcon';
import Button from 'components/ui/button/Button';
import { getEmailAddressFromString } from 'utils/helpers';
import getEmailInfoEpcIpTableData from './components/emailInfoEpcIpTableData';
import getEmailInfoEpcDomainTableData from './components/emailInfoEpcDomainTableData';
import getEmailInfoSpamAssasinData from './components/emailInfoSpamAssasinData';
import EmailView from './components/emailView';
import EmailHeaders from './components/emailHeaders';
import EmailHtml from './components/emailHtml';
import EmailIp from './components/emailIp';
import EmailDomain from './components/emailDomain';
import EmailSpamAssasin from './components/emailSpamAssasin';
import EmailAuthentication from './components/emailAuthentication';

import './email-info.scss';

import {
  getSeedlistingItemEmailInfoEpcIpData,
  getSeedlistingItemEmailInfoEpcDomainData,
  getSeedlistingItemEmailInfoSpamAssasinData,
} from 'store/actions/seedlistingItem';
import {
  localeCurrentSelector,
  seedlistingItemEmailInfoSelector,
  seedlistingItemIsLoadingEmailInfoSelector,
  seedlistingItemIsLoadingEpcIpDataEmailInfoSelector,
  seedlistingItemEmailInfoEpcIpDataSelector,
  seedlistingItemIsLoadingEmailInfoEpcDomainDataSelector,
  seedlistingItemEmailInfoEpcDomainDataSelector,
  seedlistingItemIsLoadingEmailInfoSpamAssasinDataSelector,
  seedlistingItemEmailInfoSpamAssasinDataSelector,
} from 'store/selectors';

const EmailInfo = () => {
  const { id: seedlistingId, emailId: seedlistingEmailId } = useParams();
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const localeCurrent = useSelector(localeCurrentSelector);
  const seedlistingItemEmailInfo = useSelector(seedlistingItemEmailInfoSelector);
  const seedlistingItemIsLoadingEmailInfo = useSelector(seedlistingItemIsLoadingEmailInfoSelector);
  const seedlistingItemEmailInfoEpcIpData = useSelector(seedlistingItemEmailInfoEpcIpDataSelector);
  const seedlistingItemIsLoadingEmailInfoEpcIpData = useSelector(seedlistingItemIsLoadingEpcIpDataEmailInfoSelector);
  const seedlistingItemEmailInfoEpcDomainData = useSelector(seedlistingItemEmailInfoEpcDomainDataSelector);
  const seedlistingItemIsLoadingEmailInfoEpcDomainData = useSelector(seedlistingItemIsLoadingEmailInfoEpcDomainDataSelector);
  const seedlistingItemEmailInfoSpamAssasinData = useSelector(seedlistingItemEmailInfoSpamAssasinDataSelector);
  const seedlistingItemIsLoadingEmailInfoSpamAssasinData = useSelector(seedlistingItemIsLoadingEmailInfoSpamAssasinDataSelector);

  const { email_body = '', headers = {}, ip = '', from: fromHeader = '' } =
    seedlistingItemEmailInfo || {};
  const { dns_lookup_result = [], rbl_result: ipRblResult = [] } =
    seedlistingItemEmailInfoEpcIpData || {};

  const fromEmail = getEmailAddressFromString(fromHeader).split('@');
  const [, domainName] = fromEmail;

  const emailInfoEpcIpTableData = !seedlistingItemIsLoadingEmailInfoEpcIpData
    ? getEmailInfoEpcIpTableData(ip, dns_lookup_result, ipRblResult)
    : {};
  const emailInfoEpcDomainTableData = !seedlistingItemIsLoadingEmailInfoEpcDomainData
    ? getEmailInfoEpcDomainTableData(seedlistingItemEmailInfoEpcDomainData)
    : [];
  const emailInfoSpamAssasinTableData = !seedlistingItemIsLoadingEmailInfoSpamAssasinData
    ? getEmailInfoSpamAssasinData(
        seedlistingItemEmailInfoSpamAssasinData,
        localeCurrent
      )
    : {};

  const [isShowEmailPreview, setIsShowEmailPreview] = useState(false);
  const [isShowHeaders, setIsShowHeaders] = useState(false);
  const [isShowEmailHtml, setIsShowEmailHtml] = useState(false);
  const [isShowIpInfo, setIsShowIpInfo] = useState(false);
  const [isShowDomainInfo, setIsShowDomainInfo] = useState(false);
  const [isShowSpamAssasinInfo, setIsShowSpamAssasinInfo] = useState(false);
  const [isShowAuthenticationInfo, setIsShowAuthenticationInfo] = useState(
    false
  );

  useEffect(() => {
    if (isShowIpInfo && ip && !seedlistingItemEmailInfoEpcIpData) {
      dispatch(getSeedlistingItemEmailInfoEpcIpData({ ip }));
    }
  }, [dispatch, isShowIpInfo, ip, seedlistingItemEmailInfoEpcIpData]);

  useEffect(() => {
    if (
      isShowDomainInfo &&
      domainName &&
      !seedlistingItemEmailInfoEpcDomainData
    ) {
      dispatch(
        getSeedlistingItemEmailInfoEpcDomainData({ domain: domainName })
      );
    }
  }, [
    dispatch,
    isShowDomainInfo,
    seedlistingItemEmailInfoEpcDomainData,
    domainName,
  ]);

  useEffect(() => {
    if (
      isShowSpamAssasinInfo &&
      seedlistingId &&
      seedlistingEmailId &&
      !seedlistingItemEmailInfoSpamAssasinData
    ) {
      dispatch(
        getSeedlistingItemEmailInfoSpamAssasinData({
          seedlistingId,
          seedlistingEmailId,
        })
      );
    }
  }, [
    dispatch,
    isShowSpamAssasinInfo,
    seedlistingItemEmailInfoSpamAssasinData,
    seedlistingId,
    seedlistingEmailId,
  ]);

  return (
    <div className="email-info">
      <div className="email-info__header">
        <h3 className="email-info__title">
          {t('seedlisting_email_info.title')}
        </h3>

        <div className="show-email-preview-btn">
          <Icon icon={informationCircled} data-tooltip={t('seedlisting_email_info.open_preview.tooltip.text')} />
          <Button
            mode="blue"
            text={t('seedlisting_email_info.open_preview.btn')}
            className="show-email-preview-btn__btn"
            onClick={() => setIsShowEmailPreview(!isShowEmailPreview)}
          />
        </div>
      </div>

      {seedlistingItemIsLoadingEmailInfo && <Spinner />}

      {seedlistingItemEmailInfo && (
        <Fragment>
          <div className="email-info__cols">
            <div
              className={cn('email-info__left', {
                withpl: isShowEmailPreview,
              })}
            >
              <div className="sei-accordion">
                <div className="sei-accordion__item">
                  <div
                    className="sei-accordion__item-header"
                    onClick={() => setIsShowHeaders(!isShowHeaders)}
                  >
                    {t('seedlisting_email_info.headers.title')}
                  </div>
                  {isShowHeaders && (
                    <div className="sei-accordion__item-content">
                      <EmailHeaders headers={headers} />
                    </div>
                  )}
                </div>
                <div className="sei-accordion__item">
                  <div
                    className="sei-accordion__item-header"
                    onClick={() => setIsShowEmailHtml(!isShowEmailHtml)}
                  >
                    HTML
                  </div>
                  {isShowEmailHtml && (
                    <div className="sei-accordion__item-content">
                      <EmailHtml html={email_body} />
                    </div>
                  )}
                </div>
                <div className="sei-accordion__item">
                  <div
                    className="sei-accordion__item-header"
                    onClick={() => setIsShowIpInfo(!isShowIpInfo)}
                  >
                    {t('seedlisting_email_info.email_ips.title')}
                  </div>
                  {isShowIpInfo && (
                    <div className="sei-accordion__item-content">
                      <EmailIp
                        tableData={emailInfoEpcIpTableData}
                        isLoading={seedlistingItemIsLoadingEmailInfoEpcIpData}
                      />
                    </div>
                  )}
                </div>
                <div className="sei-accordion__item">
                  <div
                    className="sei-accordion__item-header"
                    onClick={() => setIsShowDomainInfo(!isShowDomainInfo)}
                  >
                    <span>
                      {t('seedlisting_email_info.email_domain.title')}
                    </span>
                    <span className="sei-accordion__item-domain">
                      {' ('}
                      {domainName}
                      {')'}
                    </span>
                  </div>
                  {isShowDomainInfo && (
                    <div className="sei-accordion__item-content">
                      <EmailDomain
                        tableData={emailInfoEpcDomainTableData}
                        isLoading={
                          seedlistingItemIsLoadingEmailInfoEpcDomainData
                        }
                      />
                    </div>
                  )}
                </div>
                <div className="sei-accordion__item">
                  <div
                    className="sei-accordion__item-header withicon"
                    onClick={() =>
                      setIsShowSpamAssasinInfo(!isShowSpamAssasinInfo)
                    }
                  >
                    <span className="sei-accordion__item-text">
                      {t('seedlisting_email_info.email_spamassasin.title')}
                    </span>
                    <span className="sei-accordion__item-status">
                      {' '}
                      {!seedlistingItemIsLoadingEmailInfoSpamAssasinData &&
                        seedlistingItemEmailInfoSpamAssasinData && (
                          <StatusIcon
                            status={
                              !seedlistingItemEmailInfoSpamAssasinData?.spam
                            }
                            sizePx={20}
                          />
                        )}
                    </span>
                  </div>
                  {isShowSpamAssasinInfo && (
                    <div className="sei-accordion__item-content">
                      <EmailSpamAssasin
                        tableData={emailInfoSpamAssasinTableData}
                        isLoading={
                          seedlistingItemIsLoadingEmailInfoSpamAssasinData
                        }
                      />
                    </div>
                  )}
                </div>
                <div className="sei-accordion__item">
                  <div
                    className="sei-accordion__item-header"
                    onClick={() =>
                      setIsShowAuthenticationInfo(!isShowAuthenticationInfo)
                    }
                  >
                    {t('seedlisting_email_info.email_authentication.title')}
                  </div>
                  {isShowAuthenticationInfo && (
                    <div className="sei-accordion__item-content">
                      <EmailAuthentication headers={headers} />
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div
              className={cn('email-info__email-body', {
                wrapped: !isShowEmailPreview,
              })}
            >
              {isShowEmailPreview && (
                <Fragment>
                  {/* <div className="email-info__subtitle">{t('seedlisting_email_info.email_preview.title')}:</div> */}
                  <EmailView
                    contentType={
                      (headers['content-type'] && headers['content-type'][0]) ||
                      null
                    }
                    content={email_body}
                  />
                </Fragment>
              )}
            </div>
          </div>
        </Fragment>
      )}
    </div>
  );
};

export default EmailInfo;
