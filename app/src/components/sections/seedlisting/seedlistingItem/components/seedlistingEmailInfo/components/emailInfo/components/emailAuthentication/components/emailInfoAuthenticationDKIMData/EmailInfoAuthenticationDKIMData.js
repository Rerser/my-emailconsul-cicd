import React, { Fragment }  from 'react';
import InfoContainer from 'components/ui/infoContainer';
import { DKIM_SIGNATURE_HEADER, INFO_CONTAINER_STATUS } from 'utils/constants';

// -------------------------------------------------------------------------
// функция, фозвращающая домен и селектор одного из перечисленных заголовков
// -------------------------------------------------------------------------
export function getDomainSelectorFromHeadersForDKIM(headers) {
  if (headers[DKIM_SIGNATURE_HEADER]) {
    if (!(headers[DKIM_SIGNATURE_HEADER] instanceof Array)) {
      return {
        valuesOfDomainSelector: [],
        signaturesDKIM: [],
      };
    }

    const signaturesDKIM = headers[DKIM_SIGNATURE_HEADER] || []; // значения из хедера 'dkim-signature'

    // domain (d) и selector (s) из хедера 'dkim-signature'
    const valuesOfDomainSelector = signaturesDKIM.map((value) =>
      value
        .split(';')
        .map((item) => item.trim())
        .filter((item) => item.startsWith('d=') || item.startsWith('s='))
        .reduce((pred, curr) => {
          const [key, value] = curr.split('=');
          return {
            ...pred,
            [key]: value,
          };
        }, {})
    );

    return {
      valuesOfDomainSelector,
      signaturesDKIM,
    };
  }

  return {
    valuesOfDomainSelector: [],
    signaturesDKIM: [],
  };
}

// ----------------------------------------------------------------------------------------------------
// функция, возвращающая лейаут dkim-сигнатуры, public-key и warninga из заголовков и полученных данных
// ----------------------------------------------------------------------------------------------------
export function getEmailInfoDKIMLayoutsData({ headers, textualRepresentationData, signatureHeaderData }) {
  const textualRepresentationInfo = [];
  const signatureHeaderInfo = [];

  // если есть хедер 'dkim-signature' и данные от сервера сформируем лейаут
  if (headers[DKIM_SIGNATURE_HEADER]) {
    // dkim-signature
    if (signatureHeaderData?.length) {
      const signaturesDKIM = headers[DKIM_SIGNATURE_HEADER]; // значения из хедера 'dkim-signature'
      const valuesOfDKIMSignature = signaturesDKIM.map((signature) => signature.split(';').map((item) => item.trim()));

      const signatureLayouts = valuesOfDKIMSignature.map((values, index) => (
        <div key={`valuesOfDKIMSignature-${index}`}>
          {values.map((item, index) => (
            <div className="dkim__value" key={`valuesOfDKIMSignature-signature-${item}-${index}`}>
              {index !== values.length - 1 ? `${item};` : item}
            </div>
          ))}
        </div>
      ));

      const signatureNotes = signatureHeaderData.map((signature) => {
        const { warnings: warningsSignatureHeader = [], errors: errorsSignatureHeader = [] } = signature;
        return {
          warningsLayout: warningsSignatureHeader.map((warning, i) => (
            <div className="dkim__warning" key={`dkim-textual-representation-${warning}-${i}`}>
              <InfoContainer text={warning} status={INFO_CONTAINER_STATUS.info} />
            </div>
          )),
          errorsLayout: errorsSignatureHeader.map((error, i) => (
            <div className="dkim__error" key={`dkim-textual-representation-${error}-${i}`}>
              <InfoContainer text={error} status={INFO_CONTAINER_STATUS.error} />
            </div>
          )),
        };
      });

      for (let i = 0; i < signatureLayouts.length; i++) {
        signatureHeaderInfo.push({
          resultLayout: signatureLayouts[i],
          warningsLayout: signatureNotes[i] ? signatureNotes[i].warningsLayout : [],
          errorsLayout: signatureNotes[i] ? signatureNotes[i].errorsLayout : [],
        });
      }
    }

    // public-key
    if (textualRepresentationData?.length) {
      textualRepresentationData.forEach((data, index) => {
        const { result = null, errors = [], warnings = [] } = data;

        let resultLayout = [];
        let warningsLayout = [];
        let errorsLayout = [];

        if (result) {
          const resultData = Array.isArray(result)
            ? result.map((data) => data.split(';').map((item) => item.trim()))
            : [result.split(';').map((item) => item.trim())];

          if (resultData?.length) {
            // если result - массив или строка
            resultLayout = resultData.map((itemArr, indexArr) => (
              <Fragment key={`result-arr-tr-${indexArr}`}>
                {itemArr.map((item, i) => (
                  <div className="dkim__value" key={`result-tr-${item}-${i}-${indexArr}`}>
                    {i !== itemArr.length - 1 ? `${item};` : item}
                  </div>
                ))}
              </Fragment>
            ));
          }
        }

        warningsLayout = warnings.map((warning, i) => (
          <div className="dkim__warning" key={`dkim-textual-representation-warning-${i}`}>
            <InfoContainer text={warning} status={INFO_CONTAINER_STATUS.info} />
          </div>
        ));

        errorsLayout = errors.map((error, i) => (
          <div className="dkim__error" key={`dkim-textual-representation-error-${i}`}>
            <InfoContainer text={error} status={INFO_CONTAINER_STATUS.error} />
          </div>
        ));

        textualRepresentationInfo.push({
          resultLayout,
          warningsLayout,
          errorsLayout,
        });
      });
    }
  }

  return {
    textualRepresentationInfo,
    signatureHeaderInfo,
  };
}
