import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Error404 from 'components/ui/error404';

import './email-domain.scss';

import { seedlistingItemErrorsSelector } from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

const EmailDomain = ({ tableData, isLoading }) => {
  const t = useFormatMessage();

  const { epcDomainData: errorEpcDomainData } = useSelector(
    seedlistingItemErrorsSelector
  );

  const spinner = isLoading ? <Spinner /> : null;
  const table = !isLoading ? (
    <Table
      columns={[
        {
          field: 'domain',
          title: t('email_domain.table.domain.title'),
          type: 'string',
          width: 200,
        },
        {
          field: 'dns_lookup_result',
          title: t('email_domain.table.reverse_ip.title'),
          width: 400,
        },
        {
          field: 'rbl_result',
          title: t('email_domain.table.blacklist.title'),
          width: 400,
        },
      ]}
      data={tableData}
    />
  ) : null;

  return (
    <div className="email-domain">
      {spinner}
      {errorEpcDomainData?.status !== NOT_FOUND_ERROR_STATUS ? (
        table
      ) : (
        <Error404 compact />
      )}
    </div>
  );
};

EmailDomain.defaultProps = {
  tableData: [],
  isLoading: false,
};
EmailDomain.propTypes = {
  tableData: PropTypes.array,
  isLoading: PropTypes.bool,
};

export default React.memo(EmailDomain);
