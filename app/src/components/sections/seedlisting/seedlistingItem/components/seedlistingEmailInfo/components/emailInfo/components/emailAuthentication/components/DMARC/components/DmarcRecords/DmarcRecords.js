import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import InfoContainer from 'components/ui/infoContainer';
import Table from 'components/ui/table';
import { INFO_CONTAINER_STATUS } from 'utils/constants';

const DmarcRecords = ({ data }) => {
  const t = useFormatMessage();

  if (!data) {
    return null;
  }

  const { result = null, warning = null } = data;

  // функция отрисовки dmarc-записи
  function parseDmarcResultToLayout(str, i) {
    const splittedData = str.split(';').filter(s => s);
    const strLayout = splittedData.map((item, index) => (
      <div className="dmarc__value" key={`dmarc-str-${item}`}>
        {index !== splittedData.length - 1 ? `${item};` : item}
      </div>
    ));

    // колонка тегов
    const tagColumn = splittedData.map((item, index) => {
      const [tag] = item.trim().split('=');
      return (
        <div className="dmarc__tag" key={`dmarc-tag-${index}`}>
          {tag}
        </div>
      );
    });
    // колонка значений
    const valueColumn = splittedData.map((item, index) => {
      const [, value] = item.trim().split('=');
      
      return (
        <div className="dmarc__value" key={`dmarc-value-${index}`}>
          {value}
        </div>
      );
    });
    // колонка translation
    const translationColumn = splittedData.map((item, index) => {
      const [tag] = item.trim().split('=');
      const translation = t(`dmarc.tag.translation.${tag}`);
      return (
        <div className="dmarc__translation" key={`dmarc-translation-${index}`}>
          {translation}
        </div>
      );
    });

    // данные таблицы
    const tableArrs = splittedData.map((_, index) => ({
      tag: tagColumn[index],
      value: valueColumn[index],
      translation: translationColumn[index],
    }));

    return (
      <div className="dmarc__data" key={`dmarc-record-${i}`}>
        <div className="dmarc__result">
          {strLayout}
        </div>
        <div className="dmarc__table">
          <div className="dmarc__description">
            {t('email_authentication.dmarc.description.text')}
          </div>
          <Table
            columns={[
              {
                field: 'tag',
                title: t('email_authentication.dmarc.table.tag.label'),
                type: 'string',
                width: 100,
              },
              {
                field: 'value',
                title: t('email_authentication.dmarc.table.value.label'),
                type: 'string',
                width: 250,
              },
              {
                field: 'translation',
                title: t('email_authentication.dmarc.table.translation.label'),
                type: 'string',
                width: 450,
              },
            ]}
            data={tableArrs}
          />
        </div>
      </div>
    );
  }

  let resultLayout = null;

  if (result?.length) {
    // если result - массив или строка
    resultLayout = Array.isArray(result) ? (
      result.map((item, index) => parseDmarcResultToLayout(item, index))
    ) : (
      <div className="dmarc__result">
        {parseDmarcResultToLayout(result)}
      </div>
    );
  }

  // сформируем warning
  const warningLayout = warning ? (
    <div className="dkim__warning">
      <InfoContainer text={warning} status={INFO_CONTAINER_STATUS.warning} />
    </div>
  ) : null;

  return (
    <div className="dmarc__wrapper">
      {resultLayout && (
        <div className="dmarc__record">
          <div className="dmarc__description">
            {t('email_authentication.dmarc.record.text')}
          </div>
          {resultLayout}
        </div>
      )}
      {warningLayout && <div className="dmarc__warning">
        {warningLayout}
      </div>}
    </div>
  );
};

DmarcRecords.defaultProps = {
  data: {},
};
DmarcRecords.propTypes = {
  data: PropTypes.object,
};

export default React.memo(DmarcRecords);
