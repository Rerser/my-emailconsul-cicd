import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner/Spinner';
import Error404 from 'components/ui/error404';

import './dkim.scss';

import { seedlistingItemErrorsSelector } from 'store/selectors';
import { INFO_CONTAINER_STATUS, NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import InfoContainer from 'components/ui/infoContainer';

const DKIM = ({ data, isLoading }) => {
  const t = useFormatMessage();

  const { DKIMData: errorDKIMData } = useSelector(
    seedlistingItemErrorsSelector
  );

  const parseDkimData = (data) => {
    if (!data) {
      return null;
    }

    const { signatureHeaderInfo, textualRepresentationInfo } = data;

    return (
      data && (
        <div className="dkim__wrapper">
          <div className="dkim__data dkim__data--signature">
            <div className="dkim__description">
              {t('email_authentication.dkim.signature.description.text')}
            </div>
            {signatureHeaderInfo.length 
              ? signatureHeaderInfo.map((signature, index) => (
                <Fragment key={`dkim-signature-${index}`}>
                  {
                    index >= 1 && (<div className="dkim__separate"/>)
                  }
                  <span className={cn("dkim__status", {
                    "dkim__status--valid": !signature.errorsLayout.length,
                    "dkim__status--no-valid": signature.errorsLayout.length,
                  })}>
                    {' '}
                    {!signature.errorsLayout.length
                      ? t('email_authentication.dkim.is-valid.text')
                      : t('email_authentication.dkim.no-valid.text')
                    }
                  </span>
                  {signature.resultLayout && (
                    <div className="dkim__result">
                      {signature.resultLayout}
                    </div>
                  )}
                  {
                    signature.warningsLayout.map((warning, i) => (
                      <div className="dkim__warning-container" key={`warning-signature-tr-${i}`}>
                        {warning}
                      </div>
                    ))
                  }
                  {
                    signature.errorsLayout.map((error, i) => (
                      <div className="dkim__error-container" key={`error-signature-tr-${i}`}>
                        {error}
                      </div>
                    ))
                  }
                </Fragment>
              )): (
                <InfoContainer
                  text={t('email_authentication.no-domain-selector.dkim.text')}
                  key="DKIM warning"
                  status={INFO_CONTAINER_STATUS.warning}
                />
            )}
          </div>
          <div className="dkim__data dkim__data--public-key">
            <div className="dkim__description">
              {t('email_authentication.dkim.openkey.description.text')}
            </div>
            {textualRepresentationInfo.length
              ? textualRepresentationInfo.map((pKey, index) => (
                <Fragment key={`dkim-public-key-${index}`}>
                  {
                    index >= 1 && (<div className="dkim__separate"/>)
                  }
                  <span className={cn("dkim__status", {
                    "dkim__status--valid": !pKey.errorsLayout.length,
                    "dkim__status--no-valid": pKey.errorsLayout.length,
                  })}>
                    {' '}
                    {!pKey.errorsLayout.length
                      ? t('email_authentication.dkim.is-valid.text')
                      : t('email_authentication.dkim.no-valid.text')
                    }
                  </span>

                  {pKey.resultLayout.map((result, i) => (
                    <div className="dkim__result" key={`result-pkey-tr-${i}`}>
                      {pKey.resultLayout}
                    </div>
                  ))}

                  {
                    pKey.warningsLayout.map((warning, i) => (
                      <div className="dkim__warning-container" key={`warning-pkey-tr-${i}`}>
                        {warning}
                      </div>
                    ))
                  }
                  {
                    pKey.errorsLayout.map((error, i) => (
                      <div className="dkim__error-container" key={`error-pkey-tr-${i}`}>
                        {error}
                      </div>
                    ))
                  }
                </Fragment>
                )): (
                  <InfoContainer
                    text={t('email_authentication.no-domain-selector.dkim.text')}
                    key="DKIM warning"
                    status={INFO_CONTAINER_STATUS.warning}
                  />
              )}
          </div>
        </div>
      )
    );
  };

  const spinner = isLoading ? <Spinner /> : null;
  const dkimData = !isLoading ? parseDkimData(data) : null;

  return (
    <div className="dkim">
      {spinner}
      {errorDKIMData?.status !== NOT_FOUND_ERROR_STATUS ? (
        dkimData
      ) : (
        <Error404 compact />
      )}
    </div>
  );
};

DKIM.defaultProps = {
  data: {},
  isLoading: false,
};
DKIM.propTypes = {
  data: PropTypes.object,
  isLoading: PropTypes.bool,
};

export default React.memo(DKIM);
