import {
  RETURN_PATH_HEADER,
  MAIL_FROM_HEADER,
  ENVELOPE_FROM_HEADER,
  MFROM_HEADER,
  HEADER_FROM_HEADER,
} from 'utils/constants';
import { getEmailAddressFromString } from 'utils/helpers';

// --------------------------------------------------------------
// функция, возвращающая домен одного из перечисленных заголовков
// --------------------------------------------------------------
export default function (headers) {
  const headerRequired = [
    RETURN_PATH_HEADER,
    MAIL_FROM_HEADER,
    ENVELOPE_FROM_HEADER,
    MFROM_HEADER,
    HEADER_FROM_HEADER,
  ].filter((item) => headers[item]);

  if (headerRequired.length) {
    for (const hdr of headerRequired) {
      const value = headers[hdr][0];
      const fromEmail = getEmailAddressFromString(value).split('@');
      const [, domainName] = fromEmail;

      if (domainName) {
        return domainName;
      }
    }
  }

  return null;
}
