import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner/Spinner';
import Error404 from 'components/ui/error404';

import './spf.scss';

import { seedlistingItemErrorsSelector } from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import CollapseExpandAccordion from './components/collapseExpandAccordion';

const STEP_TAB = 30; // шаг отступа в px аккордиона от левой границы

const SPF = ({ data, isLoading }) => {
  const t = useFormatMessage();

  const { SPFData: errorSPFData } = useSelector(seedlistingItemErrorsSelector);

  const [isExpandedList, setIsExpandedList] = useState({ status: true });

  let count = 0; // количество входов в ф-ию parsedSpfData (для отступов слева)

  // ------------------------------------
  // функция обработки данных объекта SPF
  // ------------------------------------
  const parseSpfData = (spfInfo) => {
    const keys = Object.keys(spfInfo);
    let accordionData = {}; // данные для аккордиона

    let tabSize = 0; // отступ от левой ближайшей границы в px
    count++;

    // пробежимся по всем ключам spfInfo
    if (keys && keys.length) {
      tabSize = count > 1 ? tabSize + STEP_TAB : 0;

      return keys.map((key) => {
        const {
          result = '',
          isDnsQueryToResolve = false,
          domain = '',
          record = [],
          mechanisms = [],
          warnings = [], // содержится только в корневом элементе домена либо в data механизма 'include', либо в модификаторе 'redirect'
          warning = null, // содержится в остальных механизмах поля records
          mechanism = '',
          modifier = '',
          data = null,
        } = spfInfo;

        let { dnsQueriesToResolve = 0 } = spfInfo;

        isDnsQueryToResolve && dnsQueriesToResolve++;

        // в зависимости от имени ключа сформируем аккордионн и, при необходимости, его дочерние элементы
        switch (key.toLowerCase()) {
          // RESULT key
          case 'result':
            accordionData = {
              result,
              header: domain,
              content: record,
              queries: dnsQueriesToResolve,
              warnings,
              mechanism,
            };
            return (
              <CollapseExpandAccordion
                key={Math.random()}
                data={accordionData}
                tabSize={tabSize}
                isExpanded={isExpandedList.status}
                isFirstHeader={count === 1} // самый верхний заголовок
                children={!!mechanisms?.length && mechanisms.map((record) => parseSpfData(record))}
              />
            );

          // MODIFIER key
          case 'modifier':
            switch (modifier.toLowerCase()) {
              case 'redirect':
                const { dnsQueriesToResolve: queriesFromData = 0 } = data;
                if (data && Object.keys(data).length && domain) {
                  return parseSpfData({
                    ...data,
                    dnsQueriesToResolve: isDnsQueryToResolve ? queriesFromData + 1 : queriesFromData,
                    domain: `+redirect:${domain}`,
                  });
                }
                break;

              case 'exp':
                accordionData = { content: data };
                return (
                  <CollapseExpandAccordion
                    key={`${Math.random()}ip`}
                    data={accordionData}
                    tabSize={tabSize}
                    isExpanded={isExpandedList.status}
                    noheader
                  />
                );

              default:
                break;
            }
            break;

          // MECHANISM key
          case 'mechanism':
            switch (mechanism.toLowerCase()) {
              case 'ip':
              case 'all':
              case 'exists':
                accordionData = { content: data };
                return (
                  <CollapseExpandAccordion
                    key={`${Math.random()}ip`}
                    data={accordionData}
                    tabSize={tabSize}
                    isExpanded={isExpandedList.status}
                    noheader
                    ipData
                  />
                );

              case 'a':
              case 'mx':
              case 'ptr':
                accordionData = {
                  header: domain,
                  content: data,
                  queries: dnsQueriesToResolve,
                  warning,
                  mechanism,
                };
                return (
                  <CollapseExpandAccordion
                    key={`${Math.random()}a`}
                    data={accordionData}
                    tabSize={tabSize}
                    isExpanded={isExpandedList.status}
                    noheader
                  />
                );

              case 'include':
                const { dnsQueriesToResolve: queriesFromData = 0 } = data;
                if (data && !!Object.keys(data).length && domain) {
                  return parseSpfData({
                    ...data,
                    dnsQueriesToResolve: isDnsQueryToResolve ? queriesFromData + 1 : queriesFromData,
                    domain: `+include:${domain}`,
                  });
                }
                break;

              default:
                break;
            }
            break;

          default:
            break;
        }
        return null;
      });
    }
    return null;
  };

  const handleCollapseAccordion = (status) => {
    setIsExpandedList({ status });
  };

  const dataLayout = (
    <Fragment>
      <div className="spf__header">
        <h4 className="spf__title">
          {t('email_authentication.accordion.spf.expand.text')}
        </h4>
        <div className="spf__buttons">
          <Button
            text={t('email_authentication.accordion.spf.collapse.btn.text')}
            onClick={() => handleCollapseAccordion(true)}
          />
          <Button
            text={t('email_authentication.accordion.spf.expand.btn.text')}
            onClick={() => handleCollapseAccordion(false)}
          />
        </div>
      </div>
      {data && <div className="spf__info">
        {parseSpfData(data)}
      </div>}
    </Fragment>
  );
  const spinner = isLoading ? <Spinner /> : null;
  const spfData = !isLoading ? dataLayout : null;

  return (
    <div className="spf">
      {spinner}
      {errorSPFData?.status !== NOT_FOUND_ERROR_STATUS ? (
        spfData
      ) : (
        <Error404 compact />
      )}
    </div>
  );
};

SPF.defaultProps = {
  data: {},
  isLoading: false,
};
SPF.propTypes = {
  data: PropTypes.object,
  isLoading: PropTypes.bool,
};

export default React.memo(SPF);
