import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Spinner from 'components/ui/spinner/Spinner';
import Table from 'components/ui/table';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Error404 from 'components/ui/error404';

import './email-spamassasin.scss';

import { seedlistingItemErrorsSelector } from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

const EmailSpamAssasin = ({ tableData, isLoading }) => {
  const t = useFormatMessage();

  const { spamAssasinData: errorSpamAssasinData } = useSelector(
    seedlistingItemErrorsSelector
  );
  const { evaluation = '', allowed = '', tableArrs } = tableData;

  const spinner = isLoading ? <Spinner /> : null;
  const table =
    !isLoading && tableArrs ? (
      <Fragment>
        <div className="email-spamassasin__row">
          <span className="email-spamassasin__key">
            {t('email_spamassasin.evaluation.text')}
            :
            {' '}
          </span>
          <span className="email-spamassasin__value">
            {evaluation}
          </span>
        </div>
        <div className="email-spamassasin__row">
          <span className="email-spamassasin__key">
            {t('email_spamassasin.allowed.text')}
            :
            {' '}
          </span>
          <span className="email-spamassasin__value">
            {allowed}
          </span>
        </div>
        <Table
          columns={[
            {
              field: 'ruleName',
              title: t('email_spamassasin.table.rule.name.title'),
              type: 'string',
              width: 250,
            },
            {
              field: 'ruleDescription',
              title: t('email_spamassasin.table.rule.description.title'),
              type: 'string',
              width: 400,
            },
          ]}
          data={tableArrs}
        />
      </Fragment>
    ) : null;

  return (
    <div className="email-spamassasin">
      {spinner}
      {errorSpamAssasinData?.status !== NOT_FOUND_ERROR_STATUS ? (
        table
      ) : (
        <Error404 compact />
      )}
    </div>
  );
};

EmailSpamAssasin.defaultProps = {
  tableData: {},
  isLoading: false,
};
EmailSpamAssasin.propTypes = {
  tableData: PropTypes.object,
  isLoading: PropTypes.bool,
};

export default React.memo(EmailSpamAssasin);
