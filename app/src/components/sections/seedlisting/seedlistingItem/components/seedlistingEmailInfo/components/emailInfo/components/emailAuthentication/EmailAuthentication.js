import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import InfoContainer from 'components/ui/infoContainer';
import {
  seedlistingItemIsLoadingEmailInfoSPFDataSelector,
  seedlistingItemEmailInfoSPFDataSelector,
  seedlistingItemIsLoadingEmailInfoDKIMDataSelector,
  seedlistingItemEmailInfoDKIMTextualRepresentationDataSelector,
  seedlistingItemEmailInfoDKIMSignatureHeaderDataSelector,
  seedlistingItemIsLoadingEmailInfoDMARCDataSelector,
  seedlistingItemEmailInfoDMARCDataSelector,
} from 'store/selectors';
import {
  getSeedlistingItemEmailInfoSPFData,
  getSeedlistingItemEmailInfoDKIMData,
  getSeedlistingItemEmailInfoDMARCData,
} from 'store/actions/seedlistingItem';
import { DKIM_SIGNATURE_HEADER, INFO_CONTAINER_STATUS } from 'utils/constants';
import SPF from './components/SPF';
import DKIM from './components/DKIM';
import DMARC from './components/DMARC';
import getDomainFromHeadersForSPF from './components/emailInfoAuthenticationSPFData';
import {
  getDomainSelectorFromHeadersForDKIM,
  getEmailInfoDKIMLayoutsData,
} from './components/emailInfoAuthenticationDKIMData';
import getDomainFromHeadersForDMARC from './components/emailInfoAuthenticationDMARCData';

import './email-authentication.scss';

const EmailAuthentication = ({ headers }) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const seedlistingItemIsLoadingEmailInfoSPFData = useSelector(
    seedlistingItemIsLoadingEmailInfoSPFDataSelector
  );
  const seedlistingItemEmailInfoSPFData = useSelector(
    seedlistingItemEmailInfoSPFDataSelector
  );

  const seedlistingItemIsLoadingEmailInfoDKIMData = useSelector(
    seedlistingItemIsLoadingEmailInfoDKIMDataSelector
  );
  const seedlistingItemEmailInfoDKIMTextualRepresentationData = useSelector(
    seedlistingItemEmailInfoDKIMTextualRepresentationDataSelector
  );
  const seedlistingItemEmailInfoDKIMSignatureHeaderData = useSelector(
    seedlistingItemEmailInfoDKIMSignatureHeaderDataSelector
  );
  
  const seedlistingItemIsLoadingEmailInfoDMARCData = useSelector(
    seedlistingItemIsLoadingEmailInfoDMARCDataSelector
  );
  const seedlistingItemEmailInfoDMARCData = useSelector(
    seedlistingItemEmailInfoDMARCDataSelector
  );

  const domainNameSPF = getDomainFromHeadersForSPF(headers);
  const { valuesOfDomainSelector, signaturesDKIM } = getDomainSelectorFromHeadersForDKIM(headers);
  const domainNameDMARC = getDomainFromHeadersForDMARC(headers);

  const spf = !seedlistingItemIsLoadingEmailInfoSPFData
    ? seedlistingItemEmailInfoSPFData
    : null;
  const dkim = !seedlistingItemIsLoadingEmailInfoDKIMData
    ? getEmailInfoDKIMLayoutsData({
        headers,
        textualRepresentationData: seedlistingItemEmailInfoDKIMTextualRepresentationData,
        signatureHeaderData: seedlistingItemEmailInfoDKIMSignatureHeaderData
      })
    : null;
  const dmarc = !seedlistingItemIsLoadingEmailInfoDMARCData
    ? seedlistingItemEmailInfoDMARCData
    : null;

  const [isShowSPF, setIsShowSPF] = useState(false);
  const [isShowDKIM, setIsShowDKIM] = useState(false);
  const [isShowDMARC, setIsShowDMARC] = useState(false);

  useEffect(() => {
    if (isShowSPF && domainNameSPF && !seedlistingItemEmailInfoSPFData) {
      dispatch(getSeedlistingItemEmailInfoSPFData({ domain: domainNameSPF }));
    }
  }, [dispatch, isShowSPF, domainNameSPF, seedlistingItemEmailInfoSPFData]);

  useEffect(() => {
    if (
      isShowDKIM &&
      !seedlistingItemIsLoadingEmailInfoDKIMData &&
      !seedlistingItemEmailInfoDKIMTextualRepresentationData &&
      !seedlistingItemEmailInfoDKIMSignatureHeaderData &&
      valuesOfDomainSelector.length &&
      signaturesDKIM.length
    ) {
      dispatch(
        getSeedlistingItemEmailInfoDKIMData({ valuesOfDomainSelector, signaturesDKIM })
      );
    }
  }, [
    dispatch,
    isShowDKIM,
    seedlistingItemIsLoadingEmailInfoDKIMData,
    seedlistingItemEmailInfoDKIMTextualRepresentationData,
    seedlistingItemEmailInfoDKIMSignatureHeaderData,
    valuesOfDomainSelector,
    signaturesDKIM,
  ]);

  useEffect(() => {
    if (isShowDMARC && domainNameDMARC && !seedlistingItemEmailInfoDMARCData) {
      dispatch(
        getSeedlistingItemEmailInfoDMARCData({ domain: domainNameDMARC })
      );
    }
  }, [
    dispatch,
    isShowDMARC,
    domainNameDMARC,
    seedlistingItemEmailInfoDMARCData,
  ]);

  const warningDKIM = [];
  // нет домена и селектора для DKIM
  !valuesOfDomainSelector.length &&
    warningDKIM.push(
      <InfoContainer
        text={t('email_authentication.no-domain-selector.dkim.text')}
        key="DKIM warning"
        status={INFO_CONTAINER_STATUS.warning}
      />
    );
  // нет хедера 'dkim-signature' для DKIM
  !headers[DKIM_SIGNATURE_HEADER] &&
    warningDKIM.push(
      <InfoContainer
        text={t('email_authentication.no-header.dkim.text')}
        key="DKIM header 'dkim-signature' warning"
        status={INFO_CONTAINER_STATUS.warning}
      />
    );

  return (
    <div className="email-authentication">
      <div className="email-authentication__accordion accordion">
        <ul className="accordion__list">
          <li className="accordion__item">
            <div
              className="accordion__item-header"
              onClick={() => setIsShowSPF(!isShowSPF)}
            >
              {t('email_authentication.spf.text')}
            </div>
            {isShowSPF && (
              <div className="accordion__item-content">
                {domainNameSPF ? (
                  <SPF
                    data={spf}
                    isLoading={seedlistingItemIsLoadingEmailInfoSPFData}
                  />
                ) : (
                  <InfoContainer
                    text={t('email_authentication.no-domain.spf.text')}
                    status={INFO_CONTAINER_STATUS.warning}
                  />
                )}
              </div>
            )}
          </li>
          <li className="accordion__item">
            <div
              className="accordion__item-header"
              onClick={() => setIsShowDKIM(!isShowDKIM)}
            >
              {t('email_authentication.dkim.text')}
            </div>
            {isShowDKIM && (
              <div className="accordion__item-content">
                {valuesOfDomainSelector.length ? (
                  <DKIM
                    data={dkim}
                    isLoading={seedlistingItemIsLoadingEmailInfoDKIMData}
                  />
                ) : (
                  warningDKIM.map((warning) => warning)
                )}
              </div>
            )}
          </li>
          <li className="accordion__item">
            <div
              className="accordion__item-header"
              onClick={() => setIsShowDMARC(!isShowDMARC)}
            >
              {t('email_authentication.dmarc.text')}
            </div>
            {isShowDMARC && (
              <div className="accordion__item-content">
                {domainNameDMARC ? (
                  <DMARC
                    data={dmarc}
                    isLoading={seedlistingItemIsLoadingEmailInfoDMARCData}
                  />
                ) : (
                  <InfoContainer
                    text={t(
                      'email_authentication.dmarc.warning.no-header.from.text'
                    )}
                    status={INFO_CONTAINER_STATUS.warning}
                  />
                )}
              </div>
            )}
          </li>
        </ul>
      </div>
    </div>
  );
};

EmailAuthentication.defaultProps = {
  headers: {},
};
EmailAuthentication.propTypes = {
  headers: PropTypes.object,
};

export default React.memo(EmailAuthentication);
