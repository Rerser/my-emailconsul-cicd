import React from 'react';

export default function getEmailInfoEpcDomainTableData(domainData) {
  if (!domainData) {
    return [{
      domain: null,
      dns_lookup_result: null,
      rbl_result: null,
    }];
  }

  const {
    domain: domainName = '',
    dns_lookup_result: reverseIps = [],
    rbl_result: blacklist = []
  } = domainData;

  // domain
  const domain = (
    <span>
      {domainName}
    </span>
  );

  // список reverse ip
  const dns_lookup_result = reverseIps.map(({ ip }, i) => (
    <div key={`reverse-ip-${ip}-${i}`} className="rbl-row">
      {ip}
    </div>
  ));

  // список blacklist
  const rbl_result = blacklist.map(({ lookup_host, url }) => (
    <div
      key={url}
      className={'rbl-row'}
    >
      <a
        href={url}
        target="_blank"
        rel="noopener noreferrer"
        className="rbl-row__link"
      >
        {lookup_host}
      </a>
    </div>
  ));

  return [{
    domain,
    dns_lookup_result,
    rbl_result,
  }];
}
