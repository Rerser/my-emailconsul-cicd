import React from 'react';
import PropTypes from 'prop-types';

import './email-headers.scss';

const EmailHeaders = ({ headers }) => (
  <div className="email-headers">
    {Object.keys(headers).map((key) => {
      const headerValue = headers[key];

      // if (Array.isArray(headerValue)) {
      //   headerValue = headerValue.join(', ');
      // }

      return (
        <div key={key} className="email-headers__row">
          <div className="email-headers__key">
            {key}
            :
            {' '}
          </div>
          <div className="email-headers__value">
            {Array.isArray(headerValue)
              ? headerValue.map((h, index) => <div key={index}>
                {h}
              </div>)
              : headerValue}
          </div>
        </div>
      );
    })}
  </div>
);

EmailHeaders.defaultProps = {
  headers: {},
};
EmailHeaders.propTypes = {
  headers: PropTypes.object,
};

export default React.memo(EmailHeaders);
