import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import './email-view.scss';

import { decode, getBody } from 'utils/emailHelpers';

const EmailView = ({ contentType, content }) => {
  const iFrameRef = useRef(null);

  useEffect(() => {
    const iframe = iFrameRef.current;
    const document = iframe.contentDocument;
    const html = document.getElementsByTagName('html')[0];

    const showedContent = getBody(contentType, decode(content));

    html.innerHTML = showedContent;
    html.style.pointerEvents = 'none';
  }, [iFrameRef, content, contentType]);

  return (
    <div className="email-view">
      <iframe
        className="email-view__iframe"
        ref={iFrameRef}
        title="emailPreview"
      />
    </div>
  );
};

EmailView.defaultProps = {
  content: '',
};
EmailView.propTypes = {
  content: PropTypes.string,
};

export default React.memo(EmailView);
