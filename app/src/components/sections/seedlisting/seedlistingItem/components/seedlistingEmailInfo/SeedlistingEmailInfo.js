import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Error404 from 'components/ui/error404';

import {
  getSeedlistingItemData,
  getSeedlistingItemEmailInfo,
  clearEmailInfoAllData,
} from 'store/actions/seedlistingItem';

import {
  seedlistingItemDataSelector,
  seedlistingItemEmailInfoSelector,
  seedlistingItemErrorsSelector,
  usersMeSelector,
} from 'store/selectors';

import { getEmailAddressFromString } from 'utils/helpers';
import { DEMO_USER_EMAIL, NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import EmailInfo from './components/emailInfo';

import './seedlisting-email-info.scss';

const SeedlistingEmailInfo = (props) => {
  const [seedlistinItemName, setSeedlistinItemName] = useState('');
  const [domainName, setDomainName] = useState('');

  const { id: seedlistingId, emailId: seedlistingEmailId } = useParams();
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const seedlistingItemData = useSelector(seedlistingItemDataSelector);
  const seedlistingItemEmailInfo = useSelector(
    seedlistingItemEmailInfoSelector
  );
  const seedlistingItemErrors = useSelector(seedlistingItemErrorsSelector);

  const {
    data: errorData,
    emailInfoData: errorEmailInfoData,
    emailInfoIdData: errorEmailInfoIdData,
  } = seedlistingItemErrors;

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  useEffect(() => () => dispatch(clearEmailInfoAllData()), [dispatch]);

  useEffect(() => {
    if (seedlistingItemData) {
      const { title } = seedlistingItemData;
      setSeedlistinItemName(title);
    }
    if (seedlistingItemEmailInfo) {
      const { from: fromHeader } = seedlistingItemEmailInfo || {};
      const fromEmail = getEmailAddressFromString(fromHeader).split('@');
      const [, name = ''] = fromEmail;
      setDomainName(name);
    }
  }, [dispatch, seedlistingItemData, seedlistingItemEmailInfo]);

  useEffect(() => {
    if (seedlistingEmailId && seedlistingId && !seedlistingItemEmailInfo) {
      dispatch(
        getSeedlistingItemEmailInfo({
          id: seedlistingId,
          seedlistingEmailId,
        })
      );
    }
  }, [dispatch, seedlistingId, seedlistingEmailId, seedlistingItemEmailInfo]);

  useEffect(() => {
    if (seedlistingId && !seedlistingItemData) {
      dispatch(getSeedlistingItemData({ id: seedlistingId }));
    }
  }, [dispatch, seedlistingId, seedlistingItemData]);

  if (
    errorData?.status === NOT_FOUND_ERROR_STATUS ||
    errorEmailInfoData?.status === NOT_FOUND_ERROR_STATUS ||
    errorEmailInfoIdData?.status === NOT_FOUND_ERROR_STATUS
  )
    return <Error404 />;

  return (
    <div className="seedlisting-email-info">
      <div className="seedlisting-email-info__header">
        <h1 className="seedlisting-email-info__title">
          <Link to="/seedlisting">
            {t('seedlisting_item.title')}
          </Link>
          {' > '}
          <Link to={`/seedlisting/${seedlistingId}`}>
            {seedlistinItemName}
          </Link>
          {' > '}
          <span className={cn("seedlisting-email-info__breadcrumbs-current", {
            'seedlisting-email-info__breadcrumbs-current--blured': meEmail === DEMO_USER_EMAIL
          })}>
            {domainName}
          </span>
        </h1>
      </div>
      <div className="seedlisting-email-info__email-info">
        <EmailInfo />
      </div>
    </div>
  );
};

export default SeedlistingEmailInfo;
