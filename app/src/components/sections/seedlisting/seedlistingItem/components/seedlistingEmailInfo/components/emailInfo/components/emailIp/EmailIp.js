import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Error404 from 'components/ui/error404';

import './email-ip.scss';

import { seedlistingItemErrorsSelector } from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

const EmailIp = ({ tableData, isLoading }) => {
  const t = useFormatMessage();

  const { epcIpData: errorEpcIpData } = useSelector(
    seedlistingItemErrorsSelector
  );

  const spinner = isLoading ? <Spinner /> : null;
  const table = !isLoading ? (
    <Table
      columns={[
        {
          field: 'ip',
          title: t('email_ip.table.ip.title'),
          type: 'string',
          width: 200,
        },
        { field: 'rdns', title: t('email_ip.table.rDNS.title'), width: 400 },
        {
          field: 'rbl_result',
          title: t('email_ip.table.blacklist.title'),
          width: 400,
        },
      ]}
      data={[tableData]}
    />
  ) : null;

  return (
    <div className="email-ip">
      {spinner}
      {errorEpcIpData?.status !== NOT_FOUND_ERROR_STATUS ? (
        table
      ) : (
        <Error404 compact />
      )}
    </div>
  );
};

EmailIp.defaultProps = {
  tableData: {},
  isLoading: false,
};
EmailIp.propTypes = {
  tableData: PropTypes.object,
  isLoading: PropTypes.bool,
};

export default React.memo(EmailIp);
