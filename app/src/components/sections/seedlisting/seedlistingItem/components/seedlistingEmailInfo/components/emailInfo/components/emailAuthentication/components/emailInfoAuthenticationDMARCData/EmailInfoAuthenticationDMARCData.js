import { getEmailAddressFromString } from 'utils/helpers';
import { FROM_HEADER } from 'utils/constants';

// ------------------------------------------
// функция, фозвращающая домен из хедера from
// ------------------------------------------
export default function (headers) {
  if (headers[FROM_HEADER]) {
    if (!(headers[FROM_HEADER] instanceof Array)) {
      return null;
    }

    const fromHeaderValue = headers[FROM_HEADER][0]; // значение из хедера 'from'

    // domain из хедера 'from'
    const fromEmail = getEmailAddressFromString(fromHeaderValue).split('@');
    const [, domainNameFrom] = fromEmail;

    if (domainNameFrom) {
      return domainNameFrom;
    }
  }

  return null;
}
