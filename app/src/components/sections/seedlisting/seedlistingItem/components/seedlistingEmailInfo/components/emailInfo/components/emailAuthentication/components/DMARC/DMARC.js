import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Spinner from 'components/ui/spinner/Spinner';
import DmarcRecords from './components/DmarcRecords';
import Error404 from 'components/ui/error404';

import './dmarc.scss';

import { seedlistingItemErrorsSelector } from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

const DMARC = ({ data, isLoading }) => {
  const { DMARCData: errorDMARCData } = useSelector(
    seedlistingItemErrorsSelector
  );

  const spinner = isLoading ? <Spinner /> : null;
  const dmarcData = !isLoading ? <DmarcRecords data={data} /> : null;

  return (
    <div className="dmarc">
      {spinner}
      {errorDMARCData?.status !== NOT_FOUND_ERROR_STATUS ? (
        dmarcData
      ) : (
        <Error404 compact />
      )}
    </div>
  );
};

DMARC.defaultProps = {
  data: {},
  isLoading: false,
};
DMARC.propTypes = {
  data: PropTypes.object,
  isLoading: PropTypes.bool,
};

export default React.memo(DMARC);
