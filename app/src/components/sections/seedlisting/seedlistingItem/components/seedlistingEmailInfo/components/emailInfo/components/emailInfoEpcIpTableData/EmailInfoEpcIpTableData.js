import React from 'react';
import cn from 'classnames';

export default function getEmailInfoEpcIpTableData(
  ip,
  dns_lookup_result,
  rbl_result
) {
  const rdns = dns_lookup_result.map(({ rDNS, addresses }) => (
    <div
      key={rDNS}
      className={cn('rdns-row', {
        warning: !addresses.includes(ip),
      })}
    >
      {rDNS}
    </div>
  ));

  const rbl = rbl_result.map(({ url }) => (
    <div key={Math.random()} className="rbl-row">
      <a
        href={url}
        target="_blank"
        rel="noopener noreferrer"
        className="rbl-row__link"
      >
        {url}
      </a>
    </div>
  ));

  return {
    ip,
    rdns,
    rbl_result: rbl,
  };
}
