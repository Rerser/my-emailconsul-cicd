import React from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';

import './email-html.scss';

import 'ace-builds/src-noconflict/mode-html';
import 'ace-builds/src-noconflict/theme-monokai';

const EmailHtml = ({ html }) => (
  <div className="email-html">
    <AceEditor
      value={html}
      readOnly
      width="100%"
      mode="html"
      theme="monokai"
      className="email-html__editor"
      setOptions={{
        showInvisibles: false,
        selectionStyle: 'line',
        scrollSpeed: 3,
        showFoldWidgets: true,
        useSoftTabs: true,
        tabSize: 2,
        showPrintMargin: false,
        fontSize: 12,
      }}
      wrapEnabled
    />
  </div>
);

EmailHtml.defaultProps = {
  html: '',
};
EmailHtml.propTypes = {
  html: PropTypes.string,
};

export default React.memo(EmailHtml);
