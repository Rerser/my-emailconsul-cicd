import React from 'react';
import messages from 'utils/messages';

export default function getEmailInfoSpamAssasinData(data, locale) {
  if (data) {
    const { evaluation = '', allowed = '', rules = [] } = data;

    const ruleName = rules.map((rule) => (
      <div key={rule} className="rule-name">
        {rule}
      </div>
    ));

    const ruleDescription = rules.map((rule, index) => (
      <div key={Math.random() + rule + index} className="rule-description">
        {messages[locale][rules[index]]}
      </div>
    ));

    const tableArrs = rules.map((_, index) => ({
      ruleName: ruleName[index],
      ruleDescription: ruleDescription[index],
    }));

    return {
      evaluation,
      allowed,
      tableArrs,
    };
  }
}
