/*
  Example:
  <CollapseExpandAccordion
    data={dataObj}
    tabSize={tabSize}
    isExpanded={true | false}
    children={html | components}
    noheader
    ipData
  />;
*/

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { plus, minus } from 'react-icons-kit/fa/';
import Icon from 'react-icons-kit';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { CONTROL_ICON_COLOR, INFO_CONTAINER_STATUS } from 'utils/constants';
import InfoContainer from 'components/ui/infoContainer';
import cn from 'classnames';

import './collapse-expand-accordion.scss';

const CollapseExpandAccordion = ({
  children,
  data,
  tabSize,
  noheader,
  isExpanded,
  ipData,
  isFirstHeader,
}) => {
  const t = useFormatMessage();
  const [isExpand, setIsExpand] = useState(isExpanded);

  const {
    header = '', // название домена
    content = '', // контент в аккордионе
    result = false,
    queries = null,
    warnings = [],
    warning = null,
    mechanism = '',
  } = data;

  // если есть header (домен) - добавим ':' для текста механизма
  const inMechanismStr = header ? `${header}:` : '';
  // сформируем текст механизма
  const mechanismStr = `+${mechanism}:${inMechanismStr} `;

  const queriesLayout = queries ? (
    <span
      className="collapse-expand-accordion__dns-queries"
      data-tooltip={t(
        'email_authentication.accordion.spf.queries.tooltip.text'
      )}
    >
      {queries}
    </span>
  ) : null;

  // ------------------------------------------------------------
  // лэйаут шапки для управления аккодионом и информация о домене
  // ------------------------------------------------------------
  const headerLayout = !noheader && (
    <div
      className="collapse-expand-accordion__header"
      onClick={() => setIsExpand(!isExpand)}
    >
      <span className="collapse-expand-accordion__icon">
        <Icon
          icon={isExpand ? plus : minus}
          size="100%"
          style={{
            color: CONTROL_ICON_COLOR,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />
      </span>
      {queriesLayout}
      <span className="collapse-expand-accordion__title">
        <span className="collapse-expand-accordion__domain-text">
          {mechanism && mechanismStr}
          {header}
        </span>
        {
          isFirstHeader && (
            <span className={cn({
              "collapse-expand-accordion__valid": result,
              "collapse-expand-accordion__not-valid": !result,
            })}>
              {result ? t('email_authentication.accordion.spf.valid.text') : t('email_authentication.accordion.spf.not-valid.text')}
            </span>
          )
        }
      </span>
    </div>
  );

  // ------------------------------------------
  // лэйаут для контента и варнингов аккордиона
  // ------------------------------------------
  const textContentLayout = (text) => (
    <div
      key={`${Math.random()}content`}
      className={cn('collapse-expand-accordion__text', { ipData })}
    >
      {noheader && mechanism !== 'include' && queriesLayout}
      {mechanism && mechanismStr}
      {text}
    </div>
  );

  const dataLayout =
    content instanceof Array && !!content.length
      ? content.map((item) => textContentLayout(item))
      : textContentLayout(content);

  const warningLayout = (warningText) => (
    <div
      className="collapse-expand-accordion__warning-wrapper"
      key={`${Math.random()}warning`}
    >
      <InfoContainer text={warningText} status={INFO_CONTAINER_STATUS.warning} />
    </div>
  );

  const contentLayout = (
    <div className="collapse-expand-accordion__content">
      {dataLayout}
      {warnings && warnings.map((item) => warningLayout(item))}
      {warning && warningLayout(warning)}
    </div>
  );

  return data && Object.keys(data).length ? (
    <div
      className={cn('collapse-expand-accordion', {
        'collapse-expand-accordion--nomargin': !queries
      })}
      style={{ marginLeft: `${tabSize}px` }}
    >
      {headerLayout}
      {contentLayout}
      {!!children && (
        <div
          className={cn('collapse-expand-accordion__children', {
            hide: isExpand,
          })}
        >
          {children}
        </div>
      )}
    </div>
  ) : null;
};

CollapseExpandAccordion.defaultProps = {
  data: {},
  tabSize: 10,
  noheader: false,
  isExpanded: true,
  ipData: false,
  isFirstHeader: false,
};
CollapseExpandAccordion.propTypes = {
  data: PropTypes.object,
  tabSize: PropTypes.number,
  noheader: PropTypes.bool,
  isExpanded: PropTypes.bool,
  ipData: PropTypes.bool,
  isFirstHeader: PropTypes.bool,
};

export default React.memo(CollapseExpandAccordion);
