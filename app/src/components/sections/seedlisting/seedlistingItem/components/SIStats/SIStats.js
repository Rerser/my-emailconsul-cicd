import React from 'react';
import PropTypes from 'prop-types';
import { Chart } from 'react-google-charts';
import {
  HOST_LABEL,
  SEEDLISTING_EMAIL_STATUS,
  SEEDLISTING_STATUS,
} from 'utils/constants';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';

import './si-stats.scss';

const SIStats = ({ data, status }) => {
  const t = useFormatMessage();
  const hosts = new Set();

  /*
   * get aggregation metrics
   */
  const emailsLength = data.length;
  let inbox = 0;
  let spam = 0;
  let unreceived = 0;
  let waiting = 0;

  data.forEach((item) => {
    hosts.add(item.host);

    item.status === SEEDLISTING_EMAIL_STATUS.INBOX && inbox++;
    item.status === SEEDLISTING_EMAIL_STATUS.SPAM && spam++;
    item.status === SEEDLISTING_EMAIL_STATUS.NOT_RECEIVED && unreceived++;
    item.status === SEEDLISTING_EMAIL_STATUS.WAITING && waiting++;
  });

  const inboxPercent = parseInt((100 / emailsLength) * inbox);
  const spamPercent = parseInt((100 / emailsLength) * spam);
  const unreceivedPercent = parseInt((100 / emailsLength) * unreceived);
  const progressPercent = parseInt(
    100 - parseInt((waiting / emailsLength) * 100)
  );

  /*
   * get metric for each host
   */
  const chartData = [];

  for (const host of hosts) {
    const emails = data.filter((item) => item.host === host);

    const emailsLength = emails.length;
    let inbox = 0;
    let spam = 0;
    let unreceived = 0;

    emails.forEach((item) => {
      item.status === SEEDLISTING_EMAIL_STATUS.INBOX && inbox++;
      item.status === SEEDLISTING_EMAIL_STATUS.SPAM && spam++;
      item.status === SEEDLISTING_EMAIL_STATUS.NOT_RECEIVED && unreceived++;
    });

    const inboxPercent = parseInt((100 / emailsLength) * inbox);
    const spamPercent = parseInt((100 / emailsLength) * spam);
    const unreceivedPercent = parseInt((100 / emailsLength) * unreceived);

    const hostData = {
      host: host || t('sistats.sispanels.host.other.text'),
      inboxPercent,
      spamPercent,
      unreceivedPercent,
    };
    chartData.push(hostData);
  }

  const inboxPercentStr = isNaN(inboxPercent) ? '-' : `${inboxPercent}%`;
  const spamPercentStr = isNaN(spamPercent) ? '-' : `${spamPercent}%`;
  const unreceivedPercentStr = isNaN(unreceivedPercent)
    ? '-'
    : `${unreceivedPercent}%`;
  const progressPercentStr = isNaN(progressPercent)
    ? '-'
    : `${
        status !== SEEDLISTING_STATUS.TERMINATED &&
        status !== SEEDLISTING_STATUS.DONE &&
        progressPercent === 100
          ? 99
          : progressPercent
      }%`;

  return (
    <div className="si-stats">
      <ul className="si-stats__panels sispanels">
        <li className="sispanels__panel sispanels__panel--inbox">
          <div className="sispanels__label">
            {t('sistats.sispanels.label.inbox.text')}
          </div>
          <div className="sispanels__value">
            {inboxPercentStr}
          </div>
        </li>
        <li className="sispanels__panel sispanels__panel--spam">
          <div className="sispanels__label">
            {t('sistats.sispanels.label.spam.text')}
          </div>
          <div className="sispanels__value">
            {spamPercentStr}
          </div>
        </li>
        <li className="sispanels__panel sispanels__panel--unreceived">
          <div className="sispanels__label">
            {t('sistats.sispanels.label.unreceived.text')}
          </div>
          <div className="sispanels__value">
            {unreceivedPercentStr}
          </div>
        </li>
        <li className="sispanels__panel sispanels__panel--progress">
          <div className="sispanels__label">
            {t('sistats.sispanels.label.progress.text')}
          </div>
          <div className="sispanels__value">
            {progressPercentStr}
          </div>
        </li>
      </ul>
      <div className="si-stats__chart sischart">
        <Chart
          width="100%"
          height="100%"
          chartType="Bar"
          loader={<Spinner />}
          data={[
            [
              '',
              t('sistats.sispanels.label.inbox.text'),
              t('sistats.sispanels.label.spam.text'),
              t('sistats.sispanels.label.unreceived.text'),
            ],
            ...chartData.map((item) => [
              HOST_LABEL[item.host] || t('sistats.sispanels.host.other.text'),
              item.inboxPercent,
              item.spamPercent,
              item.unreceivedPercent,
            ]),
          ]}
          options={{
            // Material design options
            chart: {
              // title: 'Stats by host',
              subtitle: '%',
            },
            colors: ['#2ecc71', '#e74c3c', '#f39c12'],
            // legend: { position: 'none' },
          }}
        />
      </div>
      <ul className="si-stats__row-data sisrow-data">
        <li className="sisrow-data__item">
          <span className="sisrow-data__label">
            {t('sistats.sispanels.label.inbox.text')}
            :
            {' '}
          </span>
          <span className="sisrow-data__value">
            {inboxPercentStr}
          </span>
        </li>
        <li className="sisrow-data__item">
          <span className="sisrow-data__label">
            {t('sistats.sispanels.label.spam.text')}
            :
            {' '}
          </span>
          <span className="sisrow-data__value">
            {spamPercentStr}
          </span>
        </li>
        <li className="sisrow-data__item">
          <span className="sisrow-data__label">
            {t('sistats.sispanels.label.unreceived.text')}
            :
            {' '}
          </span>
          <span className="sisrow-data__value">
            {unreceivedPercentStr}
          </span>
        </li>
        <li className="sisrow-data__item">
          <span className="sisrow-data__label">
            {t('sistats.sispanels.label.progress.text')}
            :
            {' '}
          </span>
          <span className="sisrow-data__value">
            {progressPercentStr}
          </span>
        </li>
      </ul>
    </div>
  );
};

SIStats.defaultProps = {
  data: [],
};
SIStats.propTypes = {
  data: PropTypes.array,
};

export default React.memo(SIStats);
