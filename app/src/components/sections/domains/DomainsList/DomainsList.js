import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner/Spinner';
import SlideBar from 'components/ui/slideBar';
import Error404 from 'components/ui/error404';
import IpsReportSettings from 'components/sections/ips/IpsList/components/IpsReportSettings';
import HangingBtn from 'components/ui/hangingBtn';

import {
  setDomainsData,
  handleDomainsDataFilter,
  setIsOnNewDomain,
  setNewDomainsFile,
  setResponseNewDomainsUploadedFile,
  setDomainsAvailableCount,
} from 'store/actions/domains';
import {
  handleIpsReportIsOnEdit,
  getIpsReportData,
} from 'store/actions/ipsReport';
import {
  ipsReportIsOnEditSelector,
  domainsIsOnNewDomainSelector,
  myUserBillingPlanIsDataLoadingSelector,
  domainsErrorsSelector,
} from 'store/selectors';

import { LANDING_FAQ_URL, NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import TableDomainList from './components/TableDomainList';
import AddDomain from './components/AddDomain';
import DomainsListHeader from './components/DomainsListHeader';
import './domains-list.scss';

function DomainsList() {
  const t = useFormatMessage();
  const dispatch = useDispatch();

  const domainsIsOnNewDomain = useSelector(domainsIsOnNewDomainSelector);
  const ipsReportIsOnEdit = useSelector(ipsReportIsOnEditSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);
  const domainsErrors = useSelector(domainsErrorsSelector);

  useEffect(() => {
    dispatch(getIpsReportData());
    return () => {
      dispatch(handleDomainsDataFilter({ field: 'domain', value: '' }));
      dispatch(setDomainsAvailableCount(0));
      dispatch(setDomainsData({ data:[] }));
    };
  }, [dispatch]);

  const showIpsReportSlider = useMemo(() => !!ipsReportIsOnEdit, [
    ipsReportIsOnEdit,
  ]);
  const showNewDomainSlider = useMemo(() => !!domainsIsOnNewDomain, [
    domainsIsOnNewDomain,
  ]);

  if (myUserBillingPlanIsDataLoading) return <Spinner />;
  if (domainsErrors?.data?.status === NOT_FOUND_ERROR_STATUS)
    return <Error404 />;

  return (
    <div className="domains-list">
      <DomainsListHeader />

      <TableDomainList />

      <SlideBar
        isOpen={showIpsReportSlider}
        onClose={() => dispatch(handleIpsReportIsOnEdit(false))}
        className="domains-list__report-settings"
      >
        <IpsReportSettings />
      </SlideBar>

      <SlideBar
        isOpen={showNewDomainSlider}
        onClose={() => {
          dispatch(setIsOnNewDomain(false));
          dispatch(setResponseNewDomainsUploadedFile(null));
          dispatch(setNewDomainsFile(null));
        }}
        className="domains-list__new-domain"
      >
        <AddDomain />
      </SlideBar>

      <HangingBtn 
        tooltipText={t('seedlisting_list.hanging-btn.tootip.text')}
        href={`${LANDING_FAQ_URL}/index.html?faqItem=ipDomainMonitoringFaq`}
      />
    </div>
  );
}

export default React.memo(DomainsList);
