import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { basic_trashcan } from 'react-icons-kit/linea/basic_trashcan';
import Icon from 'react-icons-kit';
import Tabs from 'components/ui/tabs';
import InputFile from 'components/ui/inputFile';
import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';

import {
  setIsOnNewDomain,
  handleNewDomains,
  removeNewDomain,
  saveNewDomains,
  handleNewDomainMode,
  handelSetNewDomainsFile,
  handelUploadNewDomainsFile,
} from 'store/actions/domains';
import {
  myUserBillingPlanDataSelector,
  domainsNewDomainsSelector,
  domainsNewDomainIsLoadingSelector,
  domainsDataPaginationSelector,
  domainsNewDomainModeSelector,
  domainsNewDomainsFileSelector,
  domainsNewDomainsFileIsUploadingSelector,
  domainsResponseNewDomainsUploadedFileSelector,
} from 'store/selectors';

import { NEW_DOMAIN_MODE } from 'utils/constants';

import cn from 'classnames';
import './c-add-domain.scss';

const AddDomain = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const newDomains = useSelector(domainsNewDomainsSelector);
  const domainsNewDomainIsLoading = useSelector(domainsNewDomainIsLoadingSelector);
  const domainsDataPagination = useSelector(domainsDataPaginationSelector);
  const domainsNewDomainMode = useSelector(domainsNewDomainModeSelector);
  const domainsNewDomainsFile = useSelector(domainsNewDomainsFileSelector);
  const domainsNewDomainsFileIsUploading = useSelector(domainsNewDomainsFileIsUploadingSelector);
  const domainsResponseNewDomainsUploadedFile = useSelector(domainsResponseNewDomainsUploadedFileSelector);

  const { domains = 0 } = myUserBillingPlanData;
  const { count = 0 } = domainsDataPagination;
  const domainsAvailable = domains?.domains_per_month - count || 0;

  const handleDeleteNewDomain = (e, index) => {
    e.preventDefault();
    if (newDomains.length === 1) {
      dispatch(handleNewDomains({ value: '', index: 0 }));
    } else {
      dispatch(removeNewDomain({ index }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!domainsNewDomainIsLoading && newDomains.every((domain) => domain)) {
      dispatch(saveNewDomains({ newDomains }));
    }
  };

  return (
    <form className="c-slidebar-content c-add-domain" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('add_domain.header.text')}
      </h3>

      {domainsNewDomainIsLoading && <Spinner />}

      {!domainsNewDomainIsLoading && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <Tabs
              value={domainsNewDomainMode}
              options={[
                {
                  value: NEW_DOMAIN_MODE.SINGLE,
                  label: t('add_domain.tabs.single_domain.label'),
                },
                {
                  value: NEW_DOMAIN_MODE.CSV,
                  label: t('add_domain.tabs.many_domain.label'),
                },
              ]}
              onChange={(e) => dispatch(handleNewDomainMode(e.target.value))}
            />
          </div>

          {domainsNewDomainMode === NEW_DOMAIN_MODE.SINGLE && (
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('add_domain.domain.label')}
              </span>
              {newDomains.map((domain, index) => (
                <div key={index} className="c-add-domain__domain-row domain-row">
                  <InputText
                    value={domain}
                    onChange={(e) => dispatch(handleNewDomains({ value: e.target.value, index }))}
                    className="domain-row__input"
                  />
                  <button
                    type="button"
                    className="domain-row__remove-btn"
                    onClick={(e) => handleDeleteNewDomain(e, index)}
                  >
                    <Icon icon={basic_trashcan} />
                  </button>
                </div>
              ))}
              <Button
                text={t('add_domain.btn.add.text')}
                onClick={() => dispatch(handleNewDomains({ value: null, index: newDomains.length }))}
                disabled={newDomains.length > domainsAvailable - 1}
              />
            </label>
          )}

          {domainsNewDomainMode === NEW_DOMAIN_MODE.CSV && (
            <Fragment>
              {domainsNewDomainsFileIsUploading ? (
                <Spinner />
              ) : (
                <Fragment>
                  {domainsResponseNewDomainsUploadedFile && (
                    <div className="c-add-domain__result-uploaded-container">
                      <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                        <div className="c-add-domain__uploaded-result">
                          {t('add_domain.result.uploaded.added.text')}
                          {' '}
                          <span className={cn("c-add-domain__quantity", {
                            'c-add-domain__quantity--success': domainsResponseNewDomainsUploadedFile.added > 0,
                            'c-add-domain__quantity--failed': domainsResponseNewDomainsUploadedFile.added === 0,
                          })}>
                            {domainsResponseNewDomainsUploadedFile.added}
                          </span>
                        </div>
                      </div>
                      {!!domainsResponseNewDomainsUploadedFile.not_added && (
                        <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                          <div className="c-add-domain__uploaded-result">
                            {t('add_domain.result.uploaded.has_not_added.text')}
                            {' '}
                            <span className={cn("c-add-domain__quantity", {                              
                              'c-add-domain__quantity--success': domainsResponseNewDomainsUploadedFile.not_processed === 0,                              
                              'c-add-domain__quantity--failed': domainsResponseNewDomainsUploadedFile.not_processed > 0,
                            })}>
                              {domainsResponseNewDomainsUploadedFile.not_added}
                            </span>
                          </div>
                        </div>
                      )}
                      {!!domainsResponseNewDomainsUploadedFile.not_valid.length && (
                        <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                          <div className="c-add-domain__uploaded-result">
                            {t('add_domain.result.uploaded.not_valid.text')}
                            {' '}
                            <span className="c-add-domain__quantity c-add-gp-domain__quantity--failed">
                              {domainsResponseNewDomainsUploadedFile.not_valid.length}
                            </span>
                            {!!domainsResponseNewDomainsUploadedFile.not_valid.length && 
                              domainsResponseNewDomainsUploadedFile.not_valid.map((domain, i) => (
                                <div key={`not-valid-domains-${i}`} className="c-add-domain__uploaded">
                                  {domain}
                                </div>
                              ))
                            }
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                
                  <div className="c-slidebar-content__label">
                    {!domainsNewDomainsFile && (
                      <Fragment>
                        <span>
                          {t('add_domain.drag_file.warning.text')}
                        </span>
                        <InputFile
                          labelNodeElement={
                            <div className="c-add-domain__dragfile-container">
                              {t('add_domain.drag_file.import.text')}
                            </div>
                          }
                          onUpload={(data) => dispatch(handelSetNewDomainsFile(data.files[0]))}
                        />
                      </Fragment>
                    )}
                    {domainsNewDomainsFile && (
                      <div className="c-slidebar-content__file-row">
                        <div className="c-slidebar-content__file-label">
                          {domainsNewDomainsFile.name}
                        </div>
                        <div
                          className="c-slidebar-content__file-delete"
                          onClick={() => dispatch(handelSetNewDomainsFile(null))}
                        >
                          <Icon icon={basic_trashcan} />
                        </div>
                      </div>
                    )}
                  </div>
                </Fragment>
              )}
            </Fragment>
          )}

        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {domainsNewDomainMode === NEW_DOMAIN_MODE.SINGLE && (
          <Button
            mode="purple"
            text={t('add_domain.btn.save.text')}
            className="c-slidebar-content__btn"
            disabled={domainsNewDomainIsLoading || newDomains.some((domain) => !domain)}
            type="submit"
          />
        )}

        {domainsNewDomainMode === NEW_DOMAIN_MODE.CSV && (
          <Button
            mode="purple"
            text={t('add_domain.btn.import.text')}
            className="c-slidebar-content__btn"
            disabled={!domainsNewDomainsFile || domainsNewDomainsFileIsUploading}
            type="button"
            onClick={() => dispatch(handelUploadNewDomainsFile())}
          />
        )}

        <Button
          text={t('add_domain.btn.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setIsOnNewDomain(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(AddDomain);
