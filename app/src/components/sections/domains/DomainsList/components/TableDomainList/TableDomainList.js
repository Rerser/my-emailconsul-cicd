import React, { Fragment, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Table from 'components/ui/table';
import Pagination from 'components/ui/pagination';
import InputText from 'components/ui/inputText';
import Spinner from 'components/ui/spinner/Spinner';

import {
  getDomainsData,
  deleteDomain,
  refreshDomain,
  setDomainsDataPagination,
  handleDomainsDataFilter,
  handleDomainsDataSorting,
  handleDomainsDataIsLoading,
  deleteDomainsFromFilters,
} from 'store/actions/domains';
import {
  domainsIsDataLoadingSelector,
  domainsDataSelector,
  domainsDataPaginationSelector,
  domainsDataFilterSelector,
  domainsDataSortingSelector,
  usersMeSelector,
} from 'store/selectors';

import { DEMO_USER_EMAIL, DOMAIN_STATUS } from 'utils/constants';
import { useDebounce, usePagination } from 'utils/helpers';

function TableDomainList() {
  const dispatch = useDispatch();
  const t = useFormatMessage();
  const history = useHistory();

  const domainsData = useSelector(domainsDataSelector);
  const domainsIsDataLoading = useSelector(domainsIsDataLoadingSelector);

  const domainsDataSorting = useSelector(domainsDataSortingSelector);
  const domainsDataFilter = useSelector(domainsDataFilterSelector);

  const domainsDataPagination = useSelector(domainsDataPaginationSelector);

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  const { limit, offset } = domainsDataPagination;
  const { domain: filterDomain } = domainsDataFilter;
  const { field: sortField, order: sortOrder } = domainsDataSorting;

  useEffect(() => {
    dispatch(handleDomainsDataIsLoading(true));
  }, [dispatch]);

  const debouncedFilterDomain = useDebounce(filterDomain, 1000);

  const tableData = useMemo(
    () =>
      domainsData.map((item) => {
        const {
          dns_lookup_result = [],
          rbl_result = [],
          status,
        } = item;

        const reverseIp = dns_lookup_result.map(({ ip }, i) => (
          <div
            key={`reverseIp-value-${i}`}
            className='reverseIp-row'
          >
            {ip}
          </div>
        ));

        const rblFiltered = new Set();

        rbl_result.forEach(({ url }) => rblFiltered.add(url));

        const rbl = [...rblFiltered].map((url, i) => (
          <div key={`rbl-value-domains-${i}`} className="rbl-row">
            <a
              href={url}
              target="_blank"
              rel="noopener noreferrer"
              className="rbl-row__link"
            >
              {url}
            </a>
          </div>
        ));

        const isUpdating = 
          status === DOMAIN_STATUS.WAITING || 
          status === DOMAIN_STATUS.IN_PROGRESS;
          
        const cbsPayload = {
          isUpdating,
          isUpdateDisable: isUpdating,
        };

        return {
          ...item,
          reverse_ip: reverseIp,
          rbl_result: rbl,
          cbsPayload,
        };
      }),
    [domainsData]
  );

  useEffect(() => {
    dispatch(getDomainsData({ withLoader: false }));

    const interval = setInterval(() => {
      dispatch(getDomainsData({ withLoader: false }));
    }, 5000);

    return () => clearInterval(interval);
  }, [dispatch, limit, offset, sortField, sortOrder, debouncedFilterDomain]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: domainsDataPagination,
    action: setDomainsDataPagination,
  });

  if (domainsIsDataLoading) return <Spinner />;

  return (
    <Fragment>
      <div className="domains-list__pagination ipl-table-panel">
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="domains-list__pagination domains-list__pagination--top"
        />
        <div className="ipl-table-panel__filter itp-filter">
          <div className="itp-filter__item itp-filter__domain-filter">
            <div className="itp-filter__label">
              {t('domains_list.filter_by_domain.lable')}
            </div>
            <InputText
              value={filterDomain}
              onChange={(e) =>
                dispatch(
                  handleDomainsDataFilter({
                    field: 'domain',
                    value: e.target.value,
                  })
                )
              }
              placeholder={t('domains_list.filter_by_domain.placeholder')}
            />
          </div>
        </div>
      </div>

      <Table
        columns={[
          {
            field: 'domain',
            title: t('domains_list.table.header.domain.text'),
            type: 'string',
            width: 200,
          },
          {
            field: 'reverse_ip',
            title: t('domains_list.table.header.reverse_ip.text'),
            width: 400,
          },
          {
            field: 'rbl_result',
            title: t('domains_list.table.header.blacklist.text'),
            width: 400,
          },
        ]}
        data={tableData}
        cbs={{
          onUpdate: (item) => dispatch(refreshDomain(item)),
          onDelete: (item) => dispatch(deleteDomain(item)),
        }}
        sorting={{
          ...domainsDataSorting,
          sortFields: ['domain', 'rbl_result'],
          isApiSorting: true,
        }}
        onSorting={(data) => dispatch(handleDomainsDataSorting(data))}
        headerCbs={{
          dump: (data) => false,
          onDelete: (data) => tableData.length ? dispatch(deleteDomainsFromFilters()) : false,
        }}
        bluredColumns={meEmail === DEMO_USER_EMAIL ? ['domain', 'reverse_ip'] : []}
        onRowClick={(item) => history.push(`/domains/${item.id}/rbl-logs`)}
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="domains-list__pagination domains-list__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(TableDomainList);
