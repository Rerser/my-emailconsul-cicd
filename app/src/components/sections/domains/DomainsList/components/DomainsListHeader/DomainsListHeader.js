import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Button from 'components/ui/button/Button';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import {
  ipsReportDataSelector,
  myUserBillingPlanDataSelector,
  domainsAvailableCountSelector,
  domainsAvailableCountIsLoadingSelector,
} from 'store/selectors';
import {
  getDomainsAvailableCount,
  setIsOnNewDomain,
} from 'store/actions/domains';
import { getBillingPlanIsExpired } from 'utils/helpers';
import { handleIpsReportIsOnEdit } from 'store/actions/ipsReport';

import { BILLING_PLAN_NAME } from 'utils/constants';

import history from 'utils/history';
import cn from 'classnames';
import './domains-list-header.scss';
import Spinner from 'components/ui/spinner';

function DomainsListHeader() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const ipsReport = useSelector(ipsReportDataSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const domainsAvailable = useSelector(domainsAvailableCountSelector);
  const domainsAvailableIsLoading = useSelector(domainsAvailableCountIsLoadingSelector);

  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  const isNotAllowedToRequest =
    myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.NONE ||
    !myUserBillingPlanData?.domains?.domains_per_month ||
    billingPlanIsExpired;

  useEffect(() => {
    if (!isNotAllowedToRequest) {
      dispatch(getDomainsAvailableCount());
    }
  }, [dispatch, isNotAllowedToRequest]);

  return (
    <div className="domains-list-header">
      <h1 className="domains-list__title">
        {t('domains_list.title.text')}
      </h1>

      <div className="domains-list__header-right header-right">
        {!domainsAvailableIsLoading && (billingPlanIsExpired || domainsAvailable <= 0) && (
          <div className="header-right__upgrade-plan">
            <Button
              mode="blue"
              text={t('seedlisting_list.update_plan.btn')}
              className="domains-list__btn"
              onClick={() => history.push('/user/billing')}
            />
          </div>
        )}
        <div className="header-right__config-report">
          <Button
            mode="purple"
            text={t('ips_list.daily_report.btn')}
            className="ips-list__btn"
            onClick={() => dispatch(handleIpsReportIsOnEdit(true))}
            disabled={billingPlanIsExpired}
          />
          <div className="ips-report-status">
            {billingPlanIsExpired && (
              <div className="domains-report-status__value danger">
                {t('domains_list.billing_plan.exipe_warning_text')}
              </div>
            )}
            {!billingPlanIsExpired && (
              <Fragment>
                <span
                  className={cn('ips-report-status__value', {
                    danger: !ipsReport?.is_active,
                    ok: ipsReport?.is_active,
                  })}
                >
                  {ipsReport?.is_active
                    ? t('ips_list.ipsreport.status.on')
                    : t('ips_list.ipsreport.status.off')}
                </span>
              </Fragment>
            )}
          </div>
        </div>
        <div className="header-right__new-domain">
          <Button
            mode="purple"
            text={t('domains_list.add_domain.btn')}
            className="domains-list__btn"
            onClick={() => dispatch(setIsOnNewDomain(true))}
            disabled={billingPlanIsExpired || domainsAvailable <= 0}
          />
          {domainsAvailableIsLoading ? (
            <Spinner />
          ) : (
            <div className="domains-available">
              {billingPlanIsExpired && (
                <div className="domains-available__count danger">
                  {t('domains_list.billing_plan.exipe_warning_text')}
                </div>
              )}
              {!billingPlanIsExpired && (
                <Fragment>
                  <span
                    className={cn('domains-available__count', {
                      danger: domainsAvailable <= 0,
                      warning: domainsAvailable > 0 && domainsAvailable < 25,
                      ok: domainsAvailable >= 25,
                    })}
                  >
                    {domainsAvailable}
                  </span>
                  <span>
                    {' '}
                    {t('domains_list.billing_plan.checks_available_text')}
                  </span>
                </Fragment>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default React.memo(DomainsListHeader);
