import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import Spinner from 'components/ui/spinner';
import Error404 from 'components/ui/error404';

import {
  domainsRblChangesLogsIsLoadingSelector,
  domainsRblChangesLogsSelector,
  domainsErrorsSelector,
  usersMeSelector,
} from 'store/selectors';
import {
  getDomainRblChangesLogs,
  setDomainRblChangesLogs,
  setDomainRblChangesLogsPeriodFrom,
  setDomainRblChangesLogsPeriodTo,
} from 'store/actions/domains';

import {
  DEMO_USER_EMAIL,
  NOT_FOUND_ERROR_STATUS,
} from 'utils/constants';

import DomainDatesRange from './components/DomainDatesRange';
import DomainGraph from './components/DomainGraph';

import './domain-rbl-changes-logs.scss';

const DomainRblChangesLogs = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const { id } = useParams();

  const domainsIpRblChangesLogIsLoading = useSelector(domainsRblChangesLogsIsLoadingSelector);
  const domainsIpRblChangesLog = useSelector(domainsRblChangesLogsSelector);
  const domainsErrors = useSelector(domainsErrorsSelector);

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  useEffect(() => {
    if (id) {
      dispatch(getDomainRblChangesLogs({ id }));
    }
    return () => {
      dispatch(setDomainRblChangesLogsPeriodFrom(''));
      dispatch(setDomainRblChangesLogsPeriodTo(''));
      dispatch(setDomainRblChangesLogs([]));
    }
  }, [dispatch, id]);

  if (domainsErrors?.domainRblChangesLogs?.status === NOT_FOUND_ERROR_STATUS) {
    return <Error404 />;
  }

  return (
    <div className="domain-rblchangeslogs">
      <div className="domain-rblchangeslogs__header">
        <h1 className="domain-rblchangeslogs__title">
          <Link to="/domains">
            {t('domains_logs.title.domain-info.text')}
          </Link>
          {' > '}
          <span
            className={cn('domain-rblchangeslogs__breadcrumbs-current', {
              'domain-rblchangeslogs__breadcrumbs-current--blured': meEmail === DEMO_USER_EMAIL,
            })}
          >
            {domainsIpRblChangesLog?.domain ?? ''}
          </span>
        </h1>
      </div>

      <div className="domain-rblchangeslogs__filter">
        <DomainDatesRange disabled={!domainsIpRblChangesLog?.data?.length || false} />
      </div>

      {domainsIpRblChangesLogIsLoading ? <Spinner /> : <DomainGraph data={domainsIpRblChangesLog?.data || []} />}
    </div>
  );
};

export default React.memo(DomainRblChangesLogs);
