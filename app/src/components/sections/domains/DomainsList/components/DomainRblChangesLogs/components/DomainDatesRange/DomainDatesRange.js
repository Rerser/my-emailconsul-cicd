import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { ru, enUS } from 'date-fns/locale';

import DatePickerRange from 'components/ui/datePickerRange';

import {
  localeCurrentSelector,
  domainsRblChangesLogsPeriodFromSelector,
  domainsRblChangesLogsPeriodToSelector,
} from 'store/selectors';
import {
  setDomainRblChangesLogsPeriodFrom,
  setDomainRblChangesLogsPeriodTo
} from 'store/actions/domains';

import { DAY, LOCALE } from 'utils/constants';

function DomainDatesRange({ disabled }) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const domainsRblChangesLogPeriodFrom = useSelector(domainsRblChangesLogsPeriodFromSelector);
  const domainsRblChangesLogPeriodTo = useSelector(domainsRblChangesLogsPeriodToSelector);

  const handleChangeDateFrom = (date) => {
    if (date) {
      dispatch(setDomainRblChangesLogsPeriodFrom(new Date(date).toISOString()));
    } else {
      dispatch(setDomainRblChangesLogsPeriodFrom(''));
    }
    if (date >= new Date(domainsRblChangesLogPeriodTo)) {
      dispatch(setDomainRblChangesLogsPeriodTo(''));
    }
  };

  const handleChangeDateTo = (date) => {
    if (date) {
      dispatch(setDomainRblChangesLogsPeriodTo(new Date(date).toISOString()));
    } else {
      dispatch(setDomainRblChangesLogsPeriodTo(''));
    }
  };

  return (
    <div className="domain-list__dates-range">
      <DatePickerRange 
        locale={locale === LOCALE.ru ? ru : enUS}

        labelInputFrom={t('domains_logs.dates_range.date_from.text')}
        labelInputTo={t('domains_logs.dates_range.date_to.text')}

        dateFormat="DD-MM-yyyy HH:mm"
        dateFrom={domainsRblChangesLogPeriodFrom}
        dateTo={domainsRblChangesLogPeriodTo}
        handleChangeDateFrom={handleChangeDateFrom}
        handleChangeDateTo={handleChangeDateTo}

        hasTimeSelect
        timeFormat='HH:mm'
        timeIntervals={15}

        maxDate={new Date(Date.now() - DAY).toISOString()}

        disabled={disabled}
        disabledTo={!domainsRblChangesLogPeriodFrom || disabled}
        requiredDateTo={!disabled && !domainsRblChangesLogPeriodTo}
      />
    </div>
  );
}

export default React.memo(DomainDatesRange);
