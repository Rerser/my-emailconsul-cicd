import React, { useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button/Button';
import SlideBar from 'components/ui/slideBar';
import Error404 from 'components/ui/error404';

import './seedlisting-workers.scss';

import { setNewSeedlistingWorker } from 'store/actions/seedlistingWorkers';
import {
  seedlistingWorkersNewSeedlistingWorkerSelector,
  seedlistingWorkersErrorsSelector,
} from 'store/selectors';

import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import TableSeedlistingWorkers from './components/tableSeedlistingWorkers';
import NewSeedlistingWorker from './components/newSeedlistingWorker';

function SeedlistingWorkers() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const seedlistingWorkersNewSeedlistingWorker = useSelector(
    seedlistingWorkersNewSeedlistingWorkerSelector
  );
  const seedlistingWorkersErrors = useSelector(
    seedlistingWorkersErrorsSelector
  );

  const showSlider = useMemo(() => !!seedlistingWorkersNewSeedlistingWorker, [
    seedlistingWorkersNewSeedlistingWorker,
  ]);
  const { data: errorData } = seedlistingWorkersErrors;

  if (errorData?.status === NOT_FOUND_ERROR_STATUS) return <Error404 />;

  return (
    <div className="seedlisting-workers">
      <div className="seedlisting-workers__header">
        <h1 className="seedlisting-workers__title">
          {t('seedlisting_workers.header.text')}
        </h1>

        <div className="seedlisting-workers__header-right">
          <Button
            mode="purple"
            text={t('seedlisting_workers.btn.create.text')}
            className="seedlisting-workers__btn"
            onClick={() => dispatch(setNewSeedlistingWorker({}))}
          />
        </div>
      </div>

      <TableSeedlistingWorkers />

      <SlideBar isOpen={showSlider} onClose={() => dispatch(setNewSeedlistingWorker(null))}>
        <NewSeedlistingWorker />
      </SlideBar>
    </div>
  );
}

export default React.memo(SeedlistingWorkers);
