import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { toast } from 'react-toastify';

import InputText from 'components/ui/inputText';
import Select from 'components/ui/select';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import {
  setNewSeedlistingWorker,
  handleNewSeedlistingWorker,
  addSeedlistingWorker,
  updateSeedlistingWorker,
} from 'store/actions/seedlistingWorkers';
import {
  seedlistingWorkersNewSeedlistingWorkerSelector,
  seedlistingWorkersNewSeedlistingWorkerSavingIsLoadingSelector,
  seedlistingWorkersHostsSelector,
} from 'store/selectors';

const NewSeedlistingWorker = (props) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const newSeedlistingWorker = useSelector(
    seedlistingWorkersNewSeedlistingWorkerSelector
  );
  const newSeedlistingWorkerSavingIsLoading = useSelector(
    seedlistingWorkersNewSeedlistingWorkerSavingIsLoadingSelector
  );
  const seedlistingWorkersHosts = useSelector(seedlistingWorkersHostsSelector);

  const isOnEdit = !!newSeedlistingWorker?.id;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isOnEdit) {
      dispatch(updateSeedlistingWorker(newSeedlistingWorker));
    } else {
      dispatch(addSeedlistingWorker(newSeedlistingWorker));
    }
  };

  return (
    <form className="c-slidebar-content" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {`${
          isOnEdit
            ? t('new.seedlisting_worker.header.update.text')
            : t('new.seedlisting_worker.header.new.text')
        }`}
        {' '}
        {t('new.seedlisting_worker.header.end.text')}
      </h3>

      {newSeedlistingWorkerSavingIsLoading && <Spinner />}

      {!newSeedlistingWorkerSavingIsLoading && newSeedlistingWorker && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('new.seedlisting_worker.email.label')}
              </span>
              <InputText
                value={newSeedlistingWorker.email}
                onChange={(e) =>
                  dispatch(
                    handleNewSeedlistingWorker({
                      field: 'email',
                      value: e.target.value,
                    })
                  )
                }
              />
            </label>
          </div>

          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('new.seedlisting_worker.password.label')}
              </span>
              <InputText
                value={newSeedlistingWorker.password}
                onChange={(e) =>
                  dispatch(
                    handleNewSeedlistingWorker({
                      field: 'password',
                      value: e.target.value,
                    })
                  )
                }
              />
            </label>
          </div>

          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('new.seedlisting_worker.host.label')}
              </span>
              <Select
                value={newSeedlistingWorker.host}
                options={seedlistingWorkersHosts.map((host) => ({
                  value: host,
                  label: host,
                }))}
                onChange={(value) =>
                  dispatch(handleNewSeedlistingWorker({ field: 'host', value }))
                }
                placeholder={t(
                  'new.seedlisting_worker.select.host.placeholder.text'
                )}
              />
            </label>
          </div>

          <div className="c-slidebar-content__row">
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('new.seedlisting_worker.worker_container.label')}
              </span>
              <InputText
                value={newSeedlistingWorker.worker_container}
                onChange={(e) => {
                  if (!/^[\d]*$/.test(e.target.value)) {
                    return toast.error(
                      t(
                        'new.seedlisting_worker.number_of_checks.input.only_number_error'
                      )
                    );
                  }
                  dispatch(
                    handleNewSeedlistingWorker({
                      field: 'worker_container',
                      value: e.target.value,
                    })
                  );
                }}
                disabled={isOnEdit}
              />
            </label>
          </div>

          <div className="c-slidebar-content__btns">
            {newSeedlistingWorker && (
              <Button
                mode="purple"
                text={
                  isOnEdit
                    ? t('new.seedlisting_worker.btn.update.text')
                    : t('new.seedlisting_worker.btn.create.text')
                }
                className="c-slidebar-content__btn"
                disabled={
                  !newSeedlistingWorker.email ||
                  !newSeedlistingWorker.password ||
                  !newSeedlistingWorker.host ||
                  !newSeedlistingWorker.worker_container
                }
                type="submit"
              />
            )}

            <Button
              text={t('new.seedlisting_worker.btn.close.text')}
              className="c-slidebar-content__btn"
              onClick={() => dispatch(setNewSeedlistingWorker(null))}
            />
          </div>
        </Fragment>
      )}
    </form>
  );
};

export default React.memo(NewSeedlistingWorker);
