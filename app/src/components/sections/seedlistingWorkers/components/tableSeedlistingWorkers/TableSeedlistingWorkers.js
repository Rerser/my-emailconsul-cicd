import React, { useEffect, useMemo, Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Table from 'components/ui/table';
import Spinner from 'components/ui/spinner/Spinner';
import Pagination from 'components/ui/pagination';
import ShowHideContent from 'components/ui/showHideContent';

import {
  getSeedlistingWorkersData,
  setSeedlistingWorkersDataPagination,
  getSeedlistingWorkersHosts,
  setSeedlistingWorkersSorting,
  setNewSeedlistingWorker,
  deleteSeedlistingWorker,
  restartSeedlistingWorker,
  terminateSeedlistingWorker,
  restartAllSeedlistingWorkers,
  terminateAllSeedlistingWorkers,
  handleSeedlistingWorkersDataIsLoading,
} from 'store/actions/seedlistingWorkers';
import {
  seedlistingWorkersDataSelector,
  seedlistingWorkersIsDataLoadingSelector,
  seedlistingWorkersSortingSelector,
  seedlistingWorkersDataPaginationSelector,
} from 'store/selectors';

import { usePagination } from 'utils/helpers';

function TableSeedlistingWorkers() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const seedlistingWorkersData = useSelector(seedlistingWorkersDataSelector);
  const seedlistingWorkersIsDataLoading = useSelector(
    seedlistingWorkersIsDataLoadingSelector
  );

  const seedlistingWorkersSorting = useSelector(
    seedlistingWorkersSortingSelector
  );
  const seedlistingWorkersDataPagination = useSelector(
    seedlistingWorkersDataPaginationSelector
  );

  const tableData = useMemo(
    () =>
      seedlistingWorkersData.map((item, i) => ({
        ...item,
        payload: (
          <ShowHideContent key={`payload-${i}`} >
            {item.payload}
          </ShowHideContent>
        )
      })),
    [seedlistingWorkersData]
  );
  const isLoading = useMemo(() => seedlistingWorkersIsDataLoading, [
    seedlistingWorkersIsDataLoading,
  ]);

  const { limit, offset } = seedlistingWorkersDataPagination;
  const { field: sortField, order: sortOrder } = seedlistingWorkersSorting;

  useEffect(() => {
    dispatch(getSeedlistingWorkersHosts());
    dispatch(handleSeedlistingWorkersDataIsLoading(true));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getSeedlistingWorkersData({ withLoader: false }));

    const interval = setInterval(() => {
      dispatch(getSeedlistingWorkersData({ withLoader: false }));
    }, 5000);

    return () => clearInterval(interval);
  }, [dispatch, limit, offset, sortField, sortOrder]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: seedlistingWorkersDataPagination,
    action: setSeedlistingWorkersDataPagination,
  });

  if (isLoading) return <Spinner />;

  // const tableData = seedlistingWorkersData.map(item => ({
  //   ...item
  // }));

  return (
    <Fragment>
      <Pagination
        page={paginal.page}
        pages={paginal.pages}
        pageSize={paginal.pageSize}
        handlePageSize={handlePageSize}
        handlePage={handlePage}
        className="ips-list__pagination ips-list__pagination--top"
      />

      <Table
        columns={[
          {
            field: 'email',
            title: t('seedlisting_workers.table.email'),
            type: 'string',
            width: 200,
          },
          {
            field: 'password',
            title: t('seedlisting_workers.table.password'),
            type: 'string',
            width: 200,
          },
          {
            field: 'host',
            title: t('seedlisting_workers.table.host'),
            type: 'string',
            width: 100,
          },
          {
            field: 'worker_container',
            title: t('seedlisting_workers.table.worker_container'),
            type: 'string',
            width: 170,
          },
          {
            field: 'status',
            title: t('seedlisting_workers.table.status'),
            type: 'string',
            width: 100,
          },
          {
            field: 'payload',
            title: t('seedlisting_workers.table.payload'),
            type: 'string',
            width: 500,
          },
        ]}
        data={tableData}
        cbs={{
          onEdit: (data) => dispatch(setNewSeedlistingWorker(data)),
          onUpdate: (data) => dispatch(restartSeedlistingWorker(data)),
          onTerminate: (data) => dispatch(terminateSeedlistingWorker(data)),
          onDelete: (data) => dispatch(deleteSeedlistingWorker(data)),
        }}
        headerCbs={{
          dump: (data) => false,
          onUpdate: (data) => dispatch(restartAllSeedlistingWorkers(data)),
          onTerminate: (data) => dispatch(terminateAllSeedlistingWorkers(data)),
          dump2: (data) => false,
        }}
        sorting={{
          ...seedlistingWorkersSorting,
          isApiSorting: true,
        }}
        onSorting={(data) => dispatch(setSeedlistingWorkersSorting(data))}
        verticalPositionContentCell="up"
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ips-list__pagination ips-list__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(TableSeedlistingWorkers);
