import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import {
  getSpfData,
  setSpfData,
  setSpfDataError
} from 'store/actions/spf';
import {
  spfDataIsLoadingSelector,
  spfDataSelector
} from 'store/selectors';

import withCheckUserBP from 'components/hocs/withCheckUserBP';
import SPF from 'components/sections/seedlisting/seedlistingItem/components/seedlistingEmailInfo/components/emailInfo/components/emailAuthentication/components/SPF';
import Button from 'components/ui/button/Button';
import InputText from 'components/ui/inputText';
import Spinner from 'components/ui/spinner';

import { validateDomainAddress } from 'utils/helpers';

import './spf-page.scss';

function SPFPage() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const [domain, setDomain] = useState('');
  const [inputError, setInputError] = useState('');

  useEffect(() => {
    return () => {
      dispatch(setSpfData(null));
      dispatch(setSpfDataError(null));
    }
  }, [dispatch]);

  const spfData = useSelector(spfDataSelector);
  const spfDataIsLoading = useSelector(spfDataIsLoadingSelector);

  const handleChangeDomain = (value) => {
    if (inputError) {
      setInputError('');
    }
    setDomain(value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!inputError && validateDomainAddress(domain)) {
      setInputError('');
      return dispatch(getSpfData(domain))
    }
    return setInputError(t('spf-page.input.domain.error.text'));
  }

  return (
    <div className="spf-page">
      <div className="spf-page__header">
        <h1 className="spf-page__title">
          {t('spf-page.title')}
        </h1>

        <form className="spf-page__header-right header-right" onSubmit={handleSubmit}>
          <InputText
            className={cn("header-right__input", {
              "header-right__input--error": inputError,
            })}
            placeholder={t('spf-page.input.domain.placeholder.text')}
            value={domain}
            onChange={(e) => handleChangeDomain(e.target.value)}
            errorText={inputError}
          />
          <Button
            type="submit"
            mode="purple"
            text={t('spf-page.btn.check.text')}
            className="header-right__btn"
            disabled={Boolean(!domain || inputError || spfDataIsLoading)}
          />
        </form>
      </div>
      <div className="spf-page__data-wrapper">
        {
          spfData
            ? <SPF data={spfData} isLoading={spfDataIsLoading}/>
            : spfDataIsLoading
              ? <Spinner />
              : null
        }
      </div>
    </div>
  );
}

export default React.memo(withCheckUserBP(SPFPage));
