import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import ReCAPTCHA from 'react-google-recaptcha';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';
import Button from 'components/ui/button';

import './auth.scss';

import {
  authSetEmail,
  authSetSetIsRecaptchaVerified,
  authRecoverPassword,
} from 'store/actions/auth';
import {
  authEmailSelector,
  authIsRecaptchaVerifiedSelector,
  authIsLoadingSelector,
} from 'store/selectors';

function RecoverPassword(props) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const email = useSelector(authEmailSelector);
  const isRecaptchaVerified = useSelector(authIsRecaptchaVerifiedSelector);
  const isLoading = useSelector(authIsLoadingSelector);

  const handleUserEmailChange = (event) =>
    dispatch(authSetEmail(event.target.value));
  const handleIsRecaptchaVerified = (value) =>
    dispatch(authSetSetIsRecaptchaVerified(value));

  const recoverPassword = (e) => {
    e.preventDefault();

    dispatch(
      authRecoverPassword({
        email,
        isRecaptchaVerified,
      })
    );
  };

  return (
    <div className="auth">
      <form className="auth__form">
        <h1 className="auth__title">
          {t('recover_password.title')}
        </h1>

        {isLoading && <Spinner />}

        {!isLoading && (
          <Fragment>
            <div className="auth__form-row">
              <label className="auth__form-label" htmlFor="email">
                {t('recover_password.email.label')}
              </label>
              <input
                className="auth__form-input"
                type="email"
                name="email"
                placeholder={t('recover_password.email.input.placeholder')}
                value={email || ''}
                onChange={handleUserEmailChange}
              />
            </div>

            <div className="auth__form-row auth__form-row--recaptcha">
              <ReCAPTCHA
                sitekey="6Leh9dkUAAAAAGLNQylX-COxokVCwURL3mkflBOQ"
                onChange={() => handleIsRecaptchaVerified(true)}
                size={window.screen.width < 768 ? 'compact' : 'normal'}
              />
            </div>

            <div className="auth__form-row">
              <Button
                mode="purple"
                text={t('recover_password.btn.text')}
                className="auth__form-btn"
                onClick={recoverPassword}
                type="submit"
              />
            </div>

            <div className="auth__footer">
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.login.question')}
                </span>
                <Link to="/login" className="auth__footer-link">
                  {t('auth.login.link')}
                </Link>
              </div>
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.signup.question')}
                </span>
                <Link to="/signup" className="auth__footer-link">
                  {t('auth.signup.link')}
                </Link>
              </div>
            </div>
          </Fragment>
        )}
      </form>
    </div>
  );
}

export default RecoverPassword;
