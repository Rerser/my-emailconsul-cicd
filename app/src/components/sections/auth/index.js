import Login from './Login';
import SignUp from './SignUp';
import Activation from './Activation';
import RecoverPassword from './RecoverPassword';

export { Login, SignUp, Activation, RecoverPassword };
