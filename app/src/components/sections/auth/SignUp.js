import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import ReCAPTCHA from 'react-google-recaptcha';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import './auth.scss';

import {
  authSetEmail,
  authSetPassword,
  authSetConfirmPassword,
  authSetSetIsRecaptchaVerified,
  authSignUp,
} from 'store/actions/auth';
import {
  authEmailSelector,
  authPasswordSelector,
  authConfirmPasswordSelector,
  authIsRecaptchaVerifiedSelector,
  authIsLoadingSelector,
} from 'store/selectors';

function SignUp(props) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const email = useSelector(authEmailSelector);
  const password = useSelector(authPasswordSelector);
  const confirmPassword = useSelector(authConfirmPasswordSelector);
  const isRecaptchaVerified = useSelector(authIsRecaptchaVerifiedSelector);
  const isLoading = useSelector(authIsLoadingSelector);

  const handleUserEmailChange = (event) =>
    dispatch(authSetEmail(event.target.value));
  const handleUserPasswordChange = (event) =>
    dispatch(authSetPassword(event.target.value));
  const handleUserConfirmPasswordChange = (event) =>
    dispatch(authSetConfirmPassword(event.target.value));
  const handleIsRecaptchaVerified = (value) =>
    dispatch(authSetSetIsRecaptchaVerified(value));

  const signUp = (e) => {
    e.preventDefault();

    dispatch(
      authSignUp({
        email,
        password,
        confirmPassword,
        isRecaptchaVerified,
      })
    );
  };

  return (
    <div className="auth">
      <form className="auth__form">
        <h1 className="auth__title">
          {t('signup.title')}
        </h1>

        {isLoading && <Spinner />}

        {!isLoading && (
          <Fragment>
            <div className="auth__form-row">
              <label className="auth__form-label" htmlFor="email">
                {t('signup.email.label')}
              </label>
              <input
                className="auth__form-input"
                type="email"
                name="email"
                placeholder={t('signup.email.input.placeholder')}
                value={email || ''}
                onChange={handleUserEmailChange}
              />
            </div>

            <div className="auth__form-row">
              <label className="auth__form-label" htmlFor="password">
                {t('signup.password.label')}
              </label>
              <input
                className="auth__form-input"
                type="password"
                name="password"
                placeholder={t('signup.password.input.placeholder')}
                value={password || ''}
                onChange={handleUserPasswordChange}
              />
            </div>

            <div className="auth__form-row">
              <label className="auth__form-label" htmlFor="password2">
                {t('signup.password.confirm.label')}
              </label>
              <input
                className="auth__form-input"
                type="password"
                name="password2"
                placeholder={t('signup.password.confirm.input.placeholder')}
                value={confirmPassword || ''}
                onChange={handleUserConfirmPasswordChange}
              />
            </div>

            <div className="auth__form-row auth__form-row--recaptcha">
              <ReCAPTCHA
                sitekey="6Leh9dkUAAAAAGLNQylX-COxokVCwURL3mkflBOQ"
                onChange={() => handleIsRecaptchaVerified(true)}
                size={window.screen.width < 768 ? 'compact' : 'normal'}
              />
            </div>

            <div className="auth__form-row">
              <Button
                mode="purple"
                text={t('signup.btn.text')}
                className="auth__form-btn"
                onClick={signUp}
                type="submit"
              />
            </div>

            <div className="auth__footer">
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.login.question')}
                </span>
                <Link to="/login" className="auth__footer-link">
                  {t('auth.login.link')}
                </Link>
              </div>
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.recover_password.question')}
                </span>
                <Link to="/recover_password" className="auth__footer-link">
                  {t('auth.recover_password.link')}
                </Link>
              </div>
            </div>
          </Fragment>
        )}
      </form>
    </div>
  );
}

export default SignUp;
