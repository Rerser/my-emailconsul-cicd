import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';
import Button from 'components/ui/button';
import { getQS } from 'utils/helpers';
import { AUTH_ACTIVATION_RESULT_STATUS } from 'utils/constants';

import './auth.scss';

import { authActivation } from 'store/actions/auth';
import {
  authIsLoadingSelector,
  authActivationResultStatusSelector,
} from 'store/selectors';

function Activation(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const t = useFormatMessage();

  const isLoading = useSelector(authIsLoadingSelector);
  const activationResultStatus = useSelector(
    authActivationResultStatusSelector
  );

  useEffect(() => {
    const activationId = getQS('activation_id');
    if (activationId) {
      dispatch(authActivation({ activationId }));
    }
  }, [dispatch]);

  return (
    <div className="auth">
      <form className="auth__form">
        <h1 className="auth__title">
          {t('activation.title')}
        </h1>

        {isLoading && <Spinner />}

        {!isLoading && (
          <Fragment>
            {activationResultStatus ===
              AUTH_ACTIVATION_RESULT_STATUS.SUCCESS && (
              <div className="auth__form-row">
                <div className="activation">
                  <div className="activation__title activation__title--success">
                    {t('activation.success.title')}
                  </div>
                  <div className="activation__description">
                    {t('activation.success.desc.1')} 
                    {' '}
                    <br />
                    {' '}
                    {t('activation.success.desc.2')}
                  </div>
                  <div className="auth__form-row">
                    <Button
                      mode="purple"
                      text={t('activation.success.btn')}
                      className="auth__form-btn"
                      onClick={() => history.push('/login')}
                    />
                  </div>
                </div>
              </div>
            )}

            {activationResultStatus ===
              AUTH_ACTIVATION_RESULT_STATUS.FAILED && (
              <div className="auth__form-row">
                <div className="activation">
                  <div className="activation__title activation__title--error">
                    {t('activation.fail.title')}
                  </div>
                  <div className="activation__description">
                    {t('activation.fail.desc')}
                  </div>
                </div>
              </div>
            )}

            <div className="auth__footer">
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.login.question')}
                </span>
                <Link to="/login" className="auth__footer-link">
                  {t('auth.login.link')}
                </Link>
              </div>
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.recover_password.question')}
                </span>
                <Link to="/recover_password" className="auth__footer-link">
                  {t('auth.recover_password.link')}
                </Link>
              </div>
            </div>
          </Fragment>
        )}
      </form>
    </div>
  );
}

export default Activation;
