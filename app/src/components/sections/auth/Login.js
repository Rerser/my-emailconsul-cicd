import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import './auth.scss';

import { authSetEmail, authSetPassword, authLogin } from 'store/actions/auth';
import {
  authEmailSelector,
  authPasswordSelector,
  authIsLoadingSelector,
} from 'store/selectors';

function Login(props) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const email = useSelector(authEmailSelector);
  const password = useSelector(authPasswordSelector);
  const isLoading = useSelector(authIsLoadingSelector);

  const handleUserEmailChange = (event) =>
    dispatch(authSetEmail(event.target.value));
  const handleUserPasswordChange = (event) =>
    dispatch(authSetPassword(event.target.value));

  const login = (e) => {
    e.preventDefault();

    dispatch(
      authLogin({
        email,
        password,
      })
    );
  };

  return (
    <div className="auth">
      <form className="auth__form">
        <h1 className="auth__title">
          {t('login.title')}
        </h1>

        {isLoading && <Spinner />}

        {!isLoading && (
          <Fragment>
            <div className="auth__form-row">
              <label className="auth__form-label" htmlFor="email">
                {t('login.email.label')}
              </label>
              <input
                className="auth__form-input"
                type="email"
                name="email"
                placeholder={t('login.email.input.placeholder')}
                value={email || ''}
                onChange={handleUserEmailChange}
              />
            </div>

            <div className="auth__form-row">
              <label className="auth__form-label" htmlFor="password">
                {t('login.password.label')}
              </label>
              <input
                className="auth__form-input"
                type="password"
                name="password"
                placeholder={t('login.password.input.placeholder')}
                value={password || ''}
                onChange={handleUserPasswordChange}
              />
            </div>

            <div className="auth__form-row">
              <Button
                mode="purple"
                text={t('login.btn.text')}
                className="auth__form-btn"
                onClick={login}
                type="submit"
              />
            </div>

            <div className="auth__footer">
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.signup.question')}
                </span>
                <Link to="/signup" className="auth__footer-link">
                  {t('auth.signup.link')}
                </Link>
              </div>
              <div className="auth__footer-block">
                <span className="auth__footer-label">
                  {t('auth.recover_password.question')}
                </span>
                <Link to="/recover_password" className="auth__footer-link">
                  {t('auth.recover_password.link')}
                </Link>
              </div>
            </div>
          </Fragment>
        )}
      </form>
    </div>
  );
}

export default Login;
