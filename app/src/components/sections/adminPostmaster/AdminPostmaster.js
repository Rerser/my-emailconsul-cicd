import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button/Button';
import { getAdminPostmasterAuthUrl, setAdminPostmasterAuthUrl } from 'store/actions/adminPostmaster';
import { adminPostmasterAuthUrlIsLoadingSelector, adminPostmasterAuthUrlSelector } from 'store/selectors';

import Spinner from 'components/ui/spinner';
import toasts from 'utils/toasts';
import { gotoExternalLink } from 'utils/helpers';

import './admin-postmaster.scss';

function AdminPostmaster() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const adminPostmasterAuthUrl = useSelector(adminPostmasterAuthUrlSelector);
  const adminPostmasterAuthUrlIsLoading = useSelector(adminPostmasterAuthUrlIsLoadingSelector);

  useEffect(() => {
    if (adminPostmasterAuthUrl && !adminPostmasterAuthUrlIsLoading) {
      try {
        gotoExternalLink(adminPostmasterAuthUrl);
        dispatch(setAdminPostmasterAuthUrl(null));
      } catch (error) {
        toasts.error(t('admin_postmaster.toast.create_new_window.error'));
      }
    }
  }, [dispatch, t, adminPostmasterAuthUrl, adminPostmasterAuthUrlIsLoading]);

  return (
    <div className="admin-postmaster">
      <div className="admin-postmaster__header">
        <h1 className="admin-postmaster__title">
          {t('admin_postmaster.title')}
        </h1>

        <div className="admin-postmaster__header-right header-right">
          {adminPostmasterAuthUrlIsLoading ? (
            <Spinner />
          ) : (
            <Button
              mode="purple"
              text={t('admin_postmaster.google_postmaster.btn.text')}
              className="admin-postmaster__btn"
              onClick={() => dispatch(getAdminPostmasterAuthUrl())}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default React.memo(AdminPostmaster);
