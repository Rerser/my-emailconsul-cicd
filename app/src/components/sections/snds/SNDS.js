import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import SlideBar from 'components/ui/slideBar';
import Error404 from 'components/ui/error404';
import SNDSHeader from './components/SNDSHeader';
import SNDSCredentials from './components/SNDSCredentials';
import SNDSIpStatusTable from './components/SNDSIpStatusTable';
import SNDSIpDataTable from './components/SNDSIpDataTable';
import SNDSDashboards from './components/SNDSDashboards';
import SNDSCheckDateRun from './components/SNDSCheckDateRun';
import SNDSHistory from './components/SNDSHistory';

import {
  clearSndsAllData,
  getSndsUserIps,
} from 'store/actions/snds';
import {
  clearSndsIntegrationAllData,
  getSndsCredentials,
  setSndsDateRunIsShowSlider,
  setSndsHistoryIsShowSlider,
  setSndsIsOnAddKey,
} from 'store/actions/sndsIntegration';
import {
  sndsErrorsSelector,
  sndsInfoModeSelector,
  sndsIntegrationDateRunIsShowSliderSelector,
  sndsIntegrationHistoryIsShowSliderSelector,
  sndsIsOnAddKeySelector,
} from 'store/selectors';

import {
  SNDS_INFO_MODE,
  NOT_FOUND_ERROR_STATUS,
} from 'utils/constants';

import './snds.scss';

function SNDS() {
  const dispatch = useDispatch();

  const sndsIsOnAddKey = useSelector(sndsIsOnAddKeySelector);
  const sndsErrors = useSelector(sndsErrorsSelector);
  const sndsInfoMode = useSelector(sndsInfoModeSelector);
  
  const sndsIntegrationHistoryIsShowSlider = useSelector(sndsIntegrationHistoryIsShowSliderSelector);
  const sndsIntegrationDateRunIsShowSlider = useSelector(sndsIntegrationDateRunIsShowSliderSelector);

  useEffect(() => {
    dispatch(getSndsCredentials({ withLoader: true }));

    return () => {
      dispatch(clearSndsAllData());
      dispatch(clearSndsIntegrationAllData());
    };
  }, [dispatch]);

  useEffect(() => {
    dispatch(getSndsUserIps());
  }, [dispatch, sndsInfoMode]);

  if (
    Object.values(sndsErrors)
      .map(errValue => errValue?.status)
      .some(status => status === NOT_FOUND_ERROR_STATUS)
  ) {
    return <Error404 />;
  }

  return (
    <div className="snds">
      <SNDSHeader />

      {sndsInfoMode === SNDS_INFO_MODE.DASHBOARDS && (
        <div className="snds__dashboards snds-dashboards">
          <SNDSDashboards />
        </div>
      )}
      {sndsInfoMode === SNDS_INFO_MODE.IP_STATUS && (
        <div className="snds__ip-status ip-status">
          <SNDSIpStatusTable />
        </div>
      )}
      {sndsInfoMode === SNDS_INFO_MODE.IP_DATA && (
        <div className="snds__ip-data ip-data">
          <SNDSIpDataTable />
        </div>
      )}

      <SlideBar
        isOpen={sndsIsOnAddKey}
        onClose={() => dispatch(setSndsIsOnAddKey(false))}
        className="snds__credentials"
      >
        <SNDSCredentials />
      </SlideBar>

      <SlideBar
        isOpen={sndsIntegrationDateRunIsShowSlider}
        onClose={() => dispatch(setSndsDateRunIsShowSlider(false))}
        className="snds__date-run"
      >
        <SNDSCheckDateRun />
      </SlideBar>

      <SlideBar
        isOpen={sndsIntegrationHistoryIsShowSlider}
        onClose={() => dispatch(setSndsHistoryIsShowSlider(false))}
        className="snds__history"
      >
        <SNDSHistory />
      </SlideBar>
    </div>
  );
}

export default React.memo(SNDS);
