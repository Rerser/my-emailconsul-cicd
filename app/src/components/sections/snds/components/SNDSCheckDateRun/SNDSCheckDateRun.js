import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DatePicker from 'react-datepicker';
import { ru, enUS } from 'date-fns/locale';

import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import {
  localeCurrentSelector,
  sndsIntegrationDateRunSelector,
  sndsIntegrationDateRunIsLoadingSelector,
} from 'store/selectors';
import {
  sendSndsDateRun,
  setSndsDateRun,
  setSndsDateRunIsShowSlider,
} from 'store/actions/sndsIntegration';

import { LOCALE, YESTERDAY } from 'utils/constants';

import './snds-check-date-run.scss';

const SNDSDateRun = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const sndsIntegrationDateRun = useSelector(sndsIntegrationDateRunSelector);
  const sndsIntegrationDateRunIsLoading = useSelector(sndsIntegrationDateRunIsLoadingSelector);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(sendSndsDateRun({ date: sndsIntegrationDateRun}));
  };

  return (
    <form className="c-slidebar-content c-snds-date-run" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('snds.date-run.title')}
      </h3>

      {
        sndsIntegrationDateRunIsLoading
          ? <Spinner />
          : (
            <div className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('snds.date-run.datepicker.label')}
              </span>
              <DatePicker
                dateFormat="dd-MM-yyyy"
                selected={sndsIntegrationDateRun ? new Date(sndsIntegrationDateRun) : null}
                onChange={(value) => 
                  dispatch(
                    setSndsDateRun(new Date(new Date(value).setUTCHours(0, 0, 0, 0)).toISOString())
                  )
                }
                customInput={
                  <InputText />
                }
                maxDate={new Date(YESTERDAY)}
                wrapperClassName="c-snds-date-run__datepicker-wrap"
                placeholderText="dd-MM-yyyy"
                popperClassName="c-slidebar-content__datepicker c-slidebar-content__datepicker--popper"
                openToDate={sndsIntegrationDateRun ? new Date(sndsIntegrationDateRun) : null}
                locale={locale === LOCALE.ru ? ru : enUS}
              />
            </div>
          )
      }

      <div className="c-slidebar-content__btns">
        <Button
          mode="purple"
          text={t('snds.date-run.button.start.text')}
          className="c-slidebar-content__btn"
          type="submit"
          disabled={sndsIntegrationDateRunIsLoading}
        />
        <Button
          text={t('snds.date-run.button.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setSndsDateRunIsShowSlider(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(SNDSDateRun);
