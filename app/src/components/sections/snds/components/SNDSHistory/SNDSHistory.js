import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import {
  sndsIntegrationHistorySelector,
  sndsIntegrationHistoryIsLoadingSelector,
  sndsIntegrationHistoryIsLoadedSelector,
} from 'store/selectors';
import {
  getSndsHistory,
  setSndsHistoryIsShowSlider,
} from 'store/actions/sndsIntegration';

import {
  convertDateTime,
  getFormattedDaysMMDDFromDDMMYY,
} from 'utils/helpers';
import { SNDS_HISTORY_STATUS } from 'utils/constants';

import './snds-history.scss';

const SNDSHistory = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const sndsIntegrationHistory = useSelector(sndsIntegrationHistorySelector);
  const sndsIntegrationHistoryIsLoading = useSelector(sndsIntegrationHistoryIsLoadingSelector);
  const sndsIntegrationHistoryIsLoaded = useSelector(sndsIntegrationHistoryIsLoadedSelector);

  const limit = 5;
  const offset = 0;

  useEffect(() => {
    dispatch(getSndsHistory({ limit, offset }));
  }, [dispatch, limit, offset]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(getSndsHistory({ withLoader: false, limit, offset }));
  };

  return (
    <form className="c-slidebar-content c-snds-history" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('snds.history.title')}
      </h3>

      {
        sndsIntegrationHistoryIsLoading
          ? <Spinner />
          : (
            <ul className="c-slidebar-content__label c-snds-history__list">
              {sndsIntegrationHistory.map((sndsHistory, i) => (
                <li key={`sndsHistory-${i}`} className="c-snds-history__item">
                  <div className="c-snds-history__info-row">
                    <span className="c-snds-history__date-key">
                      {getFormattedDaysMMDDFromDDMMYY(sndsHistory.date_key)}
                    </span>
                    {' - '}
                    <span className={cn("c-snds-history__status", {
                      'c-snds-history__status--done': sndsHistory.status === SNDS_HISTORY_STATUS.DONE,
                      'c-snds-history__status--failed': sndsHistory.status === SNDS_HISTORY_STATUS.FAILED,
                      'c-snds-history__status--is-progress': sndsHistory.status === SNDS_HISTORY_STATUS.IN_PROGRESS,
                    })}>
                      {sndsHistory.status}
                    </span>
                  </div>
                  <div className="c-snds-history__key">
                    <span className="c-snds-history__text">
                      {t('snds.history.key.text')}
                      {': '}
                      <span className="c-snds-history__value">
                        {sndsHistory.key}
                      </span>
                    </span>
                  </div>
                  {
                    sndsHistory.status === SNDS_HISTORY_STATUS.FAILED && sndsHistory.payload && (
                      <div className="c-snds-history__info-row">
                        <span className="c-snds-history__text c-snds-history__text--error">
                          {sndsHistory.payload}
                        </span>
                      </div>
                    )
                  }
                  <div className="c-snds-history__info-row">
                    <span className="c-snds-history__text">
                      {t('snds.history.started_at.text')}
                    </span>
                    {' '}
                    <span className="c-snds-history__value">
                      {convertDateTime(sndsHistory.created_at, 'DD-MM-YYYY hh:mm')}
                    </span>
                  </div>
                  {
                    (sndsHistory.status === SNDS_HISTORY_STATUS.DONE || sndsHistory.status === SNDS_HISTORY_STATUS.FAILED) && (
                      <div className="c-snds-history__info-row">
                        <span className="c-snds-history__text">
                          {t('snds.history.done_at.text')}
                        </span>
                        {' '}
                        <span className="c-snds-history__value">
                          {convertDateTime(sndsHistory.updated_at, 'DD-MM-YYYY hh:mm')}
                        </span>
                      </div>
                    )
                  }
                  {i !== sndsIntegrationHistory.length - 1 && <div className="c-slidebar-content__div-line" /> }
                </li>
              ))}
            </ul>
          )
      }

      <div className="c-slidebar-content__btns">
        
        <Button
          mode="blue"
          text={t('snds.history.button.refresh.text')}
          className="c-slidebar-content__btn"
          type="submit"
          hasRefreshIcon
          disabled={!sndsIntegrationHistoryIsLoaded}
          isRotateAnimation={!sndsIntegrationHistoryIsLoaded}
        />
        <Button
          text={t('snds.history.button.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setSndsHistoryIsShowSlider(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(SNDSHistory);
