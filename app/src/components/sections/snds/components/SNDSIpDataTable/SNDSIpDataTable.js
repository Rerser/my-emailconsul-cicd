import React, { Fragment, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Pagination from 'components/ui/pagination';
import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';
import SNDSIpDataFilters from './components/SNDSIpDataFilters';
import SNDSIpDataLoadingInfo from './components/SNDSIpDataLoadingInfo';

import {
  getSndsIpData,
  handleSndsIpDataSorting,
  setSndsIpDataPagination,
} from 'store/actions/snds';
import {
  sndsIpDataDateFromSelector,
  sndsIpDataDateToSelector,
  sndsIpDataFilterSelector,
  sndsIpDataPaginationSelector,
  sndsIpDataSelector,
  sndsIpDataSortingSelector,
  sndsIsIpDataLoadingSelector,
  usersMeSelector,
} from 'store/selectors';

import {
  DEMO_USER_EMAIL,
  SNDS_FILTER_RESULT,
} from 'utils/constants';
import {
  convertDateTime,
  useDebounce,
  usePagination,
} from 'utils/helpers';

import './snds-ip-data-table.scss';

function SNDSIpDataTables() {
  const t = useFormatMessage();
  const dispatch = useDispatch();

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;
 
  const sndsIsIpDataLoading = useSelector(sndsIsIpDataLoadingSelector);
  const sndsIpData = useSelector(sndsIpDataSelector);
  const sndsIpDataFilter = useSelector(sndsIpDataFilterSelector);
  const sndsIpDataPagination = useSelector(sndsIpDataPaginationSelector);
  const sndsIpDataSorting = useSelector(sndsIpDataSortingSelector);

  const sndsIpDataDateFrom = useSelector(sndsIpDataDateFromSelector);
  const sndsIpDataDateTo = useSelector(sndsIpDataDateToSelector);

  const { ips: filterIps, filterResult } = sndsIpDataFilter;
  const { limit, offset } = sndsIpDataPagination;
  const { field: sortField, order: sortOrder } = sndsIpDataSorting;

  const debouncedFilterIp = useDebounce(filterIps, 1000);
  const debouncedFilterResult = useDebounce(filterResult, 1000);

  useEffect(() => {
    dispatch(getSndsIpData({ withLoader: false }));
  }, [
    dispatch,
    limit, offset, sortField, sortOrder,
    debouncedFilterIp, debouncedFilterResult,
    sndsIpDataDateFrom, sndsIpDataDateTo,
  ]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: sndsIpDataPagination,
    action: setSndsIpDataPagination,
  });

  const tableData = useMemo(() => 
    sndsIpData?.map(item => {
      const {
        ip: itemIp = '',
        dns_reverse_result: itemRdns = [],
        filter_result: itemFilterResult = '',
        date_timestamp: itemDate = '',
        recipients: itemRecipients = 0,
        bounce: itemBounce = 0,
        bounce_rate: itemBounceRate = 0,
        complaint_rate: itemComplaintRate = 0,
        trap_hits: itemTrapHits = 0,
        data_commands: itemDataCommands = 0,
        rcpt_commands: itemRcptCommands = 0,
      } = item;
      
      const ip = (
        <div className="ip-data__column">
          {itemIp}
        </div>
      );
      const dns_reverse_result = (
        <div className="ip-data__column">
          {itemRdns.map(({ rdns = ''}, i) => (
            <div key={`dns-reserve-result-ip-data-${rdns}-${i}`}>
              {rdns}
            </div>
          ))}
        </div>
      );
      const filter_result = (
        <div className="ip-data__column">
          <div className={cn({
            'ip-data__filter_result ip-data__filter_result--green': itemFilterResult === SNDS_FILTER_RESULT.GREEN,
            'ip-data__filter_result ip-data__filter_result--yellow': itemFilterResult === SNDS_FILTER_RESULT.YELLOW,
            'ip-data__filter_result ip-data__filter_result--red': itemFilterResult === SNDS_FILTER_RESULT.RED,
          })}>
            {itemFilterResult}
          </div>
        </div>
      );
      const date_timestamp = (
        <div className="ip-data__column">
          {convertDateTime(itemDate, "DD-MM-YYYY")}
        </div>
      );
      const recipients = (
        <div className="ip-data__column">
          {itemRecipients}
        </div>
      );
      const bounce = (
        <div className="ip-data__column">
          {itemBounce}
        </div>
      );
      const bounce_rate = (
        <div className="ip-data__column">
          {itemBounceRate.toFixed(1)}
          {'%'}
        </div>
      );
      const complaint_rate = (
        <div className="ip-data__column">
          {itemComplaintRate.toFixed(1)}
          {'%'}
        </div>
      );
      const trap_hits = (
        <div className="ip-data__column">
          {itemTrapHits}
        </div>
      );
      const data_commands = (
        <div className="ip-data__column">
          {itemDataCommands}
        </div>
      );
      const rcpt_commands = (
        <div className="ip-data__column">
          {itemRcptCommands}
        </div>
      );
     
      return {
        ip,
        dns_reverse_result,
        filter_result,
        date_timestamp,
        recipients,
        bounce,
        bounce_rate,
        complaint_rate,
        trap_hits,
        data_commands,
        rcpt_commands,
      }
  }) ?? [], [sndsIpData]);

  if (sndsIsIpDataLoading) return <Spinner />;

  return (
    <Fragment>
      <div className="ip-data__controls">
        {!!tableData?.length && (
          <Pagination
            page={paginal.page}
            pages={paginal.pages}
            pageSize={paginal.pageSize}
            handlePageSize={handlePageSize}
            handlePage={handlePage}
            className="ip-data__pagination ip-data__pagination--top"
          />
        )}
        <div className="ip-data__filters ipd-filters">
          <SNDSIpDataLoadingInfo />
          <SNDSIpDataFilters />
        </div>
      </div>

      <Table
        columns={[
          {
            field: 'ip',
            title: t('snds.ip-data.table.header.ip.text'),
            type: 'string',
            width: 200,
          },
          {
            field: 'dns_reverse_result',
            title: t('snds.ip-data.table.header.rdns.text'),
            width: 400,
          },
          {
            field: 'filter_result',
            title: t('snds.ip-data.table.header.filter_result.text'),
            width: 150,
          },
          {
            field: 'date_timestamp',
            title: t('snds.ip-data.table.header.date.text'),
            width: 100,
          },
          {
            field: 'recipients',
            title: t('snds.ip-data.table.header.recipients.text'),
            width: 100,
          },
          {
            field: 'bounce',
            title: t('snds.ip-data.table.header.bounce.text'),
            width: 100,
          },
          {
            field: 'bounce_rate',
            title: t('snds.ip-data.table.header.bounce_rate.text'),
            width: 150,
          },
          {
            field: 'complaint_rate',
            title: t('snds.ip-data.table.header.complaint_rate.text'),
            width: 150,
          },
          {
            field: 'trap_hits',
            title: t('snds.ip-data.table.header.trap_hits.text'),
            width: 100,
          },
          {
            field: 'data_commands',
            title: t('snds.ip-data.table.header.data_commands.text'),
            width: 150,
          },
          {
            field: 'rcpt_commands',
            title: t('snds.ip-data.table.header.rcpt_commands.text'),
            width: 150,
          },
        ]}
        data={tableData}
        bluredColumns={meEmail === DEMO_USER_EMAIL ? ['ipData', 'dnsReverseResultData'] : []}
        sorting={{
          ...sndsIpDataSorting,
          sortFields: [
            'ip',
            'dns_reverse_result',
            'filter_result',
            'date_timestamp',
            'data_commands',
            'rcpt_commands',
            'recipients',
            'bounce',
            'bounce_rate',
            'complaint_rate',
            'trap_hits'
          ],
          isApiSorting: true,
        }}
        onSorting={(data) => dispatch(handleSndsIpDataSorting(data))}
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ip-data__pagination ip-data__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(SNDSIpDataTables);
