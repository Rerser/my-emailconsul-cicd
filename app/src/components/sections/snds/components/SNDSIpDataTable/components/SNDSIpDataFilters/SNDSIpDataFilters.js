import React, { Fragment, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { differenceInCalendarDays } from 'date-fns';
import { ru, enUS } from 'date-fns/locale';

import Button from 'components/ui/button';
import DatePickerRange from 'components/ui/datePickerRange';
import MultipleSelect from 'components/ui/multipleSelect';
import Popup from 'components/ui/popup/Popup';

import {
  handleSndsIpDataFilter,
  setSndsIpDataDateFrom,
  setSndsIpDataDateTo,
} from 'store/actions/snds';
import {
  localeCurrentSelector,
  sndsIpDataDateFromSelector,
  sndsIpDataDateToSelector,
  sndsIpDataFilterSelector,
  sndsIpDataUserIpsIsLoadingSelector,
  sndsIpDataUserIpsSelector,
} from 'store/selectors';

import {
  DAY,
  DAY_BEFORE_YESTERDAY,
  LOCALE,
  MONTH,
  SNDS_DAYS_LIMIT,
  SNDS_FILTER_RESULT,
  SNDS_MAX_CHOSE_USER_IPS,
} from 'utils/constants';

import './snds-ip-data-filters.scss';

function SNDSIpDataFilters() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const sndsIpDataUserIpsIsLoading = useSelector(sndsIpDataUserIpsIsLoadingSelector);
  const sndsIpDataUserIps = useSelector(sndsIpDataUserIpsSelector);
  const sndsIpDataFilter = useSelector(sndsIpDataFilterSelector);

  const sndsIpDataDateFrom = useSelector(sndsIpDataDateFromSelector);
  const sndsIpDataDateTo = useSelector(sndsIpDataDateToSelector);

  const [showPopup, setShowPopup] = useState(false);

  const handleChangeDateFrom = (date) => {
    if (date) {
      dispatch(setSndsIpDataDateFrom(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
      if (
        differenceInCalendarDays(new Date(sndsIpDataDateTo), date) > SNDS_DAYS_LIMIT
      ) {
        dispatch(setSndsIpDataDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
      }
    } else {
      dispatch(setSndsIpDataDateFrom(''));
    }
    if (date >= new Date(sndsIpDataDateTo)) {
      dispatch(setSndsIpDataDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
    }
  };

  const handleChangeDateTo = (date) => {
    if (date) {
      dispatch(setSndsIpDataDateTo(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
    } else {
      dispatch(setSndsIpDataDateTo(''));
    }
  };

  const handleChangeIp = (values) => {
    if (values.length > SNDS_MAX_CHOSE_USER_IPS) {
      setShowPopup(true);
    } else {
      dispatch(
        handleSndsIpDataFilter({ field: 'ips', value: values.map(value => value.label) })
      );
    }
  }

  const handleChangeFilterResult = (values) => {
    dispatch(
      handleSndsIpDataFilter({ field: 'filterResult', value: values.map(val => val.value) })
    );
  }

  const userIpsSelectOptions = sndsIpDataUserIps.map(({ ip }) => ({ value: ip, label: ip }));
  const filterResultOptions = Object.values(SNDS_FILTER_RESULT).map(value => ({ value: value.toUpperCase(), label: value.toUpperCase() }));

  return (
    <Fragment>
      <div className="ipd-filters__user-ips">
        <div className="ipd-filters__label">
          {t('snds.ip-data.table.filter.user-ips.select.label')}
        </div>
        <MultipleSelect 
          values={sndsIpDataFilter.ips}
          onChange={handleChangeIp}
          options={userIpsSelectOptions}
          placeholder={
            sndsIpDataUserIpsIsLoading
              ? t('snds.ip-data.table.filter.select.placeholder.loading.text')
              : t('snds.ip-data.table.filter.select.placeholder.text')
          }
          disabled={!userIpsSelectOptions.length}
          className="ipd-filters__select"
        />
      </div>

      <div className="ipd-filters__filter-result">
        <div className="ipd-filters__label">
          {t('snds.ip-data.table.filter.filter-result.select.label')}
        </div>
        <MultipleSelect 
          values={sndsIpDataFilter.filterResult}
          onChange={handleChangeFilterResult}
          options={filterResultOptions}
          placeholder={t('snds.ip-data.table.filter.select.placeholder.text')}
          className="ipd-filters__select"
        />
      </div>

      <div className="ipd-filters__dates-range dates-range">
        <DatePickerRange 
          locale={locale === LOCALE.ru ? ru : enUS}

          labelInputFrom={t('snds.ip-data.table.filter.dates-range.from.label')}
          labelInputTo={t('snds.ip-data.table.filter.dates-range.to.label')}

          dateFormat="DD-MM-yyyy"
          dateFrom={sndsIpDataDateFrom}
          dateTo={sndsIpDataDateTo}
          handleChangeDateFrom={handleChangeDateFrom}
          handleChangeDateTo={handleChangeDateTo}

          maxDate={new Date(DAY_BEFORE_YESTERDAY).toISOString()}
          maxDaysDateTo={SNDS_DAYS_LIMIT}

          disabledTo={!sndsIpDataDateFrom}
          requiredDateTo={!!sndsIpDataDateFrom && !sndsIpDataDateTo}
        />
      </div>

      <Popup
        isOpen={showPopup}
        onClose={() => setShowPopup(false)}
        wrapperStyle="ipd-filters__popup ipd-popup"
      >
        <h4 className="ipd-popup__header">
          {t('snds.ip-status.popup.error.title')}
        </h4>
        <div className="ipd-popup__description">
          {t('snds.ip-status.popup.error.description.text1')}
          {' '}
          <strong>
            {SNDS_MAX_CHOSE_USER_IPS}
          </strong>
          {' '}
          {t('snds.ip-status.popup.error.description.text2')}
        </div>
        <Button
          text={t('snds.ip-status.popup.error.button.close.text')}
          className="ipd-popup__btn"
          onClick={() => setShowPopup(false)}
        />
      </Popup>
    </Fragment>
  );
}

export default React.memo(SNDSIpDataFilters);
