import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button';

import {
  sndsIsIpDataLoadedSelector,
  sndsIsIpDataLoadingSelector,
} from 'store/selectors';
import { getSndsIpData } from 'store/actions/snds';

import './ip-data-loading-info.scss';

function SNDSIpDataLoadingInfo() {
  const dispatch = useDispatch();
  const t = useFormatMessage();  
  
  const sndsIsIpDataLoaded = useSelector(sndsIsIpDataLoadedSelector);
  const sndsIsIpDataLoading = useSelector(sndsIsIpDataLoadingSelector);

  return (
    <div className="ip-data-loading-info">
      <Button
        mode="blue"
        text={t('snds.ip-data.loading-info.refresh.btn.text')}
        className="ip-status-loading-info__refresh"
        onClick={() => dispatch(getSndsIpData())}
        hasRefreshIcon
        disabled={!sndsIsIpDataLoaded || sndsIsIpDataLoading}
        isRotateAnimation={!sndsIsIpDataLoaded || sndsIsIpDataLoading}
      />
    </div>
  );
}

export default React.memo(SNDSIpDataLoadingInfo);