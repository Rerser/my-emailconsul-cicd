import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button/Button';
import Tabs from 'components/ui/tabs';

import {
  handleSndsInfoMode,
} from 'store/actions/snds';
import {
  setSndsDateRunIsShowSlider,
  setSndsHistoryIsShowSlider,
  setSndsIsOnAddKey,
} from 'store/actions/sndsIntegration';
import {
  sndsInfoModeSelector,
  sndsIntegrationCredentialsSelector,
  sndsIntegrationIsCredentialsDataLoadingSelector,
} from 'store/selectors';

import { SNDS_INFO_MODE, USER_SNDS_CREDENTIALS_STATUS } from 'utils/constants';

import './snds-header.scss';

function SNDSHeader() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const sndsInfoMode = useSelector(sndsInfoModeSelector);
  const sndsIntegrationCredentials = useSelector(sndsIntegrationCredentialsSelector);
  const sndsIntegrationIsCredentialsDataLoading = useSelector(sndsIntegrationIsCredentialsDataLoadingSelector);

  const isEmptyCredentials = !sndsIntegrationCredentials.length;
  const hasCredentialsActiveKey = sndsIntegrationCredentials.some(data => data.status === USER_SNDS_CREDENTIALS_STATUS.ACTIVE);

  return (
    <div className="snds-header">
      <h1 className="snds-header__title">
        {t('snds.title')}
      </h1>

      <div className="snds-header__header-right header-right">
        <div className="header-right__tabs">
          <Tabs
            value={sndsInfoMode}
            options={[
              {
                value: SNDS_INFO_MODE.DASHBOARDS,
                label: t('snds.header.tabs.dashboards.label'),
              },
              {
                value: SNDS_INFO_MODE.IP_STATUS,
                label: t('snds.header.tabs.ip_status.label'),
              },
              {
                value: SNDS_INFO_MODE.IP_DATA,
                label: t('snds.header.tabs.ip_data.label'),
              },
            ]}
            onChange={(e) => dispatch(handleSndsInfoMode(e.target.value))}
            containerStyle="header-right__tabs-container"
          />
        </div>
        <div className="header-right__add-key">
          <Button
            mode="purple"
            text={
              !!sndsIntegrationCredentials.length
                ? t('snds.header.btn.manage-key.label')
                : t('snds.header.btn.add-key.label')
            }
            className="snds-header__btn"
            disabled={sndsIntegrationIsCredentialsDataLoading}
            onClick={() => dispatch(setSndsIsOnAddKey(true))}
          />
        </div>

        <div className="header-right__user-integration">
          <Button
            mode="purple"
            text={t('snds.button.date-run.text')}
            className="header-right__user-integration-btn"
            disabled={
              sndsIntegrationIsCredentialsDataLoading ||
              isEmptyCredentials ||
              !hasCredentialsActiveKey
            }
            onClick={() => dispatch(setSndsDateRunIsShowSlider(true))}
          />
          <Button
            mode="blue"
            text={t('snds.button.history.text')}
            className="header-right__user-integration-btn"
            disabled={
              sndsIntegrationIsCredentialsDataLoading ||
              isEmptyCredentials ||
              !hasCredentialsActiveKey
            }
            onClick={() => dispatch(setSndsHistoryIsShowSlider(true))}
          />
        </div>
      </div>
    </div>
  );
}

export default React.memo(SNDSHeader);
