import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'react-google-charts';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';

const SNDSBounceInfo = ({ graphData }) => {
  const t = useFormatMessage();
  const chartData = [];

  for (let i = 0; i < graphData.length; i++) {
    const { date, bounce } = graphData[i];

    chartData.push([
      date,
      bounce, // column 1
    ]);
  }

  return (
    <Chart
      chartType="LineChart"
      loader={<Spinner />}
      data={[
        [
          'date', 
          t('snds.dashboards.graph_item.bounce.top-axis.text'),
        ],
        ...chartData
      ]}
      options={{
        width: '100%',
        legend: { position: 'top', alignment: 'center' },
        tooltip: { isHtml: true },
        vAxis: { minValue: 1, format: '#' },
      }}
      formatters={[
        {
          type: 'NumberFormat',
          column: 1,
          options: {
            fractionDigits: 0,
          },
        },
      ]}
    />
  );
};

SNDSBounceInfo.propTypes = {
  graphData: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string,
    bounce: PropTypes.number,
  })).isRequired,
};
SNDSBounceInfo.defaultProps = {
  graphData: [],
};

export default React.memo(SNDSBounceInfo);
