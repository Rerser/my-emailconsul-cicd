import { SNDS_FILTER_RESULT } from "utils/constants";
import {
  addNDayToDate,
  getDifferenceDays,
  getFormattedDaysMMDD,
} from "utils/helpers";

export default function ({ data, dateTo, dateFrom }) {
  const defaultRecipientsSum = {
    [SNDS_FILTER_RESULT.GREEN]: 0,
    [SNDS_FILTER_RESULT.RED]: 0,
    [SNDS_FILTER_RESULT.YELLOW]: 0,
  };

  // данные для графика filterResult и форматированные даты
  const aggregatedData = data.reduce((accum, item) => {
    if (item) {

      const recipientsSum = {
        [SNDS_FILTER_RESULT.GREEN]: (accum.recipientsSum?.[SNDS_FILTER_RESULT.GREEN] ?? 0) + (item?.recipients_green ?? 0),
        [SNDS_FILTER_RESULT.RED]: (accum.recipientsSum?.[SNDS_FILTER_RESULT.RED] ?? 0) + (item?.recipients_red ?? 0),
        [SNDS_FILTER_RESULT.YELLOW]: (accum.recipientsSum?.[SNDS_FILTER_RESULT.YELLOW] ?? 0) + (item?.recipients_yellow ?? 0),
      };
      const formattedDay = getFormattedDaysMMDD(item.date_timestamp);

      return {
        recipientsSum,
        datesObj: {
          ...accum.datesObj,
          [formattedDay]: item,
        }
      };
    }

    return { ...accum };
  }, {});

  const { datesObj = {} } = aggregatedData;
  const daysCount = getDifferenceDays(dateTo, dateFrom); // inclusive last
  const filterResultData = [];
  const recipientsData = [];
  const bounceData = [];
  const bounceRateData = [];
  const complaintRateData = [];
  const trapHitsData = [];

  for (let i = 0; i < daysCount; i++) {
    const day = addNDayToDate(dateFrom, i);
    const currentDayFormatted = getFormattedDaysMMDD(day);

    // данные для графика Ip Trends
    filterResultData.push({
      date: currentDayFormatted,
      filterResult: {
        [SNDS_FILTER_RESULT.GREEN]: datesObj[currentDayFormatted]?.filter_result_green ?? 0,
        [SNDS_FILTER_RESULT.RED]: datesObj[currentDayFormatted]?.filter_result_red ?? 0,
        [SNDS_FILTER_RESULT.YELLOW]: datesObj[currentDayFormatted]?.filter_result_yellow ?? 0,
      }
    });

    // данные для графика recipients Trends
    recipientsData.push({
      date: currentDayFormatted,
      recipients: {
        [SNDS_FILTER_RESULT.GREEN]: datesObj[currentDayFormatted]?.recipients_green ?? 0,
        [SNDS_FILTER_RESULT.RED]: datesObj[currentDayFormatted]?.recipients_red ?? 0,
        [SNDS_FILTER_RESULT.YELLOW]: datesObj[currentDayFormatted]?.recipients_yellow ?? 0,
      }
    });

    // данные для графика bounce
    bounceData.push({
      date: currentDayFormatted,
      bounce: datesObj[currentDayFormatted]?.bounce ?? 0,
    });

    // данные для графика bounce rate
    bounceRateData.push({
      date: currentDayFormatted,
      bounceRate: datesObj[currentDayFormatted]?.bounce_rate ?? 0,
    });

    // данные для графика complaint rate
    complaintRateData.push({
      date: currentDayFormatted,
      complaintRate: datesObj[currentDayFormatted]?.complaint_rate ?? 0,
    });

    // данные для графика trap hits
    trapHitsData.push({
      date: currentDayFormatted,
      trapHits: datesObj[currentDayFormatted]?.trap_hits ?? 0,
    });
  }

  return {
    recipientsSum: data.length ? aggregatedData.recipientsSum : defaultRecipientsSum,
    filterResultData,
    recipientsData,
    bounceData,
    bounceRateData,
    complaintRateData,
    trapHitsData,
  }
}
