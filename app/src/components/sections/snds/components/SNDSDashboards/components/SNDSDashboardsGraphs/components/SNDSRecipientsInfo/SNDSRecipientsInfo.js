import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'react-google-charts';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';
import {
  SNDS_FILTER_RESULT,
  SNDS_IP_DATA_FILTER_COLORS,
} from 'utils/constants';

const SNDSRecipientsInfo = ({ graphData }) => {
  const t = useFormatMessage();
  const chartData = [];

  for (let i = 0; i < graphData.length; i++) {
    const { date = '', recipients = {} } = graphData[i];
    const green = recipients[SNDS_FILTER_RESULT.GREEN] || 0;
    const red = recipients[SNDS_FILTER_RESULT.RED] || 0;
    const yellow = recipients[SNDS_FILTER_RESULT.YELLOW] || 0;

    chartData.push([
      date,
      green,
      red,
      yellow,
    ]);
  }

  const chartColors = [
    SNDS_IP_DATA_FILTER_COLORS[SNDS_FILTER_RESULT.GREEN],
    SNDS_IP_DATA_FILTER_COLORS[SNDS_FILTER_RESULT.RED],
    SNDS_IP_DATA_FILTER_COLORS[SNDS_FILTER_RESULT.YELLOW],
  ];

  return (
    <Chart
      chartType="LineChart"
      loader={<Spinner />}
      data={[
        [
          'date', 
          t('snds.dashboards.graph_item.recipients-trends.top-axis.green.text'),
          t('snds.dashboards.graph_item.recipients-trends.top-axis.red.text'),
          t('snds.dashboards.graph_item.recipients-trends.top-axis.yellow.text'),
        ],
        ...chartData
      ]}
      options={{
        width: '100%',
        legend: { position: 'top', alignment: 'center' },
        focusTarget: 'category',
        vAxis: { minValue: 1, format: '#' },
        colors: chartColors,
      }}
      formatters={[
        {
          type: 'NumberFormat',
          column: 1,
          options: {
            fractionDigits: 0,
          },
        },
        {
          type: 'NumberFormat',
          column: 2,
          options: {
            fractionDigits: 0,
          },
        },
        {
          type: 'NumberFormat',
          column: 3,
          options: {
            fractionDigits: 0,
          },
        },
      ]}
    />
  );
};

SNDSRecipientsInfo.propTypes = {
  graphData: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string,
    recipients: PropTypes.shape({
      [SNDS_FILTER_RESULT.GREEN]: PropTypes.number,
      [SNDS_FILTER_RESULT.RED]: PropTypes.number,
      [SNDS_FILTER_RESULT.YELLOW]: PropTypes.number,
    })
  })).isRequired,
};
SNDSRecipientsInfo.defaultProps = {
  graphData: [],
};

export default React.memo(SNDSRecipientsInfo);
