import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Spinner from 'components/ui/spinner';
import SNDSDataSumInfo from './components/SNDSDataSumInfo';
import SNDSFilterResultInfo from './components/SNDSFilterResultInfo';
import SNDSRecipientsInfo from './components/SNDSRecipientsInfo';
import SNDSBounceInfo from './components/SNDSBounceInfo';
import SNDSBounceRateInfo from './components/SNDSBounceRateInfo';
import SNDSComplaintRateInfo from './components/SNDSComplaintRateInfo';
import SNDSTrapHitsInfo from './components/SNDSTrapHitsInfo';
import sndsGraphData from './components/sndsGraphData';
import sndsFromIpsGraphData from './components/sndsFromIpsGraphData/sndsFromIpsGraphData';

import {
  getSndsDashboardsData,
} from 'store/actions/snds';
import {
  sndsDashboardsDataIsLoadingSelector,
  sndsDashboardsDataSelector,
  sndsDashboardsDateFromSelector,
  sndsDashboardsDateToSelector,
  sndsDashboardsFilterSelector,
} from 'store/selectors';

import { useDebounce } from 'utils/helpers';

import './snds-dashboards-graphs.scss';

const SNDSDashboardsGraphs = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const sndsDashboardsDataIsLoading = useSelector(sndsDashboardsDataIsLoadingSelector);
  const sndsDashboardsData = useSelector(sndsDashboardsDataSelector);
  const sndsDashboardsDateFrom = useSelector(sndsDashboardsDateFromSelector);
  const sndsDashboardsDateTo = useSelector(sndsDashboardsDateToSelector);
  const sndsDashboardsFilter = useSelector(sndsDashboardsFilterSelector);

  const { ips = [] } = sndsDashboardsFilter;
  const debouncedFilterIp = useDebounce(ips, 1000);

  useEffect(() => {
    if (sndsDashboardsDateFrom && sndsDashboardsDateTo) {
      dispatch(getSndsDashboardsData());
    }
  }, [dispatch, sndsDashboardsDateFrom, sndsDashboardsDateTo, debouncedFilterIp]);

  const [isShowRecipientsSum, setIsShowRecipientsSum] = useState(true);
  const [isShowFilterResult, setIsShowFilterResult] = useState(true);
  const [isShowRecipients, setIsShowRecipients] = useState(true);
  const [isShowBounce, setIsShowBounce] = useState(true);
  const [isShowBounceRate, setIsShowBounceRate] = useState(true);
  const [isShowComplaintRate, setIsShowComplaintRate] = useState(true);
  const [isShowTrapHits, setIsShowTrapHits] = useState(true);

  const dashboardsData = useMemo(
    () =>
      debouncedFilterIp.length
        ? sndsFromIpsGraphData({
            data: sndsDashboardsData,
            dateTo: sndsDashboardsDateTo,
            dateFrom: sndsDashboardsDateFrom,
          })
        : sndsGraphData({
            data: sndsDashboardsData,
            dateTo: sndsDashboardsDateTo,
            dateFrom: sndsDashboardsDateFrom,
          }),
    [sndsDashboardsData, sndsDashboardsDateTo, sndsDashboardsDateFrom, debouncedFilterIp]
  );

  if (sndsDashboardsDataIsLoading) {
    return <Spinner />;
  }

  return (
    <div className="snds-dashboards-graphs">
      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowRecipientsSum(!isShowRecipientsSum)}
          >
            <span>
              {t('snds.dashboards.graph_item.filter_result.header.text')}
            </span>
          </div>
          {isShowRecipientsSum && !!Object.keys(dashboardsData.recipientsSum).length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSDataSumInfo graphData={dashboardsData.recipientsSum} />
            </div>
          )}
        </div>
      </div>

      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowFilterResult(!isShowFilterResult)}
          >
            <span>
              {t('snds.dashboards.graph_item.ip-trends.header.text')}
            </span>
          </div>
          {isShowFilterResult && !!dashboardsData.filterResultData.length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSFilterResultInfo graphData={dashboardsData.filterResultData} />
            </div>
          )}
        </div>
      </div>

      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowRecipients(!isShowRecipients)}
          >
            <span>
              {t('snds.dashboards.graph_item.recipients-trends.header.text')}
            </span>
          </div>
          {isShowRecipients && !!dashboardsData.recipientsData.length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSRecipientsInfo graphData={dashboardsData.recipientsData} />
            </div>
          )}
        </div>
      </div>

      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowBounce(!isShowBounce)}
          >
            <span>
              {t('snds.dashboards.graph_item.bounce.header.text')}
            </span>
          </div>
          {isShowBounce && !!dashboardsData.bounceData.length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSBounceInfo graphData={dashboardsData.bounceData} />
            </div>
          )}
        </div>
      </div>

      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowBounceRate(!isShowBounceRate)}
          >
            <span>
              {t('snds.dashboards.graph_item.bounce_rate.header.text')}
            </span>
          </div>
          {isShowBounceRate && !!dashboardsData.bounceRateData.length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSBounceRateInfo graphData={dashboardsData.bounceRateData} />
            </div>
          )}
        </div>
      </div>

      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowComplaintRate(!isShowComplaintRate)}
          >
            <span>
              {t('snds.dashboards.graph_item.complaint_rate.header.text')}
            </span>
          </div>
          {isShowComplaintRate && !!dashboardsData.complaintRateData.length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSComplaintRateInfo graphData={dashboardsData.complaintRateData} />
            </div>
          )}
        </div>
      </div>

      <div className="snds-dashboards-graphs__sei-accordion sei-accordion sei-accordion--snds">
        <div className="sei-accordion__item">
          <div
            className="sei-accordion__item-header"
            onClick={() => setIsShowTrapHits(!isShowTrapHits)}
          >
            <span>
              {t('snds.dashboards.graph_item.trap_hits.header.text')}
            </span>
          </div>
          {isShowTrapHits && !!dashboardsData.trapHitsData.length && (
            <div className="sei-accordion__item-content sei-accordion__item-content--graph">
              <SNDSTrapHitsInfo graphData={dashboardsData.trapHitsData} />
            </div>
          )}
        </div>
      </div>

    </div>
  );
};

export default React.memo(SNDSDashboardsGraphs);
