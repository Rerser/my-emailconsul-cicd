import { SNDS_FILTER_RESULT } from "utils/constants";
import {
  addNDayToDate,
  getDifferenceDays,
  getFormattedDaysMMDD,
} from "utils/helpers";

export default function ({ data, dateTo, dateFrom }) {
  const defaultRecipientsSum = {
    [SNDS_FILTER_RESULT.GREEN]: 0,
    [SNDS_FILTER_RESULT.RED]: 0,
    [SNDS_FILTER_RESULT.YELLOW]: 0,
  };

  const dataObj = data.reduce((accum, item) => {
    const pred = accum[item.date_timestamp] ?? {};

    const predFilterResult = {
      [SNDS_FILTER_RESULT.GREEN]: (pred.filter_result_green || 0),
      [SNDS_FILTER_RESULT.RED]: (pred.filter_result_red || 0),
      [SNDS_FILTER_RESULT.YELLOW]: (pred.filter_result_yellow || 0),
    };
    const predRecipients = {
      [SNDS_FILTER_RESULT.GREEN]: (pred.recipients_green || 0),
      [SNDS_FILTER_RESULT.RED]: (pred.recipients_red || 0),
      [SNDS_FILTER_RESULT.YELLOW]: (pred.recipients_yellow || 0),
    };

    const modifiedDataSum = {
      bounce: (pred.bounce || 0) + item.bounce,

      bounce_rate: (pred.bounce_rate || 0) + item.bounce_rate,

      complaint_rate: (pred.complaint_rate || 0) + item.complaint_rate,

      filter_result_green: item.filter_result === SNDS_FILTER_RESULT.GREEN 
        ? predFilterResult[SNDS_FILTER_RESULT.GREEN] + 1 
        : predFilterResult[SNDS_FILTER_RESULT.GREEN],
      filter_result_red: item.filter_result === SNDS_FILTER_RESULT.RED 
        ? predFilterResult[SNDS_FILTER_RESULT.RED] + 1 
        : predFilterResult[SNDS_FILTER_RESULT.RED],
      filter_result_yellow: item.filter_result === SNDS_FILTER_RESULT.YELLOW 
        ? predFilterResult[SNDS_FILTER_RESULT.YELLOW] + 1 
        : predFilterResult[SNDS_FILTER_RESULT.YELLOW],

      recipients_green: item.filter_result === SNDS_FILTER_RESULT.GREEN
        ? predRecipients[SNDS_FILTER_RESULT.GREEN] + item.recipients
        : predRecipients[SNDS_FILTER_RESULT.GREEN],
      recipients_red: item.filter_result === SNDS_FILTER_RESULT.RED
        ? predRecipients[SNDS_FILTER_RESULT.RED] + item.recipients
        : predRecipients[SNDS_FILTER_RESULT.RED],
      recipients_yellow: item.filter_result === SNDS_FILTER_RESULT.YELLOW 
        ? predRecipients[SNDS_FILTER_RESULT.YELLOW] + item.recipients
        : predRecipients[SNDS_FILTER_RESULT.YELLOW],

      trap_hits: (pred.trap_hits || 0) + item.trap_hits,
    }

    return {
      ...accum,
      [item.date_timestamp]: modifiedDataSum,
    }
  }, {});

  const aggregatedData = Object.keys(dataObj)
    .reduce((accum, date) => {
      const value = dataObj[date];

      const recipientsSum = {
        [SNDS_FILTER_RESULT.GREEN]: (accum.recipientsSum?.[SNDS_FILTER_RESULT.GREEN] ?? 0) + (value?.recipients_green ?? 0),
        [SNDS_FILTER_RESULT.RED]: (accum.recipientsSum?.[SNDS_FILTER_RESULT.RED] ?? 0) + (value?.recipients_red ?? 0),
        [SNDS_FILTER_RESULT.YELLOW]: (accum.recipientsSum?.[SNDS_FILTER_RESULT.YELLOW] ?? 0) + (value?.recipients_yellow ?? 0),
      };
      const formattedDay = getFormattedDaysMMDD(date);

      return {
        recipientsSum,
        datesObj: {
          ...accum.datesObj,
          [formattedDay]: value,
        }
      };
    }, {});

  const { datesObj = {} } = aggregatedData;
  const daysCount = getDifferenceDays(dateTo, dateFrom); // inclusive last
  const filterResultData = [];
  const recipientsData = [];
  const bounceData = [];
  const bounceRateData = [];
  const complaintRateData = [];
  const trapHitsData = [];

  for (let i = 0; i < daysCount; i++) {
    const day = addNDayToDate(dateFrom, i);
    const currentDayFormatted = getFormattedDaysMMDD(day);

    // данные для графика Ip Trends
    filterResultData.push({
      date: currentDayFormatted,
      filterResult: {
        [SNDS_FILTER_RESULT.GREEN]: datesObj[currentDayFormatted]?.filter_result_green ?? 0,
        [SNDS_FILTER_RESULT.RED]: datesObj[currentDayFormatted]?.filter_result_red ?? 0,
        [SNDS_FILTER_RESULT.YELLOW]: datesObj[currentDayFormatted]?.filter_result_yellow ?? 0,
      }
    });

    // данные для графика recipients Trends
    recipientsData.push({
      date: currentDayFormatted,
      recipients: {
        [SNDS_FILTER_RESULT.GREEN]: datesObj[currentDayFormatted]?.recipients_green ?? 0,
        [SNDS_FILTER_RESULT.RED]: datesObj[currentDayFormatted]?.recipients_red ?? 0,
        [SNDS_FILTER_RESULT.YELLOW]: datesObj[currentDayFormatted]?.recipients_yellow ?? 0,
      }
    });

    // данные для графика bounce
    bounceData.push({
      date: currentDayFormatted,
      bounce: datesObj[currentDayFormatted]?.bounce ?? 0,
    });

    // данные для графика bounce rate
    bounceRateData.push({
      date: currentDayFormatted,
      bounceRate: datesObj[currentDayFormatted]?.bounce_rate ?? 0,
    });

    // данные для графика complaint rate
    complaintRateData.push({
      date: currentDayFormatted,
      complaintRate: datesObj[currentDayFormatted]?.complaint_rate ?? 0,
    });

    // данные для графика trap hits
    trapHitsData.push({
      date: currentDayFormatted,
      trapHits: datesObj[currentDayFormatted]?.trap_hits ?? 0,
    });
  }

  return {
    recipientsSum: data.length ? aggregatedData.recipientsSum : defaultRecipientsSum,
    filterResultData,
    recipientsData,
    bounceData,
    bounceRateData,
    complaintRateData,
    trapHitsData,
  }
}
