import React, { Fragment, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { differenceInCalendarDays } from 'date-fns';
import { ru, enUS } from 'date-fns/locale';

import Button from 'components/ui/button';
import DatePickerRange from 'components/ui/datePickerRange';
import MultipleSelect from 'components/ui/multipleSelect';
import Popup from 'components/ui/popup/Popup';

import {
  handleSndsDashboardsFilter,
  setSndsDashboardsDateFrom,
  setSndsDashboardsDateTo,
} from 'store/actions/snds';
import {
  localeCurrentSelector,
  sndsDashboardsDateFromSelector,
  sndsDashboardsDateToSelector,
  sndsDashboardsFilterSelector,
  sndsDashboardsUserIpsIsLoadingSelector,
  sndsDashboardsUserIpsSelector,
} from 'store/selectors';

import {
  DAY,
  DAY_BEFORE_YESTERDAY,
  LOCALE,
  MONTH,
  SNDS_DAYS_LIMIT,
  SNDS_MAX_CHOSE_USER_IPS,
} from 'utils/constants';

import './snds-dashboards-filters.scss';

function SNDSDashboardsFilters() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const sndsDashboardsUserIps = useSelector(sndsDashboardsUserIpsSelector);
  const sndsDashboardsFilter = useSelector(sndsDashboardsFilterSelector);
  const sndsDashboardsUserIpsIsLoading = useSelector(sndsDashboardsUserIpsIsLoadingSelector);

  const sndsDashboardsDateFrom = useSelector(sndsDashboardsDateFromSelector);
  const sndsDashboardsDateTo = useSelector(sndsDashboardsDateToSelector);

  const [showPopup, setShowPopup] = useState(false);

  const handleChangeDateFrom = (date) => {
    if (date) {
      dispatch(setSndsDashboardsDateFrom(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
      if (
        differenceInCalendarDays(new Date(sndsDashboardsDateTo), date) > SNDS_DAYS_LIMIT
      ) {
        dispatch(setSndsDashboardsDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
      }
    } else {
      dispatch(setSndsDashboardsDateFrom(''));
    }
    if (date >= new Date(sndsDashboardsDateTo)) {
      dispatch(setSndsDashboardsDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
    }
  };

  const handleChangeDateTo = (date) => {
    if (date) {
      dispatch(setSndsDashboardsDateTo(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
    } else {
      dispatch(setSndsDashboardsDateTo(''));
    }
  };

  const handleChangeIp = (values) => {
    if (values.length > SNDS_MAX_CHOSE_USER_IPS) {
      setShowPopup(true);
    } else {
      dispatch(
        handleSndsDashboardsFilter({ field: 'ips', value: values.map(val => val.value) })
      );
    }
  }

  const userIpsSelectOptions = sndsDashboardsUserIps.map(({ ip }) => ({ value: ip, label: ip }));

  return (
    <Fragment>
      <div className="ipd-filters__user-ips">
        <div className="ipd-filters__label">
          {t('snds.dashboards.table.filter.user-ips.select.label')}
        </div>
        <MultipleSelect 
          values={sndsDashboardsFilter.ips}
          onChange={handleChangeIp}
          options={userIpsSelectOptions}
          placeholder={
            sndsDashboardsUserIpsIsLoading
              ? t('snds.dashboards.table.filter.select.placeholder.loading.text')
              : t('snds.dashboards.table.filter.select.placeholder.text')
          }
          disabled={!userIpsSelectOptions.length}
          className="ipd-filters__select"
        />
      </div>

      <div className="snds-dashboards-filters__dates-range">
        <DatePickerRange 
          locale={locale === LOCALE.ru ? ru : enUS}

          labelInputFrom={t('snds.dashboards.filter.dates-range.from.label')}
          labelInputTo={t('snds.dashboards.filter.dates-range.to.label')}

          dateFormat="DD-MM-yyyy"
          dateFrom={sndsDashboardsDateFrom}
          dateTo={sndsDashboardsDateTo}
          handleChangeDateFrom={handleChangeDateFrom}
          handleChangeDateTo={handleChangeDateTo}

          maxDate={new Date(DAY_BEFORE_YESTERDAY).toISOString()}
          maxDaysDateTo={SNDS_DAYS_LIMIT}

          disabledTo={!sndsDashboardsDateFrom}
          requiredDateTo={!!sndsDashboardsDateFrom && !sndsDashboardsDateTo}
        />
      </div>

      <Popup
        isOpen={showPopup}
        onClose={() => setShowPopup(false)}
        wrapperStyle="snds-dashboards-filters__popup snds-dashboards-popup"
      >
        <h4 className="snds-dashboards-popup__header">
          {t('snds.dashboards.popup.error.title')}
        </h4>
        <div className="snds-dashboards-popup__description">
          {t('snds.dashboards.popup.error.description.text1')}
          {' '}
          <strong>
            {SNDS_MAX_CHOSE_USER_IPS}
          </strong>
          {' '}
          {t('snds.dashboards.popup.error.description.text2')}
        </div>
        <Button
          text={t('snds.dashboards.popup.error.button.close.text')}
          className="snds-dashboards-popup__btn"
          onClick={() => setShowPopup(false)}
        />
      </Popup>
    </Fragment>
  );
}

export default React.memo(SNDSDashboardsFilters);
