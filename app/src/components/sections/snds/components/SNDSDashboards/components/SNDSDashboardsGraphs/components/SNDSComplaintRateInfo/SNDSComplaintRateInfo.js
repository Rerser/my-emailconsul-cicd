import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'react-google-charts';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';

const SNDSComplaintRateInfo = ({ graphData }) => {
  const t = useFormatMessage();

  const chartData = [];

  for (let i = 0; i < graphData.length; i++) {
    const { date = '', complaintRate = 0 } = graphData[i];

    chartData.push([
      date,
      complaintRate, // column 1
    ]);
  }

  return (
    <Chart
      chartType="LineChart"
      loader={<Spinner />}
      data={[
        [
          'date', 
          t('snds.dashboards.graph_item.complaint_rate.top-axis.text'),
        ],
        ...chartData
      ]}
      options={{
        width: '100%',
        legend: { position: 'top', alignment: 'center' },
        tooltip: { isHtml: true },
        vAxis: {
          minValue: 1,
          format: "# '%'",
        },
      }}
      formatters={[
        {
          type: 'NumberFormat',
          column: 1,
          options: {
            suffix: '%', // for tooltip
          },
        },
      ]}
    />
  );
};

SNDSComplaintRateInfo.propTypes = {
  graphData: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string,
    complaintRate: PropTypes.number,
  })).isRequired,
};
SNDSComplaintRateInfo.defaultProps = {
  graphData: [],
};

export default React.memo(SNDSComplaintRateInfo);
