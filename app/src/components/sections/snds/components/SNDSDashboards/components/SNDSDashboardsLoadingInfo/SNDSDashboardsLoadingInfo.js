import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button';

import {
  sndsDashboardsDataIsLoadedSelector,
  sndsDashboardsDataIsLoadingSelector,
} from 'store/selectors';
import { getSndsDashboardsData } from 'store/actions/snds';

import './snds-dashboards-loading-info.scss';

function SNDSDashboardsLoadingInfo() {
  const dispatch = useDispatch();
  const t = useFormatMessage();  
  
  const sndsDashboardsDataIsLoaded = useSelector(sndsDashboardsDataIsLoadedSelector);
  const sndsDashboardsDataIsLoading = useSelector(sndsDashboardsDataIsLoadingSelector);

  return (
    <div className="snds-dashboards-loading-info">
      <Button
        mode="blue"
        text={t('snds.dashboards.loading-info.refresh.btn.text')}
        className="snds-dashboards-loading-info__refresh"
        onClick={() => dispatch(getSndsDashboardsData())}
        hasRefreshIcon
        disabled={!sndsDashboardsDataIsLoaded || sndsDashboardsDataIsLoading}
        isRotateAnimation={!sndsDashboardsDataIsLoaded || sndsDashboardsDataIsLoading}
      />
    </div>
  );
}

export default React.memo(SNDSDashboardsLoadingInfo);