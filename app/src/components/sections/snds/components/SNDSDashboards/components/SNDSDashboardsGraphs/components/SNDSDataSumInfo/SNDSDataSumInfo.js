import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Chart from 'react-google-charts';

import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';

import {
  SNDS_IP_DATA_FILTER_COLORS,
  SNDS_FILTER_RESULT,
  TABLETS_WIDTH_BREAKPOINT,
} from 'utils/constants';
import { getNumWithRanks } from 'utils/helpers';

import './snds-data-sum-info.scss';

const SNDSDataSumInfo = ({ graphData }) => {
  const t = useFormatMessage();

  let maxRecipientsSumValue = 0;

  // table data

  let tableData = Object.keys(graphData).map((key) => {
    const color = (
      <div
        className={cn('snds-data-sum-info__column snds-data-sum-info__colors', {
          'snds-data-sum-info__colors--green': key === SNDS_FILTER_RESULT.GREEN,
          'snds-data-sum-info__colors--yellow': key === SNDS_FILTER_RESULT.YELLOW,
          'snds-data-sum-info__colors--red': key === SNDS_FILTER_RESULT.RED,
        })}
      >
        {t(`snds.dashboards.graph_item.filter_result.chart.color.${key.toLocaleLowerCase()}.text`)}
      </div>
    );
    const verdict = (
      <div className="snds-data-sum-info__verdict">
        {key === SNDS_FILTER_RESULT.GREEN &&
          t('snds.dashboards.graph_item.filter_result.table.header.verdict.green.text')}
        {key === SNDS_FILTER_RESULT.YELLOW &&
          t('snds.dashboards.graph_item.filter_result.table.header.verdict.yellow.text')}
        {key === SNDS_FILTER_RESULT.RED && t('snds.dashboards.graph_item.filter_result.table.header.verdict.red.text')}
      </div>
    );
    const recipients = (
      <div className="snds-data-sum-info__column">
        {getNumWithRanks(graphData[key], 3, ',')}
      </div>
    );

    maxRecipientsSumValue += graphData[key];

    return {
      color,
      verdict,
      recipients,
    };
  });

  const greenData = tableData[0];
  const yellowData = tableData[2];
  const redData = tableData[1];

  tableData = [greenData, yellowData, redData]; // table rows -> green, yellow, red

  // chart data

  const chartData = Object.keys(graphData).map((key) => [
    t(`snds.dashboards.graph_item.filter_result.chart.color.${key.toLocaleLowerCase()}.text`),
    graphData[key],
    `
    <div class="snds-data-sum-info__tooltip">
      <span>
        <strong>
          ${getNumWithRanks(graphData[key], 3, ',')}
        </strong>
        <br>
        recipients
      </span>
      <br>
      <span>
        (${(graphData[key] * 100 / maxRecipientsSumValue).toFixed(1)}%)
      </span>
    </div>
    `
  ]);

  const chartColors = [
    SNDS_IP_DATA_FILTER_COLORS[SNDS_FILTER_RESULT.GREEN],
    SNDS_IP_DATA_FILTER_COLORS[SNDS_FILTER_RESULT.RED],
    SNDS_IP_DATA_FILTER_COLORS[SNDS_FILTER_RESULT.YELLOW],
  ];

  const isChartHide = chartData.every((data) => !data[1]);

  return (
    <div
      className={cn("snds-data-sum-info", {
        "snds-data-sum-info--nochart": isChartHide,
      })}
    >
      {!isChartHide && (
        <Chart
          chartType="PieChart"
          loader={<Spinner />}
          data={[
            [
              'Filter', 'Percent', {type: 'string', role: 'tooltip', 'p': {'html':true}}
            ],
            ...chartData
          ]}
          width={350}
          options={{
            legend: { position: 'none' },
            tooltip: {
              isHtml: true,
              ignoreBounds: true,
            },
            chartArea: {
              right: window.innerWidth > TABLETS_WIDTH_BREAKPOINT ? '0' : 'auto',
              height: '90%',
            },
            pieSliceText: 'percentage',
            colors: chartColors,
          }}
          formatters={[
            {
              type: 'NumberFormat',
              column: 1,
              options: {
                fractionDigits: 0,
              },
            },
          ]}
          className="snds-data-sum-info__chart"
        />
      )}
      <div className="snds-data-sum-info__table">
        <Table
          columns={[
            {
              field: 'color',
              title: t('snds.dashboards.graph_item.filter_result.table.header.color.text'),
              type: 'string',
              width: 70,
            },
            {
              field: 'verdict',
              title: t('snds.dashboards.graph_item.filter_result.table.header.verdict.text'),
              type: 'string',
              width: 130,
            },
            {
              field: 'recipients',
              title: t('snds.dashboards.graph_item.filter_result.table.header.recipients.text'),
              type: 'string',
              width: 100,
            },
          ]}
          data={tableData}
        />
      </div>
    </div>
  );
};

SNDSDataSumInfo.propTypes = {
  graphData: PropTypes.shape({
    [SNDS_FILTER_RESULT.GREEN]: PropTypes.number,
    [SNDS_FILTER_RESULT.RED]: PropTypes.number,
    [SNDS_FILTER_RESULT.YELLOW]: PropTypes.number,
  }).isRequired,
};
SNDSDataSumInfo.defaultProps = {
  graphData: {
    [SNDS_FILTER_RESULT.GREEN]: 0,
    [SNDS_FILTER_RESULT.RED]: 0,
    [SNDS_FILTER_RESULT.YELLOW]: 0,
  },
};

export default React.memo(SNDSDataSumInfo);
