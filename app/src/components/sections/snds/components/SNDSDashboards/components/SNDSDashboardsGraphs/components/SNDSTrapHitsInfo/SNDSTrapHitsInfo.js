import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'react-google-charts';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Spinner from 'components/ui/spinner';

const SNDSTrapHitsInfo = ({ graphData }) => {
  const t = useFormatMessage();
  const chartData = [];

  for (let i = 0; i < graphData.length; i++) {
    const { date = '', trapHits = 0 } = graphData[i];

    chartData.push([
      date,
      trapHits, // column 1
    ]);
  }

  return (
    <Chart
      chartType="LineChart"
      loader={<Spinner />}
      data={[
        [
          'date', 
          t('snds.dashboards.graph_item.trap_hits.top-axis.text'),
        ],
        ...chartData
      ]}
      options={{
        width: '100%',
        legend: { position: 'top', alignment: 'center' },
        tooltip: { isHtml: true },
        vAxis: { minValue: 1, format: '#' },
      }}
      formatters={[
        {
          type: 'NumberFormat',
          column: 1,
          options: {
            suffix: ' ips', // for tooltip
            fractionDigits: 0,
          },
        },
      ]}
    />
  );
};

SNDSTrapHitsInfo.propTypes = {
  graphData: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.string,
    trapHits: PropTypes.number,
  })).isRequired,
};
SNDSTrapHitsInfo.defaultProps = {
  graphData: [],
};

export default React.memo(SNDSTrapHitsInfo);
