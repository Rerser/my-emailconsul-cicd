import React, { Fragment } from 'react';

import SNDSDashboardsFilters from './components/SNDSDashboardsFilters';
import SNDSDashboardsLoadingInfo from './components/SNDSDashboardsLoadingInfo';
import SNDSDashboardsGraphs from './components/SNDSDashboardsGraphs';

const SNDSDashboards = () => {
  return (
    <Fragment>
      <div className="snds-dashboards__filters snds-dashboards-filters">
        <SNDSDashboardsLoadingInfo />
        <SNDSDashboardsFilters />
      </div>

      <SNDSDashboardsGraphs />
    </Fragment>
  );
};

export default React.memo(SNDSDashboards);
