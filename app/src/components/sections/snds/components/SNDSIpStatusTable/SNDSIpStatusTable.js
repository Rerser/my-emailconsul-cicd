import React, { Fragment, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Pagination from 'components/ui/pagination';
import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';
import SNDSIpStatusFilters from './components/SNDSIpStatusFilters';
import SNDSIpStatusLoadingInfo from './components/SNDSIpStatusLoadingInfo';

import {
  getSndsIpStatus,
  handleSndsIpStatusSorting,
  setSndsIpStatusDataPagination,
} from 'store/actions/snds';
import {
  sndsIpStatusDateFromSelector,
  sndsIpStatusDateToSelector,
  sndsIpStatusFilterSelector,
  sndsIpStatusPaginationSelector,
  sndsIpStatusSelector,
  sndsIpStatusSortingSelector,
  sndsIsIpStatusLoadingSelector,
  usersMeSelector,
} from 'store/selectors';

import {
  DEMO_USER_EMAIL,
} from 'utils/constants';
import {
  convertDateTime,
  useDebounce,
  usePagination,
} from 'utils/helpers';

import './snds-ip-status-table.scss';

function SNDSIpStatusTables() {
  const t = useFormatMessage();
  const dispatch = useDispatch();

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;
 
  const sndsIsIpStatusLoading = useSelector(sndsIsIpStatusLoadingSelector);
  const sndsIpStatus = useSelector(sndsIpStatusSelector);
  const sndsIpStatusFilter = useSelector(sndsIpStatusFilterSelector);
  const sndsIpStatusPagination = useSelector(sndsIpStatusPaginationSelector);
  const sndsIpStatusSorting = useSelector(sndsIpStatusSortingSelector);
  
  const sndsIpStatusDateFrom = useSelector(sndsIpStatusDateFromSelector);
  const sndsIpStatusDateTo = useSelector(sndsIpStatusDateToSelector);

  const { ips: filterIps } = sndsIpStatusFilter;
  const { limit, offset } = sndsIpStatusPagination;
  const { field: sortField, order: sortOrder } = sndsIpStatusSorting;

  const debouncedFilterIp = useDebounce(filterIps, 1000);

  useEffect(() => {
    dispatch(getSndsIpStatus({ withLoader: false }));
  }, [
    dispatch,
    limit, offset, sortField, sortOrder,
    debouncedFilterIp,
    sndsIpStatusDateFrom, sndsIpStatusDateTo,
  ]);

  const { paginal, handlePageSize, handlePage } = usePagination({
    data: sndsIpStatusPagination,
    action: setSndsIpStatusDataPagination,
  });

  const tableData = useMemo(() => 
    sndsIpStatus?.map(item => {
      const {
        ip: itemIp = '',
        date_timestamp: itemDate = '',
        dns_reverse_result: itemRdns = [],
        blocked: itemBlocked = null,
        details: itemDetails = null,
      } = item;

      const ip = (
        <div className="ip-status__column">
          {itemIp}
        </div>
      );
      const dns_reverse_result = (
        <div className="ip-status__column">
          {itemRdns.map(({ rdns = ''}, i) => (
            <div key={`dns-reserve-result-ip-status-${rdns}-${i}`}>
              {rdns}
            </div>
          ))}
        </div>
      );
      const date_timestamp = (
        <div className="ip-status__column">
          {convertDateTime(itemDate, "DD-MM-YYYY")}
        </div>
      );
      const blocked = itemBlocked 
        ? (
          <div className="ip-status__column">
            {itemBlocked}
          </div>
        ) : null;
      const details = itemDetails 
        ? (
          <div className="ip-status__column">
            {itemDetails}
          </div>
        ) : null;
     
      return {
        ip,
        dns_reverse_result,
        date_timestamp,
        blocked,
        details,
      }
  }) ?? [], [sndsIpStatus]);

  if (sndsIsIpStatusLoading) return <Spinner />;

  return (
    <Fragment>
      <div className="ip-status__controls">
        {!!tableData?.length && (
          <Pagination
            page={paginal.page}
            pages={paginal.pages}
            pageSize={paginal.pageSize}
            handlePageSize={handlePageSize}
            handlePage={handlePage}
            className="ip-status__pagination ip-status__pagination--top"
          />
        )}
        <div className="ip-status__filters ips-filters">
          <SNDSIpStatusLoadingInfo />
          <SNDSIpStatusFilters />
        </div>
      </div>

      <Table
        columns={[
          {
            field: 'ip',
            title: t('snds.ip-status.table.header.ip.text'),
            type: 'string',
            width: 200,
          },
          {
            field: 'dns_reverse_result',
            title: t('snds.ip-status.table.header.rdns.text'),
            width: 400,
          },
          {
            field: 'date_timestamp',
            title: t('snds.ip-status.table.header.date.text'),
            width: 100,
          },
          {
            field: 'blocked',
            title: t('snds.ip-status.table.header.blocked.text'),
            width: 150,
          },
          {
            field: 'details',
            title: t('snds.ip-status.table.header.details.text'),
            width: 400,
          },
        ]}
        data={tableData}
        bluredColumns={meEmail === DEMO_USER_EMAIL ? ['ipData', 'dnsReverseResultData'] : []}
        sorting={{
          ...sndsIpStatusSorting,
          sortFields: ['ip', 'dns_reverse_result', 'date_timestamp'],
          isApiSorting: true,
        }}
        onSorting={(data) => dispatch(handleSndsIpStatusSorting(data))}
        statisticData={paginal}
      />

      {!!tableData?.length && (
        <Pagination
          page={paginal.page}
          pages={paginal.pages}
          pageSize={paginal.pageSize}
          handlePageSize={handlePageSize}
          handlePage={handlePage}
          className="ip-status__pagination ip-status__pagination--bottom"
        />
      )}
    </Fragment>
  );
}

export default React.memo(SNDSIpStatusTables);
