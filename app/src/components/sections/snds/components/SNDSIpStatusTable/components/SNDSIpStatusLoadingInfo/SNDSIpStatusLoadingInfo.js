import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button';

import {
  sndsIsIpStatusLoadedSelector,
  sndsIsIpStatusLoadingSelector,
} from 'store/selectors';
import { getSndsIpStatus } from 'store/actions/snds';

import './ip-status-loading-info.scss';

function SNDSIpStatusLoadingInfo() {
  const dispatch = useDispatch();
  const t = useFormatMessage();  
  
  const sndsIsIpStatusLoaded = useSelector(sndsIsIpStatusLoadedSelector);
  const sndsIsIpStatusLoading = useSelector(sndsIsIpStatusLoadingSelector);

  return (
    <div className="ip-status-loading-info">
      <Button
        mode="blue"
        text={t('snds.ip-status.loading-info.refresh.btn.text')}
        className="ip-status-loading-info__refresh"
        onClick={() => dispatch(getSndsIpStatus())}
        hasRefreshIcon
        disabled={!sndsIsIpStatusLoaded || sndsIsIpStatusLoading}
        isRotateAnimation={!sndsIsIpStatusLoaded || sndsIsIpStatusLoading}
      />
    </div>
  );
}

export default React.memo(SNDSIpStatusLoadingInfo);