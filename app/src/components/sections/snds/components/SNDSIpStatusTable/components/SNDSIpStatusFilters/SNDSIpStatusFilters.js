import React, { Fragment, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { differenceInCalendarDays } from 'date-fns';
import { ru, enUS } from 'date-fns/locale';

import Button from 'components/ui/button';
import DatePickerRange from 'components/ui/datePickerRange';
import MultipleSelect from 'components/ui/multipleSelect';
import Popup from 'components/ui/popup/Popup';

import {
  handleSndsIpStatusFilter,
  setSndsIpStatusDateFrom,
  setSndsIpStatusDateTo,
} from 'store/actions/snds';
import {
  localeCurrentSelector,
  sndsIpStatusDateFromSelector,
  sndsIpStatusDateToSelector,
  sndsIpStatusFilterSelector,
  sndsIpStatusUserIpsIsLoadingSelector,
  sndsIpStatusUserIpsSelector,
} from 'store/selectors';

import {
  DAY,
  DAY_BEFORE_YESTERDAY,
  LOCALE,
  MONTH,
  SNDS_DAYS_LIMIT,
  SNDS_MAX_CHOSE_USER_IPS,
} from 'utils/constants';

import './snds-ip-status-filters.scss';

function SNDSIpStatusFilters() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const sndsIpStatusUserIpsIsLoading = useSelector(sndsIpStatusUserIpsIsLoadingSelector);
  const sndsIpStatusUserIps = useSelector(sndsIpStatusUserIpsSelector);
  const sndsIpStatusFilter = useSelector(sndsIpStatusFilterSelector);

  const sndsIpStatusDateFrom = useSelector(sndsIpStatusDateFromSelector);
  const sndsIpStatusDateTo = useSelector(sndsIpStatusDateToSelector);

  const [showPopup, setShowPopup] = useState(false);

  const handleChangeDateFrom = (date) => {
    if (date) {
      dispatch(setSndsIpStatusDateFrom(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
      if (
        differenceInCalendarDays(new Date(sndsIpStatusDateTo), date) > SNDS_DAYS_LIMIT
      ) {
        dispatch(setSndsIpStatusDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
      }
    } else {
      dispatch(setSndsIpStatusDateFrom(''));
    }
    if (date >= new Date(sndsIpStatusDateTo)) {
      dispatch(setSndsIpStatusDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
    }
  };

  const handleChangeDateTo = (date) => {
    if (date) {
      dispatch(setSndsIpStatusDateTo(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
    } else {
      dispatch(setSndsIpStatusDateTo(''));
    }
  };

  const handleChangeIp = (values) => {
    if (values.length > SNDS_MAX_CHOSE_USER_IPS) {
      setShowPopup(true);
    } else {
      dispatch(
        handleSndsIpStatusFilter({ field: 'ips', value: values.map(val => val.value) })
      );
    }
  }

  const userIpsSelectOptions = sndsIpStatusUserIps.map(({ ip }, i) => ({ value: ip, label: ip }));

  return (
    <Fragment>
      <div className="ips-filters__user-ips">
        <div className="ips-filters__label">
          {t('snds.ip-status.table.filter.select.label')}
        </div>
        <MultipleSelect 
          values={sndsIpStatusFilter.ips}
          onChange={handleChangeIp}
          options={userIpsSelectOptions}
          placeholder={
            sndsIpStatusUserIpsIsLoading
              ? t('snds.ip-status.table.filter.select.placeholder.loading.text')
              : t('snds.ip-status.table.filter.select.placeholder.text')
          }
          disabled={!userIpsSelectOptions.length}
          className="ips-filters__select"
        />
      </div>

      <div className="ips-filters__dates-range">
        <DatePickerRange 
          locale={locale === LOCALE.ru ? ru : enUS}

          labelInputFrom={t('snds.ip-status.table.filter.dates-range.from.label')}
          labelInputTo={t('snds.ip-status.table.filter.dates-range.to.label')}

          dateFormat="DD-MM-yyyy"
          dateFrom={sndsIpStatusDateFrom}
          dateTo={sndsIpStatusDateTo}
          handleChangeDateFrom={handleChangeDateFrom}
          handleChangeDateTo={handleChangeDateTo}

          maxDate={new Date(DAY_BEFORE_YESTERDAY).toISOString()}
          maxDaysDateTo={SNDS_DAYS_LIMIT}

          disabledTo={!sndsIpStatusDateFrom}
          requiredDateTo={!!sndsIpStatusDateFrom && !sndsIpStatusDateTo}
        />
      </div>

      <Popup
        isOpen={showPopup}
        onClose={() => setShowPopup(false)}
        wrapperStyle="ips-filters__popup ips-popup"
      >
        <h4 className="ips-popup__header">
          {t('snds.ip-status.popup.error.title')}
        </h4>
        <div className="ips-popup__description">
          {t('snds.ip-status.popup.error.description.text1')}
          {' '}
          <strong>
            {SNDS_MAX_CHOSE_USER_IPS}
          </strong>
          {' '}
          {t('snds.ip-status.popup.error.description.text2')}
        </div>
        <Button
          text={t('snds.ip-status.popup.error.button.close.text')}
          className="ips-popup__btn"
          onClick={() => setShowPopup(false)}
        />
      </Popup>
    </Fragment>
  );
}

export default React.memo(SNDSIpStatusFilters);
