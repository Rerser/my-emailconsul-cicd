import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Icon from 'react-icons-kit';
import { basic_trashcan } from 'react-icons-kit/linea/basic_trashcan';

import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';
import Checkbox from 'components/ui/checkbox';

import {
  sndsIntegrationCredentialsKeySelector,
  sndsIntegrationCredentialsSelector,
  sndsIntegrationIsCredentialsDataLoadingSelector,
  sndsIntegrationIsCredentialsLoadingSelector,
} from 'store/selectors';
import {
  deleteSndsCredentialKey,
  sendSndsCredentials,
  setSndsCredentialsKey,
  setSndsIsOnAddKey,
  updateSndsCredentialKey,
} from 'store/actions/sndsIntegration';

import {
  USER_SNDS_CREDENTIALS_STATUS,
} from 'utils/constants';

import cn from 'classnames';

import './snds-credentials.scss';

const SNDSCredentials = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  useEffect(() => {
    return () => {
      dispatch(setSndsCredentialsKey(''));
    }
  }, [dispatch]);

  const sndsIntegrationIsCredentialsLoading = useSelector(sndsIntegrationIsCredentialsLoadingSelector);
  const sndsIntegrationIsCredentialsDataLoading = useSelector(sndsIntegrationIsCredentialsDataLoadingSelector);
  const sndsIntegrationCredentials = useSelector(sndsIntegrationCredentialsSelector);
  const sndsIntegrationCredentialsKey = useSelector(sndsIntegrationCredentialsKeySelector);

  const handleChange = (item) => {
    const { id = '', status = '' } = item;

    if (status !== USER_SNDS_CREDENTIALS_STATUS.FAILED) {
      const newStatus = status === USER_SNDS_CREDENTIALS_STATUS.ACTIVE
        ? USER_SNDS_CREDENTIALS_STATUS.TERMINATED 
        : USER_SNDS_CREDENTIALS_STATUS.ACTIVE;

      dispatch(updateSndsCredentialKey({ id, status: newStatus}));
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(sendSndsCredentials(sndsIntegrationCredentialsKey));
  };

  return (
    <form
      className="c-slidebar-content c-snds__credentials snds-credentials" 
      onSubmit={handleSubmit}
    >
      <h3 className="c-slidebar-content__title">
        {t('snds.add-key-slidebar.header.text')}
      </h3>

      {
        sndsIntegrationIsCredentialsDataLoading
          ? <Spinner />
          : (
            <Fragment>
              <div className="c-slidebar-content__row">
                {
                  sndsIntegrationCredentials?.map((item, i) => (
                    <Fragment key={`table-credential-${i}`}>
                      <div className="snds-credentials__controls">
                        <div className={cn("snds-credentials__column", {
                          "snds-credentials__column--failed": item.status === USER_SNDS_CREDENTIALS_STATUS.FAILED
                        })}>
                          {item.key}
                        </div>
                        <div className="snds-credentials__column">
                          <Checkbox
                            className="snds-credentials__checkbox"
                            checked={item.status === USER_SNDS_CREDENTIALS_STATUS.ACTIVE}
                            onChange={() => handleChange(item)}
                          />
                          <Icon
                            icon={basic_trashcan}
                            className="snds-credentials__delete"
                            onClick={() => dispatch(deleteSndsCredentialKey(item.id))}
                          />
                        </div>
                      </div>
                      <div className="c-slidebar-content__div-line c-slidebar-content__div-line--low" />
                    </Fragment>
                  )) ?? null
                }
              </div>
              <div className="c-slidebar-content__row">
                <label className="c-slidebar-content__label">
                  <span className="c-slidebar-content__description">
                    {t('snds.add-key-slidebar.input.add-key.label')}
                  </span>
                  <InputText
                    value={sndsIntegrationCredentialsKey}
                    onChange={e => dispatch(setSndsCredentialsKey(e.target.value))}
                    className="snds-credentials__input"
                  />
                </label>
              </div>
            </Fragment>
          )
      }

      <div className="c-slidebar-content__btns">
        <Button
          mode="purple"
          text={t('snds.add-key-slidebar.btn.add-key.text')}
          className="c-slidebar-content__btn"
          type="button"
          onClick={() => dispatch(sendSndsCredentials(sndsIntegrationCredentialsKey))}
          disabled={
            !sndsIntegrationCredentialsKey ||
            sndsIntegrationIsCredentialsLoading ||
            sndsIntegrationIsCredentialsDataLoading
          }
          hasRefreshIcon
          isRotateAnimation={
            sndsIntegrationIsCredentialsLoading ||
            sndsIntegrationIsCredentialsDataLoading
          }
        />

        <Button
          text={t('snds.add-key-slidebar.btn.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setSndsIsOnAddKey(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(SNDSCredentials);
