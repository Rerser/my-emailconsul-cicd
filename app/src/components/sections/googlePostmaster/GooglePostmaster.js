import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Spinner from 'components/ui/spinner/Spinner';
import SlideBar from 'components/ui/slideBar';
import Error404 from 'components/ui/error404';
import HangingBtn from 'components/ui/hangingBtn';

import {
  clearGpAllData,
  handleGpDataFilter,
  setGpChosenData,
  setIsOnNewGpDomain
} from 'store/actions/googlePostmaster';
import {
  setGpSecretsIsShowSlider,
  setGpDateRunIsShowSlider,
  setGpDomainsHistoryIsShowSlider,
  clearGpIntegrationAllData,
} from 'store/actions/gpIntegration';
import {
  myUserBillingPlanIsDataLoadingSelector,
  googlePostmasterIsOnNewDomainSelector,
  googlePostmasterErrorsSelector,
  googlePostmasterChosenDataSelector,
  googlePostmasterShowTableModeSelector,
  googlePostmasterDataSelector,
  googlePostmasterDateFromSelector,
  googlePostmasterDateToSelector,
  gpIntegrationGPDateRunIsShowSliderSelector,
  gpIntegrationGPSecretsIsShowSliderSelector,
  gpIntegrationGPDomainsHistoryIsShowSliderSelector,
} from 'store/selectors';

import AddGPDomain from './components/AddGPDomain';
import GPDomainsListHeader from './components/GPDomainsListHeader';
import GPDatesRange from './components/GPDatesRange';
import GPDomainFilter from './components/GPDomainFilter';
import TableGPDomainList from './components/TableGPDomainList';
import TableGPIpList from './components/TableGPIpList';
import ChosenGPDomain from './components/ChosenGPDomain';
import getGPTableDomainData from './components/TableGPDomainList/components/GPTableDomainData';
import getGPTableIpData from './components/TableGPIpList/components/GPTableIPData';
import GPCheckDateRun from './components/GPCheckDateRun';
import GPIntegrate from './components/GPIntegrate';
import GPDomainsHistory from './components/GPDomainsHistory';
import GPLoadingInfo from './components/GPLoadingInfo';

import { GP_SHOW_TABLE, LANDING_FAQ_URL, NOT_FOUND_ERROR_STATUS } from 'utils/constants';

import './google-postmaster.scss';

function GooglePostmaster() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);

  const googlePostmasterIsOnNewDomain = useSelector(googlePostmasterIsOnNewDomainSelector);
  const googlePostmasterErrors = useSelector(googlePostmasterErrorsSelector);
  const googlePostmasterChosenData = useSelector(googlePostmasterChosenDataSelector);
  const googlePostmasterShowTableMode = useSelector(googlePostmasterShowTableModeSelector);
  
  const googlePostmasterData = useSelector(googlePostmasterDataSelector);
  const googlePostmasterDateFrom = useSelector(googlePostmasterDateFromSelector);
  const googlePostmasterDateTo = useSelector(googlePostmasterDateToSelector);

  const gpIntegrationGPSecretsIsShowSlider = useSelector(gpIntegrationGPSecretsIsShowSliderSelector);
  const gpIntegrationGPDomainsHistoryIsShowSlider = useSelector(gpIntegrationGPDomainsHistoryIsShowSliderSelector);
  const gpIntegrationGPDateRunIsShowSlider = useSelector(gpIntegrationGPDateRunIsShowSliderSelector);

  useEffect(() => {
    return () => {
      dispatch(handleGpDataFilter({ field: 'domain', value: '' }));
      dispatch(handleGpDataFilter({ field: 'ip', value: '' }));
      dispatch(clearGpAllData());
      dispatch(clearGpIntegrationAllData());
    };
  }, [dispatch]);

  const showNewGPDomainSlider = useMemo(() => !!googlePostmasterIsOnNewDomain, [
    googlePostmasterIsOnNewDomain,
  ]);

  const tableDomainData = useMemo(
    () => {
      if (!googlePostmasterDateFrom || !googlePostmasterDateTo) return { data: [], emptyData: []};
      return getGPTableDomainData({ googlePostmasterData, googlePostmasterDateFrom, googlePostmasterDateTo });
    },
    [googlePostmasterData, googlePostmasterDateFrom, googlePostmasterDateTo]
  );

  const tableIpData = useMemo(
    () => {
      if (!googlePostmasterDateFrom || !googlePostmasterDateTo) return {};
      return getGPTableIpData({ googlePostmasterData, googlePostmasterDateFrom, googlePostmasterDateTo });
    },
    [googlePostmasterData, googlePostmasterDateFrom, googlePostmasterDateTo]
  );

  const handleChoseDomainFromIp = (domain) => {
    const domainsList = [...tableDomainData.data, ...tableDomainData.emptyData];
    if (domain && domainsList.length) {
      const foundDomainData = domainsList.find(data => data.domain === domain);

      if (foundDomainData) {
        dispatch(setGpChosenData(foundDomainData));
      }
    }
  }

  if (myUserBillingPlanIsDataLoading) return <Spinner />;
  if (googlePostmasterErrors?.data?.status === NOT_FOUND_ERROR_STATUS)
    return <Error404 />;

  return (
    <div className="gp-domains-list">
      <GPDomainsListHeader />

      <div className="gp-domains-list__filter">
        <GPLoadingInfo />
        <GPDomainFilter />
        <GPDatesRange />
      </div>

      {googlePostmasterShowTableMode === GP_SHOW_TABLE.DOMAIN && (
        <TableGPDomainList
          domainsData={tableDomainData}
        />
      )}
      {googlePostmasterShowTableMode === GP_SHOW_TABLE.IP && (
        <TableGPIpList
          data={tableIpData}
          onChoseDomain={handleChoseDomainFromIp}
        />
      )}

      <SlideBar
        isOpen={showNewGPDomainSlider}
        onClose={() => dispatch(setIsOnNewGpDomain(false))}
        className="gp-domains-list__new-domain"
      >
        <AddGPDomain />
      </SlideBar>

      <SlideBar
        isOpen={!!googlePostmasterChosenData}
        onClose={() => dispatch(setGpChosenData(null))}
        className="gp-domains-list__gp-chosen-domain"
      >
        <ChosenGPDomain data={googlePostmasterChosenData} />
      </SlideBar>

      <SlideBar
        isOpen={gpIntegrationGPSecretsIsShowSlider}
        onClose={() => dispatch(setGpSecretsIsShowSlider(false))}
        className="gp-domains-list__gp-integrate"
      >
        <GPIntegrate />
      </SlideBar>

      <SlideBar
        isOpen={gpIntegrationGPDateRunIsShowSlider}
        onClose={() => dispatch(setGpDateRunIsShowSlider(false))}
        className="gp-domains-list__gp-date-run"
      >
        <GPCheckDateRun />
      </SlideBar>

      <SlideBar
        isOpen={gpIntegrationGPDomainsHistoryIsShowSlider}
        onClose={() => dispatch(setGpDomainsHistoryIsShowSlider(false))}
        className="gp-domains-list__gp-gomains-history"
      >
        <GPDomainsHistory />
      </SlideBar>

      <HangingBtn 
        tooltipText={t('seedlisting_list.hanging-btn.tootip.text')}
        href={`${LANDING_FAQ_URL}/index.html?faqItem=googlePostmasterFaq`}
      />
    </div>
  );
}

export default React.memo(GooglePostmaster);
