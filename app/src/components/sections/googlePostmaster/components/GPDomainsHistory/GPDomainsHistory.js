import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import {
  gpIntegrationGPDomainsHistorySelector,
  gpIntegrationGPDomainsHistoryIsLoadingSelector,
  gpIntegrationGPDomainsHistoryIsLoadedSelector,
} from 'store/selectors';
import {
  getGpDomainsHistory,
  setGpDomainsHistoryIsShowSlider,
} from 'store/actions/gpIntegration';

import { convertDateTime, getFormattedDaysMMDDFromYYYYMMDD } from 'utils/helpers';
import { GP_DOMAINS_HISTORY_STATUS } from 'utils/constants';

import './gp-domains-history.scss';

const GPDomainsHistory = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const gpIntegrationGPDomainsHistory = useSelector(gpIntegrationGPDomainsHistorySelector);
  const gpIntegrationGPDomainsHistoryIsLoading = useSelector(gpIntegrationGPDomainsHistoryIsLoadingSelector);
  const gpIntegrationGPDomainsHistoryIsLoaded = useSelector(gpIntegrationGPDomainsHistoryIsLoadedSelector);

  const limit = 5;
  const offset = 0;

  useEffect(() => {
    dispatch(getGpDomainsHistory({ limit, offset }));
  }, [dispatch, limit, offset])

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(getGpDomainsHistory({ withLoader: false, limit, offset }));
  };

  return (
    <form className="c-slidebar-content c-gp-domains-history" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('gp_integration.history.title')}
      </h3>

      {
        gpIntegrationGPDomainsHistoryIsLoading
          ? <Spinner />
          : (
            <ul className="c-slidebar-content__label c-gp-domains-history__list">
              {gpIntegrationGPDomainsHistory.map((gpDomainHistory, i) => (
                <li key={`gpDomainHistory-${i}`} className="c-gp-domains-history__item">
                  <div className="c-gp-domains-history__info-row">
                    <span className="c-gp-domains-history__key">
                      {getFormattedDaysMMDDFromYYYYMMDD(gpDomainHistory.key)}
                    </span>
                    {' - '}
                    <span className={cn("c-gp-domains-history__status", {
                      'c-gp-domains-history__status--done': gpDomainHistory.status === GP_DOMAINS_HISTORY_STATUS.DONE,
                      'c-gp-domains-history__status--failed': gpDomainHistory.status === GP_DOMAINS_HISTORY_STATUS.FAILED,
                      'c-gp-domains-history__status--is-progress': gpDomainHistory.status === GP_DOMAINS_HISTORY_STATUS.IN_PROGRESS,
                    })}>
                      {gpDomainHistory.status}
                    </span>
                  </div>
                  {
                    gpDomainHistory.status === GP_DOMAINS_HISTORY_STATUS.FAILED && gpDomainHistory.payload && (
                      <div className="c-gp-domains-history__info-row">
                        <span className="c-gp-domains-history__text c-gp-domains-history__text--error">
                          {gpDomainHistory.payload}
                        </span>
                      </div>
                    )
                  }
                  <div className="c-gp-domains-history__info-row">
                    <span className="c-gp-domains-history__text">
                      {t('gp_integration.history.started_at.text')}
                    </span>
                    {' '}
                    <span className="c-gp-domains-history__value">
                      {convertDateTime(gpDomainHistory.created_at, 'DD-MM-YYYY hh:mm')}
                    </span>
                  </div>
                  {
                    (gpDomainHistory.status === GP_DOMAINS_HISTORY_STATUS.DONE || gpDomainHistory.status === GP_DOMAINS_HISTORY_STATUS.FAILED) && (
                      <div className="c-gp-domains-history__info-row">
                        <span className="c-gp-domains-history__text">
                          {t('gp_integration.history.done_at.text')}
                        </span>
                        {' '}
                        <span className="c-gp-domains-history__value">
                          {convertDateTime(gpDomainHistory.updated_at, 'DD-MM-YYYY hh:mm')}
                        </span>
                      </div>
                    )
                  }
                  {i !== gpIntegrationGPDomainsHistory.length - 1 && <div className="c-slidebar-content__div-line" /> }
                </li>
              ))}
            </ul>
          )
      }

      <div className="c-slidebar-content__btns">
        
        <Button
          mode="blue"
          text={t('gp_integration.history.button.refresh.text')}
          className="c-slidebar-content__btn"
          type="submit"
          hasRefreshIcon
          disabled={!gpIntegrationGPDomainsHistoryIsLoaded}
          isRotateAnimation={!gpIntegrationGPDomainsHistoryIsLoaded}
        />
        <Button
          text={t('gp_integration.history.button.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setGpDomainsHistoryIsShowSlider(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(GPDomainsHistory);
