import React from 'react';
import { useSelector } from 'react-redux';
import cn from 'classnames';

import { usersMeSelector } from 'store/selectors';
import { DEMO_USER_EMAIL, GP_REPUTATION_CATEGORY } from 'utils/constants';
import { getFormattedDaysMMDDFromYYYYMMDD } from 'utils/helpers';

export const GP_TABLE_COLUMN_DOMAIN_CLASS = 'gp-table-ip__column--domain';
export const GP_TABLE_COLUMN_BAD_REPUTATION_DOMAIN_CLASS = 'gp-table__column--bad-reputation-domain';
export const GP_TABLE_COLUMN_LOW_REPUTATION_DOMAIN_CLASS = 'gp-table__column--low-reputation-domain';
export const GP_TABLE_COLUMN_MEDIUM_REPUTATION_DOMAIN_CLASS = 'gp-table__column--medium-reputation-domain';
export const GP_TABLE_COLUMN_HIGH_REPUTATION_DOMAIN_CLASS = 'gp-table__column--high-reputation-domain';
export const GP_TABLE_COLUMN_NO_INFO_REPUTATION_DOMAIN_CLASS = 'gp-table__column--no-info-reputation-domain';

export default function TableRowIp({ ip, data, onClick }) {
  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;
  const domains = [...data.domains];

  return (
    <div className="gp-table-ip__row" onClick={onClick}>
      <div className="gp-table-ip__row">
        <div className="gp-table-ip__column">
          <span
            className={cn('gp-table-ip__column-content', {
              'gp-table-ip__column-content--blured': meEmail === DEMO_USER_EMAIL,
            })}
          >
            {ip}
          </span>
        </div>
        <div className="gp-table-ip__column gp-table-ip__column--ip-reputation">
          {data.reputations.map((rep, i) => (
            <div
              key={`${data.id}-reputation-${i}`}
              className={cn('gp-table-ip__reputation-item', {
                [`gp-table-ip__reputation-item--${rep.reputation}`]: rep.reputation,
              })}
              data-tooltip={getFormattedDaysMMDDFromYYYYMMDD(rep.key)}
            />
          ))}
        </div>
        <div className={cn(`gp-table-ip__column ${GP_TABLE_COLUMN_DOMAIN_CLASS}`)}>
          {domains.map((domain, i) => (
            <span
              key={`domain-${i}`}
              className={cn('gp-table-ip__domain-value', {
                'gp-table-ip__domain-value--blured': meEmail === DEMO_USER_EMAIL,
              })}
            >
              {domain}
              {i < domains.length - 1 && ', '}
            </span>
          ))}
        </div>
        <div className={cn(`gp-table-ip__column ${GP_TABLE_COLUMN_BAD_REPUTATION_DOMAIN_CLASS}`, {
          'gp-table-ip__column--hovered': data.domainReputations[GP_REPUTATION_CATEGORY.BAD].size
        })}>
          {data.domainReputations[GP_REPUTATION_CATEGORY.BAD].size || '-'}
        </div>
        <div className={cn(`gp-table-ip__column ${GP_TABLE_COLUMN_LOW_REPUTATION_DOMAIN_CLASS}`, {
          'gp-table-ip__column--hovered': data.domainReputations[GP_REPUTATION_CATEGORY.LOW].size
        })}>
          {data.domainReputations[GP_REPUTATION_CATEGORY.LOW].size || '-'}
        </div>
        <div className={cn(`gp-table-ip__column ${GP_TABLE_COLUMN_MEDIUM_REPUTATION_DOMAIN_CLASS}`, {
          'gp-table-ip__column--hovered': data.domainReputations[GP_REPUTATION_CATEGORY.MEDIUM].size
        })}>
          {data.domainReputations[GP_REPUTATION_CATEGORY.MEDIUM].size || '-'}
        </div>
        <div className={cn(`gp-table-ip__column ${GP_TABLE_COLUMN_HIGH_REPUTATION_DOMAIN_CLASS}`, {
          'gp-table-ip__column--hovered': data.domainReputations[GP_REPUTATION_CATEGORY.HIGH].size
        })}>
          {data.domainReputations[GP_REPUTATION_CATEGORY.HIGH].size || '-'}
        </div>
        <div className={cn(`gp-table-ip__column ${GP_TABLE_COLUMN_NO_INFO_REPUTATION_DOMAIN_CLASS}`, {
          'gp-table-ip__column--hovered': data.domainReputations.NO_INFO.size
        })}>
          {data.domainReputations.NO_INFO.size || '-'}
        </div>
      </div>
    </div>
  );
}
