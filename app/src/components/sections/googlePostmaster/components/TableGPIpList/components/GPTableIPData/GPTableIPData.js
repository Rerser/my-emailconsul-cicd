import { GP_REPUTATION_CATEGORY } from 'utils/constants';
import { getDifferenceDays, addNDayToDate, getFormattedDaysYYYYMMDD } from 'utils/helpers';

export default function getGPTableIpData ({
  googlePostmasterData,
  googlePostmasterDateFrom,
  googlePostmasterDateTo,
}) {
  const daysCount = getDifferenceDays(googlePostmasterDateTo, googlePostmasterDateFrom); // inclusive last
  const ipsData = {};

  googlePostmasterData.forEach(domain => {
    if (domain?.traffic_stats?.length) {

      domain.traffic_stats.forEach(ts => {
        if (ts?.ipReputations?.length) {

          ts.ipReputations.forEach(ipRep => {
            if (ipRep?.sampleIps.length) {

              ipRep.sampleIps.forEach(ip => {

                function setCurrentIpsData(ip) {
                  if (!ipsData[ip]) {
                    ipsData[ip] = {
                      reputations: [],
                      domains: new Set([domain.domain]),
                      domainReputations: {
                        [GP_REPUTATION_CATEGORY.BAD]: new Set(),
                        [GP_REPUTATION_CATEGORY.LOW]: new Set(),
                        [GP_REPUTATION_CATEGORY.MEDIUM]: new Set(),
                        [GP_REPUTATION_CATEGORY.HIGH]: new Set(),
                        NO_INFO: new Set(),
                      },
                    }
                  } else {
                    ipsData[ip].domains.add(domain.domain);

                    if (!ipsData[ip].reputations.some(r => r.key === ts.key)) {
                      ipsData[ip].reputations.push({
                        key: ts.key,
                        reputation: ipRep.reputation,
                      });
                    }
                  }
                  if (ipsData[ip].domainReputations[ts.domainReputation]) {
                    ipsData[ip].domainReputations[ts.domainReputation].add(domain.domain);
                  } else {
                    ipsData[ip].domainReputations.NO_INFO.add(domain.domain);
                  }
                }

                if (ip.includes('-')) {
                  const [startIp, endIp] = ip.split('-');
                  const [a, b, c, startAddr] = startIp.split('.');
                  const [,,, endAddr] = endIp.split('.');
                  const numberStartAddr = Number(startAddr);
                  const numberEndAddr = Number(endAddr);
                  const length = numberEndAddr - numberStartAddr;
                  const subNetIp = `${a}.${b}.${c}`;

                  for (let i = numberStartAddr; i <= length + numberStartAddr; i++ ) {
                    setCurrentIpsData(`${subNetIp}.${i}`);
                  }
                } else {
                  setCurrentIpsData(ip);
                }

              });

            }
          });

        }
      });

    }
  });

  for (let i = 0; i < daysCount; i++) {
    const day = addNDayToDate(googlePostmasterDateFrom, i);
    const currentDayFormatted = getFormattedDaysYYYYMMDD(day);

    // get other ips reputations days
    Object.keys(ipsData).forEach(ip => {
      const foundReputation = ipsData[ip].reputations.find(ipRep => ipRep.key === currentDayFormatted);

      if (!foundReputation) {
        ipsData[ip].reputations.push({
          key: currentDayFormatted,
          reputation: GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED,
        });
      }
    });
  }

  // sort by key
  Object.keys(ipsData).forEach(ip => {
    ipsData[ip].reputations.sort((a, b) => Number(a.key) - Number(b.key));
  });

  return ipsData;
}
