import React, { Fragment, useMemo, useState} from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import Spinner from 'components/ui/spinner/Spinner';
import ContextMenu from 'components/ui/contextMenu';

import { handleContextMenuShow, setContextMenuCoords } from 'store/actions/contextMenu';
import {
  googlePostmasterIsDataLoadingSelector,
  googlePostmasterDataFilterSelector,
  googlePostmasterDateFromSelector,
  googlePostmasterDateToSelector,
  contextMenuIsShowSelector,
  usersMeSelector,
} from 'store/selectors';

import { getFormattedDaysMMDD, useDebounce } from 'utils/helpers';
import { DEMO_USER_EMAIL, GP_REPUTATION_CATEGORY } from 'utils/constants';

import GPIpListTableRow, {
  GP_TABLE_COLUMN_DOMAIN_CLASS,
  GP_TABLE_COLUMN_BAD_REPUTATION_DOMAIN_CLASS,
  GP_TABLE_COLUMN_LOW_REPUTATION_DOMAIN_CLASS,
  GP_TABLE_COLUMN_MEDIUM_REPUTATION_DOMAIN_CLASS,
  GP_TABLE_COLUMN_HIGH_REPUTATION_DOMAIN_CLASS,
  GP_TABLE_COLUMN_NO_INFO_REPUTATION_DOMAIN_CLASS
} from './components/GPIpListTableRow';

import './table-gp-ip-list.scss';

function TableGPIpList({ data, onChoseDomain }) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const me = useSelector(usersMeSelector);
  const googlePostmasterIsDataLoading = useSelector(googlePostmasterIsDataLoadingSelector);
  const googlePostmasterDataFilter = useSelector(googlePostmasterDataFilterSelector);
  const googlePostmasterDateFrom = useSelector(googlePostmasterDateFromSelector);
  const googlePostmasterDateTo = useSelector(googlePostmasterDateToSelector);
  const contextMenuIsShow = useSelector(contextMenuIsShowSelector);

  const { email: meEmail } = me;
  const { ip: filterIp } = googlePostmasterDataFilter;
  const debouncedFilterIp = useDebounce(filterIp, 1000);

  const [contextDomains, setContextDomains] = useState(null);
  const [ipReputationFilters, setIpReputationFilters] = useState([]);

  const handleFilterIpReputation = (reputation) => {
    if (!ipReputationFilters.includes(reputation)) {
      setIpReputationFilters([...ipReputationFilters, reputation]);
    } else {
      const foundIndex = ipReputationFilters.findIndex(domRep => domRep === reputation);
      if (foundIndex > -1) {
        const newDomainReputationFilters = [...ipReputationFilters];
        newDomainReputationFilters.splice(foundIndex, 1);
        setIpReputationFilters(newDomainReputationFilters);
      }
    }
  }

  const handleClickTableIpItem = (e, data) => {
    if (window.getSelection().toString()) return;

    const target = e.target;
    const domains = [...data.domains];
    const domainReputations = data.domainReputations;

    e.stopPropagation();

    if (target.closest(`.${GP_TABLE_COLUMN_DOMAIN_CLASS}`)) {
      if (domains.length > 1) {
        dispatch(setContextMenuCoords({
          x: e.clientX,
          y: e.clientY
        }));
        dispatch(handleContextMenuShow(true));
        return setContextDomains(domains);
      } else {
        return onChoseDomain(domains[0]);
      }
    } else {
      let domainRepList = [];

      if (target.closest(`.${GP_TABLE_COLUMN_BAD_REPUTATION_DOMAIN_CLASS}`)) {
        domainRepList = [...domainReputations[GP_REPUTATION_CATEGORY.BAD]];
      } else if (target.closest(`.${GP_TABLE_COLUMN_LOW_REPUTATION_DOMAIN_CLASS}`)) {
        domainRepList = [...domainReputations[GP_REPUTATION_CATEGORY.LOW]];
      } else if (target.closest(`.${GP_TABLE_COLUMN_MEDIUM_REPUTATION_DOMAIN_CLASS}`)) {
        domainRepList = [...domainReputations[GP_REPUTATION_CATEGORY.MEDIUM]];
      } else if (target.closest(`.${GP_TABLE_COLUMN_HIGH_REPUTATION_DOMAIN_CLASS}`)) {
        domainRepList = [...domainReputations[GP_REPUTATION_CATEGORY.HIGH]];
      } else if (target.closest(`.${GP_TABLE_COLUMN_NO_INFO_REPUTATION_DOMAIN_CLASS}`)) {
        domainRepList = [...domainReputations.NO_INFO];
      }
      if (domainRepList.length) {
        setContextDomains(domainRepList);
        dispatch(setContextMenuCoords({
          x: e.clientX,
          y: e.clientY
        }));
        return dispatch(handleContextMenuShow(true));
      } else {
        return setContextDomains(null);
      }
    }
  }

  const tableData = useMemo(
    () => {
      const filteredData = {};

      for (const ip in data) {
        if ((debouncedFilterIp && ip.includes(debouncedFilterIp)) || !debouncedFilterIp) {
          if (ipReputationFilters.length) {
            const filteredByReputation = {};

            for (const filter of ipReputationFilters) {
              if (data[ip].reputations.some(ipRep => ipRep.reputation.includes(filter))) {
                filteredByReputation[ip] = data[ip];
              }
            }
            if (Object.keys(filteredByReputation).length) {
              filteredData[ip] = filteredByReputation[ip];
            }
          } else {
            filteredData[ip] = data[ip];
          }
        }
      }
      return filteredData;
    },
    [data, debouncedFilterIp, ipReputationFilters]
  );

  if (googlePostmasterIsDataLoading) {
    return <Spinner />;
  }
  if (!googlePostmasterDateTo) {
    return (
      <div>
        {t('table.no.date_to.text')}
      </div>
    );
  }
  if (!Object.keys(tableData).length && !ipReputationFilters.length) {
    return (
      <div>
        {t('table.no.data.text')}
      </div>
    );
  }
  return (
    <Fragment>
      <div className="gp-domains-list__gp-table gp-table-ip">
        <div className="gp-table-ip__wrapper">
          <div className="gp-table-ip__main-header">
            <div className="gp-table-ip__column gp-table-ip__column--main-header">
              {t('gpips_list.gp-table.main-header.ip.text')}
            </div>
            <div className="gp-table-ip__column gp-table-ip__column--main-header reputation">
              <span className="gp-table-ip__date gp-table-ip__date--from">
                {getFormattedDaysMMDD(googlePostmasterDateFrom)}
              </span>
              <span>
                {t('gpips_list.gp-table.main-header.ip-reputation.text')}
              </span>
              <span className="gp-table-ip__date gp-table-ip__date--to">
                {getFormattedDaysMMDD(googlePostmasterDateTo)}
              </span>
            </div>
            <div className="gp-table-ip__column gp-table-ip__column--main-header">
              {t('gpips_list.gp-table.main-header.domain.text')}
            </div>
            <div className="gp-table-ip__column gp-table-ip__column--main-header">
              {t('gpips_list.gp-table.main-header.domain-reputation.text')}
            </div>
          </div>
          <div className="gp-table-ip__header">
            <div className="gp-table-ip__column gp-table-ip__column--header">
              <div className="gp-table-ip__reputation-filters">
                <div
                  className={cn("gp-table-ip__reputation-filter-item gp-table-ip__reputation-filter-item--high", {
                    'gp-table-ip__reputation-filter-item--selected': ipReputationFilters.includes(GP_REPUTATION_CATEGORY.HIGH)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.high.text')}
                  onClick={() => handleFilterIpReputation(GP_REPUTATION_CATEGORY.HIGH)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.high.text')}
                </div>
                <div
                  className={cn("gp-table-ip__reputation-filter-item gp-table-ip__reputation-filter-item--medium", {
                    'gp-table-ip__reputation-filter-item--selected': ipReputationFilters.includes(GP_REPUTATION_CATEGORY.MEDIUM)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.medium.text')}
                  onClick={() => handleFilterIpReputation(GP_REPUTATION_CATEGORY.MEDIUM)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.medium.text')}
                </div>
                <div
                  className={cn("gp-table-ip__reputation-filter-item gp-table-ip__reputation-filter-item--low", {
                    'gp-table-ip__reputation-filter-item--selected': ipReputationFilters.includes(GP_REPUTATION_CATEGORY.LOW)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.low.text')}
                  onClick={() => handleFilterIpReputation(GP_REPUTATION_CATEGORY.LOW)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.low.text')}
                </div>
                <div
                  className={cn("gp-table-ip__reputation-filter-item gp-table-ip__reputation-filter-item--bad", {
                    'gp-table-ip__reputation-filter-item--selected': ipReputationFilters.includes(GP_REPUTATION_CATEGORY.BAD)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.bad.text')}
                  onClick={() => handleFilterIpReputation(GP_REPUTATION_CATEGORY.BAD)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.bad.text')}
                </div>
                <div
                  className={cn("gp-table-ip__reputation-filter-item gp-table-ip__reputation-filter-item--unknown", {
                    'gp-table-ip__reputation-filter-item--selected': ipReputationFilters.includes(GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.unknown.text')}
                  onClick={() => handleFilterIpReputation(GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.unknown.text')}
                </div>
              </div>
            </div>
            
            <div
              className="gp-table-ip__column gp-table-ip__column--header gp-table-ip__column--no-pointer"
            />

            <div
              className="gp-table-ip__column gp-table-ip__column--header gp-table-ip__column--no-pointer"
            >
              {t('gpdomains_list.gp-table.main-header.domain-reputation.bad.text')}
            </div>
            <div
              className="gp-table-ip__column gp-table-ip__column--header gp-table-ip__column--no-pointer"
            >
              {t('gpdomains_list.gp-table.main-header.domain-reputation.low.text')}
            </div>
            <div
              className="gp-table-ip__column gp-table-ip__column--header gp-table-ip__column--no-pointer"
            >
              {t('gpdomains_list.gp-table.main-header.domain-reputation.med.text')}
            </div>
            <div
              className="gp-table-ip__column gp-table-ip__column--header gp-table-ip__column--no-pointer"
            >
              {t('gpdomains_list.gp-table.main-header.domain-reputation.high.text')}
            </div>
            <div
              className="gp-table-ip__column gp-table-ip__column--header gp-table-ip__column--no-pointer"
            >
              {t('gpdomains_list.gp-table.main-header.domain-reputation.no-info.text')}
            </div>
          </div>
          <div className="gp-table-ip__content">
            {Object.keys(tableData).map((ip, i) => (
              <GPIpListTableRow
                key={`gp-table-ip-row-${i}`}
                ip={ip}
                data={tableData[ip]}
                onClick={(e) => handleClickTableIpItem(e, tableData[ip])}
              />
            ))}
          </div>
        </div>
      </div>
      {contextMenuIsShow && (
        <ContextMenu>
          <ul className="gp-table-ip__context-content context-content">
            {contextMenuIsShow && 
              contextDomains?.map((domain, i) => (
                <li
                  key={`context-domain-${i}`}
                  className={cn('context-content__item context-content__item--domain', {
                    'context-content__item--blured': meEmail === DEMO_USER_EMAIL,
                  })}
                  onClick={() => {
                    dispatch(handleContextMenuShow(false));
                    onChoseDomain(domain);
                  }}
                >
                  {domain}
                </li>
              ))}
          </ul>
        </ContextMenu>
      )}
    </Fragment>
  );
}

TableGPIpList.propTypes = {
  data: PropTypes.object.isRequired,
  onChoseDomain: PropTypes.func.isRequired,
};
TableGPIpList.defaultProps = {
  data: {},
  onChoseDomain: () => {}
};

export default React.memo(TableGPIpList);
