import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DatePicker from 'react-datepicker';
import { ru, enUS } from 'date-fns/locale';

import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import {
  localeCurrentSelector,
  gpIntegrationGPDateRunSelector,
  gpIntegrationGPDateRunIsLoadingSelector,
} from 'store/selectors';
import {
  sendGpDateRun,
  setGpDateRun,
  setGpDateRunIsShowSlider,
} from 'store/actions/gpIntegration';

import { LOCALE, YESTERDAY } from 'utils/constants';

import './gp-check-date-run.scss';

const GPDateRun = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const gpIntegrationGPDateRun = useSelector(gpIntegrationGPDateRunSelector);
  const gpIntegrationGPDateRunIsLoading = useSelector(gpIntegrationGPDateRunIsLoadingSelector);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(sendGpDateRun({ date: gpIntegrationGPDateRun}));
  };

  return (
    <form className="c-slidebar-content c-gp-date-run" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('gp_integration.date-run.title')}
      </h3>

      {
        gpIntegrationGPDateRunIsLoading
          ? <Spinner />
          : (
            <div className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('gp_integration.date-run.datepicker.label')}
              </span>
              <DatePicker
                dateFormat="dd-MM-yyyy"
                selected={gpIntegrationGPDateRun ? new Date(gpIntegrationGPDateRun) : null}
                onChange={(value) => 
                  dispatch(
                    setGpDateRun(new Date(new Date(value).setUTCHours(0, 0, 0, 0)).toISOString())
                  )
                }
                customInput={
                  <InputText />
                }
                maxDate={new Date(YESTERDAY)}
                wrapperClassName="c-gp-date-run__datepicker-wrap"
                placeholderText="dd-MM-yyyy"
                popperClassName="c-slidebar-content__datepicker c-slidebar-content__datepicker--popper"
                openToDate={gpIntegrationGPDateRun ? new Date(gpIntegrationGPDateRun) : null}
                locale={locale === LOCALE.ru ? ru : enUS}
              />
            </div>
          )
      }

      <div className="c-slidebar-content__btns">
        <Button
          mode="purple"
          text={t('gp_integration.date-run.button.start.text')}
          className="c-slidebar-content__btn"
          type="submit"
          disabled={gpIntegrationGPDateRunIsLoading}
        />
        <Button
          text={t('gp_integration.date-run.button.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setGpDateRunIsShowSlider(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(GPDateRun);
