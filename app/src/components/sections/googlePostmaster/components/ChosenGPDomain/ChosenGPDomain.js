import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import Button from 'components/ui/button';

import { usersMeSelector } from 'store/selectors';
import { DEMO_USER_EMAIL } from 'utils/constants';

import GPDomainReputationInfo from './components/GPDomainReputationInfo';
import GPSpfDkimDmarcInfo from './components/GPSpfDkimDmarcInfo';
import GPInOutBoundInfo from './components/GPInOutBoundInfo';
import GPUserReportedSpamRateInfo from './components/GPUserReportedSpamRateInfo';
import GPFeedbackLoopIdentifiersInfo from './components/GPFeedbackLoopIdentifiersInfo';
import GPIpReputationInfo from './components/GPIpReputationInfo';

import {
  setGpChosenData,
} from 'store/actions/googlePostmaster';

import './chosen-gp-domain.scss';

const ChosenGPDomain = ({ data }) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const [isShowDomainReputation, setIsShowDomainReputation] = useState(true);
  const [isShowSpfDkimDmarc, setIsShowSpfDkimDmarc] = useState(true);
  const [isShowInOutBound, setIsShowInOutBound] = useState(true);
  const [isShowUserReportedSpamRate, setIsShowUserReportedSpamRate] = useState(true);
  const [isShowFeedbackLoopIdentifiers, setIsShowFeedbackLoopIdentifiers] = useState(true);
  const [isShowIpReputation, setIsShowIpReputation] = useState(true);

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  return (
    <div className="c-slidebar-content c-chosen-gp-domain">
      <h3 className={cn("c-slidebar-content__title", {
        'c-slidebar-content__title--blured': meEmail === DEMO_USER_EMAIL
      })}>
        {data.domain}
      </h3>

      <div className="c-chosen-gp-domain__graphs">
        <div className="c-chosen-gp-domain__sei-accordion sei-accordion sei-accordion--gp">
          <div className="sei-accordion__item">
            <div
              className="sei-accordion__item-header"
              onClick={() => setIsShowDomainReputation(!isShowDomainReputation)}
            >
              <span>
                {t('chosen_gp_domain.domain_reputation.title')}
              </span>
            </div>
            {isShowDomainReputation && (
              <div className="sei-accordion__item-content sei-accordion__item-content--graph">
                <GPDomainReputationInfo data={data} />
              </div>
            )}
          </div>
        </div>

        <div className="c-chosen-gp-domain__sei-accordion sei-accordion sei-accordion--gp">
          <div className="sei-accordion__item">
            <div
              className="sei-accordion__item-header"
              onClick={() => setIsShowUserReportedSpamRate(!isShowUserReportedSpamRate)}
            >
              <span>
                {t('chosen_gp_domain.spam_rate.title')}
              </span>
            </div>
            {isShowUserReportedSpamRate && (
              <div className="sei-accordion__item-content sei-accordion__item-content--graph">
                <GPUserReportedSpamRateInfo data={data} />
              </div>
            )}
          </div>
        </div>

        <div className="c-chosen-gp-domain__sei-accordion sei-accordion sei-accordion--gp">
          <div className="sei-accordion__item">
            <div
              className="sei-accordion__item-header"
              onClick={() => setIsShowIpReputation(!isShowIpReputation)}
            >
              <span>
                {t('chosen_gp_domain.ip_reputation.title')}
              </span>
            </div>
            {isShowIpReputation && (
              <div className="sei-accordion__item-content sei-accordion__item-content--graph">
                <GPIpReputationInfo data={data} />
              </div>
            )}
          </div>
        </div>

        <div className="c-chosen-gp-domain__sei-accordion sei-accordion sei-accordion--gp">
          <div className="sei-accordion__item">
            <div
              className="sei-accordion__item-header"
              onClick={() => setIsShowFeedbackLoopIdentifiers(!isShowFeedbackLoopIdentifiers)}
            >
              <span>
                {t('chosen_gp_domain.feedback_loop.title')}
              </span>
            </div>
            {isShowFeedbackLoopIdentifiers && (
              <div className="sei-accordion__item-content sei-accordion__item-content--graph">
                <GPFeedbackLoopIdentifiersInfo data={data} />
              </div>
            )}
          </div>
        </div>

        <div className="c-chosen-gp-domain__sei-accordion sei-accordion sei-accordion--gp">
          <div className="sei-accordion__item">
            <div
              className="sei-accordion__item-header"
              onClick={() => setIsShowInOutBound(!isShowInOutBound)}
            >
              <span>
                {t('chosen_gp_domain.tls_in-out-bound.title')}
              </span>
            </div>
            {isShowInOutBound && (
              <div className="sei-accordion__item-content sei-accordion__item-content--graph">
                <GPInOutBoundInfo data={data} />
              </div>
            )}
          </div>
        </div>

        <div className="c-chosen-gp-domain__sei-accordion sei-accordion sei-accordion--gp">
          <div className="sei-accordion__item">
            <div
              className="sei-accordion__item-header"
              onClick={() => setIsShowSpfDkimDmarc(!isShowSpfDkimDmarc)}
            >
              <span>
                {t('chosen_gp_domain.spf_dkim_dmarc.title')}
              </span>
            </div>
            {isShowSpfDkimDmarc && (
              <div className="sei-accordion__item-content sei-accordion__item-content--graph">
                <GPSpfDkimDmarcInfo data={data} />
              </div>
            )}
          </div>
        </div>
        
      </div>

      <div className="c-slidebar-content__btns">
        <Button
          text={t('add_gpdomain.btn.close.text')}
          className="c-slidebar-content__btn c-slidebar-content__btn--fouth-width"
          onClick={() => dispatch(setGpChosenData(null))}
        />
      </div>
    </div>
  );
};

export default React.memo(ChosenGPDomain);
