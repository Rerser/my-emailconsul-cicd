import React, { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import moment from 'moment';
import 'moment/min/locales';
import Chart from 'react-google-charts';
import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';
import { localeCurrentSelector } from 'store/selectors';
import { CHART_COLUMN_REPUTATION_COLORS, GP_REPUTATION_CATEGORY } from 'utils/constants';

const GPIpReputationInfo = ({ data }) => {
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const reputationsTranslate = {
    [GP_REPUTATION_CATEGORY.BAD]: t('chosen_gp_domain.graphics.ip_reputation.legend.bad.text'),
    [GP_REPUTATION_CATEGORY.LOW]: t('chosen_gp_domain.graphics.ip_reputation.legend.low.text'),
    [GP_REPUTATION_CATEGORY.MEDIUM]: t('chosen_gp_domain.graphics.ip_reputation.legend.medium.text'),
    [GP_REPUTATION_CATEGORY.HIGH]: t('chosen_gp_domain.graphics.ip_reputation.legend.high.text'),
  }

  const dates = data.formattedDays;
  const reputations = data.reputations.ip;
  const chartData = [];

  for (let i = 0; i < dates.length; i++) {
    // column layout below according to the reputation field in ipReputation
    chartData.push([
      moment(dates[i]).format('MM/DD'),
      reputations[i].BAD,     // column 1
      reputations[i].LOW,     // column 2
      reputations[i].MEDIUM,  // column 3
      reputations[i].HIGH,    // column 4
    ]);
  }

  const [selectedData, setSelectedData] = useState({
    header: '',
    tableData: [],
  });

  return (
    <Fragment>
      <Chart
        chartType="ColumnChart"
        loader={<Spinner />}
        data={[
          [
            '',
            t('chosen_gp_domain.graphics.ip_reputation.legend.bad.text'),
            t('chosen_gp_domain.graphics.ip_reputation.legend.low.text'),
            t('chosen_gp_domain.graphics.ip_reputation.legend.medium.text'),
            t('chosen_gp_domain.graphics.ip_reputation.legend.high.text'),
          ],
          ...chartData,
        ]}
        options={{
          width: '100%',
          legend: { position: 'top', alignment: 'center' },
          tooltip: { isHtml: true },
          vAxis: {
            viewWindow: {
              min: 0,
            },
          },
          vAxes: {
            0: {
              title: '%',
            },
          },
          isStacked: true,
          colors: [...Object.keys(CHART_COLUMN_REPUTATION_COLORS).map(key => CHART_COLUMN_REPUTATION_COLORS[key])],
        }}
        formatters={[
          {
            type: 'NumberFormat',
            column: 1,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
          {
            type: 'NumberFormat',
            column: 2,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
          {
            type: 'NumberFormat',
            column: 3,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
          {
            type: 'NumberFormat',
            column: 4,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
        ]}
        chartEvents={[
          {
            eventName: 'select',
            callback: ({ chartWrapper }) => {
              const chart = chartWrapper.getChart();
              const selection = chart.getSelection();
              
              if (selection.length === 1) {
                const [{ row, column }] = selection;

                if (row !== null && column !== null) { // row and column can be equals 0
                  const day = data.formattedDays[row];
                  const currentTrafficStats = data.originalTrafficStats.find(stat => stat.key === day);
                  
                  if (currentTrafficStats) {
                    // here columns: [1, 2, 3, 4] chart === [BAD, LOW, MEDIUM, HIGH] chartData item
                    const currentIpReputation = currentTrafficStats.ipReputations[column - 1];
                    const date = moment(day, "YYYYMMDD").locale(locale).format('MMMM DD');

                    if (!!currentIpReputation) {
                      setSelectedData({
                        header: `${reputationsTranslate[currentIpReputation.reputation]} ${t('chosen_gp_domain.graphics.ip_reputation.table.ip_reputation_col.header.text')} ${date}`,
                        tableData: currentIpReputation.sampleIps || []
                      });
                    }
                  }
                }
              }
            },
          },
        ]}
      />
      {!!selectedData.tableData.length && (
        <div className="c-chosen-gp-domain__table">
          <Table
            columns={[
              {
                field: 'ipReputation',
                title: selectedData.header,
                type: 'string',
                width: '100%',
              },
            ]}
            data={selectedData.tableData.map(data => ({
              ipReputation: data
            }))}
            horizontalPositionContentCell='left'
          />
        </div>
      )}
    </Fragment>
  );
};

export default React.memo(GPIpReputationInfo);
