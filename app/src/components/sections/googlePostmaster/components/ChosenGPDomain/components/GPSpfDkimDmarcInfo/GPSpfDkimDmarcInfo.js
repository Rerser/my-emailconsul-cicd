import React from 'react';
import moment from 'moment';
import Chart from 'react-google-charts';
import Spinner from 'components/ui/spinner';

const GPSpfDkimDmarcInfo = ({ data }) => {
  const { spfDkimDmarcData, formattedDays: dates } = data;
  const chartData = [];

  for (let i = 0; i < dates.length; i++) {
    const currentDate = moment(dates[i]).format('MM/DD');
    const { spf, dkim, dmarc } = spfDkimDmarcData[i];

    chartData.push([
      currentDate,
      spf,
      dkim,
      dmarc,
    ]);
  }

  return (
    <Chart
      chartType="LineChart"
      loader={<Spinner />}
      data={[
        [
          '', 
          'SPF', 'DKIM', 'DMARC',
        ],
        ...chartData
      ]}
      options={{
        width: '100%',
        legend: { position: 'top', alignment: 'center' },
        vAxes: {
          0: {
            title: '%',
          },
        },
        focusTarget: 'category',
      }}
      formatters={[
        {
          type: 'NumberFormat',
          column: 1,
          options: {
            suffix: "%",
            fractionDigits: 1,
          },
        },
        {
          type: 'NumberFormat',
          column: 2,
          options: {
            suffix: "%",
            fractionDigits: 1,
          },
        },
        {
          type: 'NumberFormat',
          column: 3,
          options: {
            suffix: "%",
            fractionDigits: 1,
          },
        },
      ]}
    />
  );
};

export default React.memo(GPSpfDkimDmarcInfo);
