import React, { useState, Fragment } from 'react';
import moment from 'moment';
import 'moment/min/locales';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Chart from 'react-google-charts';
import { localeCurrentSelector } from 'store/selectors';
import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';

const SPAM_COLOR = '#4B72CB';
const IDENTIFIERS_COLOR = '#E25145';

const GPFeedbackLoopIdentifiersInfo = ({ data }) => {
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const chartData = [];
  const {feedbackLoopIdentifiersData, formattedDays: dates } = data;

  for (let i = 0; i < dates.length; i++) {
    chartData.push([
      moment(dates[i]).format('MM/DD'),
      feedbackLoopIdentifiersData[i].value, // column 1
      feedbackLoopIdentifiersData[i].count, // column 2
    ]);
  }

  const [selectedData, setSelectedData] = useState({
    header: '',
    tableData: [],
  });

  return (
    <Fragment>
      <Chart
        chartType="ComboChart"
        loader={<Spinner />}
        data={[
          [
            '',
            t('chosen_gp_domain.graphics.feedback.loop_spam.legend.text'),
            t('chosen_gp_domain.graphics.feedback.loop_count.legend.text'),
          ],
          ...chartData,
        ]}
        options={{
          width: '100%',
          legend: { position: 'top', alignment: 'center' },
          tooltip: { isHtml: true },
          axes: {
            y: {
              0: {
                label: 'leftyaxis',
              },
              1: {
                side: 'right',
                label: 'rightyaxis',
              },
            },
          },
          vAxes: {
            0: {
              title: t('chosen_gp_domain.graphics.feedback.v_axes.average_spam.text'),
              textStyle: { color: SPAM_COLOR },
            },
            1: {
              title: t('chosen_gp_domain.graphics.feedback.v_axes.number_identifiers.text'),
              textStyle: { color: IDENTIFIERS_COLOR },
            },
          },
          seriesType: 'bars',
          series: {
            0: { type: 'line' },
            1: { targetAxisIndex: 1 },
          },
          colors: [SPAM_COLOR, IDENTIFIERS_COLOR],
        }}
        formatters={[
          {
            type: 'NumberFormat',
            column: 1,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
        ]}
        chartEvents={[
          {
            eventName: 'select',
            callback: ({ chartWrapper }) => {
              const chart = chartWrapper.getChart();
              const selection = chart.getSelection();
              
              if (selection.length === 1) {
                const [{ row }] = selection;

                if (row !== null) { // row can be equals 0
                  const day = data.formattedDays[row];
                  const date = moment(day, "YYYYMMDD").locale(locale).format('MMMM DD');
                  const currentTrafficStats = data.originalTrafficStats.find(stat => stat.key === day);
                  
                  if (currentTrafficStats) {
                    const currentSpammyFeedbackLoops = currentTrafficStats.spammyFeedbackLoops;

                    setSelectedData({
                      header: `${t('chosen_gp_domain.graphics.feedback.table.identifiers.header.text')} ${date}`,
                      tableData: currentSpammyFeedbackLoops.map(data => ({
                        feedbackId: data.id,
                        spamRate: parseFloat((data.spamRatio * 100).toFixed(5)) + '%'
                      })) || []
                    });
                  } else {
                    setSelectedData({
                      header: `${t('chosen_gp_domain.graphics.feedback.table.identifiers.header.text')} ${date}`,
                      tableData: [{
                        feedbackId: '-',
                        spamRate: '0%'
                      }]
                    });
                  }
                }
              }
            },
          },
        ]}
      />
      {!!selectedData.tableData.length && (
        <div className="c-chosen-gp-domain__table">
          <Table
            columns={[
              {
                field: 'feedbackId',
                title: selectedData.header,
                type: 'string',
                width: '70%',
              },
              {
                field: 'spamRate',
                title: t('chosen_gp_domain.graphics.feedback.table.spam_rate.header.text'),
                type: 'string',
                width: '30%',
              },
            ]}
            data={selectedData.tableData}
            horizontalPositionContentCell='left'
          />
        </div>
      )}
    </Fragment>
  );
};

export default React.memo(GPFeedbackLoopIdentifiersInfo);
