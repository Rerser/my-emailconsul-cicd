import React, { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';
import 'moment/min/locales';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Chart from 'react-google-charts';
import { localeCurrentSelector } from 'store/selectors';
import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';

const GPInOutBoundInfo = ({ data }) => {
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const { inOutBoundData, formattedDays: dates } = data;
  const chartData = [];

  for (let i = 0; i < dates.length; i++) {
    chartData.push([
      moment(dates[i]).format('MM/DD'),
      inOutBoundData[i].in * 100,   // column 1
      inOutBoundData[i].out * 100   // column 2
    ]);
  }

  const [selectedData, setSelectedData] = useState({
    tableData: [],
  });

  return (
    <Fragment>
      <Chart
        chartType="LineChart"
        loader={<Spinner />}
        data={[
          [
            '',
            t('chosen_gp_domain.graphics.in_out_bound.in_bound.legend.text'),
            t('chosen_gp_domain.graphics.in_out_bound.out_bound.legend.text'),
          ],
          ...chartData,
        ]}
        options={{
          width: '100%',
          legend: { position: 'top', alignment: 'center' },
          vAxis: {
            viewWindow: {
              min: 0,
            },
          },
          vAxes: {
            0: {
              title: '%',
            },
          },
          focusTarget: 'category',
        }}
        formatters={[
          {
            type: 'NumberFormat',
            column: 1,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
          {
            type: 'NumberFormat',
            column: 2,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
        ]}
        chartEvents={[
          {
            eventName: 'select',
            callback: ({ chartWrapper }) => {
              const chart = chartWrapper.getChart();
              const selection = chart.getSelection();
              
              if (selection.length === 1) {
                const [{ row }] = selection;

                if (row !== null) { // row can be equals 0
                  const day = data.formattedDays[row];
                  const date = moment(day, "YYYYMMDD").locale(locale).format('MMMM DD, YYYY');
                  const currentTrafficStats = data.originalTrafficStats.find(stat => stat.key === day);
                  
                  if (currentTrafficStats) {
                    const currentInboundEncryptionRatio = currentTrafficStats.inboundEncryptionRatio;
                    const currentOutboundEncryptionRatio = currentTrafficStats.outboundEncryptionRatio;

                    setSelectedData({
                      tableData: [{
                        date,
                        inBound: parseFloat(((currentInboundEncryptionRatio ?? 0) * 100).toFixed(5)) + '%',
                        outBound: parseFloat(((currentOutboundEncryptionRatio ?? 0) * 100).toFixed(5)) + '%'
                      }]
                    });
                  } else {
                    setSelectedData({
                      tableData: [{
                        date,
                        inBound: '0%',
                        outBound: '0%'
                      }]
                    });
                  }
                }
              }
            },
          },
        ]}
      />
      {!!selectedData.tableData.length && (
        <div className="c-chosen-gp-domain__table">
          <Table
            columns={[
              {
                field: 'date',
                title: t('chosen_gp_domain.graphics.in_out_bound.table.date.header.text'),
                type: 'string',
                width: '33%',
              },
              {
                field: 'inBound',
                title: t('chosen_gp_domain.graphics.in_out_bound.in_bound.legend.text'),
                type: 'string',
                width: '33%',
              },
              {
                field: 'outBound',
                title: t('chosen_gp_domain.graphics.in_out_bound.out_bound.legend.text'),
                type: 'string',
                width: '33%',
              },
            ]}
            data={selectedData.tableData}
            horizontalPositionContentCell='left'
          />
        </div>
      )}
    </Fragment>
  );
};

export default React.memo(GPInOutBoundInfo);
