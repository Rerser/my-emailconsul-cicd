import React from 'react';
import moment from 'moment';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { Chart } from 'react-google-charts';
import Spinner from 'components/ui/spinner';
import { CHART_COLUMN_REPUTATION_COLORS, GP_REPUTATION_CATEGORY } from 'utils/constants';

const GPDomainReputationInfo = ({ data }) => {
  const t = useFormatMessage();

  function getTooltipLayout(text, date) {
    let valueText = '';

    switch (text) {
      case GP_REPUTATION_CATEGORY.BAD:
        valueText = t('chosen_gp_domain.graphics.domain_reputation.tooltip.bad.text');
        break;
      case GP_REPUTATION_CATEGORY.LOW:
        valueText = t('chosen_gp_domain.graphics.domain_reputation.tooltip.low.text');
        break;
      case GP_REPUTATION_CATEGORY.MEDIUM:
        valueText = t('chosen_gp_domain.graphics.domain_reputation.tooltip.medium.text');
        break;
      case GP_REPUTATION_CATEGORY.HIGH:
        valueText = t('chosen_gp_domain.graphics.domain_reputation.tooltip.high.text');
        break;
      case GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED:
        valueText = t('chosen_gp_domain.graphics.domain_reputation.tooltip.unknown.text');
        break;
      default:
        break;
    }
    return `
      <div class="chart-tooltip">
        <div class="chart-tooltip__date">
          ${date}
        </div>
        <div class="chart-tooltip__value">
          ${valueText.toUpperCase()}
        </div>
      </div>
    `;
  }

  const { reputations: { domain }, formattedDays: dates } = data;
  const chartData = [];

  for (let i = 0; i < dates.length; i++) {
    const currentDate = moment(dates[i]).format('MM/DD');

    chartData.push([
      currentDate,
      2, // max value on axis Y (makes columns higher)
      `color: ${CHART_COLUMN_REPUTATION_COLORS[domain[i]]}`,
      getTooltipLayout(domain[i], currentDate),
    ]);
  }

  return (
    <Chart
      chartType="ColumnChart"
      loader={<Spinner />}
      data={[['', '', { role: 'style' }, { role: 'tooltip', p: {html: true}}], ...chartData]}
      options={{
        width: '100%',
        legend: { position: 'none' },
        tooltip: { isHtml: true },
        vAxis: {
          gridlines: {
            color: 'transparent',
          },
          textStyle: {
            color: 'transparent',
          },
          viewWindow: {
            max: 1,
            min: 1,
          },
          baselineColor: 'transparent',
        },
      }}
    />
  );
};

export default React.memo(GPDomainReputationInfo);
