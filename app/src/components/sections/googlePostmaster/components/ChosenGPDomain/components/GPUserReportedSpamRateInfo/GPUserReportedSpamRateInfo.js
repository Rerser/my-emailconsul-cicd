import React, { Fragment, useState } from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';
import 'moment/min/locales';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Chart from 'react-google-charts';
import { localeCurrentSelector } from 'store/selectors';
import Spinner from 'components/ui/spinner';
import Table from 'components/ui/table';

const GPUserReportedSpamRateInfo = ({ data }) => {
  const t = useFormatMessage();
  const chartData = [];
  const { userReportedSpamRatioData, formattedDays: dates } = data;

  const locale = useSelector(localeCurrentSelector);

  for (let i = 0; i < dates.length; i++) {
    const currentData = userReportedSpamRatioData[i] * 100;
    const currentDate = moment(dates[i]).format('MM/DD');

    chartData.push([
      currentDate,
      currentData, // column 1
    ]);
  }

  const [selectedData, setSelectedData] = useState({
    tableData: [],
  });

  return (
    <Fragment>
      <Chart
        chartType="LineChart"
        loader={<Spinner />}
        data={[
          [
            '', 
            t('chosen_gp_domain.graphics.spam_rate.legend.text'),
          ],
          ...chartData
        ]}
        options={{
          width: '100%',
          legend: { position: 'top', alignment: 'center' },
          tooltip: { isHtml: true },
          vAxes: {
            0: {
              title: '%',
            },
          },
        }}
        formatters={[
          {
            type: 'NumberFormat',
            column: 1,
            options: {
              suffix: "%",
              fractionDigits: 1,
            },
          },
        ]}
        chartEvents={[
          {
            eventName: 'select',
            callback: ({ chartWrapper }) => {
              const chart = chartWrapper.getChart();
              const selection = chart.getSelection();
              
              if (selection.length === 1) {
                const [{ row }] = selection;

                if (row !== null) { // row can be equals 0
                  const day = data.formattedDays[row];
                  const date = moment(day, "YYYYMMDD").locale(locale).format('MMMM DD, YYYY');
                  const currentTrafficStats = data.originalTrafficStats.find(stat => stat.key === day);
                  
                  if (currentTrafficStats) {
                    const currentUserReportedSpamRatio = currentTrafficStats.userReportedSpamRatio;

                    setSelectedData({
                      tableData: [{
                        date,
                        spamRate: parseFloat(((currentUserReportedSpamRatio ?? 0) * 100).toFixed(5)) + '%',
                      }]
                    });
                  } else {
                    setSelectedData({
                      tableData: [{
                        date,
                        spamRate: '0%',
                      }]
                    });
                  }
                }
              }
            },
          },
        ]}
      />
      {!!selectedData.tableData.length && (
        <div className="c-chosen-gp-domain__table">
          <Table
            columns={[
              {
                field: 'date',
                title: t('chosen_gp_domain.graphics.spam_rate.table.date.header.text'),
                type: 'string',
                width: '40%',
              },
              {
                field: 'spamRate',
                title: t('chosen_gp_domain.graphics.spam_rate.legend.text'),
                type: 'string',
                width: '60%',
              },
            ]}
            data={selectedData.tableData}
            horizontalPositionContentCell='left'
          />
        </div>
      )}
    </Fragment>
  );
};

export default React.memo(GPUserReportedSpamRateInfo);
