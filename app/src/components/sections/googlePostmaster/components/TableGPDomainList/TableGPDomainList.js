import React, { Fragment, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Spinner from 'components/ui/spinner/Spinner';
import Icon from 'components/ui/icon';
import Button from 'components/ui/button';
import ContextMenu from 'components/ui/contextMenu';

import {
  googlePostmasterIsDataLoadingSelector,
  googlePostmasterDateFromSelector,
  googlePostmasterDateToSelector,
  contextMenuIsShowSelector,
  usersMeSelector,
} from 'store/selectors';
import { handleContextMenuShow, setContextMenuCoords } from 'store/actions/contextMenu';
import { setGpChosenData } from 'store/actions/googlePostmaster';

import { getFormattedDaysMMDD } from 'utils/helpers';
import {
  DEMO_USER_EMAIL,
  GP_REPUTATION_CATEGORY,
  GP_TABLE_SORT_BY,
} from 'utils/constants';

import GPDomainListTableRow, {
  GP_TABLE_ROW_CLASS,
  GP_TABLE_COLUMN_BAD_REPUTATION_IP_CLASS,
  GP_TABLE_COLUMN_HIGH_REPUTATION_IP_CLASS,
  GP_TABLE_COLUMN_LOW_REPUTATION_IP_CLASS,
  GP_TABLE_COLUMN_MEDIUM_REPUTATION_IP_CLASS,
} from './components/GPDomainListTableRow';
import { sortingTableGPData } from './components/GPTableDomainData/GPTableDomainData';

import './table-gp-domain-list.scss';

function TableGPDomainList({ domainsData }) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const [emptyDataIsShow, setEmptyDataIsShow] = useState(false);

  const me = useSelector(usersMeSelector);
  const googlePostmasterIsDataLoading = useSelector(googlePostmasterIsDataLoadingSelector);
  const googlePostmasterDateFrom = useSelector(googlePostmasterDateFromSelector);
  const googlePostmasterDateTo = useSelector(googlePostmasterDateToSelector);
  const contextMenuIsShow = useSelector(contextMenuIsShowSelector);

  const [contextIps, setContextIps] = useState(null);
  const { email: meEmail } = me;

  const [sorting, setSorting] = useState({
    field: GP_TABLE_SORT_BY.NONE,
    order: 'asc',
  });

  const [domainReputationFilters, setDomainReputationFilters] = useState([]);

  const handleFilterDomainReputation = (reputation) => {
    if (!domainReputationFilters.includes(reputation)) {
      setDomainReputationFilters([...domainReputationFilters, reputation]);
    } else {
      const foundIndex = domainReputationFilters.findIndex(domRep => domRep === reputation);

      if (foundIndex > -1) {
        const newDomainReputationFilters = [...domainReputationFilters];
        newDomainReputationFilters.splice(foundIndex, 1);
        setDomainReputationFilters(newDomainReputationFilters);
      }
    }
  }

  function handleSort(field) {
    if (sorting.field === field) {
      setSorting({ ...sorting, order: sorting.order === 'asc' ? 'desc' : 'asc' });
    } else {
      setSorting({ order: 'desc', field });
    }
  }

  const tableData = useMemo(
    () => {
      if (domainReputationFilters.length) {
        return domainsData.data
          .filter(domainData => {
            for (const filter of domainReputationFilters) {
              if (domainData.reputations.domain.includes(filter)) {
                return true;
              }
            }
            return false;
          })
          .sort((a, b) => sortingTableGPData(a, b, sorting));
      }
      return domainsData.data.sort((a, b) => sortingTableGPData(a, b, sorting));
    },
    [domainsData, sorting, domainReputationFilters]
  );

  const tableEmptyData = useMemo(
    () => {
      if (domainReputationFilters.length) {
        return [];
      }
      return domainsData.emptyData.sort((a, b) => sortingTableGPData(a, b, sorting));
    },
    [domainsData, sorting, domainReputationFilters]
  );

  const handleClickTableDomainItem = (e, data) => {
    if (window.getSelection().toString()) return;

    const target = e.target;    
    let ipRepList = [];

    if (target.closest(`.${GP_TABLE_COLUMN_BAD_REPUTATION_IP_CLASS}`)) {
      ipRepList = [...data.ipReputationsList[GP_REPUTATION_CATEGORY.BAD]];
    } else if (target.closest(`.${GP_TABLE_COLUMN_LOW_REPUTATION_IP_CLASS}`)) {
      ipRepList = [...data.ipReputationsList[GP_REPUTATION_CATEGORY.LOW]];
    } else if (target.closest(`.${GP_TABLE_COLUMN_MEDIUM_REPUTATION_IP_CLASS}`)) {
      ipRepList = [...data.ipReputationsList[GP_REPUTATION_CATEGORY.MEDIUM]];
    } else if (target.closest(`.${GP_TABLE_COLUMN_HIGH_REPUTATION_IP_CLASS}`)) {
      ipRepList = [...data.ipReputationsList[GP_REPUTATION_CATEGORY.HIGH]];
    }
    if (ipRepList.length) {
      setContextIps(ipRepList);
      dispatch(setContextMenuCoords({
        x: e.clientX,
        y: e.clientY
      }));
      return dispatch(handleContextMenuShow(true));
    } else {
      setContextIps(null);
    }

    if (target.closest(`.${GP_TABLE_ROW_CLASS}`)) {
      return dispatch(setGpChosenData(data));
    }
  }

  if (googlePostmasterIsDataLoading) {
    return <Spinner />;
  }
  if (!googlePostmasterDateTo) {
    return (
      <div>
        {t('table.no.date_to.text')}
      </div>
    );
  }
  if (!tableData.length && !tableEmptyData.length && !domainReputationFilters.length) {
    return (
      <div>
        {t('table.no.data.text')}
      </div>
    );
  }

  return (
    <Fragment>
      <div className="gp-domains-list__gp-table gp-table">
        <div className="gp-table__wrapper">
          <div className="gp-table__main-header">
            <div className="gp-table__column gp-table__column--main-header">
              {t('gpdomains_list.gp-table.main-header.domain.text')}
            </div>
            <div className="gp-table__column gp-table__column--main-header reputation">
              <span className="gp-table__date gp-table__date--from">
                {getFormattedDaysMMDD(googlePostmasterDateFrom)}
              </span>
              <span>
                {t('gpdomains_list.gp-table.main-header.domain-reputation.text')}
              </span>
              <span className="gp-table__date gp-table__date--to">
                {getFormattedDaysMMDD(googlePostmasterDateTo)}
              </span>
            </div>
            <div className="gp-table__column gp-table__column--main-header">
              {t('gpdomains_list.gp-table.main-header.authentication-averages.text')}
            </div>
            <div className="gp-table__column gp-table__column--main-header">
              {t('gpdomains_list.gp-table.main-header.ip-reputation.text')}
            </div>
            <div className="gp-table__column gp-table__column--main-header"></div>
          </div>
          <div className="gp-table__header">
            <div className="gp-table__column gp-table__column--header">
              <div className="gp-table__reputation-filters">
                <div
                  className={cn("gp-table__reputation-filter-item gp-table__reputation-filter-item--high", {
                    "gp-table__reputation-filter-item--selected": domainReputationFilters.includes(GP_REPUTATION_CATEGORY.HIGH)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.high.text')}
                  onClick={() => handleFilterDomainReputation(GP_REPUTATION_CATEGORY.HIGH)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.high.text')}
                </div>
                <div
                  className={cn("gp-table__reputation-filter-item gp-table__reputation-filter-item--medium", {
                    "gp-table__reputation-filter-item--selected": domainReputationFilters.includes(GP_REPUTATION_CATEGORY.MEDIUM)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.medium.text')}
                  onClick={() => handleFilterDomainReputation(GP_REPUTATION_CATEGORY.MEDIUM)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.medium.text')}
                </div>
                <div
                  className={cn("gp-table__reputation-filter-item gp-table__reputation-filter-item--low", {                  
                    "gp-table__reputation-filter-item--selected": domainReputationFilters.includes(GP_REPUTATION_CATEGORY.LOW)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.low.text')}
                  onClick={() => handleFilterDomainReputation(GP_REPUTATION_CATEGORY.LOW)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.low.text')}
                </div>
                <div
                  className={cn("gp-table__reputation-filter-item gp-table__reputation-filter-item--bad", {                  
                    "gp-table__reputation-filter-item--selected": domainReputationFilters.includes(GP_REPUTATION_CATEGORY.BAD)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.bad.text')}
                  onClick={() => handleFilterDomainReputation(GP_REPUTATION_CATEGORY.BAD)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.bad.text')}
                </div>
                <div
                  className={cn("gp-table__reputation-filter-item gp-table__reputation-filter-item--unknown", {                  
                    "gp-table__reputation-filter-item--selected": domainReputationFilters.includes(GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED)
                  })}
                  data-text={t('gpdomains_list.gp-table.domain-reputation.filter.unknown.text')}
                  onClick={() => handleFilterDomainReputation(GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED)}
                >
                  {t('gpdomains_list.gp-table.domain-reputation.filter.unknown.text')}
                </div>
              </div>
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.SPF)}
            >
              {t('gpdomains_list.gp-table.main-header.authentication-averages.spf.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.SPF && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.DKIM)}
            >
              {t('gpdomains_list.gp-table.main-header.authentication-averages.dkim.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.DKIM && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.DMARC)}
            >
              {t('gpdomains_list.gp-table.main-header.authentication-averages.dmarc.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.DMARC && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.IP_REPUTATION.BAD)}
            >
              {t('gpdomains_list.gp-table.main-header.ip-reputation.bad.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.IP_REPUTATION.BAD && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.IP_REPUTATION.LOW)}
            >
              {t('gpdomains_list.gp-table.main-header.ip-reputation.low.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.IP_REPUTATION.LOW && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.IP_REPUTATION.MEDIUM)}
            >
              {t('gpdomains_list.gp-table.main-header.ip-reputation.med.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.IP_REPUTATION.MEDIUM && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div
              className="gp-table__column gp-table__column--header"
              onClick={() => handleSort(GP_TABLE_SORT_BY.IP_REPUTATION.HIGH)}
            >
              {t('gpdomains_list.gp-table.main-header.ip-reputation.high.text')}
              {
                sorting.field === GP_TABLE_SORT_BY.IP_REPUTATION.HIGH && (
                  <div className={cn('gp-table__sort-arrow', sorting.order)}>
                    <Icon type="onSort" />
                  </div>
                )
              }
            </div>
            <div className="gp-table__column gp-table__column--header"></div>
          </div>
          <div className="gp-table__content">
            {
              tableData.map((data, i) => (
                <GPDomainListTableRow
                  key={`gp-table-row-${i}`}
                  data={data}
                  onClick={(e) => handleClickTableDomainItem(e, data)}
                />
              ))
            }
            {
              emptyDataIsShow && (
                tableEmptyData.map((data, i) => (
                  <GPDomainListTableRow
                    key={`gp-table-row-empty-data-${i}`}
                    data={data}
                  />
                ))
              )
            }
          </div>
        </div>
      </div>
      {
        !!domainsData.emptyData.length && !domainReputationFilters.length && (
          <Button
            mode="purple"
            text={
              emptyDataIsShow
                ? t('gpdomains_list.table.btn.show_empty_data.hide.text')
                : t('gpdomains_list.table.btn.show_empty_data.show.text')
            }
            className="gp-table__show-hide-btn"
            onClick={() => setEmptyDataIsShow(!emptyDataIsShow)}
          />
        )
      }
      {contextMenuIsShow && (
        <ContextMenu>
          <ul className="gp-table__context-content context-content">
            {contextMenuIsShow && 
              contextIps?.map((ip, i) => (
                <li
                  key={`context-ip-${i}`}
                  className={cn('context-content__item context-content__item--ip', {
                    'context-content__item--blured': meEmail === DEMO_USER_EMAIL,
                  })}
                  onClick={() => dispatch(handleContextMenuShow(false))}
                >
                  {ip}
                </li>
              ))}
          </ul>
        </ContextMenu>
      )}
    </Fragment>
  );
}

TableGPDomainList.propTypes = {
  domainsData: PropTypes.shape({
    data: PropTypes.array,
    emptyData: PropTypes.array
  }).isRequired,
};
TableGPDomainList.defaultProps = {
  domainsData: {
    data: [],
    emptyData: []
  },
};

export default React.memo(TableGPDomainList);
