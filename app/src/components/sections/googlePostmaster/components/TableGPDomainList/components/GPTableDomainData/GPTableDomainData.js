import { GP_REPUTATION_CATEGORY, GP_TABLE_SORT_BY } from 'utils/constants';
import { getDifferenceDays, addNDayToDate, getFormattedDaysYYYYMMDD } from 'utils/helpers';

function modifyingData({ data = [], dateTo = new Date().toISOString(), dateFrom = new Date().toISOString() }) {
  return data.map((item) => {
    const { traffic_stats: trafficStats = [] } = item;
    const trafficStatsLength = trafficStats.length;
    const averagesAll = {
      spf: 0,
      dkim: 0,
      dmarc: 0,
    };
    const daysCount = getDifferenceDays(dateTo, dateFrom); // inclusive last
    const formattedDays = []; // для хранения форматированных дней 'YYYYMMDD'
    const daysISO = []; // для хранения диапазона дат в ISO
    const domainReputationKeys = []; // для хранения всех существующих key и domainReputation
    const domainReputationsData = [];
    const spfDkimDmarcKeys = []; // для хранения всех существующих key и spf, dkim, dmarc
    const spfDkimDmarcData = [];
    const inOutBoundKeys = []; // для хранения всех существующих key и spf, dkim, dmarc
    const inOutBoundData = [];
    const userReportedSpamRatioKeys = []; // для хранения всех существующих key и userReportedSpamRate
    const userReportedSpamRatioData = [];
    const feedbackLoopIdentifiersKeys = []; // для хранения всех существующих key и feedbackLoopIdentifiers
    const feedbackLoopIdentifiersData = [];
    const ipReputationsKeys = []; // для хранения всех существующих key и ipReputation
    const ipReputationsData = [];
    const badIpReputation = new Set(); // для хранения уникальных ips c репутацией BAD из поля ipReputations в элементах массива trafficStats
    const lowIpReputation = new Set(); // для хранения уникальных ips c репутацией LOW из поля ipReputations в элементах массива trafficStats
    const mediumIpReputation = new Set(); // для хранения уникальных ips c репутацией MEDIUM из поля ipReputations в элементах массива trafficStats
    const highIpReputation = new Set(); // для хранения уникальных ips c репутацией HIGH из поля ipReputations в элементах массива trafficStats

    if (trafficStatsLength) {
      trafficStats.forEach(
        ({
          key,
          domainReputation = '',
          ipReputations = [],
          spfSuccessRatio = 0,
          dkimSuccessRatio = 0,
          dmarcSuccessRatio = 0,
          inboundEncryptionRatio = 0,
          outboundEncryptionRatio = 0,
          userReportedSpamRatio = 0,
          spammyFeedbackLoops = [],
        }) => {
          // get domain reputations keys
          domainReputationKeys.push({ key, domainReputation });

          // get averages info
          averagesAll.spf += spfSuccessRatio;
          averagesAll.dkim += dkimSuccessRatio;
          averagesAll.dmarc += dmarcSuccessRatio;
          spfDkimDmarcKeys.push({
            key,
            data: {
              spf: spfSuccessRatio * 100,
              dkim: dkimSuccessRatio * 100,
              dmarc: dmarcSuccessRatio * 100,
            },
          });

          // get in/out bounds info
          inOutBoundKeys.push({
            key,
            data: {
              in: inboundEncryptionRatio,
              out: outboundEncryptionRatio,
            },
          });

          // get user reported spam rate info
          userReportedSpamRatioKeys.push({
            key,
            data: userReportedSpamRatio,
          });

          // get feedback loop identifiers info
          feedbackLoopIdentifiersKeys.push({
            key,
            data: {
              value: spammyFeedbackLoops.reduce((pred, curr) => pred + (curr.spamRatio ?? 0), 0) * 100,
              count: spammyFeedbackLoops.length,
            },
          });
          
          // get ip reputations counts and info
          function addIpReputation(ip, reputation) {
            switch (reputation) {
              case GP_REPUTATION_CATEGORY.BAD:
                badIpReputation.add(ip);
                break;
              case GP_REPUTATION_CATEGORY.LOW:
                lowIpReputation.add(ip);
                break;
              case GP_REPUTATION_CATEGORY.MEDIUM:
                mediumIpReputation.add(ip);
                break;
              case GP_REPUTATION_CATEGORY.HIGH:
                highIpReputation.add(ip);
                break;
            
              default:
                break;
            }
          }

          ipReputations.forEach(({ sampleIps, reputation }) => {
            sampleIps.forEach(ip => {
              if (ip.includes('-')) {
                const [startIp, endIp] = ip.split('-');
                const [a, b, c, startAddr] = startIp.split('.');
                const [,,, endAddr] = endIp.split('.');
                const numberStartAddr = Number(startAddr);
                const numberEndAddr = Number(endAddr);
                const length = numberEndAddr - numberStartAddr;
                const subNetIp = `${a}.${b}.${c}`;
  
                for (let i = numberStartAddr; i <= length + numberStartAddr; i++ ) {
                  addIpReputation(`${subNetIp}.${i}`, reputation);
                }
              } else {
                addIpReputation(ip, reputation);
              }
            });
          });

          const currentIpReputationCountSum = ipReputations.reduce((pred, curr) => pred + Number(curr.ipCount ?? 0), 0);

          ipReputationsKeys.push({
            key,
            data: ipReputations.reduce((pred, curr) => {
              pred = {
                ...pred,
                [curr.reputation]: Number((((curr.ipCount ?? 0) / currentIpReputationCountSum) * 100).toFixed(1)),
              };
              return pred;
            }, {}),
          });
        }
      );

      for (const average in averagesAll) {
        averagesAll[average] /= trafficStatsLength;
      }
    }

    // подсчитаем общие счетчики reputation для уникальных Ip
    const ipReputationsCountsData = {
      [GP_REPUTATION_CATEGORY.BAD]: badIpReputation.size,
      [GP_REPUTATION_CATEGORY.LOW]: lowIpReputation.size,
      [GP_REPUTATION_CATEGORY.MEDIUM]: mediumIpReputation.size,
      [GP_REPUTATION_CATEGORY.HIGH]: highIpReputation.size,
    };

    for (let i = 0; i < daysCount; i++) {
      const day = addNDayToDate(dateFrom, i);
      const currentDayFormatted = getFormattedDaysYYYYMMDD(day);
      const foundReputationIndex = domainReputationKeys.findIndex(
        (domainReputationKey) => domainReputationKey.key === currentDayFormatted
      );
      const foundSpfDkimDmarcIndex = spfDkimDmarcKeys.findIndex(
        (spfDkinDmarcKey) => spfDkinDmarcKey.key === currentDayFormatted
      );
      const foundInOutBoundIndex = inOutBoundKeys.findIndex((inOutKey) => inOutKey.key === currentDayFormatted);
      const foundUserReportedSpamRatioIndex = userReportedSpamRatioKeys.findIndex(
        (userReportedSpamRatioKey) => userReportedSpamRatioKey.key === currentDayFormatted
      );
      const foundFeedbackLoopIdentifiersIndex = feedbackLoopIdentifiersKeys.findIndex(
        (feedbackLoopIdentifiersKey) => feedbackLoopIdentifiersKey.key === currentDayFormatted
      );
      const foundIpReputationsIndex = ipReputationsKeys.findIndex(
        (ipReputationKey) => ipReputationKey.key === currentDayFormatted
      );

      daysISO.push(day);
      formattedDays.push(currentDayFormatted);
      // domain reputation
      if (foundReputationIndex > -1) {
        const rep = domainReputationKeys[foundReputationIndex].domainReputation;
        if (rep) {
          domainReputationsData[i] = rep;
        } else {
          domainReputationsData[i] = GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED;
        }
      } else {
        domainReputationsData[i] = GP_REPUTATION_CATEGORY.REPUTATION_CATEGORY_UNSPECIFIED;
      }
      // spf, dkim, dmarc
      if (foundSpfDkimDmarcIndex > -1) {
        spfDkimDmarcData.push(spfDkimDmarcKeys[foundSpfDkimDmarcIndex].data);
      } else {
        spfDkimDmarcData.push({
          spf: 0,
          dkim: 0,
          dmarc: 0,
        });
      }
      // in/out bound
      if (foundInOutBoundIndex > -1) {
        inOutBoundData.push(inOutBoundKeys[foundInOutBoundIndex].data);
      } else {
        inOutBoundData.push({
          in: 0,
          out: 0,
        });
      }
      // user spam ratio
      if (foundUserReportedSpamRatioIndex > -1) {
        userReportedSpamRatioData.push(userReportedSpamRatioKeys[foundUserReportedSpamRatioIndex].data);
      } else {
        userReportedSpamRatioData.push(0);
      }
      // feedback loop
      if (foundFeedbackLoopIdentifiersIndex > -1) {
        feedbackLoopIdentifiersData.push(feedbackLoopIdentifiersKeys[foundFeedbackLoopIdentifiersIndex].data);
      } else {
        feedbackLoopIdentifiersData.push({
          value: 0,
          count: 0,
        });
      }
      // ip reputation
      if (foundIpReputationsIndex > -1) {
        ipReputationsData.push(ipReputationsKeys[foundIpReputationsIndex].data);
      } else {
        ipReputationsData.push({
          [GP_REPUTATION_CATEGORY.BAD]: 0,
          [GP_REPUTATION_CATEGORY.LOW]: 0,
          [GP_REPUTATION_CATEGORY.MEDIUM]: 0,
          [GP_REPUTATION_CATEGORY.HIGH]: 0,
        });
      }
    }

    return {
      id: item.id,
      domain: item.domain,
      averages: averagesAll,
      ipReputationsCountsData,
      reputations: {
        domain: domainReputationsData,
        ip: ipReputationsData,
      },
      daysISO,
      formattedDays,
      spfDkimDmarcData,
      inOutBoundData,
      userReportedSpamRatioData,
      feedbackLoopIdentifiersData,
      originalTrafficStats: trafficStats,
      ipReputationsList: {
        [GP_REPUTATION_CATEGORY.BAD]: badIpReputation,
        [GP_REPUTATION_CATEGORY.LOW]: lowIpReputation,
        [GP_REPUTATION_CATEGORY.MEDIUM]: mediumIpReputation,
        [GP_REPUTATION_CATEGORY.HIGH]: highIpReputation,
      }
    };
  });
}

export default function getGPTableDomainData({
  googlePostmasterData,
  googlePostmasterDateFrom,
  googlePostmasterDateTo,
}) {
  const nonEmptyData = googlePostmasterData.filter(({ traffic_stats = [] }) => traffic_stats.length);
  const emptyData = googlePostmasterData.filter(({ traffic_stats = [] }) => !traffic_stats.length);
  
  const modifiedData = modifyingData({
    data: nonEmptyData,
    dateFrom: googlePostmasterDateFrom,
    dateTo: googlePostmasterDateTo
  });
  const modifiedEmptyData = modifyingData({
    data: emptyData,
    dateFrom: googlePostmasterDateFrom,
    dateTo: googlePostmasterDateTo
  });
  return {
    data: modifiedData,
    emptyData: modifiedEmptyData,
  }
}

export function sortingTableGPData(a, b, sorting) {
  const { field, order } = sorting;

  switch (field) {
    case GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.SPF:
      if (order === 'asc') {
        return a.averages.spf < b.averages.spf ? -1 : 1;
      }
      return a.averages.spf < b.averages.spf ? 1 : -1;

    case GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.DKIM:
      if (order === 'asc') {
        return a.averages.dkim < b.averages.dkim ? -1 : 1;
      }
      return a.averages.dkim < b.averages.dkim ? 1 : -1;

    case GP_TABLE_SORT_BY.AUTHENTICATION_AVERAGES.DMARC:
      if (order === 'asc') {
        return a.averages.dmarc < b.averages.dmarc ? -1 : 1;
      }
      return a.averages.dmarc < b.averages.dmarc ? 1 : -1;

    case GP_TABLE_SORT_BY.IP_REPUTATION.BAD:
      if (order === 'asc') {
        return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.BAD] <
          b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.BAD]
          ? -1
          : 1;
      }
      return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.BAD] <
        b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.BAD]
        ? 1
        : -1;

    case GP_TABLE_SORT_BY.IP_REPUTATION.LOW:
      if (order === 'asc') {
        return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.LOW] <
          b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.LOW]
          ? -1
          : 1;
      }
      return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.LOW] <
        b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.LOW]
        ? 1
        : -1;

    case GP_TABLE_SORT_BY.IP_REPUTATION.MEDIUM:
      if (order === 'asc') {
        return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.MEDIUM] <
          b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.MEDIUM]
          ? -1
          : 1;
      }
      return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.MEDIUM] <
        b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.MEDIUM]
        ? 1
        : -1;

    case GP_TABLE_SORT_BY.IP_REPUTATION.HIGH:
      if (order === 'asc') {
        return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.HIGH] <
          b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.HIGH]
          ? -1
          : 1;
      }
      return a.ipReputationsCountsData[GP_REPUTATION_CATEGORY.HIGH] <
        b.ipReputationsCountsData[GP_REPUTATION_CATEGORY.HIGH]
        ? 1
        : -1;

    default:
      break;
  }
  return 0;
}
