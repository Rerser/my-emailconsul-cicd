import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Icon from 'components/ui/icon';

import { usersMeSelector } from 'store/selectors';
import { deleteGpDomain } from 'store/actions/googlePostmaster';
import {
  DEMO_USER_EMAIL,
  GP_REPUTATION_CATEGORY,
  HOVERED_GP_ITEMS
} from 'utils/constants';
import { getFormattedDaysMMDD } from 'utils/helpers';
import { setCallbackConfirmationPopup } from 'store/actions/confirmationPopup';

export const GP_TABLE_ROW_CLASS = 'gp-table__row';
export const GP_TABLE_COLUMN_BAD_REPUTATION_IP_CLASS = 'gp-table__column--bad-reputation';
export const GP_TABLE_COLUMN_LOW_REPUTATION_IP_CLASS = 'gp-table__column--low-reputation';
export const GP_TABLE_COLUMN_MEDIUM_REPUTATION_IP_CLASS = 'gp-table__column--medium-reputation';
export const GP_TABLE_COLUMN_HIGH_REPUTATION_IP_CLASS = 'gp-table__column--high-reputation';

function TableRow({ data, onClick }) {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const [hoveredItem, setHoveredItem] = useState(HOVERED_GP_ITEMS.none);

  const me = useSelector(usersMeSelector);
  const { email: meEmail } = me;

  const handleDeleteDomain = (e) => {
    e.stopPropagation();
    dispatch(setCallbackConfirmationPopup(()=> {
      dispatch(deleteGpDomain({ id: data.id }));
    }));
  }

  const handleHover = (e, itemType) => {
    if (hoveredItem !== itemType) {
      const target = e.target;
      let category = null;
      let className = null;
  
      switch (itemType) {
        case HOVERED_GP_ITEMS.badReputation:
          category = GP_REPUTATION_CATEGORY.BAD;
          className = GP_TABLE_COLUMN_BAD_REPUTATION_IP_CLASS;
          break;

        case HOVERED_GP_ITEMS.lowReputation:
          category = GP_REPUTATION_CATEGORY.LOW;
          className = GP_TABLE_COLUMN_LOW_REPUTATION_IP_CLASS;
          break;

        case HOVERED_GP_ITEMS.mediumReputation:
          category = GP_REPUTATION_CATEGORY.MEDIUM;
          className = GP_TABLE_COLUMN_MEDIUM_REPUTATION_IP_CLASS;
          break;

        case HOVERED_GP_ITEMS.highReputation:
          category = GP_REPUTATION_CATEGORY.HIGH;
          className = GP_TABLE_COLUMN_HIGH_REPUTATION_IP_CLASS;
          break;
      
        default:
          break;
      }
      if (data.ipReputationsCountsData[category] && target.closest(`.${className}`)) {
        e.stopPropagation();
        return setHoveredItem(itemType);
      }
      if (itemType === HOVERED_GP_ITEMS.row) {
        return setHoveredItem(itemType);
      }
    }
  }

  const handleMouseOut = (e) => {
    e.stopPropagation();

    const target = e.target;

    if (target.closest(`.${GP_TABLE_ROW_CLASS}`)) {
      setHoveredItem(HOVERED_GP_ITEMS.row);
    }
  }

  return (
    <div
      className={cn(`${GP_TABLE_ROW_CLASS}`, {
        [`${GP_TABLE_ROW_CLASS}--hovered`]: hoveredItem === HOVERED_GP_ITEMS.row,
      })}
      onClick={onClick}
      onMouseEnter={(e) => handleHover(e, HOVERED_GP_ITEMS.row)}
      onMouseLeave={() => setHoveredItem(HOVERED_GP_ITEMS.none)}
      onMouseOut={handleMouseOut}
    >
      <div className="gp-table__column gp-table__column--domain">
        <span className={cn("gp-table__column-content", {
          'gp-table__column-content--blured': meEmail === DEMO_USER_EMAIL
        })}>
          {data.domain}
        </span>
      </div>
      <div className="gp-table__column gp-table__column--domain-reputation">
        {
          data.reputations.domain.map((rep, i) => (
            <div
              key={`${data.id}-reputation-${i}`}
              className={cn("gp-table__reputation-item", {
                [`gp-table__reputation-item--${rep}`]: rep
              })}
              data-tooltip={getFormattedDaysMMDD(data.daysISO[i])}
            />
          ))
        }
      </div>
      <div className="gp-table__column">
        <span className="gp-table__column-content">
          {Math.round(data.averages.spf * 100)}
          {'%'}
        </span>
      </div>
      <div className="gp-table__column">
        <span className="gp-table__column-content">
          {Math.round(data.averages.dkim * 100)}
          {'%'}
        </span>
      </div>
      <div className="gp-table__column">
        <span className="gp-table__column-content">
          {Math.round(data.averages.dmarc * 100)}
          {'%'}
        </span>
      </div>
      <div
        className={cn(`gp-table__column ${GP_TABLE_COLUMN_BAD_REPUTATION_IP_CLASS}`, {
          'gp-table__column--hovered': hoveredItem === HOVERED_GP_ITEMS.badReputation,
        })}
        onMouseOver={(e) => handleHover(e, HOVERED_GP_ITEMS.badReputation)}
      >
        <span className="gp-table__column-content">
          {data.ipReputationsCountsData[GP_REPUTATION_CATEGORY.BAD] || '-'}
        </span>
      </div>
      <div
        className={cn(`gp-table__column ${GP_TABLE_COLUMN_LOW_REPUTATION_IP_CLASS}`, {
          'gp-table__column--hovered': hoveredItem === HOVERED_GP_ITEMS.lowReputation,
        })}
        onMouseOver={(e) => handleHover(e, HOVERED_GP_ITEMS.lowReputation)}
      >
        <span className="gp-table__column-content">
          {data.ipReputationsCountsData[GP_REPUTATION_CATEGORY.LOW] || '-'}
        </span>
      </div>
      <div
        className={cn(`gp-table__column ${GP_TABLE_COLUMN_MEDIUM_REPUTATION_IP_CLASS}`, {
          'gp-table__column--hovered': hoveredItem === HOVERED_GP_ITEMS.mediumReputation,
        })}
        onMouseOver={(e) => handleHover(e, HOVERED_GP_ITEMS.mediumReputation)}
      >
        <span className="gp-table__column-content">
          {data.ipReputationsCountsData[GP_REPUTATION_CATEGORY.MEDIUM] || '-'}
        </span>
      </div>
      <div
        className={cn(`gp-table__column ${GP_TABLE_COLUMN_HIGH_REPUTATION_IP_CLASS}`, {
          'gp-table__column--hovered': hoveredItem === HOVERED_GP_ITEMS.highReputation,
        })}
        onMouseOver={(e) => handleHover(e, HOVERED_GP_ITEMS.highReputation)}
      >
        <span className="gp-table__column-content">
          {data.ipReputationsCountsData[GP_REPUTATION_CATEGORY.HIGH] || '-'}
        </span>
      </div>
      <div className="gp-table__column gp-table__column--cbs">
        <Icon
          type="onDelete"
          customStyle="gp-table__cb"
          tooltip={t('gpdomains_list.gp-table.tooltip.delete.text')}
          onClick={handleDeleteDomain}
        />
      </div>
    </div>
  );
}

export default React.memo(TableRow);