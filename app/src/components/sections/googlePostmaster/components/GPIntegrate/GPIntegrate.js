import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import TextArea from 'components/ui/textArea';
import Button from 'components/ui/button';
import Spinner from 'components/ui/spinner';

import {
  gpIntegrationGPSecretsSelector,
  gpIntegrationGPSecretsIsLoadingSelector,
} from 'store/selectors';
import {
  sendGpSecrets,
  setGpDateRunIsShowSlider,
  setGpSecrets,
} from 'store/actions/gpIntegration';

import './gp-integrate.scss';

const GPIntegrate = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const gpIntegrationGPSecrets = useSelector(gpIntegrationGPSecretsSelector);
  const gpIntegrationGPSecretsIsLoading = useSelector(gpIntegrationGPSecretsIsLoadingSelector);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(sendGpSecrets({ creds: gpIntegrationGPSecrets }));
  };

  return (
    <form className="c-slidebar-content c-gp-integrate" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('gp_integration.integrate.title')}
      </h3>

      {
        gpIntegrationGPSecretsIsLoading
          ? <Spinner />
          : (
            <div className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('gp_integration.integrate.add-secret.label')}
              </span>
              <TextArea 
                placeholder={t('gp_integration.integrate.add-secret.textarea.placeholder.text')}
                value={gpIntegrationGPSecrets}
                onChange={value => dispatch(setGpSecrets(value))}
              />
            </div>
          )
      }
      <div className="c-slidebar-content__btns">
        <Button
          mode="purple"
          text={t('gp_integration.integrate.button.save.text')}
          className="c-slidebar-content__btn"
          type="submit"
          disabled={gpIntegrationGPSecretsIsLoading || !gpIntegrationGPSecrets}
        />
        <Button
          text={t('gp_integration.integrate.button.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setGpDateRunIsShowSlider(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(GPIntegrate);
