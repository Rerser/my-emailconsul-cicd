import React, { Fragment, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import Button from 'components/ui/button/Button';
import Tabs from 'components/ui/tabs';
import Spinner from 'components/ui/spinner';
import Popup from 'components/ui/popup/Popup';

import {
  googlePostmasterAvailableCountIsLoadingSelector,
  googlePostmasterDomainsAvailableCountSelector,
  myUserBillingPlanDataSelector,
  googlePostmasterShowTableModeSelector,
  googlePostmasterDataFilterSelector,
  googlePostmasterDateToSelector,
  gpIntegrationUserIntegratedIsLoadingSelector,
  gpIntegrationUserIntegratedSelector,
} from 'store/selectors';
import {
  setIsOnNewGpDomain,
  handleGpShowTableMode,
  handleGpDataIsLoading,
  getGpDomainsData,
  getGpDomainsAvailableCount,
} from 'store/actions/googlePostmaster';
import {
  checkGpUserCredentials,
  setGpDateRunIsShowSlider,
  setGpDomainsHistoryIsShowSlider,
  setGpSecretsIsShowSlider,
  setGpUserIntegrated
} from 'store/actions/gpIntegration';

import history from 'utils/history';
import { getBillingPlanIsExpired, getQS, useDebounce } from 'utils/helpers';
import { BILLING_PLAN_NAME, GP_SHOW_TABLE } from 'utils/constants';

import './gp-domains-list-header.scss';

function GPDomainsListHeader() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const googlePostmasterShowTableMode = useSelector(googlePostmasterShowTableModeSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const googlePostmasterDateTo = useSelector(googlePostmasterDateToSelector);
  const { domain: filterDomain } = useSelector(googlePostmasterDataFilterSelector);
  const gpDomainsAvailable = useSelector(googlePostmasterDomainsAvailableCountSelector);
  const gpDomainsAvailableIsLoading = useSelector(googlePostmasterAvailableCountIsLoadingSelector);

  const gpIntegrationUserIntegratedIsLoading = useSelector(gpIntegrationUserIntegratedIsLoadingSelector);
  const gpIntegrationUserIntegrated = useSelector(gpIntegrationUserIntegratedSelector);

  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);
  const debouncedFilterDomain = useDebounce(filterDomain, 1000);

  const needUpdate = filterDomain === debouncedFilterDomain;

  const [successValue] = useState(getQS('success'));
  const [errorValue] = useState(getQS('error'));
  const [showPopup, setShowPopup] = useState(false);

  useEffect(() => {
    if (successValue !== null || errorValue) {
      setShowPopup(true);
      history.replace({ search: '' });
      if (successValue !== null) {
        dispatch(setGpUserIntegrated(true));
      }
    } else {
      if (!gpIntegrationUserIntegrated) {
        dispatch(checkGpUserCredentials());
      }
    }
  }, [dispatch, successValue, errorValue, gpIntegrationUserIntegrated]);

  useEffect(() => {
    dispatch(handleGpDataIsLoading(true));
  }, [dispatch]);

  useEffect(() => {
    if (googlePostmasterDateTo && needUpdate) {
      dispatch(getGpDomainsData({ withLoader: false }));
    }
  }, [dispatch, needUpdate, googlePostmasterDateTo]);

  const isNotAllowedToRequest =
    myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.NONE ||
    // !myUserBillingPlanData?.domains?.domains_per_month || // TODO: наверное должно быть gpDomains
    billingPlanIsExpired;

  useEffect(() => {
    if (!isNotAllowedToRequest) {
      dispatch(getGpDomainsAvailableCount());
    }
  }, [dispatch, isNotAllowedToRequest]);

  return (
    <div className="gp-domains-list-header">
      <h1 className="gp-domains-list__title">
        {t('gpdomains_list.title.text')}
      </h1>

      <div className="gp-domains-list__header-right header-right">
        {!gpDomainsAvailableIsLoading && (billingPlanIsExpired || gpDomainsAvailable <= 0) && (
          <div className="header-right__upgrade-plan">
            <Button
              mode="blue"
              text={t('seedlisting_list.update_plan.btn')}
              className="gp-domains-list__btn"
              onClick={() => history.push('/user/billing')}
            />
          </div>
        )}
        <div className="header-right__tabs">
          <Tabs
            value={googlePostmasterShowTableMode}
            options={[
              {
                value: GP_SHOW_TABLE.DOMAIN,
                label: t('gpdomains_list.gp-header.tabs.domain.label'),
              },
              {
                value: GP_SHOW_TABLE.IP,
                label: t('gpdomains_list.gp-header.tabs.ip.label'),
              },
            ]}
            onChange={(e) => dispatch(handleGpShowTableMode(e.target.value))}
            containerStyle="header-right__tabs-container"
          />
        </div>

        <div className="header-right__new-domain">
          <Button
            mode="purple"
            text={t('gpdomains_list.add_domain.btn')}
            className="gp-domains-list__btn"
            onClick={() => dispatch(setIsOnNewGpDomain(true))}
            disabled={billingPlanIsExpired}
          />
          {gpDomainsAvailableIsLoading ? (
            <Spinner />
          ) : (
            <div className="gp-domains-list-header__domains-available domains-available">
              {billingPlanIsExpired && (
                <div className="domains-available__count danger">
                  {t('domains_list.billing_plan.exipe_warning_text')}
                </div>
              )}
              {!billingPlanIsExpired && (
                <Fragment>
                  <span
                    className={cn('domains-available__count', {
                      danger: gpDomainsAvailable <= 0,
                      warning: gpDomainsAvailable > 0 && gpDomainsAvailable < 25,
                      ok: gpDomainsAvailable >= 25,
                    })}
                  >
                    {gpDomainsAvailable}
                  </span>
                  <span>
                    {' '}
                    {t('gpdomains_list.billing_plan.checks_available_text')}
                  </span>
                </Fragment>
              )}
            </div>
          )}
        </div>

        <div className="header-right__user-integration user-integration">
          {
            gpIntegrationUserIntegratedIsLoading ? (
              <Spinner />
            ) : (
              <div className="user-integration__buttons">
                {
                  gpIntegrationUserIntegrated ? (
                    <Fragment>
                      <Button
                        mode="purple"
                        text={t('gp_integration.button.run.text')}
                        className="user-integration__btn"
                        onClick={() => dispatch(setGpDateRunIsShowSlider(true))}
                      />
                      <Button
                        mode="blue"
                        text={t('gp_integration.button.history.text')}
                        className="user-integration__btn"
                        onClick={() => dispatch(setGpDomainsHistoryIsShowSlider(true))}
                      />
                    </Fragment>
                  ) : (
                    <Button
                      mode="purple"
                      text={t('gp_integration.button.integration.text')}
                      className="user-integration__btn user-integration__btn--integration"
                      onClick={() => dispatch(setGpSecretsIsShowSlider(true))}
                    />
                  )
                }
              </div>
            )
          }
        </div>
      </div>

      <Popup
        isOpen={showPopup}
        onClose={() => setShowPopup(false)}
        wrapperStyle="gp-domains-list__integration-popup integration-popup"
      >
        <h4 className={cn('integration-popup__header', {
          'integration-popup__header--success': successValue !== null,
          'integration-popup__header--error': errorValue,
        })}>
          {
            errorValue
              ? t('gpdomains_list.integration-popup.error.title')
              : t('gpdomains_list.integration-popup.success.title')
          }
        </h4>
        <div className="integration-popup__description">
          {errorValue || t('gpdomains_list.integration-popup.success.description.text')}
        </div>
        <Button
          text={t('gpdomains_list.integration-popup.btn.close.text')}
          className="integration-popup__btn"
          onClick={() => setShowPopup(false)}
        />
      </Popup>

    </div>
  );
}

export default React.memo(GPDomainsListHeader);
