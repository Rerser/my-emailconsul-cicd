import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import Button from 'components/ui/button';

import {
  googlePostmasterIsDataLoadedSelector,
} from 'store/selectors';
import { getGpDomainsData } from 'store/actions/googlePostmaster';

import './gp-loading-info.scss';

function GPDomainFilter() {
  const dispatch = useDispatch();
  const t = useFormatMessage();  
  
  const googlePostmasterIsDataLoaded = useSelector(googlePostmasterIsDataLoadedSelector);

  return (
    <div className="gp-loading-info">
      <Button
        mode="blue"
        text={t('gpdomains_list.refresh.btn.text')}
        className="gp-loading-info__refresh-btn"
        onClick={() => dispatch(getGpDomainsData({ withLoader: false }))}
        hasRefreshIcon
        disabled={!googlePostmasterIsDataLoaded}
        isRotateAnimation={!googlePostmasterIsDataLoaded}
      />
    </div>
  );
}

export default React.memo(GPDomainFilter);