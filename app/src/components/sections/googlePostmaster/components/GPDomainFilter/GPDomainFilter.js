import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';

import InputText from 'components/ui/inputText';

import {
  googlePostmasterDataFilterSelector,
  googlePostmasterShowTableModeSelector,
} from 'store/selectors';
import { handleGpDataFilter } from 'store/actions/googlePostmaster';

import { GP_SHOW_TABLE } from 'utils/constants';
import './gp-domain-filter.scss';

function GPDomainFilter() {
  const dispatch = useDispatch();
  const t = useFormatMessage();  
  
  const googlePostmasterShowTableMode = useSelector(googlePostmasterShowTableModeSelector);
  const { domain, ip } = useSelector(googlePostmasterDataFilterSelector);

  return (
    <div className="itp-filter">

      {googlePostmasterShowTableMode === GP_SHOW_TABLE.IP && (
        <div className="itp-filter__item itp-filter__ip-filter">
          <div className="itp-filter__label">
            {t('gpdomains_list.filter_by_ip.placeholder')}
          </div>
          <InputText
            value={ip}
            onChange={(e) =>
              dispatch(
                handleGpDataFilter({
                  field: 'ip',
                  value: e.target.value,
                })
              )
            }
            placeholder={t('gpdomains_list.filter_by_ip.placeholder')}
          />
        </div>
      )}
      <div className={cn('itp-filter__item itp-filter__domain-filter', {
        'itp-filter__item itp-filter__domain-filter--ipmode': googlePostmasterShowTableMode === GP_SHOW_TABLE.IP
      })}>
        <div className="itp-filter__label">
          {t('gpdomains_list.filter_by_domain.lable')}
        </div>
        <InputText
          value={domain}
          onChange={(e) =>
            dispatch(
              handleGpDataFilter({
                field: 'domain',
                value: e.target.value,
              })
            )
          }
          placeholder={t('gpdomains_list.filter_by_domain.placeholder')}
        />
      </div>
    </div>
  );
}

export default React.memo(GPDomainFilter);
