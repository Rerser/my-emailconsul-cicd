import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { basic_trashcan } from 'react-icons-kit/linea/basic_trashcan';
import cn from 'classnames';
import Icon from 'react-icons-kit';
import Spinner from 'components/ui/spinner';
import InputText from 'components/ui/inputText';
import InputFile from 'components/ui/inputFile';
import Button from 'components/ui/button';
import Tabs from 'components/ui/tabs';

import {
  googlePostmasterNewGPDomainsSelector,
  googlePostmasterNewGPDomainIsLoadingSelector,
  googlePostmasterNewDomainModeSelector,
  googlePostmasterNewDomainsFileSelector,
  googlePostmasterNewDomainsFileIsUploadingSelector,
  googlePostmasterResponseNewDomainsUploadedFileSelector,
  googlePostmasterDomainsAvailableCountSelector,
} from 'store/selectors';
import {
  handelSetNewGpDomainsFile,
  handelUploadNewGpDomainsFile,
  handleNewGpDomainMode,
  handleNewGpDomains,
  removeNewGpDomain,
  saveNewGpDomains,
  setIsOnNewGpDomain,
} from 'store/actions/googlePostmaster';
import { NEW_GP_DOMAIN_MODE } from 'utils/constants';

import './c-add-gp-domain.scss';

const AddGPDomain = () => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const newGPDomains = useSelector(googlePostmasterNewGPDomainsSelector);
  const googlePostmasterNewGPDomainIsLoading = useSelector(googlePostmasterNewGPDomainIsLoadingSelector);
  const googlePostmasterDomainsAvailableCount = useSelector(googlePostmasterDomainsAvailableCountSelector);
  const googlePostmasterNewDomainMode = useSelector(googlePostmasterNewDomainModeSelector);
  const googlePostmasterNewDomainsFile = useSelector(googlePostmasterNewDomainsFileSelector);
  const googlePostmasterNewDomainsFileIsUploading = useSelector(googlePostmasterNewDomainsFileIsUploadingSelector);
  const googlePostmasterResponseNewDomainsUploadedFile = useSelector(googlePostmasterResponseNewDomainsUploadedFileSelector);

  const handleDeleteNewGPDomain = (e, index) => {
    e.preventDefault();
    if (newGPDomains.length === 1) {
      dispatch(handleNewGpDomains({ value: '', index: 0 }));
    } else {
      dispatch(removeNewGpDomain({ index }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!googlePostmasterNewGPDomainIsLoading && newGPDomains.every((domain) => domain)) {
      dispatch(saveNewGpDomains({ newGPDomains }));
    }
  };

  return (
    <form className="c-slidebar-content c-add-gp-domain" onSubmit={handleSubmit}>
      <h3 className="c-slidebar-content__title">
        {t('add_gpdomain.header.text')}
      </h3>

      {googlePostmasterNewGPDomainIsLoading && <Spinner />}

      {!googlePostmasterNewGPDomainIsLoading && (
        <Fragment>
          <div className="c-slidebar-content__row">
            <Tabs
              value={googlePostmasterNewDomainMode}
              options={[
                {
                  value: NEW_GP_DOMAIN_MODE.SINGLE,
                  label: t('add_gpdomain.tabs.single_domain.label'),
                },
                {
                  value: NEW_GP_DOMAIN_MODE.CSV,
                  label: t('add_gpdomain.tabs.many_domain.label'),
                },
              ]}
              onChange={(e) => dispatch(handleNewGpDomainMode(e.target.value))}
            />
          </div>

          {googlePostmasterNewDomainMode === NEW_GP_DOMAIN_MODE.SINGLE && (
            <label className="c-slidebar-content__label">
              <span className="c-slidebar-content__description">
                {t('add_gpdomain.domain.label')}
              </span>
              {newGPDomains.map((domain, index) => (
                <div key={index} className="c-add-gp-domain__domain-row gp-domain-row">
                  <InputText
                    value={domain}
                    onChange={(e) => dispatch(handleNewGpDomains({ value: e.target.value, index }))}
                    className="gp-domain-row__input"
                  />
                  <button
                    type="button"
                    className="gp-domain-row__remove-btn"
                    onClick={(e) => handleDeleteNewGPDomain(e, index)}
                  >
                    <Icon icon={basic_trashcan} />
                  </button>
                </div>
              ))}
              <Button
                text={t('add_gpdomain.btn.add.text')}
                onClick={() => dispatch(handleNewGpDomains({ value: null, index: newGPDomains.length }))}
                disabled={newGPDomains.length >= googlePostmasterDomainsAvailableCount}
              />
            </label>
          )}

          {googlePostmasterNewDomainMode === NEW_GP_DOMAIN_MODE.CSV && (
            <Fragment>
              {googlePostmasterNewDomainsFileIsUploading ? (
                <Spinner />
              ) : (
                <Fragment>
                  {googlePostmasterResponseNewDomainsUploadedFile && (
                    <div className="c-add-gp-domain__result-uploaded-container">
                      <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                        <div className="c-add-gp-domain__uploaded-result">
                          {t('add_gpdomain.result.uploaded.added.text')}
                          {' '}
                          <span className={cn("c-add-gp-domain__quantity", {
                            'c-add-domain__quantity--success': googlePostmasterResponseNewDomainsUploadedFile.added > 0,                            
                            'c-add-domain__quantity--failed': googlePostmasterResponseNewDomainsUploadedFile.added === 0,
                          })}>
                            {googlePostmasterResponseNewDomainsUploadedFile.added}
                          </span>
                        </div>
                      </div>
                      {!!googlePostmasterResponseNewDomainsUploadedFile.not_added && (
                        <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                          <div className="c-add-gp-domain__uploaded-result">
                            {t('add_gpdomain.result.uploaded.has_not_added.text')}
                            {' '}
                            <span className={cn("c-add-gp-domain__quantity", {
                              'c-add-domain__quantity--success': googlePostmasterResponseNewDomainsUploadedFile.not_processed === 0,                              
                              'c-add-domain__quantity--failed': googlePostmasterResponseNewDomainsUploadedFile.not_processed > 0,
                            })}>
                              {googlePostmasterResponseNewDomainsUploadedFile.not_added}
                            </span>
                          </div>
                        </div>
                      )}
                      {!!googlePostmasterResponseNewDomainsUploadedFile.not_valid.length && (
                        <div className="c-slidebar-content__file-row c-slidebar-content__file-row--mt-half">
                          <div className="c-add-gp-domain__uploaded-result">
                            {t('add_gpdomain.result.uploaded.not_valid.text')}
                            {' '}
                            <span className="c-add-gp-domain__quantity c-add-gp-domain__quantity--failed">
                              {googlePostmasterResponseNewDomainsUploadedFile.not_valid.length}
                            </span>
                            {!!googlePostmasterResponseNewDomainsUploadedFile.not_valid.length && 
                              googlePostmasterResponseNewDomainsUploadedFile.not_valid.map((domain, i) => (
                                <div key={`not-valid-gp-domains-${i}`} className="c-add-gp-domain__uploaded">
                                  {domain}
                                </div>
                              ))
                            }
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                
                  <div className="c-slidebar-content__label">
                    {!googlePostmasterNewDomainsFile && (
                      <Fragment>
                        <span>
                          {t('add_gpdomain.drag_file.warning.text')}
                        </span>
                        <InputFile
                          labelNodeElement={
                            <div className="c-add-gp-domain__dragfile-container">
                              {t('add_gpdomain.drag_file.import.text')}
                            </div>
                          }
                          onUpload={(data) => dispatch(handelSetNewGpDomainsFile(data.files[0]))}
                        />
                      </Fragment>
                    )}
                    {googlePostmasterNewDomainsFile && (
                      <div className="c-slidebar-content__file-row">
                        <div className="c-slidebar-content__file-label">
                          {googlePostmasterNewDomainsFile.name}
                        </div>
                        <div
                          className="c-slidebar-content__file-delete"
                          onClick={() => dispatch(handelSetNewGpDomainsFile(null))}
                        >
                          <Icon icon={basic_trashcan} />
                        </div>
                      </div>
                    )}
                  </div>
                </Fragment>
              )}
            </Fragment>
          )}
        </Fragment>
      )}

      <div className="c-slidebar-content__btns">
        {googlePostmasterNewDomainMode === NEW_GP_DOMAIN_MODE.SINGLE && (
          <Button
            mode="purple"
            text={t('add_gpdomain.btn.save.text')}
            className="c-slidebar-content__btn"
            disabled={googlePostmasterNewGPDomainIsLoading || newGPDomains.some((domain) => !domain)}
            type="submit"
          />
        )}

        {googlePostmasterNewDomainMode === NEW_GP_DOMAIN_MODE.CSV && (
          <Button
            mode="purple"
            text={t('add_gpdomain.btn.import.text')}
            className="c-slidebar-content__btn"
            disabled={!googlePostmasterNewDomainsFile || googlePostmasterNewDomainsFileIsUploading}
            type="button"
            onClick={() => dispatch(handelUploadNewGpDomainsFile())}
          />
        )}

        <Button
          text={t('add_gpdomain.btn.close.text')}
          className="c-slidebar-content__btn"
          onClick={() => dispatch(setIsOnNewGpDomain(false))}
        />
      </div>
    </form>
  );
};

export default React.memo(AddGPDomain);
