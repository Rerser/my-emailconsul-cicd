import React from 'react';
import { ru, enUS } from 'date-fns/locale';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import { differenceInCalendarDays } from 'date-fns';

import DatePickerRange from 'components/ui/datePickerRange';

import {
  setGpDateFrom,
  setGpDateTo
} from 'store/actions/googlePostmaster';
import {
  localeCurrentSelector,
  googlePostmasterDateFromSelector,
  googlePostmasterDateToSelector,
} from 'store/selectors';

import { LOCALE, DAY, MONTH, GP_DAYS_LIMIT } from 'utils/constants';

function GPDatesRange() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const locale = useSelector(localeCurrentSelector);

  const googlePostmasterDateFrom = useSelector(googlePostmasterDateFromSelector);
  const googlePostmasterDateTo = useSelector(googlePostmasterDateToSelector);

  const handleChangeDateFrom = (date) => {
    if (date) {
      dispatch(setGpDateFrom(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
      if (
        differenceInCalendarDays(new Date(googlePostmasterDateTo), date) > GP_DAYS_LIMIT
      ) {
        dispatch(setGpDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
      }
    } else {
      dispatch(setGpDateFrom(''));
    }
    if (date >= new Date(googlePostmasterDateTo)) {
      dispatch(setGpDateTo(new Date(date.setUTCHours(0, 0, 0, 0) + MONTH - DAY).toISOString()));
    }
  };

  const handleChangeDateTo = (date) => {
    if (date) {
      dispatch(setGpDateTo(new Date(new Date(date).setUTCHours(0, 0, 0, 0)).toISOString()));
    } else {
      dispatch(setGpDateTo(''));
    }
  };

  return (
    <div className="gp-domains-list__dates-range">
      <DatePickerRange 
        locale={locale === LOCALE.ru ? ru : enUS}

        labelInputFrom={t('gpdomains_list.dates_range.date_from.text')}
        labelInputTo={t('gpdomains_list.dates_range.date_to.text')}

        dateFormat="DD-MM-yyyy"
        dateFrom={googlePostmasterDateFrom}
        dateTo={googlePostmasterDateTo}
        handleChangeDateFrom={handleChangeDateFrom}
        handleChangeDateTo={handleChangeDateTo}

        maxDate={new Date(Date.parse(googlePostmasterDateFrom) + MONTH - DAY).toISOString()}
        maxDaysDateTo={GP_DAYS_LIMIT}

        disabledTo={!googlePostmasterDateFrom}
        requiredDateTo={!!googlePostmasterDateFrom && !googlePostmasterDateTo}
      />
    </div>
  );
}

export default React.memo(GPDatesRange);
