/**
 * ИСПОЛЬЗОВАНИЕ

 * Props:
 * onChange: функция по клику, обязательная,
   получает в качестве
       первого - параметр name
       второго аргумента значения checked (true или false),
 * checked: значение по умолчание (true или false)
 * className: дополниельные классы для компонента
 * name: параметр name
 * partly: тут хранится статус того одинаково ли состояние чекбоксов ниже по иерархии

 * Пример:
    <Checkbox
        onChange={this.doSomething}
        checked={true}
        className={someAdvancedClasses}
    />
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './c-checkbox.scss';

export default class Checkbox extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string,
    className: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    partly: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    name: '',
    checked: false,
    disabled: false,
    partly: false,
  };

  handleChange = () => {
    this.props.onChange.call(null, this.props.name, this.props.checked);
  };

  render() {
    const { className, checked, disabled, partly } = this.props;

    return (
      <div
        className={cn('c-checkbox', className, {
          checked: checked && !partly,
          disabled,
          partly,
        })}
        onClick={this.handleChange}
      />
    );
  }
}
