/**
  * Example:

    <Pagination
      page={page}
      pages={pages}
      pageSize={pageSize}
      handlePageSize={handlePageSize}
      handlePage={handlePage}
    />
*/

import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './Pagination.scss';

const Pagination = ({
  page,
  pages,
  pageSize,
  handlePageSize,
  handlePage,
  className,
}) => {
  const pagesMass = [];
  for (let i = 0; i < pages; i++) {
    pagesMass.push({
      label: i + 1,
      value: i,
    });
  }

  return (
    <div className={cn('pagination', className)}>
      <div className="pagination__pages">
        {pagesMass.length && (
          <div
            className={[
              'pagination__page',
              pagesMass[0].value === page ? 'active' : '',
            ].join(' ')}
            onClick={handlePage.bind(null, pagesMass[0].value)}
          >
            {pagesMass[0].label}
          </div>
        )}

        {pagesMass.length > 7 && page - 3 > 0 && <span>...</span>}

        {pagesMass.length > 2 &&
          (() => {
            const showedPages = pagesMass.slice();

            showedPages.shift();
            showedPages.pop();

            if (showedPages.length > 5) {
              showedPages.splice(0, page - 2);
              showedPages.splice(3, showedPages.length);
            }

            return showedPages.map((p) => (
              <div
                key={p.value}
                className={[
                  'pagination__page',
                  p.value === page ? 'active' : '',
                ].join(' ')}
                onClick={handlePage.bind(null, p.value)}
              >
                {p.label}
              </div>
            ));
          })()}

        {pagesMass.length > 7 && page + 3 < pagesMass.length && (
          <span>...</span>
        )}

        {pagesMass.length > 1 && (
          <div
            className={[
              'pagination__page',
              pagesMass[pagesMass.length - 1].value === page ? 'active' : '',
            ].join(' ')}
            onClick={handlePage.bind(
              null,
              pagesMass[pagesMass.length - 1].value
            )}
          >
            {pagesMass[pagesMass.length - 1].label}
          </div>
        )}
      </div>

      <select value={pageSize} onChange={(e) => handlePageSize(e.target.value)}>
        <option value="10">10</option>
        <option value="30">30</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>
    </div>
  );
};

Pagination.propTypes = {
  page: PropTypes.number,
  pages: PropTypes.number,
  pageSize: PropTypes.number,
  handlePageSize: PropTypes.func.isRequired,
  handlePage: PropTypes.func.isRequired,
  className: PropTypes.string,
};

Pagination.defaultProps = {
  page: 0,
  pages: 1,
  pageSize: 24,
  className: '',
};

export default React.memo(Pagination);
