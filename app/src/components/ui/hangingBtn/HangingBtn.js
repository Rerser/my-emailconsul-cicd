import React, { useEffect, useState } from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './hanging-btn.scss';

const id = 'hangingBtn';

function HangingBtn({ tooltipText, href, customContainerStyle }) {
  const [element, setElement] = useState(null);
  const [showTooltip, setShowTooltip] = useState(false);

  useEffect(() => {
    let div = document.getElementById(id);

    if (!div) {
      div = document.createElement('div');
      div.id = id;
      document.body.appendChild(div);
    }
    setElement(div);
  }, []);

  const layout = (
    <div className={cn("hanging-btn", customContainerStyle)}>
      <div className={cn("hanging-btn__tooltip", {
        'hanging-btn__tooltip--show': showTooltip
      })}>
        {tooltipText}
      </div>
      <a
        className={cn("hanging-btn__btn", {
          'hanging-btn__btn--stop-animate': showTooltip
        })}
        href={href}
        target="_blank"
        rel="noopener noreferrer"
        onMouseEnter={() => setShowTooltip(true)}
        onMouseLeave={() => setShowTooltip(false)}
      >
        <span className="hanging-btn__icon">
          {'?'}
        </span>
      </a>
    </div>
  );

  return element && ReactDom.createPortal(layout, element);
}

HangingBtn.defaultProps = {
  href: '/',
  tooltipText: '',
  customStyle: '',
};
HangingBtn.propTypes = {
  href: PropTypes.string.isRequired,
  tooltipText: PropTypes.string,
  customStyle: PropTypes.string,
};

export default React.memo(HangingBtn);
