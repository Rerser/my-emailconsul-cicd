import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { Icon } from 'react-icons-kit';
import { arrows_remove } from 'react-icons-kit/linea/arrows_remove';

import './c-slidebar.scss';
import './c-slidebar-content.scss';

export default class SlideBar extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  render() {
    return (
      <div
        className={cn('c-slidebar', this.props.className, {
          showed: this.props.isOpen,
          hidden: !this.props.isOpen,
        })}
      >
        <div className="c-slidebar__background" onClick={this.props.onClose}>
          &nbsp;
        </div>
        <div className="c-slidebar__content">
          <div className="c-slidebar__close" onClick={this.props.onClose}>
            <Icon size="100%" icon={arrows_remove} />
          </div>
          {this.props.isOpen && this.props.children}
        </div>
      </div>
    );
  }
}
