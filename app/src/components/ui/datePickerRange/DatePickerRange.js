/**
 Пример использования:

  <DatePickerRange 
    locale={locale === LOCALE.ru ? ru : enUS}

    labelInputFrom={'Date From'}
    labelInputTo={'Date To'}

    dateFormat="DD-MM-yyyy HH:mm"
    dateFrom={dateFrom}
    dateTo={ipsIpRblChangesLogPeriodTo}
    handleChangeDateFrom={handleChangeDateFrom}
    handleChangeDateTo={handleChangeDateTo}

    hasTimeSelect
    timeFormat='HH:mm'
    timeIntervals={15}

    maxDate={new Date(Date.now() - DAY).toISOString()}

    disabled={disabled}
    disabledTo={!ipsIpRblChangesLogPeriodFrom || disabled}
    requiredDateTo={!disabled && !ipsIpRblChangesLogPeriodTo}
  />
 */
import React, { useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DatePicker from 'react-datepicker';
import { enUS } from 'date-fns/locale';
import {
  differenceInCalendarDays,
  getHours,
  getMinutes,
  setHours,
  setMinutes,
} from 'date-fns';

import InputText from 'components/ui/inputText';
import Button from 'components/ui/button';

import { DAY, MONTH, WEEK, TABLETS_WIDTH_BREAKPOINT } from 'utils/constants';
import { convertDateTimeStr } from 'utils/helpers';

import './date-picker-range.scss';

const DATE_RANGE_CLASS = 'date-range';

function DatePickerRange(props) {
  const t = useFormatMessage();

  // статус открытия/закрытия всплывающего окна с датапикерами
  const [isDatePickersShow, setIsDatePickersShow] = useState(false);

  // fix стилей для датапикеров
  const datePickerFromWrapperRef = useRef(null);
  const datePickerToWrapperRef = useRef(null);

  useEffect(() => {
    const datePickerFromWrapper = datePickerFromWrapperRef?.current;
    const datePickerToWrapper = datePickerToWrapperRef?.current;

    if (datePickerFromWrapper && datePickerToWrapper) {
      const divFrom = datePickerFromWrapper.firstElementChild;
      const divTo = datePickerToWrapper.firstElementChild;

      divFrom.style = `height: 100%`;
      divTo.style = `height: 100%`;
    }
  }, [datePickerFromWrapperRef, datePickerToWrapperRef]);

  // положение всплывающего окна с датапикерами
  const inputsWrapperRef = useRef(null);
  const controlsWrapperRef = React.createRef();

  useEffect(() => {
    const inputsWrapper = inputsWrapperRef?.current;
    const controlsWrapper = controlsWrapperRef?.current;

    if (inputsWrapper && controlsWrapper) {
      const winWidth = window.innerWidth;
      const winHeight = window.innerHeight;
      const inputsWrapperParams = inputsWrapper.getBoundingClientRect();

      if (winWidth > TABLETS_WIDTH_BREAKPOINT) {
        if (
          // левый верхний угол
          inputsWrapperParams.left <= winWidth / 2 &&
          inputsWrapperParams.top <= winHeight / 2
        ) {
          controlsWrapper.style = `top: 100%; left: 0%;`;
        } else if (
          // левый нижний угол
          inputsWrapperParams.left <= winWidth / 2 &&
          inputsWrapperParams.top > winHeight / 2
        ) {
          controlsWrapper.style = `bottom: 100%; left: 0%;`;
        } else if (
          // правый верхний угол
          inputsWrapperParams.left > winWidth / 2 &&
          inputsWrapperParams.top <= winHeight / 2
        ) {
          controlsWrapper.style = `top: 100%; right: 0%;`;
        } else if (
          // правый нижний угол
          inputsWrapperParams.left > winWidth / 2 &&
          inputsWrapperParams.top > winHeight / 2
        ) {
          controlsWrapper.style = `bottom: 100%; right: 0%;`;
        }
      } else {
        controlsWrapper.style = `
          position: fixed;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        `;
      }
    }
  }, [controlsWrapperRef, inputsWrapperRef]);

  const handleInputsWrapperClick = () => {
    if (props.disabledFrom || props.disabledTo || props.disabled) return;
    setIsDatePickersShow(true);
  }

  const handleChangeInputFrom = (date) => {};
  const handleChangeInputTo = (date) => {};

  // обработчик выбора даты from
  const handleChangeDateFrom = (date) => {
    if (props.disabledFrom || props.disabledTo || props.disabled) return;
    props.handleChangeDateFrom(date);
  };

  // обработчик выбора даты to
  const handleChangeDateTo = (date) => {
    if (!props.dateFrom || props.disabledFrom || props.disabledTo || props.disabled) return;
    if (!props.hasTimeSelect) {
      props.handleChangeDateTo(date);
    } else {
      const hoursFrom = getHours(Date.parse(props.dateFrom));
      const minutesFrom = getMinutes(Date.parse(props.dateFrom));
      const hoursTo = getHours(Date.parse(date));
      const minutesTo = getMinutes(Date.parse(date));
      
      // если разница в днях между from и to <= 0
      // и время from [часы:минуты] больше чем время to
      if (
        differenceInCalendarDays(new Date(props.dateFrom), new Date(props.dateTo)) <= 0 &&
        (hoursTo < hoursFrom || (hoursTo <= hoursFrom && minutesTo < minutesFrom))
      ) {
        // установим время для to как время from + интервальные минуты (из props)
        const dateTo = setHours(
          setMinutes(new Date(date), minutesFrom + (props.timeIntervals || 1)),
          hoursFrom
        );

        props.handleChangeDateTo(dateTo);
      } else {
        props.handleChangeDateTo(date);
      }
    }
  };

  // разница в количестве дней от текущего с props maxDate
  const diffDaysFromMaxDayToDateNow = useMemo(
    () => differenceInCalendarDays(Date.now(), Date.parse(props.maxDate)), 
  [props.maxDate]);

  // обработчик кликов по кнопкам готовых сценариев
  const handleButtonClick = (script) => {
    if (props.disabledFrom || props.disabledTo || props.disabled) return;

    const maxDays = diffDaysFromMaxDayToDateNow > 0 ? diffDaysFromMaxDayToDateNow : 0;
    const toDay = Date.now();

    switch (script) {
      case 'day':
        props.handleChangeDateFrom(new Date(toDay - DAY - maxDays * DAY).toISOString());
        break;

      case 'week':
        props.handleChangeDateFrom(new Date(toDay - WEEK - maxDays * DAY).toISOString());
        break;

      case 'month':
        props.handleChangeDateFrom(new Date(toDay - MONTH - maxDays * DAY).toISOString());
        break;
    
      default:
        break;
    }
    props.handleChangeDateTo(new Date(toDay - maxDays * DAY).toISOString());
  }

  // даты from и to для датапикеров
  const dateFromForDatePicker = !!Date.parse(props.dateFrom) ? new Date(props.dateFrom) : null;
  const dateToForDatePicker = !!Date.parse(props.dateTo) ? new Date(props.dateTo) : null;

  // максимальная дата для dateFrom
  const maxDateForFateFrom = useMemo(() => {
    if (props.dateTo && props.maxDate && Date.parse(props.maxDate) <= Date.parse(props.dateTo)) {
      return new Date(props.maxDate);
    }
    if (Date.parse(props.dateTo)) {
      return new Date(props.dateTo);
    }
    return new Date();
  }, [props.maxDate, props.dateTo]);

  // максимальная дата для dateTo
  const maxDateForDateTo = useMemo(() => {
    if (!!props.maxDaysDateTo && Date.parse(props.dateFrom)) {
      return new Date(Date.parse(props.dateFrom) + (props.maxDaysDateTo - 1) * DAY);
    }
    return null;
  }, [props.dateFrom, props.maxDaysDateTo]);

  // минимальная дата dateFrom
  const minDate = props.dateFrom ? new Date(props.dateFrom) : null;

  return (
    <div className={cn(DATE_RANGE_CLASS, {
      'date-range--disabled': props.disabled,
    })}>
      <div
        className="date-range__inputs" ref={inputsWrapperRef}
        onClick={handleInputsWrapperClick}
      >
        <div className="date-range__input-container">
          <div className="date-range__label">
            {props.labelInputFrom}
          </div>
          <InputText
            value={convertDateTimeStr(props.dateFrom, props.dateFormat)}
            onChange={handleChangeInputFrom}
            placeholder={props.placeholderInputFrom || props.dateFormat || ''}
            required={props.requiredDateFrom || props.required}
            disabled={props.disabledFrom || props.disabled}
            className="date-range__input date-range__input--from"
          />
        </div>
        <div className="date-range__separator"></div>
        <div className="date-range__input-container">
          <div className="date-range__label">
            {props.labelInputTo}
          </div>
          <InputText
            value={convertDateTimeStr(props.dateTo, props.dateFormat)}
            onChange={handleChangeInputTo}
            placeholder={props.placeholderInputTo || props.dateFormat || ''}
            required={props.requiredDateTo || props.required}
            disabled={props.disabledTo || props.disabled}
            className="date-range__input date-range__input--to"
          />
        </div>
      </div>

      {isDatePickersShow && (
        <div className="date-range__controls" ref={controlsWrapperRef}>
          <div className="date-range__row date-range__header">
            <div className="date-range__diffdays">
              {diffDaysFromMaxDayToDateNow > 0 && (
                <span>
                  {convertDateTimeStr(new Date(Date.now() - diffDaysFromMaxDayToDateNow * DAY).toISOString(), 'DD-MM-yyyy')}
                  {' - '}
                  {t('date-picker-range.diff-days.text')}
                </span>
              )}
            </div>
            <div className="date-range__close" onClick={() => setIsDatePickersShow(false)}/>
          </div>
          <div className="date-range__row date-range__datepickers datepickers">
            <div
              className={cn("datepickers__container datepickers__container--from", {
                'datepickers__container--disabled': props.disabledFrom || props.disabled
              })}
              ref={datePickerFromWrapperRef}
            >
              <DatePicker
                inline
                locale={props.locale}
                disabled={props.disabledFrom || props.disabled}

                dateFormat={props.dateFormat}
                openToDate={dateFromForDatePicker}
                selected={dateFromForDatePicker}
                onChange={handleChangeDateFrom}
                calendarClassName="datepickers__wrapper"

                showTimeSelect={props.hasTimeSelect}
                timeCaption={t('date-picker-range.datepicker.time-select.header.text')}
                timeFormat={props.timeFormat}
                timeIntervals={props.timeIntervals}

                minTime={setHours(setMinutes(new Date(), 0), 0)}
                maxTime={
                  !differenceInCalendarDays(new Date(props.dateFrom), new Date(props.dateTo))
                    ? setHours(setMinutes(new Date(), getMinutes(new Date(props.dateTo))), getHours(new Date(props.dateTo)))
                    : setHours(setMinutes(new Date(), 59), 23)
                }
                maxDate={maxDateForFateFrom}

                selectsStart
                startDate={dateFromForDatePicker}
                endDate={dateToForDatePicker}
              />
            </div>
            <div
              className={cn("date-range__row datepickers__container datepickers__container--to", {
                'datepickers__container--disabled': props.disabledTo || props.disabled
              })}
              ref={datePickerToWrapperRef}
            >
              <DatePicker
                inline
                locale={props.locale}
                disabled={props.disabledTo || props.disabled}
                wrapperClassName="datepickers__1"

                dateFormat={props.dateFormat}
                openToDate={!!Date.parse(props.dateTo) ? new Date(props.dateTo) : null}
                selected={!!Date.parse(props.dateTo) ? new Date(props.dateTo) : null}
                onChange={handleChangeDateTo}
                calendarClassName="datepickers__wrapper"

                showTimeSelect={props.hasTimeSelect}
                timeCaption={t('date-picker-range.datepicker.time-select.header.text')}
                timeFormat={props.timeFormat}
                timeIntervals={props.timeIntervals}
                                
                minTime={
                  !differenceInCalendarDays(new Date(props.dateFrom), new Date(props.dateTo))
                    ? setHours(
                        setMinutes(new Date(), getMinutes(new Date(props.dateFrom))),
                        getHours(new Date(props.dateFrom))
                      )
                    : setHours(setMinutes(new Date(), 0), 0)
                }
                maxTime={setHours(setMinutes(new Date(), 59), 23)}
                minDate={minDate}
                maxDate={maxDateForDateTo}

                selectsEnd
                startDate={dateFromForDatePicker}
                endDate={dateToForDatePicker}

                popperModifiers={{
                  preventOverflow: {
                    enabled: true,
                  },
                }}
              />
            </div>
          </div>
          <div className={cn("date-range__row date-range__buttons", {
            'date-range__buttons--disabled':
              props.disabledFrom || props.disabledTo || props.disabled,
          })}>
            <Button
              mode="blue"
              text={t('date-picker-range.pasts.day.text')}
              className="date-range__button"
              onClick={() => handleButtonClick('day')}
            />
            <Button
              mode="blue"
              text={t('date-picker-range.pasts.week.text')}
              className="date-range__button"
              onClick={() => handleButtonClick('week')}
            />
            <Button
              mode="blue"
              text={t('date-picker-range.pasts.month.text')}
              className="date-range__button"
              onClick={() => handleButtonClick('month')}
            />
          </div>
        </div>
      )}
    </div>
  );
}

DatePickerRange.propTypes = {
  // формат даты и времени
  dateFormat: PropTypes.string,
  timeFormat: PropTypes.string,

  // локаль датапикеров (из 'date-fns/locale')
  locale: PropTypes.object,

  // дата от и до (в ISO string)
  dateFrom: PropTypes.string.isRequired,
  dateTo: PropTypes.string.isRequired,

  // обработчики выбора дат
  handleChangeDateFrom: PropTypes.func.isRequired,
  handleChangeDateTo: PropTypes.func.isRequired,

  // disabled состояния
  disabledFrom: PropTypes.bool,
  disabledTo: PropTypes.bool,
  disabled: PropTypes.bool,

  // label и placeholder
  labelInputFrom: PropTypes.string,
  labelInputTo: PropTypes.string,
  placeholderInputFrom: PropTypes.string,
  placeholderInputTo: PropTypes.string,

  // статусы required
  requiredDateFrom: PropTypes.bool,
  requiredDateTo: PropTypes.bool,
  required: PropTypes.bool,

  // подключать или нет колонку с выбором времени
  hasTimeSelect: PropTypes.bool,
  // интервалы показа времени (мин)
  timeIntervals: PropTypes.number,

  // максимальная дата для dateFrom (в ISO string)
  maxDate: PropTypes.string,
  // максимальное количество дней для dateTo в разнице от dayFrom
  maxDaysDateTo: PropTypes.number,
};

DatePickerRange.defaultProps = {
  dateFormat: 'HH:mm:ss DD-MM-YYYY',
  timeFormat: 'HH:mm:ss',

  locale: enUS,

  disabledFrom: false,
  disabledTo: false,
  disabled: false,

  labelInputFrom: '',
  labelInputTo: '',
  placeholderInputFrom: '',
  placeholderInputTo: '',

  requiredDateFrom: false,
  requiredDateTo: false,
  required: false,

  hasTimeSelect: false,
  timeIntervals: 1, // 1 мин
  
  maxDate: '',
  maxDaysDateTo: 0,
};

export default React.memo(DatePickerRange);
