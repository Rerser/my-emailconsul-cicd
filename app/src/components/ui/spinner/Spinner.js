import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './spinner.scss';

export default class Spinner extends Component {
  static propTypes = {
    transparent: PropTypes.bool,
  };

  static defaultProps = {
    transparent: false,
  };

  render() {
    return (
      <div
        className={cn('spinner', {
          transparent: this.props.transparent,
        })}
      >
        <div className="spinner__wrapper">
          <div className="spinner__animation" />
        </div>
      </div>
    );
  }
}
