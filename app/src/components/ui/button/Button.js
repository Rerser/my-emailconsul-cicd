/**
 * Пример:
    <Button
      mode="purple"
      text="Send"
      className="classes"
      onClick={function}
      type="button"
    />
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Icon from 'react-icons-kit';
import { loop2 } from 'react-icons-kit/icomoon/loop2'

import './button.scss';

export default class Button extends Component {
  static propTypes = {
    className: PropTypes.string,
    mode: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    hasRefreshIcon: PropTypes.bool,
    isRotateAnimation: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    mode: 'light',
    disabled: false,
    type: 'button',
    hasRefreshIcon: false,
    isRotateAnimation: false,
  };

  render() {
    const {
      className,
      mode,
      disabled,
      onClick,
      type,
      hasRefreshIcon,
      isRotateAnimation,
    } = this.props;

    return (
      <button
        className={cn('button', className, {
          'button--blue': mode === 'blue',
          'button--light': mode === 'light',
          'button--purple': mode === 'purple',
          'button--red': mode === 'red',
          disabled: !!disabled,
          'button--with-icon': hasRefreshIcon,
        })}
        onClick={onClick}
        type={type}
      >
        {
          hasRefreshIcon && (
            <span className={"button__icon-wrap"}>
              <Icon 
                size="100%"
                className={cn("button__icon", {
                  'button__icon--rotate-animation': isRotateAnimation
                })}
                icon={loop2}
              />
            </span>
          )
        }
        {this.props.text}
      </button>
    );
  }
}
