import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Portal from 'components/ui/portal';
import Button from 'components/ui/button';

import './confirmation-popup.scss';

const ConfirmationPopup = ({ title, onSumbit, onClose }) => {
  const t = useFormatMessage();

  return (
    <Portal>
      <div className="confirmation-popup">
        <div className="confirmation-popup__text">
          {title || t('confirmation_popup.title.text')}
          <br />
          <br />
          {t('confirmation_popup.title.select')}
        </div>

        <div className="confirmation-popup__footer">
          <Button
            mode="purple"
            text={t('confirmation_popup.title.btn.confirm')}
            className="classes"
            onClick={onSumbit}
            type="button"
          />
          <Button
            mode="light"
            text={t('confirmation_popup.title.btn.close')}
            className="classes"
            onClick={onClose}
            type="button"
          />
        </div>
      </div>
    </Portal>
  );
};

ConfirmationPopup.defaultProps = {
  title: '',
  onSumbit: () => {},
  onClose: () => {},
};
ConfirmationPopup.propTypes = {
  title: PropTypes.string,
  onSumbit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default React.memo(ConfirmationPopup);
