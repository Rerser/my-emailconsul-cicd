import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './c-input-range.scss';

const InputRange = ({ className, min, max, step, value, onChange }) => (
  <div className={cn('c-input-range', className)}>
    <input
      className="c-input-range__input"
      type="range"
      min={min}
      max={max}
      step={step}
      value={value}
      onChange={onChange}
    />
  </div>
);

InputRange.defaultProps = {
  className: '',
  min: 0,
  max: 100,
  step: 1,
  value: null,
};
InputRange.propTypes = {
  className: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  value: PropTypes.number,
  onChange: PropTypes.func.isRequired,
};

export default React.memo(InputRange);
