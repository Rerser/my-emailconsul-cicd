import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './c-popup.css';

export default class Popup extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    wrapperStyle: PropTypes.string,
  };
  static defaultProps = {
    isOpen: false,
    onClose: () => {},
    wrapperStyle: ''
  };

  render() {
    const {onClose, wrapperStyle, children} = this.props;

    return (
      <div className={cn(`c-popup ${this.props.isOpen ? 'showed' : 'hidden'}`, wrapperStyle)}>
        <div className="c-popup__background" onClick={onClose}>
          &nbsp;
        </div>
        <div className="c-popup__content" id="popup-content">
          {children}
        </div>
      </div>
    );
  }
}
