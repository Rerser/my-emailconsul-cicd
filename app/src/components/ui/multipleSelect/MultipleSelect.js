import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import SelectOptionsPortal from './components/selectOptionsPortal';

import { ENTER_KEY_CODE } from 'utils/constants';

import './multiple-select.scss';

function filteringOptions(options, values) {
  if (values.length) {
    const valuesValue = values.map((val) => val?.value ?? 0);
    const newOptions = [];

    options.forEach((option) => {
      if (!valuesValue.includes(option.value)) {
        newOptions.push(option);
      }
    });
    return newOptions;
  }
  return options;
}

function MultipleSelect(props) {
  const [options, setOptions] = useState(props.options || []);
  const [showOptions, setShowOptions] = useState(false);
  const [selectParams, setSelectParams] = useState(null);
  const [filterOption, setFilterOption] = useState('');

  // получить габариты селекта для отрисовки опций
  const selectContainerRef = useRef(null);

  const getSelectParams = useCallback(() => {
    if (selectContainerRef) {
      const selectContainer = selectContainerRef.current;

      if (selectContainer) {
        const { x, y, height, width } = selectContainer.getBoundingClientRect();

        setSelectParams({ x, y, height, width });
      }
    }
  }, [selectContainerRef]);
  
  useEffect(() => {
    if (showOptions) {
      if (!selectParams) {
        getSelectParams();
      }
    } else {
      setSelectParams(null);
    }
  }, [getSelectParams, selectParams, showOptions]);

  // фильтрация опций
  const values = useMemo(
    () => props.values
      .map((value) => props.options.find((opt) => opt.label === value))
      .filter((value) => value),
    [props.values, props.options]
  );

  useEffect(() => {
    if (props.options.length) {
      if (values.length) {
        setOptions(filteringOptions(props.options, values));
      } else {
        setOptions(props.options);
      }
    } else {
      setOptions([]);
    }
  }, [props.options, values]);

  // обработчик клика по обертке селекта для вызова опций
  const handleWrapperClick = () => {
    if (!props.disabled) {
      setShowOptions(!showOptions);
    }
  };

  // обработчик клика по селекту или выбранным значениям для вызова опций
  const handleClick = (e) => {
    e.preventDefault();
    if (!props.disabled) {
      setShowOptions(!showOptions);
    }
  };

  // обработчик удаление одного значения
  const handleDeleteValue = (e, valueId) => {
    e.stopPropagation();
    if (props.disabled) return;

    const newValues = values?.filter((item) => {
      if (item) {
        return String(item.value) !== String(valueId);
      }
      return false;
    });

    if (props.onChange) {
      props.onChange(newValues ?? []);
    }
    getSelectParams();
  };

  // обработчик удаление всех значений
  const handleDeleteAllValues = (e) => {
    e.stopPropagation();
    if (props.disabled) return;

    if (props.onChange) {
      props.onChange([]);
    }
    getSelectParams();
  };

  // обработчик набора фильтра опций
  const handleFilterOptions = (e) => {
    setOptions(
      e.target.value
        ? props.options.filter(opt => String(opt.label).toLowerCase().includes(e.target.value))
        : props.options.map(opt => ({ ...opt }))
    );
    setFilterOption(e.target.value);
    setShowOptions(true);
  }

  // обработчик клика (выбора) опций
  const handleOptionClick = (value) => {
    const foundValue = values.find((val) => val?.value === value);
    const foundOptionIndex = options.findIndex((opt) => opt.value === value);

    if (!foundValue && foundOptionIndex > -1 && props.onChange) {
      props.onChange([
        ...(values ?? []),
        {
          ...options[foundOptionIndex],
        },
      ]);
    }
    setShowOptions(false);
    setFilterOption('');
  };

  // обработчик нажатия клавиши Enter для выбора опций
  const handleKeyDown = (e) => {
    if (e.keyCode === ENTER_KEY_CODE) {
      if (options.length === 1 && options.find(option => option.label === filterOption)) {
        handleOptionClick(filterOption);
      }
    }
  }

  // флаг максимально выбранных значений
  const isMaxValues = (!!props.maxItems && values.length >= props.maxItems) || values.length >= props.options.length;
  // флаг показа опций
  const isSelectOptionsShow = showOptions && selectParams && !props.disabled && !!props.options.length && !isMaxValues;

  return (
    <div
      className={cn('c-multiple-select', props.className, {
        'c-multiple-select--fix-height': !values.length,
        'c-multiple-select--disabled': props.disabled,
      })}
      onClick={handleWrapperClick}
      onMouseLeave={() => setShowOptions(false)}
      ref={selectContainerRef}
    >
      <label className="c-multiple-select__label">
        <div className="c-multiple-select__values">
          <div
            onClick={handleClick}
            className="c-multiple-select__select-container"
          >
            {values?.map(item => (
              <div
                className="c-multiple-select__value"
                key={`c-multiple-select-value-${item?.value || Math.random()}`}
              >
                <div className="c-multiple-select__text" onClick={handleClick}>
                  {item.label}
                </div>
                <div
                  className="c-multiple-select__btn c-multiple-select__btn--clear-one"
                  onClick={(e) => handleDeleteValue(e, item.value)}
                >
                  {'X'}
                </div>
              </div>
            ))}
            <input
              type="text"
              placeholder={props.placeholder}
              className={cn("c-multiple-select__filter-options", {
                'c-multiple-select__filter-options--hide': isMaxValues
              })}
              required={props.isRequired}
              disabled={props.disabled || false}
              onChange={handleFilterOptions}
              onKeyDown={handleKeyDown}
              value={filterOption}
            />
          </div>
        </div>
        {!!values.length && (
          <div
            className="c-multiple-select__btn c-multiple-select__btn--clear-all"
            onClick={(e) => handleDeleteAllValues(e)}
          >
            {'x'}
          </div>
        )}
        {!values.length && (
          <button className="c-multiple-select__btn c-multiple-select__btn--open" type="button">
            {'>'}
          </button>
        )}
      </label>
      {isSelectOptionsShow && (
        <SelectOptionsPortal
          coords={{ x: selectParams?.x ?? 0, y: (selectParams?.y ?? 0) + (selectParams?.height ?? 0) }}
          isShow={showOptions}
          width={selectParams?.width ?? 50}
        >
          <ul className="c-multiple-select__options">
            {options.map(item => (
              <li
                key={`option_item_${item.value}`}
                className={cn("c-multiple-select__option", {
                  "c-multiple-select__option--disabled": props.optionsDisabled.includes(item.value),
                })}
                onMouseDown={() => handleOptionClick(String(item.value))}
              >
                {item.label}
              </li>
            ))}
          </ul>
        </SelectOptionsPortal>
      )}
    </div>
  );
}

MultipleSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.string,
    })
  ),
  values: PropTypes.array,
  optionPlaceholderDisabled: PropTypes.bool,
  disabledValue: PropTypes.string,
  disabled: PropTypes.bool,
  optionsDisabled: PropTypes.arrayOf(PropTypes.string),

  placeholder: PropTypes.string,

  maxItems: PropTypes.number,

  className: PropTypes.string,
};

MultipleSelect.defaultProps = {
  options: [],
  values: [],
  optionPlaceholderDisabled: false,
  disabled: false,
  optionsDisabled: [],
  placeholder: '',
  className: '',
  maxItems: 0,
};

export default React.memo(MultipleSelect);
