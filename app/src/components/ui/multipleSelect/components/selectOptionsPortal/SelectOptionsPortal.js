import { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

const SelectOptionsPortal = ({ isShow, coords, children, width }) => {
  const element = useRef(document.createElement('div'));

  useEffect(() => {
    const id = `selectOptionsPortal-${Math.random()}`;
    const div = element.current;

    div.id = id;
    div.style.position = 'absolute';
    div.style.top = `${coords.y}px`;
    div.style.left = `${coords.x}px`;
    div.style.width = `${width}px`;
    div.style.zIndex = '10000'; // 9999 = modalWindow
    document.body.insertAdjacentElement('beforeend', div);

    return () => {
      const parent = div.parentNode;

      if (parent) {
        parent.removeChild(div);
      }
    };
  }, [coords, width]);

  return isShow && element ? ReactDOM.createPortal(children, element.current) : null;
};

export default SelectOptionsPortal;
