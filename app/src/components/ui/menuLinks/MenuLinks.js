import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-icons-kit';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import ForRole from 'components/forRole';

import './menu-links.scss';

class MenuLinks extends PureComponent {
  static propTypes = {
    icon: PropTypes.object,
    menuItemList: PropTypes.arrayOf(
      PropTypes.exact({
        to: PropTypes.string,
        text: PropTypes.string,
        func: PropTypes.func,
        roles: PropTypes.arrayOf(PropTypes.string),
        icon: PropTypes.object,
      })
    ),
    vertical: PropTypes.bool,
  };

  static defaultProps = {
    icon: {},
    menuItemList: [
      {
        to: '',
        text: '',
        func: () => {},
        roles: [''],
        icon: {},
      },
    ],
    vertical: false,
  };

  render() {
    const itemMenuLayout = (func, to, text, icon) =>
      func ? (
        <div className="menu-links__link" onClick={func}>
          <span className="menu-links__text">
            {text}
          </span>
          {icon && (
            <div className="menu-links__icon">
              <Icon icon={icon} size="100%" />
            </div>
          )}
        </div>
      ) : (
        <Link className="menu-links__link" to={to}>
          <span className="menu-links__text">
            {text}
          </span>
          {icon && (
            <div className="menu-links__icon">
              <Icon icon={icon} size="100%" />
            </div>
          )}
        </Link>
      );

    return (
      <div
        className={cn('menu-links', {
          vertical: this.props.vertical,
        })}
      >
        <ul className="menu-links__list">
          {this.props.menuItemList.map(({ func, to, text, roles, icon }) =>
            roles ? (
              <ForRole roles={roles} key={Math.random()}>
                <li className="menu-links__item">
                  {itemMenuLayout(func, to, text, icon)}
                </li>
              </ForRole>
            ) : (
              <li className="menu-links__item" key={Math.random()}>
                {itemMenuLayout(func, to, text, icon)}
              </li>
            )
          )}
        </ul>
      </div>
    );
  }
}

export default MenuLinks;
