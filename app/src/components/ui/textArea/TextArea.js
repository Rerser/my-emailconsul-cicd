import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './c-textarea.scss';

const TextArea = ({
  value,
  onChange,
  className,
  errorText,
  disabled,
  placeholder,
  required,
  readOnly,
  autoHeight
}) => {
  const textareaRef = useRef(null);

  useEffect(() => {
    if (autoHeight && textareaRef?.current) {
      const textarea = textareaRef.current;
      textarea.style.height = textarea.scrollHeight + 'px';
    }
  }, [textareaRef, autoHeight]);

  const handleChange = (text) => {
    if (onChange && !readOnly) {
      onChange(text);
    }
  }

  return (
    <textarea
      ref={textareaRef}
      placeholder={placeholder}
      className={cn('c-textarea', className, {
        'c-textarea--errortext': errorText,
        'c-textarea--required': required,
        'c-textarea--readonly': readOnly,
        'c-textarea--autoheight': autoHeight,
        'c-textarea--disabled': disabled,
      })}
      value={value}
      onChange={e => handleChange(e.target.value)}
      disabled={disabled}
      readOnly={readOnly}
    />
  );
};

TextArea.defaultProps = {
  value: '',
  onChange: () => {},
  className: '',
  errorText: '',
  disabled: false,
  placeholder: '',
  required: false,
  readOnly: false,
  autoHeight: false,
};
TextArea.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
  errorText: PropTypes.string,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  readOnly: PropTypes.bool,
  autoHeight: PropTypes.bool,
};

export default React.memo(TextArea);
