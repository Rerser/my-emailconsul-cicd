import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

import './error404.scss';

const Error404 = ({ compact, withHomeButton }) => {
  const t = useFormatMessage();
  const history = useHistory();

  return (
    <div className="error404">
      <div className={cn('error404__container', { compact })}>
        <div className="error404__status-error">
          {NOT_FOUND_ERROR_STATUS}
        </div>
        <div className="error404__description">
          {t('error404.description.text')}
        </div>
        {withHomeButton && (
          <button className="error404__btn" type="button" onClick={() => history.push('/')}>
            {t('error404.button.home.caption')}
          </button>
        )}
      </div>
    </div>
  );
};

Error404.defaultProps = {
  compact: false,
};
Error404.propTypes = {
  compact: PropTypes.bool,
};

export default React.memo(Error404);
