/*
  Example:
  <Table
    columns={[
      {
        field: 'id',
        title: 'id',
        type: 'number',
        width: 100
      },
      {
        field: 'brand_cat_type_name',
        title: 'name',
        type: 'string',
        width: 200
      },
    ]}
    data={[
      {
        cbsPayload: {
          isUpdating: true,
          isUpdateDisable: true
        },
        id: 1,
        ...other fields
      }
    ]}
    cbs={{
      onDelete: (item) => console.log('onDelete', item),
      onEdit: (item) => console.log('onEdit',item)
      onSend: (item) => console.log('onSend',item)
      onUpdate: (item) => console.log('onUpdate',item)
      onInfo: (item) => console.log('onInfo',item)
      onTerminate: (item) => console.log('onTerminate',item)
    }}
    headerCbs={{
      onDelete: (item) => console.log('onDelete', item),
      onEdit: (item) => console.log('onEdit',item)
      onSend: (item) => console.log('onSend',item)
      onUpdate: (item) => console.log('onUpdate',item)
      onInfo: (item) => console.log('onInfo',item)
      onTerminate: (item) => console.log('onTerminate',item)
    }}
    btns={[
      {text: 'Add new', onClick: () => console.log('add new')}
    ]}
    sorting={{
      field: 'id',
      order: 'asc',
      sortFields: ['id', 'name'],
      isApiSorting: false
    }}
    onSorting: ({ order, field }) => true
    titlesForConfirmationPopup={{
      onDelete: 'header1',
      onTerminate: 'header2',
      ...
    }}
  />
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cn from 'classnames';

import Button from 'components/ui/button';
import Icon from 'components/ui/icon';
import TableStatistic from './table_statistic';

import { setConfirmationPopupTitle, setCallbackConfirmationPopup } from 'store/actions/confirmationPopup';
import { localeCurrentSelector } from 'store/selectors';

import messages from 'utils/messages';
import { convertDateTime, deepCompare } from 'utils/helpers';
import { COLUMN_TABLE_MIN_WIDTH_PORTION } from 'utils/constants';

import ChangeTableOrder from './change_table_order/ChangeTableOrder';

import './c-table.scss';

class Table extends React.PureComponent {
  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.object),
    data: PropTypes.arrayOf(PropTypes.object),
    cbs: PropTypes.objectOf(PropTypes.func),
    headerCbs: PropTypes.objectOf(PropTypes.func),
    btns: PropTypes.arrayOf(PropTypes.object),
    sorting: PropTypes.object,
    onSorting: PropTypes.func,
    tableOf: PropTypes.string,
    onRowClick: PropTypes.func,
    resizabled: PropTypes.bool,
    breakword: PropTypes.bool,
    titlesForConfirmationPopup: PropTypes.objectOf(PropTypes.string),
    verticalPositionContentCell: PropTypes.string,
    bluredColumns: PropTypes.arrayOf(PropTypes.string),
    horizontalPositionContentCell: PropTypes.string,
    cbsColumnWidth: PropTypes.number,
    statisticData: PropTypes.shape({
      page: PropTypes.number,
      pages: PropTypes.number,
      pageSize: PropTypes.number,
    }),
  };

  static defaultProps = {
    columns: [],
    data: [],
    cbs: {},
    headerCbs: {},
    btns: [],
    sorting: null,
    tableOf: null,
    resizabled: false,
    breakword: false,
    horizontalPositionContentCell: 'center',
    titlesForConfirmationPopup: {},
    verticalPositionContentCell: '',
    bluredColumns: [],
    cbsColumnWidth: 150,
  };

  getField = ({ column, item }) => {
    if (column.type === 'boolean') {
      return item[column.field] ? 'Yes' : 'No';
    }
    if (column.type === 'datetime') {
      return convertDateTime(item[column.field], "HH:mm:ss DD-MM-YYYY");
    }
    return item[column.field];
  };

  constructor(props) {
    super(props);
    this.state = {
      isOnTableOrderConfig: false,
      columns: null,
      isResize: false,
      currentColumn: null,
      cbsNames: [],
      headerCbsNames: [],
    };
  }

  onCancel = () => {
    localStorage.removeItem(this.props.tableOf);
    this.setState({
      columns: [
        ...this.props.columns,
        {
          title: '',
          cbs: this.props.cbs,
          type: 'cbs',
          width: this.props.cbsColumnWidth ?? 150,
        },
      ],
      isOnTableOrderConfig: false,
    });
  };

  onSubmit = (newColumns) => {
    localStorage.setItem(this.props.tableOf, JSON.stringify(newColumns));
    this.setState({
      columns: newColumns,
      isOnTableOrderConfig: false,
    });
  };

  onClose = () => this.setState({ isOnTableOrderConfig: false });

  handleHeaderClick = (column) => {
    const { sorting, onSorting } = this.props;

    if (sorting && sorting.sortFields.includes(column.field) && onSorting) {
      const newSorting = { ...sorting };

      newSorting.order =
        sorting.field === column.field
          ? sorting.order === 'asc'
            ? 'desc'
            : 'asc'
          : 'asc';
      newSorting.field = column.field;
      onSorting(newSorting);
    }
  };

  handleButtonClick = (e) => {
    e.preventDefault();
    this.setState({
      isOnTableOrderConfig: true,
    });
  };

  handleRowClick = (item) => {
    const { onRowClick } = this.props;

    if (onRowClick && !window.getSelection().toString()) {
      onRowClick(item);
    }
  };

  handleRowCallback = (cbName, item, e) => {
    e.stopPropagation();
    const { cbs, titlesForConfirmationPopup } = this.props;
    // titlesForConfirmationPopup = объект с заголовками для cbs в ConfirmationPopup

    const runCB = () => {
      cbs[cbName](item);
    };

    switch (cbName) {
      // если нажаты
      case 'onDelete': // удалить
      case 'onTerminate': // остановить
        // то показать ConfirmationPopup
        this.props.setConfirmationPopupTitle(
          titlesForConfirmationPopup ? titlesForConfirmationPopup[cbName] : ''
        );
        this.props.setCallbackConfirmationPopup(runCB);
        break;
      // иначе запустить колбек
      default:
        runCB();
        break;
    }
  };

  handleHeaderCallback = (cbName, e) => {
    e.stopPropagation();

    const { headerCbs } = this.props;

    const runCB = () => {
      headerCbs[cbName]();
    };

    switch (cbName) {
      // если нажаты
      case 'onDelete': // удалить
      case 'onTerminate': // остановить
        // то показать ConfirmationPopup
        this.props.setConfirmationPopupTitle('');
        this.props.setCallbackConfirmationPopup(runCB);
        break;
      // иначе запустить колбек
      default:
        runCB();
        break;
    }
  };

  handleResizeColumnMouseMove = (e) => {
    e.stopPropagation();
    if (this.state.isResize) {
      const { pageX } = e; // координата Х мыши относительно левой границы документа
      const currentLeft = this.state.currentColumn.offsetLeft;
      const currentWidth = this.state.currentColumn.width;
      const currentScrollLeft = this.state.currentColumn.scrollLeft;
      const changedWidth = pageX - currentLeft + currentScrollLeft;

      // если изменяемая ширина не меньше [COLUMN_TABLE_MIN_WIDTH_PORTION] от ширины ячейки
      if (
        changedWidth >=
        Math.floor(currentWidth * COLUMN_TABLE_MIN_WIDTH_PORTION)
      ) {
        const currentColumns = this.state.columns.map((item, index) =>
          index === this.state.currentColumn.index
            ? {
                ...item,
                width: changedWidth,
              }
            : item
        );

        this.setState({
          columns: currentColumns,
        });
      }
    }
  };

  handleResizeColumnMouseDown = (index, e) => {
    e.stopPropagation();
    if (!this.state.isResize) {
      const table = e.target.closest('.c-table__content'); // таблица
      const currentCell = e.target.closest('.c-table__cell'); // текушая ячейка
      const width = Math.floor(
        parseInt(window.getComputedStyle(currentCell).width)
      ); // ширина текущей ячейки
      const offsetLeft = Math.floor(currentCell.offsetLeft); // расстояние от левой границы документа до текущей ячейки
      const scrollLeft = Math.floor(table.scrollLeft); // количество скролла таблицы по горизонтали

      this.setState({
        isResize: true,
        currentColumn: {
          index,
          width,
          offsetLeft,
          scrollLeft,
        },
      });
    }
  };

  handleResizeColumnMouseUp = (e) => {
    e.stopPropagation();
    if (this.state.isResize) {
      this.setState({
        isResize: false,
        currentColumn: null,
      });
    }
  };

  static getDerivedStateFromProps(props, state) {
    // если columns содержит cbs
    let filteredColumnsState = state?.columns;
    const isIncludeCbs = state?.columns?.some(
      (column) => column.type === 'cbs'
    );
    if (isIncludeCbs) {
      // отсечь cbs
      filteredColumnsState = state?.columns?.slice(
        0,
        state?.columns?.length - 1
      );
    }

    if (!localStorage.getItem(props.tableOf)) {
      // если в state еще нет columns (первая загрузка)
      if (!state.columns) {
        // записать colunms в state из props
        // если передаются cbs
        if (Object.keys(props.cbs).length > 0) {
          // дописать в state.columns cbs из props
          return {
            ...state,
            columns: [
              ...props.columns,
              {
                title: 'Act',
                cbs: props.cbs,
                type: 'cbs',
                width: props.cbsColumnWidth ?? 150,
              },
            ],
            cbsNames: Object.keys(props?.cbs) || [],
            headerCbsNames: Object.keys(props?.headerCbs) || [],
          };
        }
        return {
          ...state,
          columns: [...props.columns],
        };
        // если данные из props.columns === данным state.columns без cbs
      }
      if (deepCompare({ arr: filteredColumnsState }, { arr: props.columns })) {
        // (filteredColumnsState === props.columns) {
        // не обнолять state
        return null;
        // если данные в state.columns и props.columns отличаются и в компонент не передаем cbs
      }
      if (Object.keys(props.cbs).length === 0) {
        // записать новые columns
        return {
          ...state,
          columns: [...props.columns],
        };
      }
      return null;
      // return {
      //   columns: state.columns,
      // };
    }
    return {
      ...state,
      columns: JSON.parse(localStorage.getItem(props.tableOf)),
      cbsNames: Object.keys(props?.cbs) || [],
      headerCbsNames: Object.keys(props?.headerCbs) || [],
    };
  }

  getCbTooltip = (cb) => {
    switch (cb) {
      case 'onDelete':
        return messages[this.props.locale]['table.tooltip.delete.text'];
      case 'onEdit':
        return messages[this.props.locale]['table.tooltip.edit.text'];
      case 'onSend':
        return messages[this.props.locale]['table.tooltip.send.text'];
      case 'onUpdate':
        return messages[this.props.locale]['table.tooltip.update.text'];
      case 'onInfo':
        return messages[this.props.locale]['table.tooltip.info.text'];
      case 'onTerminate':
        return messages[this.props.locale]['table.tooltip.terminate.text'];
      case 'onSwitch':
        return messages[this.props.locale]['table.tooltip.switch.text'];
      case 'onLogs':
        return messages[this.props.locale]['table.tooltip.logs.text'];
      default:
        return '';
    }
  };

  render() {
    const { data, sorting, onRowClick } = this.props;

    if (sorting && data && !sorting.isApiSorting) {
      data.sort((a, b) => {
        const aField = a[sorting.field];
        const bField = b[sorting.field];
        if (aField > bField) return sorting.order === 'asc' ? 1 : -1;
        if (aField < bField) return sorting.order === 'asc' ? -1 : 1;
        return 0;
      });
    }

    return (
      <div className="c-table">
        <div className="c-table__nav">
          <div className="c-table__btns">
            {this.props.btns.map((btn, index) => (
              <Button
                key={`btn-${index}`}
                mode="dark"
                text={btn.text}
                blockClasses="c-table__btn"
                onClick={btn.onClick}
              />
            ))}
          </div>
          {this.props.tableOf && (
            <div className="c-table__btns">
              {!!this.props.data.length && (
                <Button mode="white" text="Table order" blockClasses="c-table__btn" onClick={this.handleButtonClick} />
              )}
            </div>
          )}
        </div>
        
        <div
          className={cn('c-table__content', {
            isResize: this.state.isResize,
          })}
          onMouseMove={this.handleResizeColumnMouseMove}
          onMouseUp={this.handleResizeColumnMouseUp}
        >
          <ul className="c-table__header">
            {this.state.columns.map((column, index) => (
              <li
                key={`header-cell-${index}`}
                className={cn(
                  'c-table__cell',
                  'c-table__cell--header',
                  `c-table__cell--${this.props.horizontalPositionContentCell}`,
                  sorting?.sortFields.includes(column.field) && 'c-table__header-sort',
                  column.type === 'cbs' && cn('c-table__cell', 'c-table__cell--cbs'),
                )}
                style={{
                  width: column.width,
                  minWidth: column.width,
                }}
                onClick={this.handleHeaderClick.bind(null, column)}
              >
                {column.type !== 'cbs'
                  ? column.title
                  : this.state.headerCbsNames.map((cb) => (
                    <div
                        key={Math.random()}
                        className={cn(
                          'c-table__cb',
                          `c-table__cb--${cb}`,
                          {
                            'c-table__cb--disabled': !this.props.data.length,
                          },
                        )}
                        onClick={this.handleHeaderCallback.bind(this, cb)}
                      >
                      <Icon type={cb} tooltip={this.getCbTooltip(cb)}/>
                    </div>
                    ))}
                {sorting && sorting.field === column.field && (
                  <div className={cn('c-table__sort-arow', sorting.order)}>
                    <Icon type="onSort" tooltip={this.getCbTooltip('onSort')}/>
                  </div>
                )}
                {this.props.resizabled && (
                  <span
                    className="c-table__cell-width-control"
                    onMouseDown={this.handleResizeColumnMouseDown.bind(
                      null,
                      index
                    )}
                  />
                )}
              </li>
            ))}
          </ul>

          {!!this.props.data.length && (
            <ul className="c-table__body">
              {data.map((item, indexData) => (
                <li
                  key={`body_row-${indexData}`}
                  className={cn('c-table__row', {
                    'clickable-row': !!onRowClick,
                  })}
                  onClick={this.handleRowClick.bind(null, item)}
                >
                  {this.state.columns.map((column) => (
                    <div
                      key={`body-cell-${column.title}`}
                      className={cn(
                        'c-table__cell',
                        `c-table__cell--top-${this.props.verticalPositionContentCell}`,
                        `c-table__cell--${this.props.horizontalPositionContentCell}`,
                        {
                          'c-table__cell--cbs': column.type === 'cbs',
                          'c-table__cell--breakword': this.props.breakword,
                        },
                      )}
                      style={{
                        width: column.width,
                        minWidth: column.width,
                      }}
                    >
                      {column.type !== 'cbs'
                        ? (
                          <span className={cn('c-table__cell-content', {
                            'c-table__cell-content--blured': this.props.bluredColumns.includes(column.field)
                          })}>
                            {this.getField({ column, item })}
                          </span>
                        ) : this.state.cbsNames.map((cb, index) => (
                          <div
                              key={`row-cbs-${index}`}
                              className={cn(
                                'c-table__cb',
                                `c-table__cb--${cb}`,
                                {
                                  updating: cb === 'onUpdate' && item?.cbsPayload?.isUpdating,
                                  isDeleteDisable: cb === 'onDelete' && item?.cbsPayload?.isDeleteDisable,
                                  isEditDisable: cb === 'onEdit' && item?.cbsPayload?.isEditDisable,
                                  isSendDisable: cb === 'onSend' && item?.cbsPayload?.isSendDisable,
                                  isUpdateDisable: cb === 'onUpdate' && item?.cbsPayload?.isUpdateDisable,
                                  isInfoDisable: cb === 'onInfo' && item?.cbsPayload?.isInfoDisable,
                                  isTerminateDisable: cb === 'onTerminate' && item?.cbsPayload?.isTerminateDisable,
                                  isSwitchDisable: cb === 'onSwitch' && item?.cbsPayload?.isSwitchDisable,
                                  isLogsDisable: cb === 'onLogs' && item?.cbsPayload?.isLogsDisable,
                                }
                              )}
                              onClick={this.handleRowCallback.bind(
                                null,
                                cb,
                                item
                              )}
                            >
                            <Icon type={cb} tooltip={this.getCbTooltip(cb)}/>
                          </div>
                          ))}
                    </div>
                  ))}
                </li>
              ))}
            </ul>
          )}
        </div>

        {!this.props.data.length && (
          <div>
            {messages[this.props.locale]['table.no.data.text']}
          </div>
        )}
        {this.props.tableOf && (
          <ChangeTableOrder
            isOnTableOrderConfig={this.state.isOnTableOrderConfig}
            tableOf={this.props.tableOf}
            columns={this.state.columns}
            onClose={this.onClose}
            onCancel={this.onCancel}
            onSubmit={this.onSubmit}
          />
        )}
        {!!this.props.data.length && this.props.statisticData && (
          <TableStatistic
            statistic={this.props.statisticData}
          /> 
        )}
      </div>
    );
  }
  // getSnapshotBeforeUpdate(prevProps, prevState) {}
  // componentDidMount() {};
  // componentDidUpdate(prevProps, prevState) {};
  // componentWillUnmount() {};
  // componentDidCatch(error, info) {};
  // static getDerivedStateFromError(error) {}
}

export default connect(
  (state) => ({
    locale: localeCurrentSelector(state),
  }),
  (dispatch, ownProps) => ({
    setConfirmationPopupTitle: (title) => dispatch(setConfirmationPopupTitle(title)),
    setCallbackConfirmationPopup: (cb) => dispatch(setCallbackConfirmationPopup(cb)),
  })
)(Table);
