import React, { useState, useEffect } from 'react';
import arrayMove from 'array-move';
import SlideBar from 'components/ui/slideBar';
import SortableList from './sortable_list/SortableList';
import Button from '../../button/Button';
import './change-table-order.css';

export default function ChangeTableOrder(props) {
  const { isOnTableOrderConfig } = props;
  const [columns, setColumns] = useState(props.columns);

  const onSortEnd = ({ oldIndex, newIndex }) => {
    setColumns(arrayMove(columns, oldIndex, newIndex));
  };

  const submit = () => {
    props.onSubmit(columns);
  };

  useEffect(() => {
    setColumns(props.columns);
  }, [props.columns]);

  return (
    <SlideBar isOpen={isOnTableOrderConfig} onClose={props.onClose}>
      <div className="c-slidebar-content">
        <h3 className="c-slidebar-content__title">Изменить порядок</h3>
        <div className="c-slidebar-content--left c-slidebar-content--bigger-interval">
          <SortableList items={columns} onSortEnd={onSortEnd} useDragHandle />
        </div>

        <div className="c-slidebar-content__btns">
          <Button
            key="btn-table-order-confirm"
            mode="dark"
            text="Применить"
            blockClasses="c-slidebar-content__btn"
            onClick={submit}
          />
          <Button
            key="btn-trackers"
            mode="white"
            text="Сброс"
            blockClasses="c-slidebar-content__btn"
            onClick={props.onCancel}
          />
        </div>
      </div>
    </SlideBar>
  );
}
