import React from 'react';
import { SortableContainer } from 'react-sortable-hoc';
import SortableItem from '../helpers/SortableItem';

export const SortableList = React.memo(
  SortableContainer((items) => (
    <ul className="sortable-list">
      {items.items.map((column, index) => (
        <SortableItem
          key={`column-${column.field}`}
          index={index}
          value={column.title}
        />
      ))}
    </ul>
  ))
);

export default SortableList;
