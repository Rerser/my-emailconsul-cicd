import React from 'react';
import { SortableHandle } from 'react-sortable-hoc';

export const DragHandle = React.memo(
  SortableHandle(() => <span className="drag-handle" />)
);

export default DragHandle;
