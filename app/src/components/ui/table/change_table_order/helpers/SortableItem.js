import React from 'react';
import { SortableElement } from 'react-sortable-hoc';
import DragHandle from './DragHandle';

export const SortableItem = React.memo(
  SortableElement(({ value }) => (
    <li className="sortable-item">
      <DragHandle />
      {value}
    </li>
  ))
);

export default SortableItem;
