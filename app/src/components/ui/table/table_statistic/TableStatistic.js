import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';

import './table-statistic.scss';

const TableStatistic = ({ statistic }) => {
  const t = useFormatMessage();

  const { page = 0, pages = 0, pageSize = 0 } = statistic;

  if (!pageSize) return null;

  const fromCount = page * pageSize + 1;
  const allCount = Math.ceil(pages * pageSize);
  const toCount = Math.min((page + 1) * pageSize, allCount);

  return (
    <div className="table-statistic">
      <span>
        {t('table-statistic.range.from.text')}
        {' '}
        {fromCount}
      </span>
      
      <span>
        {' '}
        {t('table-statistic.range.to.text')}        
        {' '}
        {toCount}
      </span>
      
      <span>
        {' '}
        {t('table-statistic.of.text')}        
        {' '}
        {allCount}
        {' '}
        {t('table-statistic.entries.text')}
      </span>
    </div>
  );
};

TableStatistic.propTypes = {
  statistic: PropTypes.shape({
    page: PropTypes.number,
    pages: PropTypes.number,
    pageSize: PropTypes.number,
  }),
};

TableStatistic.defaultProps = {
  statistic: {
    page: 0,
    pages: 0,
    pageSize: 0,
  },
};

export default React.memo(TableStatistic);
