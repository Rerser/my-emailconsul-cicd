/**
  ** Example

  <Tooltip
    className="my-class"
  />
*/

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Portal from 'components/ui/portal';
import { getIsMobile } from 'utils/helpers';

import './tooltip.scss';

const Tooltip = ({ className }) => {
  const [text, setText] = useState(null);
  const [chords, setChords] = useState(null);

  useEffect(() => {
    const { body } = document;

    body.addEventListener('mousemove', (e) => {
      const isMobile = getIsMobile();

      if (isMobile) return;

      const tooltipElem = e.target.closest('*[data-tooltip]');

      if (tooltipElem) {
        const tooltipText = tooltipElem.dataset.tooltip;

        const cbr = tooltipElem.getBoundingClientRect();
        let top = cbr.top + window.pageYOffset;
        let left = cbr.left + window.pageXOffset;

        top = top + tooltipElem.offsetHeight + 10;
        left += tooltipElem.offsetWidth / 2;

        if (!tooltipText) return;
        
        setText(tooltipText);
        setChords(JSON.stringify({ top, left }));
      } else {
        setText(null);
        setChords(null);
      }
    });
  }, []);

  const parsedChords = JSON.parse(chords);

  return (
    <Portal pure>
      <div
        className={cn('tooltip', className)}
        style={
          parsedChords
            ? {
                top: parsedChords.top,
                left: parsedChords.left,
              }
            : {
                top: -1000,
                left: -1000,
              }
        }
      >
        {text}
      </div>
    </Portal>
  );
};

Tooltip.defaultProps = {
  className: '',
};
Tooltip.propTypes = {
  className: PropTypes.string,
};

export default React.memo(Tooltip);
