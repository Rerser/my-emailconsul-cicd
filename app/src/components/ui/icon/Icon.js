import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './icon.scss';

function Icon({ type, customStyle, tooltip, onClick }) {
  return (
    <div className={cn("icon", customStyle)} data-tooltip={tooltip} onClick={onClick}>
      <div className={cn('icon__img', `icon__img--${type}`)} />
    </div>
  );
}

Icon.defaultProps = {
  type: 'blank',
  customStyle: '',
  tooltip: '',
  onClick: () => {},
};
Icon.propTypes = {
  type: PropTypes.string.isRequired,
  customStyle: PropTypes.string,
  tooltip: PropTypes.string,
  onClick: PropTypes.func,
};

export default React.memo(Icon);
