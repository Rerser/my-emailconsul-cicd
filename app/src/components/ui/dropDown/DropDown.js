import React, { PureComponent, createRef } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-icons-kit';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import ForRole from 'components/forRole';

import './drop-down.scss';

class DropDown extends PureComponent {
  static propTypes = {
    icon: PropTypes.object,
    menuText: PropTypes.string,
    menuItemList: PropTypes.arrayOf(
      PropTypes.exact({
        to: PropTypes.string,
        text: PropTypes.string,
        func: PropTypes.func,
        external: PropTypes.bool,
        roles: PropTypes.arrayOf(PropTypes.string),
      })
    ),
  };

  static defaultProps = {
    icon: {},
    menuText: '',
    menuItemList: [
      {
        to: '',
        text: '',
        func: () => {},
        roles: [''],
        external: false,
      },
    ],
  };

  constructor(props) {
    super(props);
    this.state = {
      menuIsOpen: false,
    };
  }

  refIcon = createRef(null);

  refText = createRef(null);

  setMenuIsOpen = (isOpen) => {
    this.setState({
      menuIsOpen: isOpen,
    });
  };

  handleDocClick = (e) => {
    const isOpenWithIcon =
      this.refIcon?.current &&
      !e?.path?.includes(this.refIcon.current);

    const isOpenWithText =
      this.refText?.current &&
      !e?.path?.includes(this.refText.current);

    if (isOpenWithIcon || isOpenWithText) {
      this.setMenuIsOpen(false);
    }
  };

  componentDidMount() {
    document.addEventListener('click', this.handleDocClick);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleDocClick);
  }

  render() {
    const itemMenuLayout = (func, to, text) =>
      func ? (
        <div className="dmenu__link" onClick={func}>
          <span>
            {text}
          </span>
        </div>
      ) : (
        <Link className="dmenu__link" to={to}>
          <span>
            {text}
          </span>
        </Link>
      );

    return (
      <div
        className="dropdown"
        onMouseLeave={() => this.setMenuIsOpen(false)}
      >
        {!!Object.keys(this.props.icon).length && (
          <div
            className="dropdown__icon"
            onMouseEnter={() => this.setMenuIsOpen(true)}
            onTouchStart={() => this.setMenuIsOpen(true)}
            ref={this.refIcon}
          >
            <Icon icon={this.props.icon} size="100%" />
          </div>
        )}

        {this.props.menuText && !Object.keys(this.props.icon).length && (
          <div
            className={cn("dropdown__text", {
              "dropdown__text--hovered": this.state.menuIsOpen
            })}
            onMouseEnter={() => this.setMenuIsOpen(true)}
            onTouchStart={() => this.setMenuIsOpen(true)}
            ref={this.refText}
          >
            {this.props.menuText}
          </div>
        )}

        <ul
          className={cn('dropdown__menu dmenu', {
            hidden: !this.state.menuIsOpen,
          })}
        >
          {this.props.menuItemList.map(({ func, to, text, roles, external }) =>
            roles ? (
              <ForRole roles={roles} key={Math.random()}>
                {external ? (
                  <li className="dmenu__item">
                    <a className="dmenu__link" href={to} target="_blank" rel="noopener noreferrer">
                      <span>
                        {text}
                      </span>
                    </a>
                  </li>
                ) : (
                  <li className="dmenu__item">
                    {itemMenuLayout(func, to, text)}
                  </li>
                )}
              </ForRole>
            ) : 
              external ? (
                <li className="dmenu__item" key={Math.random()}>
                  <a className="dmenu__link" href={to} target="_blank" rel="noopener noreferrer">
                    <span>
                      {text}
                    </span>
                  </a>
                </li>
              ) : (
                <li key={Math.random()} className="dmenu__item">
                  {itemMenuLayout(func, to, text)}
                </li>
              )
          )}
        </ul>
      </div>
    );
  }
}

export default DropDown;
