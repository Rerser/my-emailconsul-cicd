import React from 'react';
import PropTypes from 'prop-types';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import cn from 'classnames';
import { infoCircle, warning, timesCircle } from 'react-icons-kit/fa/';
import Icon from 'react-icons-kit';
import { ERROR_ICON_COLOR, INFO_ICON_COLOR, WARNING_ICON_COLOR, INFO_CONTAINER_STATUS } from 'utils/constants';

import './info-container.scss';

const InfoContainer = ({ text, status, noIcon }) => {
  const t = useFormatMessage();

  let iconColor = '';
  let icon = null;
  let title = '';

  switch (status) {
    case INFO_CONTAINER_STATUS.error:
      iconColor = ERROR_ICON_COLOR;
      icon = timesCircle;
      title = t('error.text');
      break;

    case INFO_CONTAINER_STATUS.warning:
      iconColor = WARNING_ICON_COLOR;
      icon = warning;
      title = t('warning.text');
      break;

    case INFO_CONTAINER_STATUS.info:
      iconColor = INFO_ICON_COLOR;
      icon = infoCircle;
      title = t('info.text');
      break;
  
    default:
      iconColor = INFO_ICON_COLOR;
      icon = infoCircle;
      title = t('info.text');
      break;
  }

  return (
    <div className="info-container">
      <div className={cn("info-container__wrapper", {
        "info-container__wrapper--error": status === INFO_CONTAINER_STATUS.error,
        "info-container__wrapper--warning": status === INFO_CONTAINER_STATUS.warning,
        "info-container__wrapper--info": status === INFO_CONTAINER_STATUS.info,
      })}>
        {!noIcon && (
          <div className="info-container__icon">
            <Icon
              icon={icon}
              size="100%"
              style={{
                color: iconColor,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            />
          </div>
        )}
        <div className="info-container__text">
          {title}
          {' '}
          {text}
        </div>
      </div>
    </div>
  );
};

InfoContainer.defaultProps = {
  text: '',
  status: 'info',
  noIcon: false,
};
InfoContainer.propTypes = {
  text: PropTypes.string.isRequired,
  status: PropTypes.oneOf(Object.values(INFO_CONTAINER_STATUS)).isRequired,
  noIcon: PropTypes.bool,
};

export default React.memo(InfoContainer);
