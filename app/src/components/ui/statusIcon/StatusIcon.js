import React from 'react';
import PropTypes from 'prop-types';
import { check, close } from 'react-icons-kit/fa/';
import Icon from 'react-icons-kit';

import './status-icon.scss';

const BROWSER_CONTEXT = 16;
const SUCCESS_STATUS_COLOR = '#2ecc71';
const FAILURE_STATUS_COLOR = '#e74c3c';

const StatusIcon = ({ status, sizePx }) => {
  const sizeRem = `${sizePx / BROWSER_CONTEXT}rem`;
  const icon = status ? check : close;

  return (
    <div className="status-icon">
      <div
        className="status-icon__container"
        style={{ height: sizeRem, width: sizeRem }}
      >
        <Icon
          icon={icon}
          size="100%"
          style={{
            color: status ? SUCCESS_STATUS_COLOR : FAILURE_STATUS_COLOR,
            display: 'initial',
            verticalAlign: 'initial',
          }}
        />
      </div>
    </div>
  );
};

StatusIcon.defaultProps = {
  status: false,
  sizePx: 100,
};

StatusIcon.propTypes = {
  status: PropTypes.bool,
  sizePx: PropTypes.number,
};

export default React.memo(StatusIcon);
