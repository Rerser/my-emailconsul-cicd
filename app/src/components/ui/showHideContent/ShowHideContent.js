import React, { useState } from 'react';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from '../button';

import './show-hide-content.scss';

function ShowHideContent({ children }) {
  const t = useFormatMessage();

  const [isShow, setIsShow] = useState(false);

  if (!children) return null;

  return (
    <div className="show-hide-content">
      <div className="show-hide-content__btns">
        <Button 
          text={!isShow ? t('show-hide-content.button.show.text') : t('show-hide-content.button.hide.text')}
          className="show-hide-content__btn"
          onClick={() => setIsShow(!isShow)}
        />
      </div>
      {isShow && (
        <div className="show-hide-content__content">
          {children}
        </div>
      )}
    </div>  
  );
}

export default React.memo(ShowHideContent);
