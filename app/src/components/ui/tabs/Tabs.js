/**
  Example:

  <Tabs
    value={1}
    options={[
      { value: 1, label: 'Команда'},
      { value: 2, label: 'Группа команд'},
    ]}
    onChange={(e) => console.log(e.target.value)}
  />
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './c-tabs.scss';

export default class Tabs extends Component {
  static propTypes = {
    options: PropTypes.arrayOf(PropTypes.object).isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    containerStyle: PropTypes.string,
  };

  static defaultProps = {};

  render() {
    const name = Math.random();
    return (
      <div className={cn("c-tabs", this.props.containerStyle)}>
        {this.props.options.map((option) => (
          <div
            key={`c-tabs__id-${name}-${option.value}`}
            className="c-tabs__tab"
          >
            <input
              id={`c-tabs__id-${name}-${option.value}`}
              type="radio"
              name={name}
              value={option.value}
              className="c-tabs__input"
              onChange={this.props.onChange}
              checked={this.props.value === option.value}
            />
            <label
              htmlFor={`c-tabs__id-${name}-${option.value}`}
              className="c-tabs__label"
            >
              {option.label}
            </label>
          </div>
        ))}
      </div>
    );
  }
}
