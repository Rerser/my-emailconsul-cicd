/**
 * ИСПОЛЬЗОВАНИЕ

    ** Props:
    * onChange: функция по клику, обязательная,
        получает в качестве первого аргумента значения value,
        второго - параметр name
    * value: значение по умолчание value
    * blockClasses: дополнительные классы для компонента
    * name: параметр name
    * options: массив объектов, которые имеют два значения: value и label
    * disabledValue: значение которое передается в option disabled
    * placeholder

 * Пример:
    <Select
      options={[
        {value: 'one', label: 'one', key: 'key1'},
        {value: 'two', label: 'two'},
        {value: 'three', label: 'something'}
      ]}
      onChange={this.doSomething}
    />
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './c-select.scss';

export default class Select extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array,
    name: PropTypes.string,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    disabledValue: PropTypes.string,
    placeholder: PropTypes.string,
    resetBtnAvailable: PropTypes.bool,
    optionPlaceholderDisabled: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    name: '',
    disabled: false,
    placeholder: null,
    resetBtnAvailable: true,
    optionPlaceholderDisabled: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.value || '',
    };
  }

  handleChange = (e) => {
    this.setState(
      {
        value: e.target.value,
      },
      () => {
        this.props.onChange.call(null, this.state.value, this.props.name);
      }
    );
  };

  componentDidUpdate(prevProps) {
    if (this.props.options.length !== prevProps.options.length) {
      this.setState({
        value: '',
      });
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (state.value !== props.value) {
      return {
        value: state.value || '',
      };
    }
    return null;
  }

  render() {
    const {
      options,
      disabled,
      disabledValue,
      placeholder,
      className,
      resetBtnAvailable,
      optionPlaceholderDisabled,
    } = this.props;
    const { value } = this.state;

    return (
      <div className={cn('c-select', className)}>
        <select
          value={value || ''}
          onChange={this.handleChange}
          disabled={disabled}
        >
          <option
            disabled={optionPlaceholderDisabled}
            value={disabledValue || ''}
          >
            {placeholder || 'Select...'}
          </option>
          {options.map((option) => (
            <option
              value={option.value}
              key={`select-key-${option.key || option.value}`}
              disabled={option.disabled}
            >
              {option.label}
            </option>
          ))}
        </select>
        {!value && (
          <button className="c-select__btn c-select__btn--open" type="button">
            {'>'}
          </button>
        )}
        {!!value && resetBtnAvailable && (
          <button
            type="button"
            className="c-select__btn c-select__btn--clear"
            onClick={this.handleChange.bind(null, { target: { value: null } })}
          >
            x
          </button>
        )}
        {value && !resetBtnAvailable && (
          <button className="c-select__btn c-select__btn--choise" type="button">
            {'>'}
          </button>
        )}
      </div>
    );
  }
}
