/**
  ** Example
  Note: pre-install in redux-store a coords and set isShow flag to true value

  <ContextMenu>
    <div>
      Menu
    </div>
  </ContextMenu>
*/

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { handleContextMenuShow } from 'store/actions/contextMenu';
import { contextMenuCoordsSelector, contextMenuIsShowSelector } from 'store/selectors';
import Portal from '../portal';

import './context-menu.scss';

const BASE_PADDING_FROM_BORDER = 30;
const id = 'contextMenuPortal';

const ContextMenu = ({ children }) => {
  const dispatch = useDispatch();

  const contextMenuIsShow = useSelector(contextMenuIsShowSelector);
  const contextMenuCoords = useSelector(contextMenuCoordsSelector);

  useEffect(() => {
    const screenWidth = window.innerWidth;
    const screenHeight = window.innerHeight;
    
    const div = document.getElementById(id);

    if (div) {
      const { width: divWidth, height: divHeight } = div.getBoundingClientRect();
      let x = contextMenuCoords.x;
      let y = contextMenuCoords.y;

      if (x + divWidth > screenWidth) {
        x = screenWidth - divWidth - BASE_PADDING_FROM_BORDER;
      }
      if (y + divHeight > screenHeight) {
        y = screenHeight - divHeight - BASE_PADDING_FROM_BORDER;
      }
      div.style.left = `${x}px`;
      div.style.top = `${y}px`;
    }
  }, [contextMenuCoords]);

  useEffect(() => {
    const { body } = document;

    function handleClick(e) {
      const closest = e.target.closest(`#${id}`);
      if (!closest && contextMenuIsShow) {
        dispatch(handleContextMenuShow(false));
      }
    }

    body.addEventListener('click', handleClick);

    return () => {
      body.removeEventListener('click', handleClick);
    };
  }, [dispatch, contextMenuIsShow]);

  return (
    <Portal pure>
      <div id={id} className="context-menu">
        {children}
      </div>
    </Portal>
  );
};

export default React.memo(ContextMenu);
