/**
 * ИСПОЛЬЗОВАНИЕ

 * Пример:
    <InputText
      value={verification_code}
      onChange={this.handleChangeVCode}
      errorText={verification_code_error}
    />
*/

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './c-input-text.scss';

export default class InputText extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func,
    className: PropTypes.string,
    errorText: PropTypes.string,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    required: PropTypes.bool,
    searchIcon: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    errorText: '',
    value: '',
    placeholder: '',
    disabled: false,
    required: false,
    searchIcon: false,
  };

  render() {
    let {
      value,
      onChange,
      errorText,
      disabled,
      required,
      placeholder,
      onFocus,
      onBlur,
    } = this.props;

    if (value === null || value === undefined) {
      value = '';
    }

    const error = () => {
      if (errorText) {
        return <div className="c-input-text__errortext">
          {errorText}
        </div>;
      }
      return false;
    };

    const input = () => {
      if (onChange) {
        return (
          <input
            type={this.props.type || 'text'}
            value={value}
            placeholder={placeholder}
            onChange={onChange}
            onFocus={onFocus}
            onBlur={onBlur}
            disabled={disabled}
            required={required}
          />
        );
      }

      return (
        <input
          type={this.props.type || 'text'}
          value={value}
          placeholder={placeholder}
          onFocus={onFocus}
          onBlur={onBlur}
          readOnly
          disabled={disabled}
        />
      );
    };

    return (
      <div
        className={cn('c-input-text', this.props.className, {
          'c-input-text--search': !!this.props.searchIcon,
          'c-input-text--error': this.props.errorText,
          'c-input-text--required': this.props.required,
          'c-input-text--disabled': this.props.disabled,
        })}
      >
        {input()}
        {error()}
      </div>
    );
  }
}
