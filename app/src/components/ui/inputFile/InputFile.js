/**
 * ИСПОЛЬЗОВАНИЕ

 * Props:
    onDragStart: PropTypes.func,
    onDrop: PropTypes.func,
    onDragEnter: PropTypes.func,
    onDragLeave: PropTypes.func,
    onDragOver: PropTypes.func,
    onDragEnd: PropTypes.func,
    onUpload: PropTypes.func основная, выполняется когда файл загружен с помощью dnd или простого выбора
    innerText: PropTypes.string
    labelNodeElement: PropTypes.node

 * Пример:
    <InputFile onUpload={(data) => this.setState({uploadedFile: data.files[0]})}>
      Drop files here...
    </InputFile>
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './c-input-file.scss';

export default class InputFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDragEnter: false,
    };
  }

  static propTypes = {
    onDragStart: PropTypes.func,
    onDrop: PropTypes.func,
    onDragEnter: PropTypes.func,
    onDragLeave: PropTypes.func,
    onDragOver: PropTypes.func,
    onDragEnd: PropTypes.func,
    onUpload: PropTypes.func,
    innerText: PropTypes.string,
    labelNodeElement: PropTypes.node,
  };

  inputElement = React.createRef();

  static defaultProps = {
    innerText: 'Drag and drop file here, or click for select a file to upload',
  };

  handleDragStart = (event) => {
    if (typeof this.props.onDragStart === 'function') {
      this.props.onDragStart(event);
    }
  };

  handleDrag = (event) => {
    if (typeof this.props.onDrag === 'function') {
      this.props.onDrag(event);
    }
  };

  handleDragEnter = (event) => {
    this.setState({
      isDragEnter: true,
    });
    if (typeof this.props.onDragEnter === 'function') {
      this.props.onDragEnter(event);
    }
  };

  handleDragLeave = (event) => {
    this.setState({
      isDragEnter: false,
    });
    if (typeof this.props.onDragLeave === 'function') {
      this.props.onDragLeave(event);
    }
  };

  handleDragOver = (event) => {
    event.preventDefault();

    if (typeof this.props.onDragOver === 'function') {
      this.props.onDragOver(event);
    }
  };

  handleDrop = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      isDragEnter: false,
    });

    if (event.dataTransfer.files.length > 0) {
      if (typeof this.props.onDrop === 'function') {
        this.props.onDrop(event.dataTransfer);
      } else if (typeof this.props.onUpload === 'function') {
        this.props.onUpload(event.dataTransfer, this.inputElement);
      }
    }
  };

  handleDragEnd = (event) => {
    if (typeof this.props.onDragEnd === 'function') {
      this.props.onDragEnd(event);
    }
  };

  handleChoose = (event) => {
    if (event.target.files.length > 0) {
      if (typeof this.props.onUpload === 'function') {
        this.props.onUpload(event.target, this.inputElement);
        this.inputElement.current.value = '';
      }
    }
  };

  render() {
    const id = Math.random();
    return (
      <div className="c-input-file">
        <input ref={this.inputElement} type="file" id={id} onChange={this.handleChoose} style={{ display: 'none' }} />
        <label
          htmlFor={id}
          onDragStart={this.handleDragStart}
          onDrag={this.handleDrop}
          onDragEnter={this.handleDragEnter}
          onDragLeave={this.handleDragLeave}
          onDragOver={this.handleDragOver}
          onDrop={this.handleDrop}
          onDragEnd={this.handleDragEnd}
          className={cn('c-input-file__label', {            
            'c-input-file__label--no_node': !this.props.labelNodeElement,            
            'c-input-file__label--dragEnter': this.state.isDragEnter,
          })}
        >
          {!this.props.labelNodeElement && this.props.innerText}
          {this.props.labelNodeElement && this.props.labelNodeElement}
        </label>
      </div>
    );
  }
}
