import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
// import PropTypes from 'prop-types';

import './c-sidebar.css';

class Sidebar extends Component {
  // static propTypes = {};
  // static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  // static getDerivedStateFromProps(props, state) {};
  // shouldComponentUpdate(nextProps, nextState) {};
  render() {
    return (
      <div className="c-sidebar">
        <div className="c-sidebar__nav">
          <Link className="c-sidebar__link c-sidebar__link--demo" to="/">
            <span>Дашборд</span>
          </Link>
          <Link
            className="c-sidebar__link c-sidebar__link--demo"
            to="/trackers"
          >
            <span>Трекеры</span>
          </Link>
          <Link className="c-sidebar__link c-sidebar__link--demo" to="/config">
            <span>Команды конфигурации</span>
          </Link>
          <Link
            className="c-sidebar__link c-sidebar__link--demo"
            to="/commands_groups"
          >
            <span>Группы команд</span>
          </Link>
        </div>
      </div>
    );
  }
  // getSnapshotBeforeUpdate(prevProps, prevState) {}
  // componentDidMount() {};
  // componentDidUpdate(prevProps, prevState) {};
  // componentWillUnmount() {};
  // componentDidCatch(error, info) {};
  // static getDerivedStateFromError(error) {}
}

export default connect(
  (state) => ({
    // state: state
  }),
  (dispatch, ownProps) => ({
    // fname: (data) => {
    //     dispatch(fname(data));
    // }
  })
)(Sidebar);
