import React, { Fragment, useState } from 'react';
import ConfirmationPopup from 'components/ui/confirmationPopup';

// HOC, позволяющий использовать ConfirmationPopup в WrappedComponent
export default function (WrappedComponent) {
  return function (props) {
    const [show, setShow] = useState(false); // статус показа
    const [title, setTitle] = useState(''); // заголовок ConfirmationPopup
    const [cbObj, setCbObj] = useState({ cb: null }); // колбек, который необходимо выполнить при нажатии на кнопку "confirm"

    const handleConfirm = () => {
      const { cb } = cbObj;
      if (cb && typeof cb === 'function') {
        cb();
      }
      setShow(false);
    };

    const handleShow = (title, cb) => {
      // title - заголовок ConfirmationPopup
      // cb - колбек
      setTitle(title || undefined); // если не передали заголовок, то показывать заголовок по умолчанию
      setCbObj({ cb });
      setShow(true);
    };

    return (
      <Fragment>
        <WrappedComponent {...props} showPopup={handleShow} />
        {show && (
          <ConfirmationPopup
            title={title}
            onSumbit={handleConfirm}
            onClose={() => setShow(false)}
          />
        )}
      </Fragment>
    );
  };
}
