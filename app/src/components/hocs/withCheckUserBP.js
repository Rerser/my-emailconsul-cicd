import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import {
  myUserBillingPlanDataSelector,
} from 'store/selectors';

import { BILLING_PLAN_NAME } from 'utils/constants';

// HOC, позволяющий проверять биллинг план пользователя в WrappedComponent
// и перенаправлять его на главную страницу 
export default function (WrappedComponent) {
  return function (props) {
    const history = useHistory();

    const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);

    useEffect(() => {
      if (
        myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.NONE ||
        myUserBillingPlanData?.current_plan === BILLING_PLAN_NAME.EXPIRED
      ) {
        history.push('/user/billing');
      }
    }, [history, myUserBillingPlanData]);
    
    return (
      <WrappedComponent {...props} />
    );
  };
}
