// import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { usersMeSelector } from 'store/selectors';

function ForRole({ roles, usersMe, children }) {
  if (!usersMe || !roles.includes(usersMe.role)) {
    return null;
  }
  return children;
}

ForRole.propTypes = {
  usersMe: PropTypes.object,
  roles: PropTypes.array,
};
ForRole.defaultProps = {
  usersMe: null,
  roles: [],
};

export default connect(
  (state) => ({
    usersMe: usersMeSelector(state),
  }),
  {}
)(ForRole);
