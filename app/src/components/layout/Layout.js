import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Navbar from './components/navbar';
import ConfirmationPopupRedux from './components/confirmationPopupRedux';

import './c-layout.scss';

function Layout(props) {
  return (
    <Fragment>
      <Navbar />
      <div className="c-layout">
        <div className="c-layout__content">
          {props.title && <h1 className="c-layout__title">
            {props.title}
          </h1>}
          {props.children}
        </div>
      </div>
      <ConfirmationPopupRedux />
    </Fragment>
  );
}

Layout.propTypes = {
  title: PropTypes.string,
};

export default Layout;
