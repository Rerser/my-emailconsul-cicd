import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Portal from 'components/ui/portal';
import Button from 'components/ui/button';

import {
  handleConfirmationPopupShow,
  setConfirmationPopupTitle,
  setConfirmationPopupCallback,
} from 'store/actions/confirmationPopup';
import {
  confirmationPopupIsShowSelector,
  confirmationPopupTitleSelector,
  confirmationPopupCallbackSelector,
} from 'store/selectors';

import './confirmation-popup-redux.scss';

function ConfirmationPopupRedux() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const confirmationPopupIsShow = useSelector(confirmationPopupIsShowSelector);
  const confirmationPopupTitle = useSelector(confirmationPopupTitleSelector);
  const confirmationPopupCallback = useSelector(confirmationPopupCallbackSelector);

  const handleCallback = () => {
    confirmationPopupCallback();
    dispatch(handleConfirmationPopupShow(false));
    dispatch(setConfirmationPopupTitle(''));
    dispatch(setConfirmationPopupCallback(null));
  };

  return confirmationPopupIsShow ? (
    <Portal>
      <div className="confirmation-popup-redux">
        <div className="confirmation-popup-redux__text">
          {confirmationPopupTitle || t('confirmation_popup-redux.title.default.text')}
          <br />
          <br />
          {t('confirmation_popup-redux.title.select')}
        </div>

        <div className="confirmation-popup-redux__footer">
          <Button
            mode="purple"
            text={t('confirmation_popup-redux.title.btn.confirm')}
            className="classes"
            onClick={handleCallback}
            type="button"
          />
          <Button
            mode="light"
            text={t('confirmation_popup-redux.title.btn.close')}
            className="classes"
            onClick={() => dispatch(handleConfirmationPopupShow(false))}
            type="button"
          />
        </div>
      </div>
    </Portal>
  ) : null;
}

export default React.memo(ConfirmationPopupRedux);
