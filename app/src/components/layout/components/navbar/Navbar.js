import React, { useEffect, useState, Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import LogoImg from 'assets/images/logo-color.svg';
import { MIN_SCREEN_WIDTH_BREAKPOINT } from 'utils/constants';
import { authIsSwitchedUserModeSelector } from 'store/selectors';

import UserMenu from './components/UserMenu';
import LangSwitcher from './components/LangSwitcher';
import MainMenu from './components/MainMenu';
import InfoMenu from './components/InfoMenu/InfoMenu';
import ReturnToAdmin from './components/ReturnToAdmin';
import WarningExpired from './components/WarningExpired';
import AdminMenu from './components/adminMenu';
import MBPsMenu from './components/MBPsMenu';

import './c-navbar.scss';

function Navbar() {
  const isSwitchedUserMode = useSelector(authIsSwitchedUserModeSelector);

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    function handleResize() {
      setWindowWidth(window.innerWidth);
    }
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    }
  }, []);

  return (
    <nav className="c-navbar">
      <ul className="c-navbar__list">
        {!isSwitchedUserMode && (
          <li>
            <Link className="c-navbar__logo" to="/">
              <span
                style={{
                  backgroundImage: `url(${LogoImg})`,
                }}
              />
            </Link>
          </li>
        )}
        <li>
          <ReturnToAdmin />
        </li>
        <li>
          <WarningExpired />
        </li>
      </ul>

      <ul className="c-navbar__list">
        {windowWidth > MIN_SCREEN_WIDTH_BREAKPOINT && (
          <li className="right">
            <div className="c-navbar__user">
              <AdminMenu />
            </div>
          </li>
        )}

        <MainMenu />

        {windowWidth > MIN_SCREEN_WIDTH_BREAKPOINT && (
          <Fragment>
            <li className="right">
              <div className="c-navbar__user">
                <MBPsMenu />
              </div>
            </li>
            <li className="right">
              <div className="c-navbar__user">
                <InfoMenu />
              </div>
            </li>
          </Fragment>
        )}

        <li className="right">
          <div className="c-navbar__user">
            <UserMenu />
          </div>
        </li>
        <li className="right">
          <div className="c-navbar__user">
            <LangSwitcher />
          </div>
        </li>
      </ul>
    </nav>
  );
}

export default React.memo(Navbar);
