import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { userCircleO } from 'react-icons-kit/fa/userCircleO';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import withConfirmationPopup from 'components/hocs/withConfirmationPopup';
import DropDown from 'components/ui/dropDown';

import { authLogout } from 'store/actions/auth';
import { LANDING_FAQ_URL } from 'utils/constants';

const UserMenu = ({ showPopup }) => {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const menuItemList = [
    {
      to: '/user/settings',
      text: t('navbar.user_dropdown.settings.link'),
    },
    {
      to: '/user/billing',
      text: t('navbar.user_dropdown.billing.link'),
    },
    {
      to: LANDING_FAQ_URL,
      text: t('navbar.user_dropdown.faq.link'),
      external: true,
    },
    {
      to: '/user/help',
      text: t('navbar.user_dropdown.help.link'),
    },
    {
      func: () =>
        showPopup(t('confirmation_popup.title.logout'), () =>
          dispatch(authLogout())
        ),
      text: t('navbar.user_dropdown.logout.link'),
      to: '',
    },
  ];

  return (
    <div className="user-menu">
      <DropDown icon={userCircleO} menuItemList={menuItemList} />
    </div>
  );
};

UserMenu.defaultProps = {
  showPopup: () => {},
};
UserMenu.propTypes = {
  showPopup: PropTypes.func,
};

export default React.memo(withConfirmationPopup(UserMenu));
