import React from 'react';
import { useSelector } from 'react-redux';
import { myUserBillingPlanDataSelector } from 'store/selectors';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import {
  getWillBeExpired,
  getDayExpired,
  getBillingPlanIsExpired,
} from 'utils/helpers';
import './warning-expired.scss';

function WarningExpired() {
  const t = useFormatMessage();
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);

  const isWillBeExpired = getWillBeExpired(myUserBillingPlanData);
  const dayExpired = getDayExpired(myUserBillingPlanData);

  const billingPlanIsExpired = getBillingPlanIsExpired(myUserBillingPlanData);

  return isWillBeExpired ? (
    <div className="will_expired">
      <span className="will_expired__text">
        {billingPlanIsExpired
          ? t('user_billing.plan_is_expired.text')
          : t('user_billing.plan_will_be_expired.text')}
      </span>
      {!billingPlanIsExpired && (
        <span className="will_expired__expired-day">
          {dayExpired}
        </span>
      )}
    </div>
  ) : null;
}

export default React.memo(WarningExpired);
