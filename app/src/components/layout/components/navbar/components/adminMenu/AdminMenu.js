import React from 'react';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DropDown from 'components/ui/dropDown';
import { USER_ROLE } from 'utils/constants';
import ForRole from 'components/forRole';

const AdminMenu = () => {
  const t = useFormatMessage();

  const menuItemList = [
    {
      to: '/users',
      text: t('navbar.users.link'),
      roles: [USER_ROLE.ADMIN],
    },
    {
      to: '/seedlisting-workers',
      text: t('navbar.seedlisting_workers.link'),
      roles: [USER_ROLE.ADMIN],
    },
    // {
    //   to: '/admin-google-postmaster',
    //   text: t('navbar.admin-postmaster.link'),
    //   roles: [USER_ROLE.ADMIN],
    // },
  ];

  return (
    <ForRole roles={[USER_ROLE.ADMIN]}>
      <DropDown menuText={t('navbar.admin_menu.text')} menuItemList={menuItemList} />
    </ForRole>
  );
};

export default React.memo(AdminMenu);
