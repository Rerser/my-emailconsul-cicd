import React, { Fragment, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { navicon } from 'react-icons-kit/fa/navicon';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DropDown from 'components/ui/dropDown';
import MenuLinks from 'components/ui/menuLinks';

import {
  usersMeSelector,
  usersMeIsLoadingSelector,
  myUserBillingPlanDataSelector,
  myUserBillingPlanIsDataLoadingSelector,
} from 'store/selectors';

import { USER_ROLE, BILLING_PLAN_NAME, MIN_SCREEN_WIDTH_BREAKPOINT } from 'utils/constants';

import './main-menu.scss';

const MainMenu = () => {
  const t = useFormatMessage();

  const usersMe = useSelector(usersMeSelector);
  const usersMeIsLoading = useSelector(usersMeIsLoadingSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    function handleResize() {
      setWindowWidth(window.innerWidth);
    }
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const isShowMenu =
    !usersMeIsLoading &&
    usersMe &&
    !myUserBillingPlanIsDataLoading &&
    myUserBillingPlanData &&
    myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.NONE;

  const menu = isShowMenu
    ? [
        {
          to: '/seedlisting',
          text: t('navbar.seedlisting.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
      ]
    : [];

  if (windowWidth <= MIN_SCREEN_WIDTH_BREAKPOINT) {
    const adminMenu = [
      {
        to: '/users',
        text: t('navbar.users.link'),
        roles: [USER_ROLE.ADMIN],
      },
      {
        to: '/seedlisting-workers',
        text: t('navbar.seedlisting_workers.link'),
        roles: [USER_ROLE.ADMIN],
      },
      // {
      //   to: '/admin-google-postmaster',
      //   text: t('navbar.admin-postmaster.link'),
      //   roles: [USER_ROLE.ADMIN],
      // },
    ];
    const additionalTools = 
      myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.NONE ||
      myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.EXPIRED
        ? [
          {
            to: '/spf',
            text: t('navbar.spf.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
          {
            to: '/dkim',
            text: t('navbar.dkim.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
          {
            to: '/dmarc',
            text: t('navbar.dmarc.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
        ] : [];
    const userMenu = isShowMenu
      ? [
          {
            to: '/google-postmaster',
            text: t('navbar.google-postmaster.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
          {
            to: '/snds',
            text: t('navbar.snds.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
          ...additionalTools,
          {
            to: '/ips',
            text: t('navbar.ips.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
          {
            to: '/domains',
            text: t('navbar.domains.link'),
            roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
          },
        ]
      : [];

    if (usersMe.role === USER_ROLE.ADMIN) {
      menu.unshift(...adminMenu);
    }
    menu.push(...userMenu);
  }

  return (
    <li className="main-menu">
      {!!menu.length && (
        <Fragment>
          <MenuLinks icon={navicon} menuItemList={menu} />
          <DropDown icon={navicon} menuItemList={menu} />
        </Fragment>
      )}
    </li>
  );
};

export default React.memo(MainMenu);
