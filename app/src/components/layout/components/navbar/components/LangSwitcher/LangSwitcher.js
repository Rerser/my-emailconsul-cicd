import React from 'react';
import { useDispatch } from 'react-redux';
import { language } from 'react-icons-kit/fa/language';
import DropDown from 'components/ui/dropDown';

import { setCurrent } from 'store/actions/locale';
import { LOCALE, LOCAL_STORAGE_LOCALE_KEY } from 'utils/constants';

const LangSwitcher = () => {
  const dispatch = useDispatch();

  const handleLocale = (val) => {
    dispatch(setCurrent(val));
    window.localStorage.setItem(LOCAL_STORAGE_LOCALE_KEY, val);
  };

  const menuItemList = [
    {
      to: null,
      text: 'En',
      roles: null,
      func: () => handleLocale(LOCALE.en),
    },
    {
      to: null,
      text: 'Ру',
      roles: null,
      func: () => handleLocale(LOCALE.ru),
    },
  ];

  return (
    <div className="lang-switcher">
      <DropDown icon={language} menuItemList={menuItemList} />
    </div>
  );
};

export default React.memo(LangSwitcher);
