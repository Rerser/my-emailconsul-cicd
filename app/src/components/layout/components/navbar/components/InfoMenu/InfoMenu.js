import React from 'react';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DropDown from 'components/ui/dropDown';

import {
  usersMeSelector,
  usersMeIsLoadingSelector,
  myUserBillingPlanDataSelector,
  myUserBillingPlanIsDataLoadingSelector,
} from 'store/selectors';

import { BILLING_PLAN_NAME, USER_ROLE } from 'utils/constants';

const InfoMenu = () => {
  const t = useFormatMessage();

  const usersMe = useSelector(usersMeSelector);
  const usersMeIsLoading = useSelector(usersMeIsLoadingSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);

  const isShowMenu =
    !usersMeIsLoading &&
    usersMe &&
    !myUserBillingPlanIsDataLoading &&
    myUserBillingPlanData &&
    myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.NONE;

  const menuItemList = isShowMenu
    ? [
        {
          to: '/ips',
          text: t('navbar.ips.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
        {
          to: '/domains',
          text: t('navbar.domains.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
      ]
    : [];

  if (!menuItemList.length) return null;

  return (
    <div className="user-menu">
      <DropDown menuText={t('navbar.info_menu.text')} menuItemList={menuItemList} />
    </div>
  );
};

export default React.memo(InfoMenu);
