import React from 'react';
import { useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import DropDown from 'components/ui/dropDown';

import {
  usersMeSelector,
  usersMeIsLoadingSelector,
  myUserBillingPlanDataSelector,
  myUserBillingPlanIsDataLoadingSelector,
} from 'store/selectors';

import { BILLING_PLAN_NAME, USER_ROLE } from 'utils/constants';

const MBPsMenu = () => {
  const t = useFormatMessage();

  const usersMe = useSelector(usersMeSelector);
  const usersMeIsLoading = useSelector(usersMeIsLoadingSelector);
  const myUserBillingPlanData = useSelector(myUserBillingPlanDataSelector);
  const myUserBillingPlanIsDataLoading = useSelector(myUserBillingPlanIsDataLoadingSelector);

  const isShowMenu =
    !usersMeIsLoading &&
    usersMe &&
    !myUserBillingPlanIsDataLoading &&
    myUserBillingPlanData &&
    myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.NONE;
  
  const additionalTools = 
    myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.NONE ||
    myUserBillingPlanData?.current_plan !== BILLING_PLAN_NAME.EXPIRED
      ? [
        {
          to: '/spf',
          text: t('navbar.spf.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
        {
          to: '/dkim',
          text: t('navbar.dkim.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
        {
          to: '/dmarc',
          text: t('navbar.dmarc.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
      ] : [];

  const menuItemList = isShowMenu
    ? [
        {
          to: '/google-postmaster',
          text: t('navbar.google-postmaster.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
        {
          to: '/snds',
          text: t('navbar.snds.link'),
          roles: [USER_ROLE.ADMIN, USER_ROLE.USER],
        },
        ...additionalTools,
      ]
    : [];

  if (!menuItemList.length) return null;

  return (
    <div className="user-menu">
      <DropDown menuText={t('navbar.mbps_menu.text')} menuItemList={menuItemList} />
    </div>
  );
};

export default React.memo(MBPsMenu);
