import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormatMessage } from '@comparaonline/react-intl-hooks';
import Button from 'components/ui/button/Button';
import Spinner from 'components/ui/spinner/Spinner';

import { setSwitchedUser } from 'store/actions/auth';
import { authSwitchedUserSelector, authSwitchedUserIsLoadingSelector } from 'store/selectors';

import './return-to-admin.scss';

function ReturnToAdmin() {
  const dispatch = useDispatch();
  const t = useFormatMessage();

  const switchedUser = useSelector(authSwitchedUserSelector);
  const switchedUserIsLoading = useSelector(authSwitchedUserIsLoadingSelector);

  const spinner = switchedUserIsLoading ? <Spinner /> : null;
  const toAdminData =
    !switchedUserIsLoading && switchedUser ? (
      <Fragment>
        <div className="to-admin__control">
          <Button
            mode="purple"
            text={t('navbar.return_to_admin.btn.text')}
            className="to-admin__btn"
            onClick={() => dispatch(setSwitchedUser(null))}
          />
        </div>
        <div className="to-admin__info">
          <div className="to-admin__data role">
            <span className="to-admin__description">
              {t('navbar.return_to_admin.role.text')}
            </span>
            <span className="to-admin__type">
              {switchedUser.role}
            </span>
          </div>
          <div className="to-admin__data email">
            <span className="to-admin__description">
              {t('navbar.return_to_admin.email.text')}
            </span>
            <span className="to-admin__type">
              {switchedUser.email}
            </span>
          </div>
        </div>
      </Fragment>
    ) : null;

  return (
    <div className="to-admin">
      {spinner}
      {toAdminData}
    </div>
  );
}

export default React.memo(ReturnToAdmin);
