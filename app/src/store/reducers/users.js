import actionsTypes from 'store/actionsTypes';

const defaultState = {
  meIsLoading: false,
  me: null,

  data: [],
  pagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },
  isDataLoading: false,
  sorting: {
    field: 'host',
    order: 'asc',
    sortFields: ['email', 'host', 'worker_container', 'status'],
  },

  editedUser: null,
  editedUserSavingIsLoading: false,

  errors: {
    data: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.USERS_SET_ME: {
      return {
        ...state,
        me: payload.data,
      };
    }

    case actionsTypes.USERS_HANDLE_ME_IS_LOADING: {
      return {
        ...state,
        meIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_USERS_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_USERS_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;
      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_USERS_SORTING: {
      return {
        ...state,
        sorting: payload.data,
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.SET_USERS_DATA_PAGINATION: {
      return {
        ...state,
        pagination: payload.data,
      };
    }

    case actionsTypes.SET_EDITED_USER: {
      return {
        ...state,
        editedUser: payload.data,
      };
    }

    case actionsTypes.HANDLE_EDITED_USER: {
      const { field, value } = payload.data;
      return {
        ...state,
        editedUser: {
          ...state.editedUser,
          [field]: value,
        },
      };
    }

    case actionsTypes.HANDLE_EDITED_USER_SAVING_IS_LOADING: {
      return {
        ...state,
        editedUserSavingIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_USERS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
