import actionsTypes from 'store/actionsTypes';
import { LOCAL_STORAGE_TOKEN_KEY } from 'utils/constants';

const defaultState = {
  email: null,
  password: null,
  confirmPassword: null,
  isRecaptchaVerified: false,
  token: window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY) || null,
  isLoading: false,
  activationResultStatus: null,

  currentPassword: null,
  newPassword: null,
  confirmNewPassword: null,

  switchedUser: null,
  switchedUserIsLoading: false,
  isSwitchedUserMode: false,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.AUTH_SET_EMAIL: {
      return {
        ...state,
        email: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_PASSWORD: {
      return {
        ...state,
        password: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_CONFIRM_PASSWORD: {
      return {
        ...state,
        confirmPassword: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_IS_RECAPTCHA_VERIFIED: {
      return {
        ...state,
        isRecaptchaVerified: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_TOKEN: {
      return {
        ...state,
        token: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_IS_LOADING: {
      return {
        ...state,
        isLoading: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_ACTIVATION_RESULT_STATUS: {
      return {
        ...state,
        activationResultStatus: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_CURRENT_PASSWORD: {
      return {
        ...state,
        currentPassword: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_NEW_PASSWORD: {
      return {
        ...state,
        newPassword: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_CONFIRM_NEW_PASSWORD: {
      return {
        ...state,
        confirmNewPassword: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_SWITCHED_USER: {
      return {
        ...state,
        switchedUser: payload.data,
      };
    }

    case actionsTypes.AUTH_SET_SWITCHED_USER_IS_LOADING: {
      return {
        ...state,
        switchedUserIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_IS_SWITCHED_USER_MODE: {
      return {
        ...state,
        isSwitchedUserMode: payload.data,
      };
    }

    default:
      return state;
  }
};
