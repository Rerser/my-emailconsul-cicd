import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isDataLoading: false,
  data: null,

  error: null,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_SPF_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_SPF_DATA: {
      return {
        ...state,
        data: payload.data,
      };
    }

    case actionsTypes.SET_SPF_DATA_ERROR: {
      return {
        ...state,
        error: payload.data,
      };
    }

    default:
      return state;
  }
};
