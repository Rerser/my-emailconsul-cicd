import actionsTypes from 'store/actionsTypes';
import {
  GP_SHOW_TABLE,
  NEW_GP_DOMAIN_MODE,
  MINUS_30_DAYS_FROM_YESTERDAY,
  DAY_BEFORE_YESTERDAY,
} from 'utils/constants';

const defaultState = {
  isDataLoading: false,
  isDataLoaded: true,
  data: [],

  dateFrom: MINUS_30_DAYS_FROM_YESTERDAY.toISOString(),
  dateTo: DAY_BEFORE_YESTERDAY.toISOString(),

  filter: {
    domain: '',
    ip: '',
  },
  
  isOnNewGPDomain: false,
  newGPDomainMode: NEW_GP_DOMAIN_MODE.SINGLE,
  newGPDomains: [null],
  newGPDomainIsLoading: false,

  newGPDomainsFile: null,
  newGPDomainsFileIsUploading: false,
  responseNewGPDomainsUploadedFile: null,

  gpDomainsAvailableCount: 0,
  gpDomainsAvailableCountIsLoading: false,

  chosenGPData: null,

  showTableMode: GP_SHOW_TABLE.DOMAIN,

  errors: {
    data: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_GP_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_DATA_IS_LOADED: {
      return {
        ...state,
        isDataLoaded: payload.data,
      };
    }

    case actionsTypes.SET_GP_DATA: {
      const { data, meta: { count = 0, offset = 0, limit = 30 } = {} } = payload.data;

      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.HANDLE_GP_DATA_FILTER: {
      const { field, value } = payload.data;
      return {
        ...state,
        filter: {
          ...state.filter,
          [field]: value,
        },
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.SET_IS_ON_NEW_GP_DOMAIN: {
      const newState = {
        ...state,
        isOnNewGPDomain: payload.data,
      };

      if (!payload.data) {
        newState.newGPDomains = [null];
      }

      return {
        ...newState,
      };
    }

    case actionsTypes.HANDLE_NEW_GP_DOMAINS: {
      const { value, index } = payload.data;
      const newGPDomains = [...state.newGPDomains];

      newGPDomains[index] = value;

      return {
        ...state,
        newGPDomains,
      };
    }

    case actionsTypes.REMOVE_NEW_GP_DOMAIN: {
      const { index } = payload.data;
      const newGPDomains = [...state.newGPDomains];

      newGPDomains.splice(index, 1);

      return {
        ...state,
        newGPDomains,
      };
    }

    case actionsTypes.HANDLE_NEW_GP_DOMAIN_IS_LOADING: {
      return {
        ...state,
        newGPDomainIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_GP_DATE_FROM: {
      return {
        ...state,
        dateFrom: payload.data,
        filter: {
          domain: '',
        },
      };
    }

    case actionsTypes.SET_GP_DATE_TO: {
      return {
        ...state,
        dateTo: payload.data,
      };
    }

    case actionsTypes.SET_GP_CHOSEN_DATA: {
      return {
        ...state,
        chosenGPData: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_SHOW_TABLE_MODE: {
      return {
        ...state,
        showTableMode: payload.data,
      };
    }

    case actionsTypes.SET_GP_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    case actionsTypes.SET_GP_DOMAINS_AVAILABLE_COUNT: {
      return {
        ...state,
        gpDomainsAvailableCount: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_DOMAINS_AVAILABLE_COUNT_IS_LOADING: {
      return {
        ...state,
        gpDomainsAvailableCountIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_GP_DOMAIN_MODE: {
      return {
        ...state,
        newGPDomainMode: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_GP_DOMAINS_FILE_IS_UPLOADING: {
      return {
        ...state,
        newGPDomainsFileIsUploading: payload.data,
      };
    }

    case actionsTypes.SET_NEW_GP_DOMAINS_FILE: {
      return {
        ...state,
        newGPDomainsFile: payload.data,
      };
    }

    case actionsTypes.SET_RESPONSE_NEW_GP_DOMAINS_UPLOADED_FILE: {
      return {
        ...state,
        responseNewGPDomainsUploadedFile: payload.data,
      };
    }

    case actionsTypes.CLEAR_GP_ALL_DATA: {
      return {
        ...defaultState
      };
    }

    default:
      return state;
  }
};
