import actionsTypes from 'store/actionsTypes';
import { YESTERDAY } from 'utils/constants';

const defaultState = {
  // credentials

  isOnAddKey: false,

  isCredentialsDataLoading: false, // credentials data fetching
  isCredentialsLoading: false, // credentials item data send, delete, update
  credentials: [],
  credentialsKey: '',

  // history

  sndsHistoryIsShowSlider: false,
  sndsHistoryIsLoading: false,
  sndsHistoryIsLoaded: true,
  sndsHistory: [],

  // date run

  sndsDateRunIsShowSlider: false,
  sndsDateRun: YESTERDAY.toISOString(),
  sndsDateRunIsLoading: false,

  errors: {
    credentials: null,
    sndsHistory: null,
    dateRun: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.SET_SNDS_INTEGRATION_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    // on add key slider

    case actionsTypes.SET_SNDS_IS_ON_ADD_KEY: {
      return {
        ...state,
        isOnAddKey: payload.data,
      };
    }

    // credentials

    case actionsTypes.HANDLE_SNDS_CREDENTIALS_IS_LOADING: {
      return {
        ...state,
        isCredentialsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_CREDENTIALS_DATA_IS_LOADING: {
      return {
        ...state,
        isCredentialsDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_CREDENTIALS: {
      return {
        ...state,
        credentials: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_CREDENTIALS_KEY: {
      return {
        ...state,
        credentialsKey: payload.data,
      };
    }

    // history 

    case actionsTypes.SET_SNDS_HISTORY_IS_SHOW_SLIDER: {
      return {
        ...state,
        sndsHistoryIsShowSlider: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_HISTORY_IS_LOADING: {
      return {
        ...state,
        sndsHistoryIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_HISTORY_IS_LOADED: {
      return {
        ...state,
        sndsHistoryIsLoaded: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_HISTORY: {
      return {
        ...state,
        sndsHistory: payload.data,
      };
    }

    // date run

    case actionsTypes.SET_SNDS_DATE_RUN_IS_SHOW_SLIDER: {
      return {
        ...state,
        sndsDateRunIsShowSlider: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_DATE_RUN: {
      return {
        ...state,
        sndsDateRun: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_DATE_RUN_IS_LOADING: {
      return {
        ...state,
        sndsDateRunIsLoading: payload.data,
      };
    }

    case actionsTypes.CLEAR_SNDS_INTEGRATION_ALL_DATA: {
      return {
        ...defaultState,
      };
    }

    default:
      return state;
  }
};
