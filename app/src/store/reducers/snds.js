import actionsTypes from 'store/actionsTypes';
import {
  DAY_BEFORE_YESTERDAY,
  MINUS_30_DAYS_FROM_YESTERDAY,
  SNDS_INFO_MODE
} from 'utils/constants';

// dashboards

const defaultDashboardsState = {
  isDashboardsDataLoading: false,
  isDashboardsDataLoaded: true,
  dashboardsData: [],

  dashboardsFilters: {
    ips: [],
  },

  isDashboardsUserIpsLoading: false,
  dashboardsUserIps: [],

  dashboardsDateFrom: MINUS_30_DAYS_FROM_YESTERDAY.toISOString(),
  dashboardsDateTo: DAY_BEFORE_YESTERDAY.toISOString(),
}

// ipStatus

const defaultIpStatusState = {
  isIpStatusLoading: false,
  isIpStatusLoaded: false,
  ipStatus: [],

  isIpStatusUserIpsLoading: false,
  ipStatusUserIps: [],

  ipStatusDateFrom: MINUS_30_DAYS_FROM_YESTERDAY.toISOString(),
  ipStatusDateTo: DAY_BEFORE_YESTERDAY.toISOString(),

  ipStatusPagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },
  ipStatusFilters: {
    ips: [],
  },
  ipStatusSorting: {
    field: 'date_timestamp',
    order: 'desc',
  },
}

// ipData

const defaultIpDataState = {
  isIpDataLoading: false,
  isIpDataLoaded: false,
  ipData: [],

  isIpDataUserIpsLoading: false,
  ipDataUserIps: [],

  ipDataDateFrom: MINUS_30_DAYS_FROM_YESTERDAY.toISOString(),
  ipDataDateTo: DAY_BEFORE_YESTERDAY.toISOString(),

  ipDataPagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },
  ipDataFilters: {
    ips: [],
    filterResult: [],
  },
  ipDataSorting: {
    field: 'date_timestamp',
    order: 'desc',
  },
}

const defaultState = {
  // mode

  infoMode: SNDS_INFO_MODE.DASHBOARDS,

  // tabs

  ...defaultDashboardsState,

  ...defaultIpStatusState,

  ...defaultIpDataState,  

  errors: {
    dashboards: null,
    ipStatus: null,
    ipData: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    // mode

    case actionsTypes.HANDLE_SNDS_INFO_MODE: {
      return {
        ...state,
        infoMode: payload.data,
      };
    }

    // Dashboards

    case actionsTypes.HANDLE_SNDS_DASHBOARDS_USER_IPS_IS_LOADING: {
      return {
        ...state,
        isDashboardsUserIpsLoading: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_DASHBOARDS_USER_IPS: {
      return {
        ...state,
        dashboardsUserIps: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_DASHBOARDS_DATA_IS_LOADING: {
      return {
        ...state,
        isDashboardsDataLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_DASHBOARDS_DATA_IS_LOADED: {
      return {
        ...state,
        isDashboardsDataLoaded: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_DASHBOARDS_DATA: {
      return {
        ...state,
        dashboardsData: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_DASHBOARDS_DATE_FROM: {
      return {
        ...state,
        dashboardsDateFrom: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_DASHBOARDS_DATE_TO: {
      return {
        ...state,
        dashboardsDateTo: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_DASHBOARDS_FILTER: {
      const { field, value } = payload.data;

      return {
        ...state,
        dashboardsFilters: {
          ...state.dashboardsFilters,
          [field]: value,
        },
      };
    }

    // IP Status

    case actionsTypes.HANDLE_SNDS_IP_STATUS_USER_IPS_IS_LOADING: {
      return {
        ...state,
        isIpStatusUserIpsLoading: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_IP_STATUS_USER_IPS: {
      return {
        ...state,
        ipStatusUserIps: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_STATUS_IS_LOADING: {
      return {
        ...state,
        isIpStatusLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_STATUS_IS_LOADED: {
      return {
        ...state,
        isIpStatusLoaded: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_IP_STATUS: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;

      return {
        ...state,
        ipStatus: data,
        ipStatusPagination: { count, offset, limit },
        errors: {
          ...state.errors,
          ipStatus: null,
        },
      };
    }

    case actionsTypes.SET_SNDS_IP_STATUS_DATE_FROM: {
      return {
        ...state,
        ipStatusDateFrom: payload.data,
      };
    }
    
    case actionsTypes.SET_SNDS_IP_STATUS_DATE_TO: {
      return {
        ...state,
        ipStatusDateTo: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_IP_STATUS_PAGINATION: {
      return {
        ...state,
        ipStatusPagination: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_STATUS_FILTER: {
      const { field, value } = payload.data;

      return {
        ...state,
        ipStatusFilters: {
          ...state.ipStatusFilters,
          [field]: value,
        },
        ipStatusPagination: {
          ...state.ipStatusPagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_STATUS_SORTING: {
      const { field, order } = payload.data;

      return {
        ...state,
        ipStatusSorting: {
          field,
          order,
        },
        ipStatusPagination: {
          ...state.ipStatusPagination,
          offset: 0,
        },
      };
    }

    // IP Data

    case actionsTypes.HANDLE_SNDS_IP_DATA_USER_IPS_IS_LOADING: {
      return {
        ...state,
        isIpDataUserIpsLoading: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_IP_DATA_USER_IPS: {
      return {
        ...state,
        ipDataUserIps: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_DATA_IS_LOADING: {
      return {
        ...state,
        isIpDataLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_DATA_IS_LOADED: {
      return {
        ...state,
        isIpDataLoaded: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_IP_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;

      return {
        ...state,
        ipData: data,
        ipDataPagination: { count, offset, limit },
        errors: {
          ...state.errors,
          ipData: null,
        },
      };
    }

    case actionsTypes.SET_SNDS_IP_DATA_DATE_FROM: {
      return {
        ...state,
        ipDataDateFrom: payload.data,
      };
    }
    
    case actionsTypes.SET_SNDS_IP_DATA_DATE_TO: {
      return {
        ...state,
        ipDataDateTo: payload.data,
      };
    }

    case actionsTypes.SET_SNDS_IP_DATA_PAGINATION: {
      return {
        ...state,
        ipDataPagination: payload.data,
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_DATA_FILTER: {
      const { field, value } = payload.data;

      return {
        ...state,
        ipDataFilters: {
          ...state.ipDataFilters,
          [field]: value,
        },
        ipDataPagination: {
          ...state.ipDataPagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.HANDLE_SNDS_IP_DATA_SORTING: {
      const { field, order } = payload.data;

      return {
        ...state,
        ipDataSorting: {
          field,
          order,
        },
        ipDataPagination: {
          ...state.ipDataPagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.CLEAR_SNDS_ALL_DATA: {
      return {
        ...defaultState,
      };
    }

    case actionsTypes.SET_SNDS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
