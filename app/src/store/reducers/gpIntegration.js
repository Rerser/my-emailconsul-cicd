import actionsTypes from 'store/actionsTypes';
import { YESTERDAY } from 'utils/constants';

const defaultState = {
  userIntegratedIsLoading: false,
  userIntegrated: false,

  gpSecretsIsShowSlider: false,
  gpSecrets: '',
  gpSecretsIsLoading: false,

  gpDomainsHistoryIsShowSlider: false,
  gpDomainsHistoryIsLoading: false,
  gpDomainsHistoryIsLoaded: true,
  gpDomainsHistory: [],

  gpDateRunIsShowSlider: false,
  gpDateRun: YESTERDAY.toISOString(),
  gpDateRunIsLoading: false,

  errors: {
    userIntegration: null,
    gpDomainsHistory: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.SET_GP_USER_INTEGRATION_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    case actionsTypes.HANDLE_GP_USER_INTEGRATED_IS_LOADING: {
      return {
        ...state,
        userIntegratedIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_GP_USER_INTEGRATED: {
      return {
        ...state,
        userIntegrated: payload.data,
      };
    }

    case actionsTypes.SET_GP_SECRETS_IS_SHOW_SLIDER: {
      return {
        ...state,
        gpSecretsIsShowSlider: payload.data,
      };
    }

    case actionsTypes.SET_GP_SECRETS: {
      return {
        ...state,
        gpSecrets: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_SECRETS_IS_LOADING: {
      return {
        ...state,
        gpSecretsIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_GP_DOMAINS_HISTORY_IS_SHOW_SLIDER: {
      return {
        ...state,
        gpDomainsHistoryIsShowSlider: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_DOMAINS_HISTORY_IS_LOADING: {
      return {
        ...state,
        gpDomainsHistoryIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_DOMAINS_HISTORY_IS_LOADED: {
      return {
        ...state,
        gpDomainsHistoryIsLoaded: payload.data,
      };
    }

    case actionsTypes.SET_GP_DOMAINS_HISTORY: {
      return {
        ...state,
        gpDomainsHistory: payload.data,
      };
    }

    case actionsTypes.SET_GP_DATE_RUN_IS_SHOW_SLIDER: {
      return {
        ...state,
        gpDateRunIsShowSlider: payload.data,
      };
    }

    case actionsTypes.SET_GP_DATE_RUN: {
      return {
        ...state,
        gpDateRun: payload.data,
      };
    }

    case actionsTypes.HANDLE_GP_DATE_RUN_IS_LOADING: {
      return {
        ...state,
        gpDateRunIsLoading: payload.data,
      };
    }

    case actionsTypes.CLEAR_GP_INTEGRATION_ALL_DATA: {
      return {
        ...defaultState,
      };
    }

    default:
      return state;
  }
};
