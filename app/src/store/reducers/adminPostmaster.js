import actionsTypes from 'store/actionsTypes';

const defaultState = {
  authUrlIsLoading: false,  
  authUrl: null,

  errors: {
    authUrl: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_ADMIN_POSTMASTER_AUTH_URL_IS_LOADING: {
      return {
        ...state,
        authUrlIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_ADMIN_POSTMASTER_AUTH_URL: {
      return {
        ...state,
        authUrl: payload.data,
      };
    }

    case actionsTypes.SET_ADMIN_POSTMASTER_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
