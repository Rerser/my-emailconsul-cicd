import actionsTypes from 'store/actionsTypes';
import { NEW_DOMAIN_MODE } from 'utils/constants';

const defaultState = {
  isDataLoading: false,
  data: [],
  pagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },
  filter: {
    domain: null,
  },
  sorting: {
    field: 'domain',
    order: 'asc',
  },

  isOnNewDomain: false,
  newDomainMode: NEW_DOMAIN_MODE.SINGLE,
  newDomains: [null],
  newDomainIsLoading: false,

  domainRblChangesLogsPeriodFrom: '',
  domainRblChangesLogsPeriodTo: '',
  domainRblChangesLogsIsLoading: false,
  domainRblChangesLogs: null,

  newDomainsFile: null,
  newDomainsFileIsUploading: false,
  responseNewDomainsUploadedFile: null,

  domainsAvailableCount: 0,
  domainsAvailableCountIsLoading: false,

  errors: {
    data: null,
    domainRblChangesLogs: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_DOMAINS_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_DOMAINS_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;
      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_DOMAINS_DATA_PAGINATION: {
      return {
        ...state,
        pagination: payload.data,
      };
    }

    case actionsTypes.HANDLE_DOMAINS_DATA_FILTER: {
      const { field, value } = payload.data;
      return {
        ...state,
        filter: {
          ...state.filter,
          [field]: value,
        },
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.HANDLE_DOMAINS_DATA_SORTING: {
      const { field, order } = payload.data;
      return {
        ...state,
        sorting: {
          field,
          order,
        },
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.SET_IS_ON_NEW_DOMAIN: {
      const newState = {
        ...state,
        isOnNewDomain: payload.data,
      };

      if (!payload.data) {
        newState.newDomains = [null];
        newState.newDomainMode = NEW_DOMAIN_MODE.SINGLE;
      }

      return {
        ...newState,
      };
    }

    case actionsTypes.HANDLE_NEW_DOMAINS: {
      const { value, index } = payload.data;
      const newDomains = [...state.newDomains];

      newDomains[index] = value;

      return {
        ...state,
        newDomains,
      };
    }

    case actionsTypes.REMOVE_NEW_DOMAIN: {
      const { index } = payload.data;
      const newDomains = [...state.newDomains];

      newDomains.splice(index, 1);

      return {
        ...state,
        newDomains,
      };
    }

    case actionsTypes.HANDLE_NEW_DOMAIN_IS_LOADING: {
      return {
        ...state,
        newDomainIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_DOMAINS_AVAILABLE_COUNT: {
      return {
        ...state,
        domainsAvailableCount: payload.data,
      };
    }

    case actionsTypes.HANDLE_DOMAINS_AVAILABLE_COUNT_IS_LOADING: {
      return {
        ...state,
        domainsAvailableCountIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_DOMAIN_MODE: {
      return {
        ...state,
        newDomainMode: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_DOMAINS_FILE_IS_UPLOADING: {
      return {
        ...state,
        newDomainsFileIsUploading: payload.data,
      };
    }

    case actionsTypes.SET_NEW_DOMAINS_FILE: {
      return {
        ...state,
        newDomainsFile: payload.data,
      };
    }

    case actionsTypes.SET_RESPONSE_NEW_DOMAINS_UPLOADED_FILE: {
      return {
        ...state,
        responseNewDomainsUploadedFile: payload.data,
      };
    }

    case actionsTypes.SET_DOMAIN_RBL_CHANGES_LOGS_PERIOD_FROM: {
      return {
        ...state,
        domainRblChangesLogsPeriodFrom: payload.data,
      };
    }

    case actionsTypes.SET_DOMAIN_RBL_CHANGES_LOGS_PERIOD_TO: {
      return {
        ...state,
        domainRblChangesLogsPeriodTo: payload.data,
      };
    }

    case actionsTypes.HANDLE_DOMAIN_RBL_CHANGES_LOGS_IS_LOADING: {
      return {
        ...state,
        domainRblChangesLogsIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_DOMAIN_RBL_CHANGES_LOGS: {
      return {
        ...state,
        domainRblChangesLogs: payload.data,
      };
    }

    case actionsTypes.SET_CURRENT_DOMAIN_STATUS: {
      const newData = [...state.data];
      const { index, status } = payload.data;

      newData[index].status = status;

      return {
        ...state,
        data: newData,
      };
    }

    case actionsTypes.SET_DOMAINS_RBL_CHANGES_LOGS_ERRORS:
    case actionsTypes.SET_DOMAINS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
