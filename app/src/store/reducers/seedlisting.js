import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isDataLoading: false,
  data: [],
  pagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },

  newSeedlisting: null,
  newSeedlistingIsCreating: false,
  showedInfo: null,

  errors: {
    data: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_SEEDLISTING_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;
      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_SEEDLISTING_DATA_PAGINATION: {
      return {
        ...state,
        pagination: payload.data,
      };
    }

    case actionsTypes.SET_NEW_SEEDLISTING: {
      return {
        ...state,
        newSeedlisting: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_SEEDLISTING: {
      const { field, value } = payload.data;
      return {
        ...state,
        newSeedlisting: {
          ...state.newSeedlisting,
          [field]: value,
        },
      };
    }

    case actionsTypes.HANDLE_NEW_SEEDLISTING_IS_CREATING: {
      return {
        ...state,
        newSeedlistingIsCreating: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_SHOWED_INFO: {
      return {
        ...state,
        showedInfo: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
