import actionsTypes from 'store/actionsTypes';

const defaultState = {
  data: [],
  pagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },
  isDataLoading: false,
  sorting: {
    field: 'status',
    order: 'desc',
    sortFields: ['email', 'host', 'worker_container', 'status'],
  },

  newSeedlistingWorker: null,
  newSeedlistingWorkerSavingIsLoading: false,

  hosts: [],
  hostsIsLoading: false,

  errors: {
    data: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_SEEDLISTING_WORKERS_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_WORKERS_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;
      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_SEEDLISTING_WORKERS_DATA_PAGINATION: {
      return {
        ...state,
        pagination: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_WORKERS_SORTING: {
      return {
        ...state,
        sorting: payload.data,
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.SET_NEW_SEEDLISTING_WORKER: {
      return {
        ...state,
        newSeedlistingWorker: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_SEEDLISTING_WORKER: {
      const { field, value } = payload.data;
      return {
        ...state,
        newSeedlistingWorker: {
          ...state.newSeedlistingWorker,
          [field]: value,
        },
      };
    }

    case actionsTypes.HANDLE_NEW_SEEDLISTING_WORKER_SAVING_IS_LOADING: {
      return {
        ...state,
        newSeedlistingWorkerSavingIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_WORKERS_HOSTS: {
      return {
        ...state,
        hosts: payload.data,
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_WORKERS_HOSTS_IS_LOADING: {
      return {
        ...state,
        hostsIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_WORKERS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
