import actionsTypes from 'store/actionsTypes';

const defaultState = {
  data: null,
  isDataLoading: false,

  editedUserBillingPlanUserId: false,

  hosts: null,
  maxEmailsPerIteration: null,
  generalByPlanIsLoading: false,

  general: null,
  generalIsLoading: false,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.SET_USER_BILLING_PLAN_DATA: {
      return {
        ...state,
        data: payload.data,
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_IS_DATA_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_EDITED_USER_BILLING_PLAN_USER_ID: {
      return {
        ...state,
        editedUserBillingPlanUserId: payload.data,
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_DATA: {
      const { field, value } = payload.data;
      return {
        ...state,
        data: {
          ...state.data,
          [field]: value,
        },
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_LISTCLEANING: {
      const { field, value } = payload.data;
      return {
        ...state,
        data: {
          ...state.data,
          listcleaning: {
            ...state.data.listcleaning,
            [field]: value,
          },
        },
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_SEEDLISTING: {
      const { field, value } = payload.data;
      return {
        ...state,
        data: {
          ...state.data,
          seedlisting: {
            ...state.data.seedlisting,
            [field]: value,
          },
        },
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_IPS: {
      const { field, value } = payload.data;
      return {
        ...state,
        data: {
          ...state.data,
          ips: {
            ...state.data.ips,
            [field]: value,
          },
        },
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_DOMAINS: {
      const { field, value } = payload.data;
      return {
        ...state,
        data: {
          ...state.data,
          domains: {
            ...state.data.domains,
            [field]: value,
          },
        },
      };
    }

    case actionsTypes.SET_USER_BILLING_PLAN_HOSTS: {
      return {
        ...state,
        hosts: payload.data,
      };
    }

    case actionsTypes.SET_USER_BILLING_PLAN_MAX_EMAILS_PER_ITERATION: {
      return {
        ...state,
        maxEmailsPerIteration: payload.data,
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_GENERAL_BY_PLAN_IS_LOADING: {
      return {
        ...state,
        generalByPlanIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_USER_BILLING_PLAN_GENERAL: {
      return {
        ...state,
        general: payload.data,
      };
    }

    case actionsTypes.HANDLE_USER_BILLING_PLAN_GENERAL_IS_LOADING: {
      return {
        ...state,
        generalIsLoading: payload.data,
      };
    }

    default:
      return state;
  }
};
