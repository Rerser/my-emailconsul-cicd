import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isDataLoading: false,
  data: null,
  isOnEdit: false,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_IPS_REPORT_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_IPS_REPORT_DATA: {
      return {
        ...state,
        data: payload.data,
      };
    }

    case actionsTypes.HANDLE_IPS_REPORT_DATA: {
      const { field, value } = payload.data;
      return {
        ...state,
        data: {
          ...state.data,
          [field]: value,
        },
      };
    }

    case actionsTypes.HANDLE_IPS_REPORT_IS_ON_EDIT: {
      return {
        ...state,
        isOnEdit: payload.data,
      };
    }

    default:
      return state;
  }
};
