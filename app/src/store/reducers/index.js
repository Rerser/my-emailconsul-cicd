import { combineReducers } from 'redux';

import listcleaning from './listcleaning';
import auth from './auth';
import users from './users';
import userBillingPlan from './userBillingPlan';
import myUserBillingPlan from './myUserBillingPlan';
import seedlisting from './seedlisting';
import seedlistingItem from './seedlistingItem';
import seedlistingWorkers from './seedlistingWorkers';
import payment from './payment';
import locale from './locale';
import ips from './ips';
import domains from './domains';
import ipsReport from './ipsReport';
import netmasks from './netmasks';
import confirmationPopup from './confirmationPopup';
import userLogs from './userLogs';
// import adminPostmaster from './adminPostmaster';
import googlePostmaster from './googlePostmaster';
import gpIntegration from './gpIntegration';
import contextMenu from './contextMenu';
import spf from './spf';
import dkim from './dkim';
import dmarc from './dmarc';
import snds from './snds';
import sndsIntegration from './sndsIntegration';

export default combineReducers({
  listcleaning,
  auth,
  users,
  userBillingPlan,
  myUserBillingPlan,
  seedlisting,
  seedlistingItem,
  seedlistingWorkers,
  payment,
  locale,
  ips,
  ipsReport,
  domains,
  netmasks,
  confirmationPopup,
  userLogs,
  // adminPostmaster,
  googlePostmaster,
  gpIntegration,
  contextMenu,
  spf,
  dkim,
  dmarc,
  snds,
  sndsIntegration,
});
