import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isDataLoading: false,
  data: [],
  errors: {
    data: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_NETMASKS_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_NETMASKS_DATA: {
      return {
        ...state,
        data: payload.data,
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_NETMASKS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
