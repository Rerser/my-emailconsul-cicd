import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isShow: false,
  title: null,
  callback: null,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_CONFIRMATION_POPUP_SHOW: {
      return {
        ...state,
        isShow: payload.data,
      };
    }

    case actionsTypes.SET_CONFIRMATION_POPUP_TITLE: {
      return {
        ...state,
        title: payload.data,
      };
    }

    case actionsTypes.SET_CONFIRMATION_POPUP_CALLBACK: {
      return {
        ...state,
        callback: payload.data,
      };
    }

    default:
      return state;
  }
};
