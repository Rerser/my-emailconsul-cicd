import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isDataLoading: false,
  data: [],
  // sorting: {
  //   field: 'name',
  //   order: 'desc',
  //   sortFields: ['name'],
  // },
  newListCleaning: null,
  newListCleaningFileIsUploading: false,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_LISTCLEANING_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_LISTCLEANING_DATA: {
      return {
        ...state,
        data: payload.data,
      };
    }

    // case actionsTypes.SET_FILES_SORT: {
    //   return {
    //     ...state,
    //     sorting: action.payload.data,
    //   }
    // }

    case actionsTypes.SET_NEW_LISTCLEANING: {
      return {
        ...state,
        newListCleaning: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_LISTCLEANING_FILE: {
      return {
        ...state,
        newListCleaning: {
          ...state.newListCleaning,
          file: payload.data,
        },
      };
    }

    case actionsTypes.HANDLE_NEW_LISTCLEANING_FILE_IS_UPLOADING: {
      return {
        ...state,
        newListCleaningFileIsUploading: payload.data,
      };
    }

    default:
      return state;
  }
};
