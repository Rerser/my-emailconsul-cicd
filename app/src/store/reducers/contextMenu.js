import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isShow: false,

  coords: {
    x: 0,
    y: 0,
  }
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_CONTEXT_MENU_SHOW: {
      return {
        ...state,
        isShow: payload.data,
      };
    }

    case actionsTypes.SET_CONTEXT_MENU_COORDS: {
      return {
        ...state,
        coords: payload.data,
      };
    }

    default:
      return state;
  }
};
