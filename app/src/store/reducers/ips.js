import actionsTypes from 'store/actionsTypes';
import { NEW_IP_MODE } from 'utils/constants';

const defaultState = {
  isDataLoading: false,
  data: [],
  pagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },
  filter: {
    netmask: null,
    ip: null,
  },
  sorting: {
    field: 'ip',
    order: 'asc',
  },

  editedIp: null, // redundant, is not using now
  editedIpIsLoading: false, // redundant, is not using now

  isOnNewIp: false,
  newIps: [null],
  newNetmask: null,
  newIpMode: NEW_IP_MODE.SINGLE,
  newIpIsLoading: false,

  ipRblChangesLogsPeriodFrom: '',
  ipRblChangesLogsPeriodTo: '',
  ipRblChangesLogsIsLoading: false,
  ipRblChangesLogs: null,

  newIpsFile: null,
  newIpsFileIsUploading: false,
  responseNewIpsUploadedFile: null,

  ipsAvailableCount: 0,
  ipsAvailableCountIsLoading: false,

  errors: {
    data: null,
    ipRblChangesLogs: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_IPS_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_IPS_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;
      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_IPS_DATA_PAGINATION: {
      return {
        ...state,
        pagination: payload.data,
      };
    }

    case actionsTypes.HANDLE_IPS_DATA_FILTER: {
      const { field, value } = payload.data;
      return {
        ...state,
        filter: {
          ...state.filter,
          [field]: value,
        },
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.HANDLE_IPS_DATA_SORTING: {
      const { field, order } = payload.data;
      return {
        ...state,
        sorting: {
          field,
          order,
        },
        pagination: {
          ...state.pagination,
          offset: 0,
        },
      };
    }

    case actionsTypes.SET_EDITED_IP: {
      // redundant, is not using now
      return {
        ...state,
        editedIp: payload.data,
      };
    }

    case actionsTypes.HANDLE_EDITED_IP: {
      // redundant, is not using now
      const { field, value } = payload.data;
      return {
        ...state,
        editedIp: {
          ...state.editedIp,
          [field]: value,
        },
      };
    }

    case actionsTypes.HANDLE_EDITED_IP_IS_LOADING: {
      return {
        ...state,
        editedIpIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_IS_ON_NEW_IP: {
      const newState = {
        ...state,
        isOnNewIp: payload.data,
      };

      if (!payload.data) {
        newState.newIps = [null];
        newState.newNetmask = null;
        newState.newIpMode = NEW_IP_MODE.SINGLE;
      }

      return {
        ...newState,
      };
    }

    case actionsTypes.HANDLE_NEW_IPS: {
      const { value, index } = payload.data;
      const newIps = [...state.newIps];

      newIps[index] = value;

      return {
        ...state,
        newIps,
      };
    }

    case actionsTypes.HANDLE_NEW_NETMASK: {
      return {
        ...state,
        newNetmask: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_IP_MODE: {
      return {
        ...state,
        newIpMode: payload.data,
      };
    }

    case actionsTypes.REMOVE_NEW_IP: {
      const { index } = payload.data;
      const newIps = [...state.newIps];

      newIps.splice(index, 1);

      return {
        ...state,
        newIps,
      };
    }

    case actionsTypes.HANDLE_NEW_IP_IS_LOADING: {
      return {
        ...state,
        newIpIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_IPS_AVAILABLE_COUNT: {
      return {
        ...state,
        ipsAvailableCount: payload.data,
      };
    }

    case actionsTypes.HANDLE_IPS_AVAILABLE_COUNT_IS_LOADING: {
      return {
        ...state,
        ipsAvailableCountIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_NEW_IPS_FILE_IS_UPLOADING: {
      return {
        ...state,
        newIpsFileIsUploading: payload.data,
      };
    }

    case actionsTypes.SET_NEW_IPS_FILE: {
      return {
        ...state,
        newIpsFile: payload.data,
      };
    }

    case actionsTypes.SET_RESPONSE_NEW_IPS_UPLOADED_FILE: {
      return {
        ...state,
        responseNewIpsUploadedFile: payload.data,
      };
    }

    case actionsTypes.SET_IP_RBL_CHANGES_LOGS_PERIOD_FROM: {
      return {
        ...state,
        ipRblChangesLogsPeriodFrom: payload.data,
      };
    }

    case actionsTypes.SET_IP_RBL_CHANGES_LOGS_PERIOD_TO: {
      return {
        ...state,
        ipRblChangesLogsPeriodTo: payload.data,
      };
    }

    case actionsTypes.HANDLE_IP_RBL_CHANGES_LOGS_IS_LOADING: {
      return {
        ...state,
        ipRblChangesLogsIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_IP_RBL_CHANGES_LOGS: {
      return {
        ...state,
        ipRblChangesLogs: payload.data,
      };
    }

    case actionsTypes.SET_CURRENT_IP_STATUS: {
      const newData = [...state.data];
      const { index, status } = payload.data;

      newData[index].status = status;

      return {
        ...state,
        data: newData,
      };
    }

    case actionsTypes.SET_IP_RBL_CHANGES_LOGS_ERRORS:
    case actionsTypes.SET_IPS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
