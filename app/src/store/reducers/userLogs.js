import actionsTypes from 'store/actionsTypes';

const defaultState = {
  data: [],
  isDataLoading: false,

  pagination: {
    count: 0,
    offset: 0,
    limit: 30,
  },

  errors: {
    data: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.SET_USER_LOGS_DATA: {
      const {
        data,
        meta: { count = 0, offset = 0, limit = 30 } = {},
      } = payload.data;
      return {
        ...state,
        data,
        pagination: { count, offset, limit },
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.HANDLE_USER_LOGS_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_USER_LOGS_DATA_PAGINATION: {
      return {
        ...state,
        pagination: payload.data,
      };
    }

    case actionsTypes.SET_USER_LOGS_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    default:
      return state;
  }
};
