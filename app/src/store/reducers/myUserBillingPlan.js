import actionsTypes from 'store/actionsTypes';

const defaultState = {
  data: null,
  isDataLoading: false,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.SET_MY_USER_BILLING_PLAN_DATA: {
      return {
        ...state,
        data: payload.data,
      };
    }

    case actionsTypes.HANDLE_MY_USER_BILLING_PLAN_IS_DATA_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    default:
      return state;
  }
};
