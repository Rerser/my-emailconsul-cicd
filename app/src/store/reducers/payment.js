import actionsTypes from 'store/actionsTypes';
import { PAYMENT_CURRENCY, BILLING_PLAN_TYPE } from 'utils/constants';

const defaultState = {
  liqpayNewPaymentData: null,
  liqpayNewPaymentDataIsLoading: false,

  selectedPaymentBillingPlan: null,
  selectedPaymentBillingPlanType: BILLING_PLAN_TYPE.YEARLY,
  selectedPaymentCurrency: PAYMENT_CURRENCY.USD,

  exchangeData: null,
  exchangeDataIsLoading: false,

  onGoPaymentParams: {
    seedlistingEmailsAvailable: null,
  },
  onGoPaymentParamsIsEdit: false,
  showWarningPopup: false,
  showSuccessPopup: false,

  wireTransferPaymentDataIsSend: false,
  wireTransferPaymentDataIsSended: false,

  deliverabilityConsultingDataIsSending: false,
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.SET_LIQPAY_NEW_PAYMENT_DATA: {
      return {
        ...state,
        liqpayNewPaymentData: payload.data,
      };
    }

    case actionsTypes.HANDLE_LIQPAY_NEW_PAYMENT_DATA_IS_LOADING: {
      return {
        ...state,
        liqpayNewPaymentDataIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_SELECTED_PAYMENT_BILLING_PLAN: {
      return {
        ...state,
        selectedPaymentBillingPlan: payload.data,
      };
    }

    case actionsTypes.SET_SELECTED_PAYMENT_BILLING_PLAN_TYPE: {
      return {
        ...state,
        selectedPaymentBillingPlanType: payload.data,
      };
    }

    case actionsTypes.SET_SELECTED_PAYMENT_CURRENCY: {
      return {
        ...state,
        selectedPaymentCurrency: payload.data,
      };
    }

    case actionsTypes.SET_EXCHANGE_DATA: {
      return {
        ...state,
        exchangeData: payload.data,
      };
    }

    case actionsTypes.HANDLE_EXCHANGE_DATA_IS_LOADING: {
      return {
        ...state,
        exchangeDataIsLoading: payload.data,
      };
    }

    case actionsTypes.SET_ON_GO_PAYMENT_PARAMS: {
      return {
        ...state,
        onGoPaymentParams: payload.data,
      };
    }

    case actionsTypes.HANDLE_ON_GO_PAYMENT_PARAMS_IS_EDIT: {
      return {
        ...state,
        onGoPaymentParamsIsEdit: payload.data,
      };
    }

    case actionsTypes.HANDLE_SHOW_WARNING_POPUP: {
      return {
        ...state,
        showWarningPopup: payload.data,
      };
    }

    case actionsTypes.HANDLE_SHOW_SUCCESS_POPUP: {
      return {
        ...state,
        showSuccessPopup: payload.data,
      };
    }

    case actionsTypes.HANDLE_WIRE_TRANSFER_PAYMENT_DATA_IS_SEND: {
      return {
        ...state,
        wireTransferPaymentDataIsSend: payload.data,
      };
    }

    case actionsTypes.HANDLE_WIRE_TRANSFER_PAYMENT_DATA_IS_SENDED: {
      return {
        ...state,
        wireTransferPaymentDataIsSended: payload.data,
      };
    }

    case actionsTypes.HANDLE_DELIVERABILITY_CONSULTING_DATA_IS_SENDING: {
      return {
        ...state,
        deliverabilityConsultingDataIsSending: payload.data,
      };
    }

    default:
      return state;
  }
};
