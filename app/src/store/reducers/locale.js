import actionsTypes from 'store/actionsTypes';
import { getLocale } from 'utils/helpers';

const defaultState = {
  current: getLocale(),
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.LOCALE_SET_CURRENT: {
      return {
        ...state,
        current: payload.data,
      };
    }

    default:
      return state;
  }
};
