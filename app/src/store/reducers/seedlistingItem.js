import actionsTypes from 'store/actionsTypes';

const defaultState = {
  isDataLoading: false,
  data: null,

  isOnShareableLink: false,
  shareableLinkData: null,
  shareableLinkDataIsLoading: false,

  isLoadingEmailInfo: false,
  emailInfo: null,
  emailInfoId: null,

  isLoadingEmailInfoEpcIpData: false,
  emailInfoEpcIpData: null,

  isLoadingEmailInfoEpcDomainData: false,
  emailInfoEpcDomainData: null,

  isLoadingEmailInfoSpamAssasinData: false,
  emailInfoSpamAssasinData: null,

  isLoadingEmailInfoSPFData: false,
  emailInfoSPFData: null,

  isLoadingEmailInfoDKIMData: false,
  emailInfoDKIMData: {
    textual_representation: null,
    signature_header: null,
  },

  isLoadingEmailInfoDMARCData: false,
  emailInfoDMARCData: null,

  errors: {
    data: null,
    emailInfoData: null,
    emailInfoIdData: null,
    epcIpData: null,
    epcDomainData: null,
    spamAssasinData: null,
    SPFData: null,
    DKIMData: null,
    DMARCData: null,
  },
};

export default (state = defaultState, { type, payload }) => {
  switch (type) {
    case actionsTypes.HANDLE_SEEDLISTING_ITEM_DATA_IS_LOADING: {
      return {
        ...state,
        isDataLoading: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_DATA: {
      return {
        ...state,
        data: payload.data,
        errors: {
          ...state.errors,
          data: null,
        },
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_SHAREABLE_LINK_IS_ON: {
      return {
        ...state,
        isOnShareableLink: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_SHAREABLE_LINK_DATA: {
      return {
        ...state,
        shareableLinkData: payload.data,
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_SHAREABLE_LINK_IS_LOADING: {
      return {
        ...state,
        shareableLinkDataIsLoading: payload.data,
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO: {
      return {
        ...state,
        isLoadingEmailInfo: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO: {
      return {
        ...state,
        emailInfo: payload.data,
        errors: {
          ...state.errors,
          emailInfoData: null,
        },
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_ID: {
      return {
        ...state,
        emailInfoId: payload.data,
        errors: {
          ...state.errors,
          emailInfoIdData: null,
        },
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_EPC_IP_DATA: {
      return {
        ...state,
        isLoadingEmailInfoEpcIpData: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_EPC_IP_DATA: {
      return {
        ...state,
        emailInfoEpcIpData: payload.data,
        errors: {
          ...state.errors,
          epcIpData: null,
        },
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_EPC_DOMAIN_DATA: {
      return {
        ...state,
        isLoadingEmailInfoEpcDomainData: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_EPC_DOMAIN_DATA: {
      return {
        ...state,
        emailInfoEpcDomainData: payload.data,
        errors: {
          ...state.errors,
          epcDomainData: null,
        },
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_SPAMASSASIN_DATA: {
      return {
        ...state,
        isLoadingEmailInfoSpamAssasinData: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_SPAMASSASIN_DATA: {
      return {
        ...state,
        emailInfoSpamAssasinData: payload.data,
        errors: {
          ...state.errors,
          spamAssasinData: null,
        },
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_SPF_DATA: {
      return {
        ...state,
        isLoadingEmailInfoSPFData: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_SPF_DATA: {
      return {
        ...state,
        emailInfoSPFData: payload.data,
        errors: {
          ...state.errors,
          SPFData: null,
        },
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_DKIM_DATA: {
      return {
        ...state,
        isLoadingEmailInfoDKIMData: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_DKIM_TEXTUAL_REPRESENTATION_DATA: {
      return {
        ...state,
        emailInfoDKIMData: {
          ...state.emailInfoDKIMData, 
          textual_representation: payload.data,
        },
        errors: {
          ...state.errors,
          DKIMData: null,
        },
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_DKIM_SIGNATURE_HEADER_DATA: {
      return {
        ...state,
        emailInfoDKIMData: {
          ...state.emailInfoDKIMData, 
          signature_header: payload.data,
        },
        errors: {
          ...state.errors,
          DKIMData: null,
        },
      };
    }

    case actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_DMARC_DATA: {
      return {
        ...state,
        isLoadingEmailInfoDMARCData: payload.data,
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_DMARC_DATA: {
      return {
        ...state,
        emailInfoDMARCData: payload.data,
        errors: {
          ...state.errors,
          DMARCData: null,
        },
      };
    }

    case actionsTypes.SET_SEEDLISTING_ITEM_ERRORS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          ...payload.data,
        },
      };
    }

    case actionsTypes.CLEAR_SEEDLISTING_ITEM_EMAIL_INFO_ALL_DATA: {
      return {
        ...defaultState,
      };
    }    

    default:
      return state;
  }
};
