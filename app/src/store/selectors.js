import { createSelector } from 'reselect';
import { PAYMENT_CURRENCY, BILLING_PLAN_NAME, BILLING_PLAN_TYPE } from 'utils/constants';

// listcleaning
export const listcleaningDataSelector = (state) => state.listcleaning.data;
export const listcleaningIsDataLoadingSelector = (state) => state.listcleaning.isDataLoading;
export const newListCleaningSelector = (state) => state.listcleaning.newListCleaning;
export const newListCleaningFileIsUploadingSelector = (state) => state.listcleaning.newListCleaningFileIsUploading;

// auth
export const authEmailSelector = (state) => state.auth.email;
export const authPasswordSelector = (state) => state.auth.password;
export const authConfirmPasswordSelector = (state) => state.auth.confirmPassword;
export const authIsRecaptchaVerifiedSelector = (state) => state.auth.isRecaptchaVerified;
export const authTokenSelector = (state) => state.auth.token;
export const authIsLoadingSelector = (state) => state.auth.isLoading;
export const authActivationResultStatusSelector = (state) => state.auth.activationResultStatus;
export const authCurrentPasswordSelector = (state) => state.auth.currentPassword;
export const authNewPasswordSelector = (state) => state.auth.newPassword;
export const authConfirmNewPasswordSelector = (state) => state.auth.confirmNewPassword;
export const authSwitchedUserSelector = (state) => state.auth.switchedUser;
export const authSwitchedUserIsLoadingSelector = (state) => state.auth.switchedUserIsLoading;
export const authIsSwitchedUserModeSelector = (state) => state.auth.isSwitchedUserMode;

// users
export const usersMeSelector = (state) => state.users.me;
export const usersMeIsLoadingSelector = (state) => state.users.meIsLoading;
export const usersDataSelector = (state) => state.users.data;
export const usersSortingSelector = (state) => state.users.sorting;
export const usersPaginationSelector = (state) => state.users.pagination;
export const usersIsDataLoadingSelector = (state) => state.users.isDataLoading;
export const usersEditedUserSelector = (state) => state.users.editedUser;
export const usersEditedUserSavingIsLoadingSelector = (state) => state.users.editedUserSavingIsLoading;
export const usersErrorsSelector = (state) => state.users.errors;

// user logs
export const userLogsSelector = (state) => state.userLogs.data;
export const userLogsIsLoadingSelector = (state) => state.userLogs.isDataLoading;
export const userLogsPaginationSelector = (state) => state.userLogs.pagination;
export const userLogsErrorsSelector = (state) => state.userLogs.errors;

// userBillingPlan
export const userBillingPlanDataSelector = (state) => state.userBillingPlan.data;
export const userBillingPlanIsDataLoadingSelector = (state) => state.userBillingPlan.isDataLoading;
export const editedUserBillingPlanUserIdSelector = (state) => state.userBillingPlan.editedUserBillingPlanUserId;
export const userBillingPlanHostsSelector = (state) => state.userBillingPlan.hosts;
export const userBillingPlanMaxEmailsPerIterationSelector = (state) => state.userBillingPlan.maxEmailsPerIteration;
export const userBillingPlanGeneralByPlanIsLoadingSelector = (state) => state.userBillingPlan.generalByPlanIsLoading;
export const userBillingPlanGeneralSelector = (state) => state.userBillingPlan.general;
export const userBillingPlanGeneralIsLoadingSelector = (state) => state.userBillingPlan.generalIsLoading;

// myUserBillingPlan
export const myUserBillingPlanDataSelector = (state) => state.myUserBillingPlan.data;
export const myUserBillingPlanIsDataLoadingSelector = (state) => state.myUserBillingPlan.isDataLoading;

// seedlisting
export const seedlistingDataSelector = (state) => state.seedlisting.data;
export const seedlistingErrorsSelector = (state) => state.seedlisting.errors;
export const seedlistingDataPaginationSelector = (state) => state.seedlisting.pagination;
export const seedlistingIsDataLoadingSelector = (state) => state.seedlisting.isDataLoading;
export const newSeedlistingSelector = (state) => state.seedlisting.newSeedlisting;
export const newSeedlistingIsCreatingSelector = (state) => state.seedlisting.newSeedlistingIsCreating;
export const newSeedlistingIsCheckedPartlySelector = (state) => state.seedlisting.newSeedlistingIsCheckedPartly;
export const seedlistingShowedInfoSelector = (state) => state.seedlisting.showedInfo;
export const newSeedlistingIsCheckedInfoSelector = createSelector(newSeedlistingSelector, (newSeedlisting) => {
  const hosts = newSeedlisting?.hosts;
  if (hosts !== null && hosts !== undefined) {
    const selectedHosts = hosts.filter((h) => h.selected);
    const disabledHosts = hosts.filter((h) => h?.disabled);
    if (selectedHosts.length === hosts.length - disabledHosts.length) {
      return {
        isCheckedAll: true,
        isCheckedPartly: false,
      };
    }
    if (selectedHosts.length < hosts.length - disabledHosts.length) {
      if (selectedHosts.length > 0) {
        return {
          isCheckedAll: false,
          isCheckedPartly: true,
        };
      }
      return {
        isCheckedAll: false,
        isCheckedPartly: false,
      };
    }
  }
  return {
    isCheckedAll: false,
    isCheckedPartly: false,
  };
});

// seedlistingItem
export const seedlistingItemDataSelector = (state) => state.seedlistingItem.data;
export const seedlistingItemIsDataLoadingSelector = (state) => state.seedlistingItem.isDataLoading;
export const seedlistingItemIsLoadingEmailInfoSelector = (state) => state.seedlistingItem.isLoadingEmailInfo;
export const seedlistingItemEmailInfoSelector = (state) => state.seedlistingItem.emailInfo;
export const seedlistingItemEmailInfoIdSelector = (state) => state.seedlistingItem.emailInfoId;
export const seedlistingItemIsLoadingEpcIpDataEmailInfoSelector = (state) => state.seedlistingItem.isLoadingEmailInfoEpcIpData;
export const seedlistingItemEmailInfoEpcIpDataSelector = (state) => state.seedlistingItem.emailInfoEpcIpData;
export const seedlistingItemIsLoadingEmailInfoEpcDomainDataSelector = (state) => state.seedlistingItem.isLoadingEmailInfoEpcDomainData;
export const seedlistingItemEmailInfoEpcDomainDataSelector = (state) => state.seedlistingItem.emailInfoEpcDomainData;
export const seedlistingItemIsLoadingEmailInfoSpamAssasinDataSelector = (state) => state.seedlistingItem.isLoadingEmailInfoSpamAssasinData;
export const seedlistingItemEmailInfoSpamAssasinDataSelector = (state) => state.seedlistingItem.emailInfoSpamAssasinData;
export const seedlistingItemIsLoadingEmailInfoSPFDataSelector = (state) => state.seedlistingItem.isLoadingEmailInfoSPFData;
export const seedlistingItemEmailInfoSPFDataSelector = (state) => state.seedlistingItem.emailInfoSPFData;
export const seedlistingItemIsLoadingEmailInfoDKIMDataSelector = (state) => state.seedlistingItem.isLoadingEmailInfoDKIMData;
export const seedlistingItemEmailInfoDKIMTextualRepresentationDataSelector = (state) => state.seedlistingItem.emailInfoDKIMData.textual_representation;
export const seedlistingItemEmailInfoDKIMSignatureHeaderDataSelector = (state) => state.seedlistingItem.emailInfoDKIMData.signature_header;
export const seedlistingItemIsLoadingEmailInfoDMARCDataSelector = (state) => state.seedlistingItem.isLoadingEmailInfoDMARCData;
export const seedlistingItemEmailInfoDMARCDataSelector = (state) => state.seedlistingItem.emailInfoDMARCData;
export const seedlistingItemErrorsSelector = (state) => state.seedlistingItem.errors;
export const seedlistingItemIsOnShareableLinkSelector = (state) => state.seedlistingItem.isOnShareableLink;
export const seedlistingItemShareableLinkDataSelector = (state) => state.seedlistingItem.shareableLinkData;
export const seedlistingItemShareableLinkDataIsLoadingSelector = (state) => state.seedlistingItem.shareableLinkDataIsLoading;

// seedlistingWorkers
export const seedlistingWorkersDataSelector = (state) => state.seedlistingWorkers.data;
export const seedlistingWorkersErrorsSelector = (state) => state.seedlistingWorkers.errors;
export const seedlistingWorkersDataPaginationSelector = (state) => state.seedlistingWorkers.pagination;
export const seedlistingWorkersIsDataLoadingSelector = (state) => state.seedlistingWorkers.isDataLoading;
export const seedlistingWorkersSortingSelector = (state) => state.seedlistingWorkers.sorting;
export const seedlistingWorkersNewSeedlistingWorkerSelector = (state) => state.seedlistingWorkers.newSeedlistingWorker;
export const seedlistingWorkersNewSeedlistingWorkerSavingIsLoadingSelector = (state) => state.seedlistingWorkers.newSeedlistingWorkerSavingIsLoading;
export const seedlistingWorkersHostsSelector = (state) => state.seedlistingWorkers.hosts;
export const seedlistingWorkersHostsIsLoadingSelector = (state) => state.seedlistingWorkers.hostsIsLoading;

// payment
export const liqpayNewPaymentDataSelector = (state) => state.payment.liqpayNewPaymentData;
export const liqpayNewPaymentDataIsLoadingSelector = (state) => state.payment.liqpayNewPaymentDataIsLoading;
export const selectedPaymentBillingPlanSelector = (state) => state.payment.selectedPaymentBillingPlan;
export const selectedPaymentBillingPlanTypeSelector = (state) => state.payment.selectedPaymentBillingPlanType;
export const selectedPaymentCurrencySelector = (state) => state.payment.selectedPaymentCurrency;
export const paymentExchangeDataSelector = (state) => state.payment.exchangeData;
export const paymentExchangeDataIsLoadingSelector = (state) => state.payment.exchangeDataIsLoading;
export const onGoPaymentParamsSelector = (state) => state.payment.onGoPaymentParams;
export const onGoPaymentParamsIsEditSelector = (state) => state.payment.onGoPaymentParamsIsEdit;
export const showWarningPopupSelector = (state) => state.payment.showWarningPopup;
export const showSuccessPopupSelector = (state) => state.payment.showSuccessPopup;
export const wireTransferPaymentDataIsSendSelector = (state) => state.payment.wireTransferPaymentDataIsSend;
export const wireTransferPaymentDataIsSendedSelector = (state) => state.payment.wireTransferPaymentDataIsSended;
export const deliverabilityConsultingDataIsSendingSelector = (state) => state.payment.deliverabilityConsultingDataIsSending;

// locale
export const localeCurrentSelector = (state) => state.locale.current;

// ips
export const ipsDataSelector = (state) => state.ips.data;
export const ipsIsDataLoadingSelector = (state) => state.ips.isDataLoading;
export const ipsDataPaginationSelector = (state) => state.ips.pagination;
export const ipsDataFilterSelector = (state) => state.ips.filter;
export const ipsDataSortingSelector = (state) => state.ips.sorting;
export const ipsEditedIpSelector = (state) => state.ips.editedIp; // redundant, is not using now
export const ipsEditedIpIsLoadingSelector = (state) => state.ips.editedIpIsLoading; // redundant, is not using now
export const ipsIsOnNewIpSelector = (state) => state.ips.isOnNewIp;
export const ipsNewIpsSelector = (state) => state.ips.newIps;
export const ipsNewNetmaskSelector = (state) => state.ips.newNetmask;
export const ipsNewIpModeSelector = (state) => state.ips.newIpMode;
export const ipsNewIpIsLoadingSelector = (state) => state.ips.newIpIsLoading;
export const ipsAvailableCountSelector = (state) => state.ips.ipsAvailableCount;
export const ipsAvailableCountIsLoadingSelector = (state) => state.ips.ipsAvailableCountIsLoading;
export const ipsNewIpsFileSelector = (state) => state.ips.newIpsFile;
export const ipsNewIpsFileIsUploadingSelector = (state) => state.ips.newIpsFileIsUploading;
export const ipsResponseNewIpsUploadedFileSelector = (state) => state.ips.responseNewIpsUploadedFile;
export const ipsChosenIpInfoSelector = (state) => state.ips.chosenIpInfo;
export const ipsIpRblChangesLogPeriodFromSelector = (state) => state.ips.ipRblChangesLogsPeriodFrom;
export const ipsIpRblChangesLogPeriodToSelector = (state) => state.ips.ipRblChangesLogsPeriodTo;
export const ipsIpRblChangesLogIsLoadingSelector = (state) => state.ips.ipRblChangesLogsIsLoading;
export const ipsIpRblChangesLogSelector = (state) => state.ips.ipRblChangesLogs;
export const ipsErrorsSelector = (state) => state.ips.errors;

// domains
export const domainsDataSelector = (state) => state.domains.data;
export const domainsIsDataLoadingSelector = (state) => state.domains.isDataLoading;
export const domainsDataPaginationSelector = (state) => state.domains.pagination;
export const domainsDataFilterSelector = (state) => state.domains.filter;
export const domainsDataSortingSelector = (state) => state.domains.sorting;
export const domainsIsOnNewDomainSelector = (state) => state.domains.isOnNewDomain;
export const domainsNewDomainsSelector = (state) => state.domains.newDomains;
export const domainsNewDomainIsLoadingSelector = (state) => state.domains.newDomainIsLoading;
export const domainsAvailableCountSelector = (state) => state.domains.domainsAvailableCount;
export const domainsAvailableCountIsLoadingSelector = (state) => state.domains.domainsAvailableCountIsLoading;
export const domainsNewDomainModeSelector = (state) => state.domains.newDomainMode;
export const domainsNewDomainsFileSelector = (state) => state.domains.newDomainsFile;
export const domainsNewDomainsFileIsUploadingSelector = (state) => state.domains.newDomainsFileIsUploading;
export const domainsResponseNewDomainsUploadedFileSelector = (state) => state.domains.responseNewDomainsUploadedFile;
export const domainsRblChangesLogsPeriodFromSelector = (state) => state.domains.domainRblChangesLogsPeriodFrom;
export const domainsRblChangesLogsPeriodToSelector = (state) => state.domains.domainRblChangesLogsPeriodTo;
export const domainsRblChangesLogsIsLoadingSelector = (state) => state.domains.domainRblChangesLogsIsLoading;
export const domainsRblChangesLogsSelector = (state) => state.domains.domainRblChangesLogs;
export const domainsErrorsSelector = (state) => state.domains.errors;

// ipsReport
export const ipsReportDataSelector = (state) => state.ipsReport.data;
export const ipsReportIsDataLoadingSelector = (state) => state.ipsReport.isDataLoading;
export const ipsReportIsOnEditSelector = (state) => state.ipsReport.isOnEdit;

// netmasks
export const netmasksIsDataLoadingSelector = (state) => state.netmasks.isDataLoading;
export const netmasksDataSelector = (state) => state.netmasks.data;
export const netmasksErrorsSelector = (state) => state.netmasks.errors;

/*
 * Reselect
 */

// userBillingPlan
export const billingPlanGeneralPricesSelector = createSelector(
  userBillingPlanGeneralSelector,
  paymentExchangeDataSelector,
  selectedPaymentCurrencySelector,
  (generalData, exchangeData, currency) => {
    if (generalData && exchangeData && currency) {
      const usdExchangeData = exchangeData.find((item) => item.ccy === PAYMENT_CURRENCY.USD);
      const currentExchangeData = exchangeData.find((item) => item.ccy === currency);

      const juniorPricePerMonth = generalData?.prices[BILLING_PLAN_TYPE.MONTHLY][BILLING_PLAN_NAME.JUNIOR];
      const middlePricePerMonth = generalData?.prices[BILLING_PLAN_TYPE.MONTHLY][BILLING_PLAN_NAME.MIDDLE];
      const seniorPricePerMonth = generalData?.prices[BILLING_PLAN_TYPE.MONTHLY][BILLING_PLAN_NAME.SENIOR];

      const juniorPricePerYearly = generalData?.prices[BILLING_PLAN_TYPE.YEARLY][BILLING_PLAN_NAME.JUNIOR];
      const middlePricePerYearly = generalData?.prices[BILLING_PLAN_TYPE.YEARLY][BILLING_PLAN_NAME.MIDDLE];
      const seniorPricePerYearly = generalData?.prices[BILLING_PLAN_TYPE.YEARLY][BILLING_PLAN_NAME.SENIOR];

      if (currency === PAYMENT_CURRENCY.UAH) {
        return {
          currency,
          data: {
            MONTHLY: {
              [BILLING_PLAN_NAME.JUNIOR]: juniorPricePerMonth * usdExchangeData.buy,
              [BILLING_PLAN_NAME.MIDDLE]: middlePricePerMonth * usdExchangeData.buy,
              [BILLING_PLAN_NAME.SENIOR]: seniorPricePerMonth * usdExchangeData.buy,
              [BILLING_PLAN_NAME.ON_GO]: generalData?.on_go_price_per_email * usdExchangeData.buy,
            },
            YEARLY: {
              [BILLING_PLAN_NAME.JUNIOR]: juniorPricePerYearly * usdExchangeData.buy,
              [BILLING_PLAN_NAME.MIDDLE]: middlePricePerYearly * usdExchangeData.buy,
              [BILLING_PLAN_NAME.SENIOR]: seniorPricePerYearly * usdExchangeData.buy,
              [BILLING_PLAN_NAME.ON_GO]: generalData?.on_go_price_per_email * usdExchangeData.buy,
            },
          },
        };
      }

      return {
        currency,
        data: {
          MONTHLY: {
            [BILLING_PLAN_NAME.JUNIOR]: (juniorPricePerMonth * usdExchangeData.buy) / currentExchangeData.buy,
            [BILLING_PLAN_NAME.MIDDLE]: (middlePricePerMonth * usdExchangeData.buy) / currentExchangeData.buy,
            [BILLING_PLAN_NAME.SENIOR]: (seniorPricePerMonth * usdExchangeData.buy) / currentExchangeData.buy,
            [BILLING_PLAN_NAME.ON_GO]:
              (generalData?.on_go_price_per_email * usdExchangeData.buy) / currentExchangeData.buy,
          },
          YEARLY: {
            [BILLING_PLAN_NAME.JUNIOR]: (juniorPricePerYearly * usdExchangeData.buy) / currentExchangeData.buy,
            [BILLING_PLAN_NAME.MIDDLE]: (middlePricePerYearly * usdExchangeData.buy) / currentExchangeData.buy,
            [BILLING_PLAN_NAME.SENIOR]: (seniorPricePerYearly * usdExchangeData.buy) / currentExchangeData.buy,
            [BILLING_PLAN_NAME.ON_GO]:
              (generalData?.on_go_price_per_email * usdExchangeData.buy) / currentExchangeData.buy,
          },
        },
      };
    }

    return {
      currency,
      data: {
        MONTHLY: {
          [BILLING_PLAN_NAME.JUNIOR]: 0,
          [BILLING_PLAN_NAME.MIDDLE]: 0,
          [BILLING_PLAN_NAME.SENIOR]: 0,
          [BILLING_PLAN_NAME.ON_GO]: 0,
        },
        YEARLY: {
          [BILLING_PLAN_NAME.JUNIOR]: 0,
          [BILLING_PLAN_NAME.MIDDLE]: 0,
          [BILLING_PLAN_NAME.SENIOR]: 0,
          [BILLING_PLAN_NAME.ON_GO]: 0,
        },
      },
    };
  }
);

// payment
export const onGoCalculatedPricePerSeedlistingEmailsAvailableSelector = createSelector(
  billingPlanGeneralPricesSelector,
  onGoPaymentParamsSelector,
  (billingPlanGeneralPrices, onGoPaymentParams) =>   Math.ceil(billingPlanGeneralPrices?.data[BILLING_PLAN_NAME.ON_GO]) *
      +onGoPaymentParams?.seedlistingEmailsAvailable || 0
);

// confirmation popup
export const confirmationPopupIsShowSelector = (state) => state.confirmationPopup.isShow;
export const confirmationPopupTitleSelector = (state) => state.confirmationPopup.title;
export const confirmationPopupCallbackSelector = (state) => state.confirmationPopup.callback;

// admin postmaster
export const adminPostmasterAuthUrlSelector = (state) => state.adminPostmaster.authUrl;
export const adminPostmasterAuthUrlIsLoadingSelector = (state) => state.adminPostmaster.authUrlIsLoading;

// google postmaster
export const googlePostmasterDataSelector = (state) => state.googlePostmaster.data;
export const googlePostmasterIsDataLoadingSelector = (state) => state.googlePostmaster.isDataLoading;
export const googlePostmasterIsDataLoadedSelector = (state) => state.googlePostmaster.isDataLoaded;
export const googlePostmasterDataPaginationSelector = (state) => state.googlePostmaster.pagination;
export const googlePostmasterDataFilterSelector = (state) => state.googlePostmaster.filter;
export const googlePostmasterDataSortingSelector = (state) => state.googlePostmaster.sorting;
export const googlePostmasterIsOnNewDomainSelector = (state) => state.googlePostmaster.isOnNewGPDomain;
export const googlePostmasterNewGPDomainsSelector = (state) => state.googlePostmaster.newGPDomains;
export const googlePostmasterNewGPDomainIsLoadingSelector = (state) => state.googlePostmaster.newGPDomainIsLoading;
export const googlePostmasterErrorsSelector = (state) => state.googlePostmaster.errors;
export const googlePostmasterDateFromSelector = (state) => state.googlePostmaster.dateFrom;
export const googlePostmasterDateToSelector = (state) => state.googlePostmaster.dateTo;
export const googlePostmasterChosenDataSelector = (state) => state.googlePostmaster.chosenGPData;
export const googlePostmasterShowTableModeSelector = (state) => state.googlePostmaster.showTableMode;
export const googlePostmasterDomainsAvailableCountSelector = (state) => state.googlePostmaster.gpDomainsAvailableCount;
export const googlePostmasterAvailableCountIsLoadingSelector = (state) => state.googlePostmaster.gpDomainsAvailableCountIsLoading;
export const googlePostmasterNewDomainModeSelector = (state) => state.googlePostmaster.newGPDomainMode;
export const googlePostmasterNewDomainsFileSelector = (state) => state.googlePostmaster.newGPDomainsFile;
export const googlePostmasterNewDomainsFileIsUploadingSelector = (state) => state.googlePostmaster.newGPDomainsFileIsUploading;
export const googlePostmasterResponseNewDomainsUploadedFileSelector = (state) => state.googlePostmaster.responseNewGPDomainsUploadedFile;

// google postmaster user integration
export const gpIntegrationErrorsSelector = (state) => state.gpIntegration.errors;
export const gpIntegrationUserIntegratedIsLoadingSelector = (state) => state.gpIntegration.userIntegratedIsLoading;
export const gpIntegrationUserIntegratedSelector = (state) => state.gpIntegration.userIntegrated;
export const gpIntegrationGPSecretsIsShowSliderSelector = (state) => state.gpIntegration.gpSecretsIsShowSlider;
export const gpIntegrationGPSecretsSelector = (state) => state.gpIntegration.gpSecrets;
export const gpIntegrationGPSecretsIsLoadingSelector = (state) => state.gpIntegration.gpSecretsIsLoading;
export const gpIntegrationGPDomainsHistoryIsShowSliderSelector = (state) => state.gpIntegration.gpDomainsHistoryIsShowSlider;
export const gpIntegrationGPDomainsHistoryIsLoadingSelector = (state) => state.gpIntegration.gpDomainsHistoryIsLoading;
export const gpIntegrationGPDomainsHistoryIsLoadedSelector = (state) => state.gpIntegration.gpDomainsHistoryIsLoaded;
export const gpIntegrationGPDomainsHistorySelector = (state) => state.gpIntegration.gpDomainsHistory;
export const gpIntegrationGPDateRunIsShowSliderSelector = (state) => state.gpIntegration.gpDateRunIsShowSlider;
export const gpIntegrationGPDateRunSelector = (state) => state.gpIntegration.gpDateRun;
export const gpIntegrationGPDateRunIsLoadingSelector = (state) => state.gpIntegration.gpDateRunIsLoading;

// context menu
export const contextMenuIsShowSelector = (state) => state.contextMenu.isShow;
export const contextMenuCoordsSelector = (state) => state.contextMenu.coords;

// spf
export const spfDataSelector = (state) => state.spf.data;
export const spfDataIsLoadingSelector = (state) => state.spf.isDataLoading;

// dkim
export const dkimDataSelector = (state) => state.dkim.data;
export const dkimDataIsLoadingSelector = (state) => state.dkim.isDataLoading;

// dmarc
export const dmarcDataSelector = (state) => state.dmarc.data;
export const dmarcDataIsLoadingSelector = (state) => state.dmarc.isDataLoading;

// snds
export const sndsInfoModeSelector = (state) => state.snds.infoMode;
export const sndsDashboardsUserIpsIsLoadingSelector = (state) => state.snds.isDashboardsUserIpsLoading;
export const sndsDashboardsUserIpsSelector = (state) => state.snds.dashboardsUserIps;
export const sndsDashboardsDataIsLoadingSelector = (state) => state.snds.isDashboardsDataLoading;
export const sndsDashboardsDataIsLoadedSelector = (state) => state.snds.isDashboardsDataLoaded;
export const sndsDashboardsDataSelector = (state) => state.snds.dashboardsData;
export const sndsDashboardsDateFromSelector = (state) => state.snds.dashboardsDateFrom;
export const sndsDashboardsDateToSelector = (state) => state.snds.dashboardsDateTo;
export const sndsDashboardsFilterSelector = (state) => state.snds.dashboardsFilters;
export const sndsIpStatusUserIpsIsLoadingSelector = (state) => state.snds.isIpStatusUserIpsLoading;
export const sndsIpStatusUserIpsSelector = (state) => state.snds.ipStatusUserIps;
export const sndsIsIpStatusLoadingSelector = (state) => state.snds.isIpStatusLoading;
export const sndsIsIpStatusLoadedSelector = (state) => state.snds.isIpStatusLoaded;
export const sndsIpStatusSelector = (state) => state.snds.ipStatus;
export const sndsIpStatusDateFromSelector = (state) => state.snds.ipStatusDateFrom;
export const sndsIpStatusDateToSelector = (state) => state.snds.ipStatusDateTo;
export const sndsIpStatusPaginationSelector = (state) => state.snds.ipStatusPagination;
export const sndsIpStatusFilterSelector = (state) => state.snds.ipStatusFilters;
export const sndsIpStatusSortingSelector = (state) => state.snds.ipStatusSorting;
export const sndsIpDataUserIpsIsLoadingSelector = (state) => state.snds.isIpDataUserIpsLoading;
export const sndsIpDataUserIpsSelector = (state) => state.snds.ipDataUserIps;
export const sndsIsIpDataLoadingSelector = (state) => state.snds.isIpDataLoading;
export const sndsIsIpDataLoadedSelector = (state) => state.snds.isIpDataLoaded;
export const sndsIpDataSelector = (state) => state.snds.ipData;
export const sndsIpDataDateFromSelector = (state) => state.snds.ipDataDateFrom;
export const sndsIpDataDateToSelector = (state) => state.snds.ipDataDateTo;
export const sndsIpDataPaginationSelector = (state) => state.snds.ipDataPagination;
export const sndsIpDataFilterSelector = (state) => state.snds.ipDataFilters;
export const sndsIpDataSortingSelector = (state) => state.snds.ipDataSorting;
export const sndsErrorsSelector = (state) => state.snds.errors;

// snds user integration
export const sndsIsOnAddKeySelector = (state) => state.sndsIntegration.isOnAddKey;
export const sndsIntegrationIsCredentialsLoadingSelector = (state) => state.sndsIntegration.isCredentialsLoading;
export const sndsIntegrationIsCredentialsDataLoadingSelector = (state) => state.sndsIntegration.isCredentialsDataLoading;
export const sndsIntegrationCredentialsSelector = (state) => state.sndsIntegration.credentials;
export const sndsIntegrationCredentialsKeySelector = (state) => state.sndsIntegration.credentialsKey;
export const sndsIntegrationErrorsSelector = (state) => state.sndsIntegration.errors;
export const sndsIntegrationUserIntegratedIsLoadingSelector = (state) => state.sndsIntegration.userIntegratedIsLoading;
export const sndsIntegrationUserIntegratedSelector = (state) => state.sndsIntegration.userIntegrated;
export const sndsIntegrationSecretsIsShowSliderSelector = (state) => state.sndsIntegration.sndsSecretsIsShowSlider;
export const sndsIntegrationSecretsSelector = (state) => state.sndsIntegration.sndsSecrets;
export const sndsIntegrationSecretsIsLoadingSelector = (state) => state.sndsIntegration.sndsSecretsIsLoading;
export const sndsIntegrationHistoryIsShowSliderSelector = (state) => state.sndsIntegration.sndsHistoryIsShowSlider;
export const sndsIntegrationHistoryIsLoadingSelector = (state) => state.sndsIntegration.sndsHistoryIsLoading;
export const sndsIntegrationHistoryIsLoadedSelector = (state) => state.sndsIntegration.sndsHistoryIsLoaded;
export const sndsIntegrationHistorySelector = (state) => state.sndsIntegration.sndsHistory;
export const sndsIntegrationDateRunIsShowSliderSelector = (state) => state.sndsIntegration.sndsDateRunIsShowSlider;
export const sndsIntegrationDateRunSelector = (state) => state.sndsIntegration.sndsDateRun;
export const sndsIntegrationDateRunIsLoadingSelector = (state) => state.sndsIntegration.sndsDateRunIsLoading;
