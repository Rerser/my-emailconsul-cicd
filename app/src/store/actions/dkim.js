import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
} from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import { getStatusError } from 'utils/helpers';
import messages from 'utils/messages';

export const handleDkimDataIsLoading = (data) => ({
  type: actionsTypes.HANDLE_DKIM_DATA_IS_LOADING,
  payload: {
    data,
  },
});

export const setDkimData = (data) => ({
  type: actionsTypes.SET_DKIM_DATA,
  payload: {
    data,
  },
});

export const setDkimDataError = (data) => ({
  type: actionsTypes.SET_DKIM_DATA_ERROR,
  payload: {
    data,
  },
});

export const getDkimData = (domain, selector) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!domain) {
      return toast.error(messages[locale]['dkim.toast.domain.error']);
    }
    if (!selector) {
      return toast.error(messages[locale]['dkim.toast.selector.error']);
    }
    dispatch(handleDkimDataIsLoading(true));

    const data = await Api.getAuthenticationDKIMTextualRepresentation(domain, selector);
    dispatch(setDkimData(data));

    dispatch(handleDkimDataIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setDkimDataError({
          message: err.message,
          status,
        })
      );
    }
    dispatch(handleDkimDataIsLoading(false));
    return console.error('err', err);
  }
};