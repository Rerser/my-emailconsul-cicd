import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
  sndsIntegrationCredentialsSelector,
} from 'store/selectors';
import { getStatusError } from 'utils/helpers';
import messages from 'utils/messages';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

// errors, clear all data

export const clearSndsIntegrationAllData = () => ({
  type: actionsTypes.CLEAR_SNDS_INTEGRATION_ALL_DATA,
});

export const setSndsUserIntegrationErrors = (value) => ({
  type: actionsTypes.SET_SNDS_INTEGRATION_ERRORS,
  payload: {
    data: value,
  },
});

// credentials

export const setSndsIsOnAddKey = (value) => ({
  type: actionsTypes.SET_SNDS_IS_ON_ADD_KEY,
  payload: {
    data: value,
  },
});

export const handleSndsCredentialsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_CREDENTIALS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleSndsCredentialsIsDataLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_CREDENTIALS_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSndsCredentials = (value) => ({
  type: actionsTypes.SET_SNDS_CREDENTIALS,
  payload: {
    data: value,
  },
});

export const setSndsCredentialsKey = (value) => ({
  type: actionsTypes.SET_SNDS_CREDENTIALS_KEY,
  payload: {
    data: value,
  },
});

export const getSndsCredentials = ({ withLoader = true }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  try {
    if (withLoader) {
      dispatch(handleSndsCredentialsIsDataLoading(true));
    }

    const data = await Api.getSndsCredentials();

    dispatch(setSndsCredentialsKey(''));
    dispatch(setSndsCredentials(data));
    dispatch(handleSndsCredentialsIsDataLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsCredentialsIsDataLoading(false));
    dispatch(
      setSndsUserIntegrationErrors({
        credentials: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};

export const sendSndsCredentials = (key) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!key) {
    return toast.error(messages[locale]['snds.toast.credentials.no-key.error']);
  }

  try {
    dispatch(handleSndsCredentialsIsLoading(true));

    const data = await Api.sendSndsCredentials({ key });

    if (data) {
      const currentCredentials = sndsIntegrationCredentialsSelector(state);

      dispatch(setSndsCredentials([...currentCredentials, data]));
    }
    dispatch(handleSndsCredentialsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsCredentialsIsLoading(false));
    dispatch(
      setSndsUserIntegrationErrors({
        credentials: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};

export const deleteSndsCredentialKey = (id) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['snds.toast.credentials.no-id.error']
    );
  }

  try {
    dispatch(handleSndsCredentialsIsLoading(true));

    const data = await Api.deleteSndsCredentialKey(id);

    if (data) {
      const currentCredentials = sndsIntegrationCredentialsSelector(state);

      dispatch(setSndsCredentials(
        currentCredentials.filter(credential => credential.id !== id)
      ));
    }
    dispatch(handleSndsCredentialsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsCredentialsIsLoading(false));
    dispatch(
      setSndsUserIntegrationErrors({
        credentials: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return toast.error(err);
  }
}

export const updateSndsCredentialKey = ({ id, status }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['snds.toast.credentials.no-id.error']
    );
  }
  if (!status) {
    return toast.error(
      messages[locale]['snds.toast.credentials.no-status.error']
    );
  }

  try {
    dispatch(handleSndsCredentialsIsLoading(true));

    const data = await Api.updateSndsCredentialKey({ id, status });

    if (data) {
      const currentCredentials = sndsIntegrationCredentialsSelector(state);

      dispatch(
        setSndsCredentials(
          currentCredentials.map(credential => (credential.id === id ? { ...credential, status } : credential))
        )
      );
    }
    dispatch(handleSndsCredentialsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsCredentialsIsLoading(false));
    dispatch(
      setSndsUserIntegrationErrors({
        credentials: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return toast.error(err);
  }
}

// run snds with check date

export const setSndsDateRunIsShowSlider = (value) => ({
  type: actionsTypes.SET_SNDS_DATE_RUN_IS_SHOW_SLIDER,
  payload: {
    data: value,
  },
});

export const setSndsDateRun = (value) => ({
  type: actionsTypes.SET_SNDS_DATE_RUN,
  payload: {
    data: value,
  },
});

export const handleSndsDateRunIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_DATE_RUN_IS_LOADING,
  payload: {
    data: value,
  },
});

export const sendSndsDateRun = ({ date }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!date) {
    return toast.error(messages[locale]['snds_integration.check_date.no-date.error']);
  }

  try {
    dispatch(handleSndsDateRunIsLoading(true));

    const data = await Api.sendSndsDateRun({ date });

    dispatch(handleSndsDateRunIsLoading(false));
    if (data) {
      dispatch(setSndsDateRunIsShowSlider(false));
      dispatch(setSndsHistoryIsShowSlider(true));
    }
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsDateRunIsLoading(false));
    dispatch(
      setSndsUserIntegrationErrors({
        dateRun: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

// history

export const setSndsHistoryIsShowSlider = (value) => ({
  type: actionsTypes.SET_SNDS_HISTORY_IS_SHOW_SLIDER,
  payload: {
    data: value,
  },
});

export const handleSndsHistoryIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_HISTORY_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleSndsHistoryIsLoaded = (value) => ({
  type: actionsTypes.HANDLE_SNDS_HISTORY_IS_LOADED,
  payload: {
    data: value,
  },
});

export const setSndsHistory = (value) => ({
  type: actionsTypes.SET_SNDS_HISTORY,
  payload: {
    data: value,
  },
});

export const getSndsHistory = ({ withLoader = true, limit, offset }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  try {
    if (withLoader) {
      dispatch(handleSndsHistoryIsLoading(true));
    }
    dispatch(handleSndsHistoryIsLoaded(false));

    const data = await Api.getSndsHistory({ limit, offset });

    dispatch(setSndsHistory(data));
    dispatch(handleSndsHistoryIsLoaded(true));
    dispatch(handleSndsHistoryIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsHistoryIsLoaded(true));
    dispatch(handleSndsHistoryIsLoading(false));
    dispatch(
      setSndsUserIntegrationErrors({
        sndsHistory: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};
