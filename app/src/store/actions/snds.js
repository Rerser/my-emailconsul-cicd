import { toast } from 'react-toastify';
import {
  differenceInCalendarDays,
} from 'date-fns';
import Api from 'api';

import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
  sndsIpStatusDateFromSelector,
  sndsIpStatusDateToSelector,
  sndsIpStatusPaginationSelector,
  sndsIpStatusSortingSelector,
  sndsIpStatusFilterSelector,
  sndsIpDataDateFromSelector,
  sndsIpDataDateToSelector,
  sndsIpDataPaginationSelector,
  sndsIpDataSortingSelector,
  sndsIpDataFilterSelector,
  sndsDashboardsDateToSelector,
  sndsDashboardsDateFromSelector,
  sndsInfoModeSelector,
  sndsDashboardsDataSelector,
  sndsIpStatusSelector,
  sndsIpDataSelector,
  sndsDashboardsUserIpsSelector,
  sndsIpStatusUserIpsSelector,
  sndsIpDataUserIpsSelector,
  sndsDashboardsFilterSelector,
} from 'store/selectors';

import {
  NOT_FOUND_ERROR_STATUS,
  SNDS_DASHBOARDS_REQ_IPS_LIMIT,
  SNDS_FILTER_RESULT,
  SNDS_INFO_MODE,
} from 'utils/constants';
import {
  deepCompare,
  getStatusError,
} from 'utils/helpers';
import messages from 'utils/messages';

// clear, errors

export const clearSndsAllData = () => ({
  type: actionsTypes.CLEAR_SNDS_ALL_DATA,
});

export const setSndsErrors = (value) => ({
  type: actionsTypes.SET_SNDS_ERRORS,
  payload: {
    data: value,
  },
});

// mode, slidebar

export const handleSndsInfoMode = (value) => ({
  type: actionsTypes.HANDLE_SNDS_INFO_MODE,
  payload: {
    data: value,
  },
});

// user ips

export const handleSndsDashboardsUserIpsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_DASHBOARDS_USER_IPS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSndsDashboardsUserIps = (value) => ({
  type: actionsTypes.SET_SNDS_DASHBOARDS_USER_IPS,
  payload: {
    data: value,
  },
});

export const handleSndsDashboardsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_DASHBOARDS_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleSndsDashboardsIsLoaded = (value) => ({
  type: actionsTypes.HANDLE_SNDS_DASHBOARDS_DATA_IS_LOADED,
  payload: {
    data: value,
  },
});

export const setSndsDashboardsData = (value) => ({
  type: actionsTypes.SET_SNDS_DASHBOARDS_DATA,
  payload: {
    data: value,
  },
});

export const setSndsDashboardsDateFrom = (value) => ({
  type: actionsTypes.SET_SNDS_DASHBOARDS_DATE_FROM,
  payload: {
    data: value,
  },
});

export const setSndsDashboardsDateTo = (value) => ({
  type: actionsTypes.SET_SNDS_DASHBOARDS_DATE_TO,
  payload: {
    data: value,
  },
});

export const handleSndsDashboardsFilter = (value) => ({
  type: actionsTypes.HANDLE_SNDS_DASHBOARDS_FILTER,
  payload: {
    data: value,
  },
});

export const getSndsDashboardsData = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const currentData = sndsDashboardsDataSelector(state);

  const fromDate = sndsDashboardsDateFromSelector(state);
  const toDate = sndsDashboardsDateToSelector(state);

  const { ips = [] } = sndsDashboardsFilterSelector(state);
  let diffDays = 0;

  if (Date.parse(fromDate) && Date.parse(toDate)) {
    diffDays = differenceInCalendarDays(new Date(toDate), new Date(fromDate)) + 1; // + 1, т.к. differenceInCalendarDays не берет в расчет последний день
  }

  try {
      let data = [];

      if (!currentData.length) {
        dispatch(handleSndsDashboardsIsLoading(true));
      }
      dispatch(handleSndsDashboardsIsLoaded(false));

      if (ips.length && diffDays) {
        const response = await Api.getSndsIpData({
          fromDate, toDate,
          offset: 0,
          limit: SNDS_DASHBOARDS_REQ_IPS_LIMIT,
          sort: 'asc',
          ips,
          filterResult: Object.values(SNDS_FILTER_RESULT),
        });

        data = response.data;
      } else {
        data = await Api.getSndsDashboardsData({ fromDate, toDate });
      }

      if (!deepCompare(currentData, data)) {
        dispatch(
          setSndsDashboardsData(
            data?.sort(
              (a, b) => Date.parse(a.date_timestamp) - Date.parse(b.date_timestamp)
            ) ?? []
          )
        );
      }

      dispatch(handleSndsDashboardsIsLoading(false));
      dispatch(handleSndsDashboardsIsLoaded(true));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsDashboardsIsLoading(false));
    dispatch(handleSndsDashboardsIsLoaded(true));
    dispatch(
      setSndsErrors({
        dashboards: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};

// IP Status

export const handleSndsIpStatusUserIpsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_STATUS_USER_IPS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSndsIpStatusUserIps = (value) => ({
  type: actionsTypes.SET_SNDS_IP_STATUS_USER_IPS,
  payload: {
    data: value,
  },
});

export const handleSndsIpStatusIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_STATUS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleSndsIpStatusIsLoaded = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_STATUS_IS_LOADED,
  payload: {
    data: value,
  },
});

export const setSndsIpStatus = (value) => ({
  type: actionsTypes.SET_SNDS_IP_STATUS,
  payload: {
    data: value,
  },
});

export const setSndsIpStatusDateFrom = (value) => ({
  type: actionsTypes.SET_SNDS_IP_STATUS_DATE_FROM,
  payload: {
    data: value,
  },
});

export const setSndsIpStatusDateTo = (value) => ({
  type: actionsTypes.SET_SNDS_IP_STATUS_DATE_TO,
  payload: {
    data: value,
  },
});

export const setSndsIpStatusDataPagination = (value) => ({
  type: actionsTypes.SET_SNDS_IP_STATUS_PAGINATION,
  payload: {
    data: value,
  },
});

export const handleSndsIpStatusFilter = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_STATUS_FILTER,
  payload: {
    data: value,
  },
});

export const handleSndsIpStatusSorting = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_STATUS_SORTING,
  payload: {
    data: value,
  },
});

export const getSndsIpStatus = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const currentData = sndsIpStatusSelector(state);

  const fromDate = sndsIpStatusDateFromSelector(state);
  const toDate = sndsIpStatusDateToSelector(state);
  const paginationData = sndsIpStatusPaginationSelector(state);
  const filterData = sndsIpStatusFilterSelector(state);

  const { field, order } = sndsIpStatusSortingSelector(state);
  const { offset, limit } = paginationData;
  const { ips } = filterData;

  const sort = order === 'desc' ? `-${field}` : field;

  try {
    if (!currentData.length) {
      dispatch(handleSndsIpStatusIsLoading(true));
    }
    dispatch(handleSndsIpStatusIsLoaded(false));

    const data = await Api.getSndsIpStatus({ fromDate, toDate, offset, limit, sort, ips });

    if (!deepCompare(currentData, data)) {
      dispatch(setSndsIpStatus(data));
    }
    dispatch(handleSndsIpStatusIsLoading(false));
    dispatch(handleSndsIpStatusIsLoaded(true));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsIpStatusIsLoading(false));
    dispatch(handleSndsIpStatusIsLoaded(true));
    dispatch(
      setSndsErrors({
        ipStatus: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};

// IP Data

export const handleSndsIpDataUserIpsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_DATA_USER_IPS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSndsIpDataUserIps = (value) => ({
  type: actionsTypes.SET_SNDS_IP_DATA_USER_IPS,
  payload: {
    data: value,
  },
});

export const handleSndsIpDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleSndsIpDataIsLoaded = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_DATA_IS_LOADED,
  payload: {
    data: value,
  },
});

export const setSndsIpData = (value) => ({
  type: actionsTypes.SET_SNDS_IP_DATA,
  payload: {
    data: value,
  },
});

export const setSndsIpDataDateFrom = (value) => ({
  type: actionsTypes.SET_SNDS_IP_DATA_DATE_FROM,
  payload: {
    data: value,
  },
});

export const setSndsIpDataDateTo = (value) => ({
  type: actionsTypes.SET_SNDS_IP_DATA_DATE_TO,
  payload: {
    data: value,
  },
});

export const setSndsIpDataPagination = (value) => ({
  type: actionsTypes.SET_SNDS_IP_DATA_PAGINATION,
  payload: {
    data: value,
  },
});

export const handleSndsIpDataFilter = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_DATA_FILTER,
  payload: {
    data: value,
  },
});

export const handleSndsIpDataSorting = (value) => ({
  type: actionsTypes.HANDLE_SNDS_IP_DATA_SORTING,
  payload: {
    data: value,
  },
});

export const getSndsIpData = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const currentData = sndsIpDataSelector(state);

  const fromDate = sndsIpDataDateFromSelector(state);
  const toDate = sndsIpDataDateToSelector(state);
  const paginationData = sndsIpDataPaginationSelector(state);
  const filterData = sndsIpDataFilterSelector(state);

  const { field, order } = sndsIpDataSortingSelector(state);
  const { offset, limit } = paginationData;
  const { ips, filterResult } = filterData;

  const sort = order === 'desc' ? `-${field}` : field;

  try {
    if (!currentData.length) {
      dispatch(handleSndsIpDataIsLoading(true));
    }
    dispatch(handleSndsIpDataIsLoaded(false));

    const data = await Api.getSndsIpData({ fromDate, toDate, offset, limit, sort, ips, filterResult });
    if (!deepCompare(currentData, data)) {
      dispatch(setSndsIpData(data));
    }
    dispatch(handleSndsIpDataIsLoading(false));
    dispatch(handleSndsIpDataIsLoaded(true));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsIpDataIsLoading(false));
    dispatch(handleSndsIpDataIsLoaded(true));
    dispatch(
      setSndsErrors({
        ipData: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};

// get user ips

export const getSndsUserIps = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const modeSnds = sndsInfoModeSelector(state);

  const dashboardsUserIps = sndsDashboardsUserIpsSelector(state);
  const ipStatusUserIps = sndsIpStatusUserIpsSelector(state);
  const ipDataUserIps = sndsIpDataUserIpsSelector(state);

  try {
    switch (modeSnds) {
      case SNDS_INFO_MODE.DASHBOARDS:
        if (!dashboardsUserIps.length) {
          dispatch(handleSndsDashboardsUserIpsIsLoading(true));

          const data = await Api.getSndsUserIps(SNDS_INFO_MODE.IP_DATA);

          dispatch(setSndsDashboardsUserIps(data));
          dispatch(handleSndsDashboardsUserIpsIsLoading(false));
        }
        break;

      case SNDS_INFO_MODE.IP_STATUS:
        if (!ipStatusUserIps.length) {
          dispatch(handleSndsIpStatusUserIpsIsLoading(true));

          const data = await Api.getSndsUserIps(SNDS_INFO_MODE.IP_STATUS);

          dispatch(setSndsIpStatusUserIps(data));
          dispatch(handleSndsIpStatusUserIpsIsLoading(false));
        }
        break;

      case SNDS_INFO_MODE.IP_DATA:
        if (!ipDataUserIps.length) {
          dispatch(handleSndsIpDataUserIpsIsLoading(true));

          const data = await Api.getSndsUserIps(SNDS_INFO_MODE.IP_DATA);

          dispatch(setSndsIpDataUserIps(data));
          dispatch(handleSndsIpDataUserIpsIsLoading(false));
        }
        break;
    
      default:
        break;
    }
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSndsDashboardsUserIpsIsLoading(false));
    dispatch(handleSndsIpStatusUserIpsIsLoading(false));
    dispatch(handleSndsIpDataUserIpsIsLoading(false));
    dispatch(
      setSndsErrors({
        userIps: {
          message: err.messages,
          status,
        },
      })
    );
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};
