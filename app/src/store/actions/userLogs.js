import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import messages from 'utils/messages';
import { getStatusError } from 'utils/helpers';
import {
  localeCurrentSelector,
  userLogsPaginationSelector,
} from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

export const handleUserLogsIsLoading = (data) => ({
  type: actionsTypes.HANDLE_USER_LOGS_DATA_IS_LOADING,
  payload: {
    data,
  },
});

export const setUserLogsData = (data) => ({
  type: actionsTypes.SET_USER_LOGS_DATA,
  payload: {
    data,
  },
});

export const setUserLogsDataPagination = (data) => ({
  type: actionsTypes.SET_USER_LOGS_DATA_PAGINATION,
  payload: {
    data,
  },
});

export const setUserLogsErrors = (data) => ({
  type: actionsTypes.SET_USER_LOGS_ERRORS,
  payload: {
    data,
  },
});

export const getUserLogs = ({
  user_id = null,
  withLoader = true,
} = {}) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!user_id) {
    return toast.error(
      messages[locale]['user_logs.user_id.required.text.error']
    );
  }

  try {
    if (withLoader) {
      dispatch(handleUserLogsIsLoading(true));
    }

    const { offset, limit } = userLogsPaginationSelector(state);
    const data = await Api.getUserLogs({ user_id, offset, limit });

    dispatch(setUserLogsData(data));
    dispatch(handleUserLogsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setUserLogsErrors({
          data: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleUserLogsIsLoading(false));
    return console.error(err);
  }
};
