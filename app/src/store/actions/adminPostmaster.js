import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { getStatusError } from 'utils/helpers';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

export const handleAdminPostmasterAuthUrlIsLoading = (data) => ({
  type: actionsTypes.HANDLE_ADMIN_POSTMASTER_AUTH_URL_IS_LOADING,
  payload: {
    data,
  },
});

export const setAdminPostmasterAuthUrl = (data) => ({
  type: actionsTypes.SET_ADMIN_POSTMASTER_AUTH_URL,
  payload: {
    data,
  },
});

export const setAdminPostmasterErrors = (data) => ({
  type: actionsTypes.SET_ADMIN_POSTMASTER_ERRORS,
  payload: {
    data,
  },
});

export const getAdminPostmasterAuthUrl = () => async (dispatch) => {
  try {
    dispatch(handleAdminPostmasterAuthUrlIsLoading(true));

    const data = await Api.getAdminPostmasterAuthUrl();

    dispatch(setAdminPostmasterAuthUrl(data));
    dispatch(handleAdminPostmasterAuthUrlIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setAdminPostmasterErrors({
          authUrl: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleAdminPostmasterAuthUrlIsLoading(false));
    return console.error(err);
  }
};
