import { toast } from 'react-toastify';
import Api from 'api';
import ApiMock from 'api/apiMock';
import actionsTypes from 'store/actionsTypes';
import messages from 'utils/messages';
import {
  localeCurrentSelector,
  seedlistingItemDataSelector,
  seedlistingItemShareableLinkDataSelector,
  usersMeSelector,
} from 'store/selectors';
import { getStatusError } from 'utils/helpers';
import { DEMO_USER_EMAIL, NOT_FOUND_ERROR_STATUS } from 'utils/constants';

export const setSeedlistingItemErrors = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_ERRORS,
  payload: {
    data: value,
  },
});

export const handleSeedlistingItemDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemData = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_DATA,
  payload: {
    data: value,
  },
})

// seedlisting item
export const getSeedlistingItemData = ({
  id,
  withLoader = true,
} = {}) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    const me = usersMeSelector(state);
    const { email: meEmail } = me;

    if (meEmail === DEMO_USER_EMAIL) {
      const data = await ApiMock.getSeedlistingItemData();
      return dispatch(setSeedlistingItemData(data));
    }

    if (!id) {
      return toast.error(
        messages[locale]['seedlisting_item.toast.get_seedlisting_item.id.error']
      );
    }
    if (withLoader) {
      dispatch(handleSeedlistingItemDataIsLoading(true));
    }

    const data = await Api.getSeedlistingItemData({ id });

    dispatch(setSeedlistingItemData(data));
    if (withLoader) {
      dispatch(handleSeedlistingItemDataIsLoading(false));
    }
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          data: {
            message: err.message,
            status,
          },
        })
      );
    }
    if (withLoader) {
      dispatch(handleSeedlistingItemDataIsLoading(false));
    }
    return console.error('err', err);
  }
};

// email info
export const handleSeedlistingItemIsLoadingEmailInfo = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfo = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoId = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_ID,
  payload: {
    data: value,
  },
});

export const getSeedlistingItemEmailInfo = ({
  id,
  seedlistingEmailId,
} = {}) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    const me = usersMeSelector(state);
    const { email: meEmail } = me;

    if (meEmail === DEMO_USER_EMAIL) {
      const data = await ApiMock.getSeedlistingItemEmailInfo();
      return dispatch(setSeedlistingItemEmailInfo(data));
    }

    if (!id) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.id.error'
        ]
      );
    }
    if (!seedlistingEmailId) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.seedlisting.email_id.error'
        ]
      );
    }

    dispatch(handleSeedlistingItemIsLoadingEmailInfo(true));

    const data = await Api.getSeedlistingItemEmailInfo({
      id,
      seedlistingEmailId,
    });

    dispatch(setSeedlistingItemEmailInfo(data));
    dispatch(handleSeedlistingItemIsLoadingEmailInfo(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          emailInfoData: {
            message: err.message,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfo(false));
    return console.error('err', err);
  }
};

// ip
export const handleSeedlistingItemIsLoadingEmailInfoEpcIpData = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_EPC_IP_DATA,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoEpcIpData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_EPC_IP_DATA,
  payload: {
    data,
  },
});

export const getSeedlistingItemEmailInfoEpcIpData = ({ ip } = {}) => async (
  dispatch,
  getState
) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!ip) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.epc_ip.ip.error'
        ]
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoEpcIpData(true));

    const data = await Api.getEpcIp(ip);
    dispatch(setSeedlistingItemEmailInfoEpcIpData(data));

    dispatch(handleSeedlistingItemIsLoadingEmailInfoEpcIpData(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          epcIpData: {
            message: err.message,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoEpcIpData(false));
    return console.error('err', err);
  }
};

// domain
export const handleSeedlistingItemIsLoadingEmailInfoEpcDomainData = (value) => ({
  type:
    actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_EPC_DOMAIN_DATA,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoEpcDomainData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_EPC_DOMAIN_DATA,
  payload: {
    data,
  },
});

export const getSeedlistingItemEmailInfoEpcDomainData = ({
  domain,
} = {}) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!domain) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.epc_domain.domain.error'
        ]
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoEpcDomainData(true));

    const data = await Api.getEpcDomain(domain);
    dispatch(setSeedlistingItemEmailInfoEpcDomainData(data));

    dispatch(handleSeedlistingItemIsLoadingEmailInfoEpcDomainData(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          epcDomainData: {
            message: err.message,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoEpcDomainData(false));
    return console.error('err', err);
  }
};

// spamassasin
export const hanldeSeedlistingItemIsLoadingEmailInfoSpamAssasinData = (
  value
) => ({
  type:
    actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_SPAMASSASIN_DATA,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoSpamAssasinData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_SPAMASSASIN_DATA,
  payload: {
    data,
  },
});

export const getSeedlistingItemEmailInfoSpamAssasinData = ({
  seedlistingId,
  seedlistingEmailId,
} = {}) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    const me = usersMeSelector(state);
    const { email: meEmail } = me;

    if (meEmail === DEMO_USER_EMAIL) {
      const data = await ApiMock.getSpamAssasin();
      return dispatch(setSeedlistingItemEmailInfoSpamAssasinData(data));
    }

    if (!seedlistingId) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.spamassasin.seedlisting.id.error'
        ]
      );
    }
    if (!seedlistingEmailId) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.spamassasin.seedlisting.email_id.error'
        ]
      );
    }
    dispatch(hanldeSeedlistingItemIsLoadingEmailInfoSpamAssasinData(true));

    const data = await Api.getSpamAssasin({
      seedlistingId,
      seedlistingEmailId,
    });
    dispatch(setSeedlistingItemEmailInfoSpamAssasinData(data));

    dispatch(hanldeSeedlistingItemIsLoadingEmailInfoSpamAssasinData(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          spamAssasinData: {
            message: err.message,
            status,
          },
        })
      );
    }
    dispatch(hanldeSeedlistingItemIsLoadingEmailInfoSpamAssasinData(false));
    return console.error('err', err);
  }
};

// spf
export const handleSeedlistingItemIsLoadingEmailInfoSPFData = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_SPF_DATA,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoSPFData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_SPF_DATA,
  payload: {
    data,
  },
});

export const getSeedlistingItemEmailInfoSPFData = ({ domain } = {}) => async (
  dispatch,
  getState
) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!domain) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.spf.domain.error'
        ]
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoSPFData(true));

    const data = await Api.getAuthenticationSPF(domain);
    dispatch(setSeedlistingItemEmailInfoSPFData(data));

    dispatch(handleSeedlistingItemIsLoadingEmailInfoSPFData(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          SPFData: {
            message: err.message,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoSPFData(false));
    return console.error('err', err);
  }
};

// dkim
export const handleSeedlistingItemIsLoadingEmailInfoDKIMData = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_DKIM_DATA,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoDKIMTextualRepresentationData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_DKIM_TEXTUAL_REPRESENTATION_DATA,
  payload: {
    data,
  },
});

export const setSeedlistingItemEmailInfoDKIMSignatureHeaderData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_DKIM_SIGNATURE_HEADER_DATA,
  payload: {
    data,
  },
});

export const getSeedlistingItemEmailInfoDKIMData = ({
  valuesOfDomainSelector = [], signaturesDKIM = []
} = {}) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!valuesOfDomainSelector.length) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.values_d_s.error'
        ]
      );
    }
    if (!signaturesDKIM.length) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.dkim.signature-header.error'
        ]
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoDKIMData(true));

    // textualRepresentation
    const valuesPromises = valuesOfDomainSelector.map(
      ({ d: domain, s: selector }) => Api.getAuthenticationDKIMTextualRepresentation(domain, selector)
    );
    const textualRepresentationResponse = await Promise.allSettled(valuesPromises);
    const textualRepresentation = textualRepresentationResponse
      .map(data => data.status === 'fulfilled' ? data.value : null)
      .filter(data => data);

    dispatch(setSeedlistingItemEmailInfoDKIMTextualRepresentationData(textualRepresentation));

    // validateSignatureHeader
    const signaturePromises = signaturesDKIM.map(
      signature => Api.getAuthenticationDKIMValidateHeader(signature)
    );
    const validateSignatureHeaderResponse = await Promise.allSettled(signaturePromises);
    const validateSignatureHeader = validateSignatureHeaderResponse
      .map(data => data.status === 'fulfilled' ? data.value : null)
      .filter(data => data);

    dispatch(setSeedlistingItemEmailInfoDKIMSignatureHeaderData(validateSignatureHeader));

    dispatch(handleSeedlistingItemIsLoadingEmailInfoDKIMData(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          DKIMData: {
            message: err.message,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoDKIMData(false));
    return console.error('err', err);
  }
};

// dmarc
export const handleSeedlistingItemIsLoadingEmailInfoDMARCData = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_IS_LOADING_EMAIL_INFO_DMARC_DATA,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemEmailInfoDMARCData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_EMAIL_INFO_DMARC_DATA,
  payload: {
    data,
  },
});

export const getSeedlistingItemEmailInfoDMARCData = ({
  domain = '',
} = {}) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!domain) {
      return toast.error(
        messages[locale][
          'seedlisting_item.toast.get_seedlisting_item_email_info.dmarc.domain.error'
        ]
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoDMARCData(true));

    const data = await Api.getAuthenticationDMARC(domain);
    dispatch(setSeedlistingItemEmailInfoDMARCData(data));

    dispatch(handleSeedlistingItemIsLoadingEmailInfoDMARCData(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingItemErrors({
          DMARCData: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingItemIsLoadingEmailInfoDMARCData(false));
    return console.error('err', err);
  }
};

// CLEAR DATA
export const clearEmailInfoAllData = () => ({
  type: actionsTypes.CLEAR_SEEDLISTING_ITEM_EMAIL_INFO_ALL_DATA,
});

// shareable link
export const setSeedlistingItemShareableLinkIsOn = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_SHAREABLE_LINK_IS_ON,
  payload: {
    data: value,
  },
});

export const setSeedlistingItemShareableLinkData = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ITEM_SHAREABLE_LINK_DATA,
  payload: {
    data: value,
  },
});

export const handleSeedlistingItemShareableLinkIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_ITEM_SHAREABLE_LINK_IS_LOADING,
  payload: {
    data: value,
  },
});

export const getSeedlistingShareableLinkData = (seedlistingId) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!seedlistingId) {
    return toast.error(messages[locale]['seedlisting_item.toast.get_seedlisting_shareable_link.no_data.error']);
  }

  try {
    dispatch(handleSeedlistingItemShareableLinkIsLoading(true));

    const data = await Api.getSeedlistingShareableLink(seedlistingId);

    dispatch(handleSeedlistingItemShareableLinkIsLoading(false));
    dispatch(setSeedlistingItemShareableLinkData(data));
  } catch (err) {
    dispatch(handleSeedlistingItemShareableLinkIsLoading(false));
    dispatch(setSeedlistingItemShareableLinkData(null));
  }
}

export const addSeedlistingShareableLink = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);
  const { id: seedlistingId = '' } = seedlistingItemDataSelector(state);

  if (!seedlistingId) {
    return toast.error(messages[locale]['seedlisting_item.toast.get_seedlisting_shareable_link.no_data.error']);
  }

  try {    
    dispatch(handleSeedlistingItemShareableLinkIsLoading(true));
    dispatch(setSeedlistingItemShareableLinkIsOn(true));

    const data = await Api.addSeedlistingShareableLink({ seedlisting_id: seedlistingId });

    dispatch(handleSeedlistingItemShareableLinkIsLoading(false));
    dispatch(setSeedlistingItemShareableLinkData(data));
    toast.success(messages[locale]['seedlisting_item.toast.add_seedlisting_shareable_link.success']);
  } catch (err) {
    dispatch(handleSeedlistingItemShareableLinkIsLoading(false));
    dispatch(setSeedlistingItemShareableLinkIsOn(false));
    return console.error('err', err);
  }
}

export const deleteSeedlistingShareableLink = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);
  const { id = '' } = seedlistingItemShareableLinkDataSelector(state);
  const { id: seedlistingId = '' } = seedlistingItemDataSelector(state);

  if (!id) {
    return toast.error(messages[locale]['seedlisting_item.toast.get_seedlisting_shareable_link.no_data.error']);
  }

  try {    
    dispatch(handleSeedlistingItemShareableLinkIsLoading(true));
    await Api.deleteSeedlistingShareableLink(id);
    dispatch(setSeedlistingItemShareableLinkIsOn(false));
    dispatch(getSeedlistingShareableLinkData(seedlistingId));
    dispatch(handleSeedlistingItemShareableLinkIsLoading(false));
    toast.success(messages[locale]['seedlisting_item.toast.delete_seedlisting_shareable_link.success']);
  } catch (err) {
    dispatch(handleSeedlistingItemShareableLinkIsLoading(false));
    dispatch(setSeedlistingItemShareableLinkIsOn(false));
    return console.error('err', err);
  }
}