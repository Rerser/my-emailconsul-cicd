import actionsTypes from 'store/actionsTypes';

export const handleContextMenuShow = (value) => ({
  type: actionsTypes.HANDLE_CONTEXT_MENU_SHOW,
  payload: {
    data: value,
  },
});

export const setContextMenuCoords = (value) => ({
  type: actionsTypes.SET_CONTEXT_MENU_COORDS,
  payload: {
    data: value,
  },
});
