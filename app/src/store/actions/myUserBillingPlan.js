import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { usersMeSelector } from 'store/selectors';
import { USER_ROLE } from 'utils/constants';
import history from 'utils/history';

export const setMyUserBillingPlanData = (data) => ({
  type: actionsTypes.SET_MY_USER_BILLING_PLAN_DATA,
  payload: {
    data,
  },
});

export const handleMyUserBillingPlanIsDataLoading = (data) => ({
  type: actionsTypes.HANDLE_MY_USER_BILLING_PLAN_IS_DATA_LOADING,
  payload: {
    data,
  },
});

export const getMyUserBillingPlan = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  const state = getState();
  const me = usersMeSelector(state);

  if (withLoader) {
    dispatch(handleMyUserBillingPlanIsDataLoading(true));
  }

  Api.getMyUserBillingPlan()
    .then((data) => {
      const is404Error = data.hasOwnProperty('is404Error');
      if (is404Error) {
        delete data.is404Error;
      }
      dispatch(setMyUserBillingPlanData(data));

      if (withLoader) {
        dispatch(handleMyUserBillingPlanIsDataLoading(false));
      }
      if (is404Error && me?.role === USER_ROLE.USER) {
        history.push('/user/billing');
      }
    })
    .catch((err) => {
      if (withLoader) {
        dispatch(handleMyUserBillingPlanIsDataLoading(false));
      }
      return console.error(err);
    });
};
