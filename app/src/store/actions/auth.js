import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  LOCAL_STORAGE_TOKEN_KEY,
  AUTH_ACTIVATION_RESULT_STATUS,
  NOT_FOUND_ERROR_STATUS,
} from 'utils/constants';
import { usersSetMe } from 'store/actions/users';
import { validateEmail, getStatusError } from 'utils/helpers';
import history from 'utils/history';

import { localeCurrentSelector } from 'store/selectors';
import messages from 'utils/messages';

export const authSetEmail = (value) => ({
  type: actionsTypes.AUTH_SET_EMAIL,
  payload: {
    data: value,
  },
});

export const authSetPassword = (value) => ({
  type: actionsTypes.AUTH_SET_PASSWORD,
  payload: {
    data: value,
  },
});

export const authSetConfirmPassword = (value) => ({
  type: actionsTypes.AUTH_SET_CONFIRM_PASSWORD,
  payload: {
    data: value,
  },
});

export const authSetSetIsRecaptchaVerified = (value) => ({
  type: actionsTypes.AUTH_SET_IS_RECAPTCHA_VERIFIED,
  payload: {
    data: value,
  },
});

export const authSetToken = (value) => ({
  type: actionsTypes.AUTH_SET_TOKEN,
  payload: {
    data: value,
  },
});

export const authSetIsLoading = (value) => ({
  type: actionsTypes.AUTH_SET_IS_LOADING,
  payload: {
    data: value,
  },
});

export const authSetActivationResultStatus = (value) => ({
  type: actionsTypes.AUTH_SET_ACTIVATION_RESULT_STATUS,
  payload: {
    data: value,
  },
});

export const authSetCurrentPassword = (value) => ({
  type: actionsTypes.AUTH_SET_CURRENT_PASSWORD,
  payload: {
    data: value,
  },
});

export const authSetNewPassword = (value) => ({
  type: actionsTypes.AUTH_SET_NEW_PASSWORD,
  payload: {
    data: value,
  },
});

export const authSetConfirmNewPassword = (value) => ({
  type: actionsTypes.AUTH_SET_CONFIRM_NEW_PASSWORD,
  payload: {
    data: value,
  },
});

export const authSetSwitchedUser = (value) => ({
  type: actionsTypes.AUTH_SET_SWITCHED_USER,
  payload: {
    data: value,
  },
});

export const authSetSwitchedUserIsLoading = (value) => ({
  type: actionsTypes.AUTH_SET_SWITCHED_USER_IS_LOADING,
  payload: {
    data: value,
  },
});

export const authLogin = (data) => (dispatch, getState) => {
  const { email, password } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!email || !password) {
    return toast.error(
      messages[locale]['auth.toast.login.email_password.error']
    );
  }

  if (!validateEmail(email)) {
    return toast.error(messages[locale]['auth.toast.login.email.valid.error']);
  }

  dispatch(authSetIsLoading(true));

  Api.authLogin({ email, password })
    .then((data) => {
      dispatch(authSetToken(data.token));
      window.localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, data.token);
      dispatch(authSetEmail(null));
      dispatch(authSetPassword(null));
      dispatch(authSetIsLoading(false));
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(authSetIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - authLogin`
        );
      }
      return toast.error(err);
    });
};

export const authLogout = (value) => (dispatch) => {
  dispatch(authSetToken(null));
  dispatch(usersSetMe(null));
  window.localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY);
};

export const authSignUp = (data) => (dispatch, getState) => {
  /*
    get locale for messages
  */
  const state = getState();
  const locale = localeCurrentSelector(state);

  /*
    validate and handle data
  */
  const { email, password, confirmPassword, isRecaptchaVerified } = data;

  if (!email || !password || !confirmPassword) {
    return toast.error(
      messages[locale][
        'auth.toast.signup.email.password.confirm_password.error'
      ]
    );
  }

  if (password !== confirmPassword) {
    return toast.error(
      messages[locale][
        'auth.toast.signup.password.confirm_password.not_equal.error'
      ]
    );
  }

  if (!validateEmail(email)) {
    return toast.error(messages[locale]['auth.toast.signup.email.valid.error']);
  }

  if (!isRecaptchaVerified) {
    return toast.error(
      messages[locale]['auth.toast.signup.recaptcha.verified.error']
    );
  }

  dispatch(authSetIsLoading(true));

  Api.authSignUp({ email, password, confirm_password: confirmPassword })
    .then((data) => {
      dispatch(authSetEmail(null));
      dispatch(authSetPassword(null));
      dispatch(authSetConfirmPassword(null));
      dispatch(authSetIsLoading(false));
      dispatch(authSetSetIsRecaptchaVerified(false));
      toast.success(
        `${messages[locale]['auth.toast.signup.success.1']} ${email}. ${messages[locale]['auth.toast.signup.success.2']}`,
        {
          autoClose: false,
        }
      );
      history.push('login');
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(authSetIsLoading(false));
      dispatch(authSetSetIsRecaptchaVerified(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - authSignUp`
        );
      }
      return toast.error(err);
    });
};

export const authActivation = (data) => (dispatch, getState) => {
  const { activationId } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!activationId) {
    return toast.error(messages[locale]['auth.toast.activation.id.error']);
  }

  dispatch(authSetIsLoading(true));

  Api.authActivation({ activation_id: activationId })
    .then((data) => {
      dispatch(authSetIsLoading(false));
      dispatch(
        authSetActivationResultStatus(AUTH_ACTIVATION_RESULT_STATUS.SUCCESS)
      );
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(authSetIsLoading(false));
      dispatch(
        authSetActivationResultStatus(AUTH_ACTIVATION_RESULT_STATUS.FAILED)
      );
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - autActivation`
        );
      }
      return toast.error(err);
    });
};

export const authRecoverPassword = (data) => (dispatch, getState) => {
  const { email, isRecaptchaVerified } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!email) {
    return toast.error(
      messages[locale]['auth.toast.recover_password.email.error']
    );
  }

  if (!isRecaptchaVerified) {
    return toast.error(
      messages[locale]['auth.toast.recover_password.recaptcha.verified.error']
    );
  }

  if (!validateEmail(email)) {
    return toast.error(
      messages[locale]['auth.toast.recover_password.email.valid.error']
    );
  }

  dispatch(authSetIsLoading(true));

  Api.authDropPassword({ email })
    .then((data) => {
      dispatch(authSetEmail(null));
      dispatch(authSetIsLoading(false));
      dispatch(authSetSetIsRecaptchaVerified(false));
      toast.success(
        `${messages[locale]['auth.toast.recover_password.new_password.send.success']} ${email}.`,
        {
          autoClose: false,
        }
      );
      history.push('login');
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(authSetIsLoading(false));
      dispatch(authSetSetIsRecaptchaVerified(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - authDropPassword`
        );
      }
      return toast.error(err);
    });
};

export const authChangePassword = (data) => (dispatch, getState) => {
  const { currentPasword, newPassword, confirmNewPassword } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!currentPasword || !newPassword || !confirmNewPassword) {
    return toast.error(
      messages[locale][
        'Current pasword, new password and confirm new password are required'
      ]
    );
  }

  if (newPassword !== confirmNewPassword) {
    return toast.error(
      messages[locale][
        'auth.toast.change_password.new_passwords.confirm_passwords.error'
      ]
    );
  }

  dispatch(authSetIsLoading(true));

  Api.authChangePassword({
    current_password: currentPasword,
    new_password: newPassword,
    confirm_new_password: confirmNewPassword,
  })
    .then((data) => {
      dispatch(authSetCurrentPassword(null));
      dispatch(authSetNewPassword(null));
      dispatch(authSetConfirmNewPassword(null));
      dispatch(authSetIsLoading(false));
      toast.success(
        messages[locale]['auth.toast.change_password.changed.success']
      );
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(authSetIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - authChangePassword`
        );
      }
      return toast.error(err);
    });
};

export const handleIsSwitchedUserMode = (value) => ({
  type: actionsTypes.HANDLE_IS_SWITCHED_USER_MODE,
  payload: {
    data: value,
  },
});

export const setSwitchedUser = (user) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  dispatch(authSetSwitchedUserIsLoading(true));
  if (user) {
    Api.authSwitchUser({ uid: user.id })
      .then((data) => {
        dispatch(authSetToken(data.token));
        dispatch(authSetSwitchedUser(user));
        dispatch(authSetSwitchedUserIsLoading(false));
        dispatch(handleIsSwitchedUserMode(true));
        history.push('/');
      })
      .catch((err) => {
        const status = getStatusError(err);

        dispatch(authSetSwitchedUserIsLoading(false));
        dispatch(authSetSwitchedUser(null));
        dispatch(
          authSetToken(window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY))
        );
        dispatch(authSetSwitchedUserIsLoading(false));
        dispatch(handleIsSwitchedUserMode(false));
        if (status === NOT_FOUND_ERROR_STATUS) {
          return toast.error(
            `${messages[locale]['error404.toast.description.text']} - authSwitchedUser`
          );
        }
        return toast.error(err);
      });
  } else {
    dispatch(authSetSwitchedUser(null));
    dispatch(
      authSetToken(window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY))
    );
    dispatch(authSetSwitchedUserIsLoading(false));
  }
};
