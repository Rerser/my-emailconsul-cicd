import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { netmasksDataSelector } from 'store/selectors';
import { deepCompare, getStatusError } from 'utils/helpers';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

export const setNetmaskErrors = (value) => ({
  type: actionsTypes.SET_NETMASKS_ERRORS,
  payload: {
    data: value,
  },
});

export const handleNetmasksDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_NETMASKS_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setNetmasksData = (value) => ({
  type: actionsTypes.SET_NETMASKS_DATA,
  payload: {
    data: value,
  },
});

export const getNetmasksData = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  const state = getState();
  const netmaskData = netmasksDataSelector(state);

  if (withLoader) {
    dispatch(handleNetmasksDataIsLoading(true));
  }

  Api.getNetmaskList()
    .then((data) => {
      if (!deepCompare(data, netmaskData)) {
        dispatch(setNetmasksData(data));
      }

      if (withLoader) {
        dispatch(handleNetmasksDataIsLoading(false));
      }
    })
    .catch((err) => {
      if (withLoader) {
        dispatch(handleNetmasksDataIsLoading(false));
      }
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        dispatch(
          setNetmaskErrors({
            data: {
              message: err,
              status,
            },
          })
        );
      }
      return console.error('err', err);
    });
};
