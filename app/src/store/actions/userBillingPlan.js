import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { localeCurrentSelector } from 'store/selectors';
import { DISCARD_UPDATE_MODE, ESTABLISH_UPDATE_MODE, SET_NEW_LIMITS_UPDATE_MODE } from 'utils/constants';
import messages from 'utils/messages';

export const setUserBillingPlanData = (data) => ({
  type: actionsTypes.SET_USER_BILLING_PLAN_DATA,
  payload: {
    data,
  },
});

export const handleUserBillingPlanIsDataLoading = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_IS_DATA_LOADING,
  payload: {
    data,
  },
});

export const setEditedUserBillingPlanUserId = (data) => ({
  type: actionsTypes.SET_EDITED_USER_BILLING_PLAN_USER_ID,
  payload: {
    data,
  },
});

export const getUserBillingPlan = (userId) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!userId) {
    return toast.error(messages[locale]['user_billing_plan.toast.get_userbilling_plan.user_id.error']);
  }

  dispatch(handleUserBillingPlanIsDataLoading(true));

  Api.getUserBillingPlan({ user_id: userId })
    .then((data) => {
      dispatch(setUserBillingPlanData(data));
      dispatch(handleUserBillingPlanIsDataLoading(false));
    })
    .catch((err) => {
      dispatch(handleUserBillingPlanIsDataLoading(false));
      return console.error(err);
    });
};

export const handleUserBillingPlanData = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_DATA,
  payload: {
    data,
  },
});

export const handleUserBillingPlanDataListcleaning = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_LISTCLEANING,
  payload: {
    data,
  },
});

export const handleUserBillingPlanDataSeedlisting = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_SEEDLISTING,
  payload: {
    data,
  },
});

export const handleUserBillingPlanDataIps = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_IPS,
  payload: {
    data,
  },
});

export const handleUserBillingPlanDataDomains = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_DATA_DOMAINS,
  payload: {
    data,
  },
});

export const updateUserBillingPlan = ({ data, mode }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!data.user_id) {
    return toast.error(messages[locale]['user_billing_plan.toast.update_userbilling_plan.user_id.error']);
  }
  if (!data.current_plan) {
    return toast.error(messages[locale]['user_billing_plan.toast.update_userbilling_plan.current_plan.error']);
  }
  if (!data.current_plan_type) {
    return toast.error(messages[locale]['user_billing_plan.toast.update_userbilling_plan.current_plan_type.error']);
  }
  if (!data.timestamp_from) {
    data.timestamp_from = new Date(Date.now());
  }

  let requestMethod = '';
  let sendedData = { user_id: data.user_id };
  const ips = { ips_per_month: data.ips.ips_per_month };
  const domains = { domains_per_month: data.domains.domains_per_month };

  switch (mode) {
    case ESTABLISH_UPDATE_MODE:
      requestMethod = 'establishUserBillingPlan';
      sendedData = {
        ...sendedData,
        data: {
          current_plan: data.current_plan,
          current_plan_type: data.current_plan_type,
          timestamp_from: data.timestamp_from,
          listcleaning: data.listcleaning,
          seedlisting: data.seedlisting,
          ips,
          domains,
        },
      };
      break;
    case SET_NEW_LIMITS_UPDATE_MODE:
      requestMethod = 'setNewLimitsUserBillingPlan';
      sendedData = {
        ...sendedData,
        data: {
          listcleaning: data.listcleaning,
          seedlisting: data.seedlisting,
          ips,
          domains,
        },
      };
      break;
    case DISCARD_UPDATE_MODE:
      requestMethod = 'discardUserBillingPlan';
      sendedData = {
        ...sendedData,
        data: {},
      };
      break;

    default:
      return;
  }

  dispatch(handleUserBillingPlanIsDataLoading(true));

  Api[requestMethod](sendedData)
    .then(() => {
      dispatch(setEditedUserBillingPlanUserId(null));
      dispatch(setUserBillingPlanData(null));
      dispatch(handleUserBillingPlanIsDataLoading(false));
    })
    .catch((err) => {
      dispatch(handleUserBillingPlanIsDataLoading(false));
      return console.error('err', err);
    });
};

export const setUserBillingPlanHosts = (data) => ({
  type: actionsTypes.SET_USER_BILLING_PLAN_HOSTS,
  payload: {
    data,
  },
});

export const setUserBillingPlanMaxEmailsPerIteration = (data) => ({
  type: actionsTypes.SET_USER_BILLING_PLAN_MAX_EMAILS_PER_ITERATION,
  payload: {
    data,
  },
});

export const handleUserBillingPlanGeneralByPlanIsLoading = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_GENERAL_BY_PLAN_IS_LOADING,
  payload: {
    data,
  },
});

export const getUserBillingPlanGeneralByPlan = () => (dispatch) => {
  dispatch(handleUserBillingPlanGeneralByPlanIsLoading(true));

  Api.getUserBillingPlanGeneralByPlan()
    .then((data) => {
      dispatch(setUserBillingPlanHosts(data?.hosts));
      dispatch(setUserBillingPlanMaxEmailsPerIteration(data?.max_emails_per_iteration));
      dispatch(handleUserBillingPlanGeneralByPlanIsLoading(false));
    })
    .catch((err) => {
      dispatch(handleUserBillingPlanGeneralByPlanIsLoading(false));
      return console.error(err);
    });
};

export const setUserBillingPlanGeneral = (data) => ({
  type: actionsTypes.SET_USER_BILLING_PLAN_GENERAL,
  payload: {
    data,
  },
});

export const handleUserBillingPlanGeneralIsLoading = (data) => ({
  type: actionsTypes.HANDLE_USER_BILLING_PLAN_GENERAL_IS_LOADING,
  payload: {
    data,
  },
});

export const getUserBillingPlanGeneral = () => (dispatch) => {
  dispatch(handleUserBillingPlanGeneralIsLoading(true));

  Api.getUserBillingPlanGeneral()
    .then((data) => {
      dispatch(setUserBillingPlanGeneral(data));
      dispatch(handleUserBillingPlanGeneralIsLoading(false));
    })
    .catch((err) => {
      dispatch(handleUserBillingPlanGeneralIsLoading(false));
      return console.error(err);
    });
};
