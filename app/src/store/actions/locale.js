import actionsTypes from 'store/actionsTypes';

export const setCurrent = (value) => ({
  type: actionsTypes.LOCALE_SET_CURRENT,
  payload: {
    data: value,
  },
});
