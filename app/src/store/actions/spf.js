import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
} from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import { getStatusError } from 'utils/helpers';
import messages from 'utils/messages';

export const handleSpfDataIsLoading = (data) => ({
  type: actionsTypes.HANDLE_SPF_DATA_IS_LOADING,
  payload: {
    data,
  },
});

export const setSpfData = (data) => ({
  type: actionsTypes.SET_SPF_DATA,
  payload: {
    data,
  },
});

export const setSpfDataError = (data) => ({
  type: actionsTypes.SET_SPF_DATA_ERROR,
  payload: {
    data,
  },
});

export const getSpfData = (domain) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!domain) {
      return toast.error(messages[locale]['spf.toast.domain.error']);
    }
    dispatch(handleSpfDataIsLoading(true));

    const data = await Api.getAuthenticationSPF(domain);
    dispatch(setSpfData(data));

    dispatch(handleSpfDataIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSpfDataError({
          message: err.message,
          status,
        })
      );
    }
    dispatch(handleSpfDataIsLoading(false));
    return console.error('err', err);
  }
};
