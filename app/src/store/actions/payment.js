import { toast } from 'react-toastify';
import Api from 'api';
import axios from 'axios';
import actionsTypes from 'store/actionsTypes';
import {
  BILLING_PLAN_NAME,
  COUNTS_PERIOD_REQUEST,
  DELIVERABILITY_CONSULTING_TYPE,
  DELIVERABILITY_CONSULTING_MAX_REQUEST,
} from 'utils/constants';
import messages from 'utils/messages';
import { localeCurrentSelector } from 'store/selectors';
import { getAllCounts, setCounts } from 'utils/helpers';

export const setLiqpayNewPaymentData = (value) => ({
  type: actionsTypes.SET_LIQPAY_NEW_PAYMENT_DATA,
  payload: {
    data: value,
  },
});

export const handleLiqpayNewPaymentDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_LIQPAY_NEW_PAYMENT_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSelectedPaymentBillingPlan = (value) => ({
  type: actionsTypes.SET_SELECTED_PAYMENT_BILLING_PLAN,
  payload: {
    data: value,
  },
});

export const setSelectedPaymentBillingPlanType = (value) => ({
  type: actionsTypes.SET_SELECTED_PAYMENT_BILLING_PLAN_TYPE,
  payload: {
    data: value,
  },
});

export const setSelectedPaymentCurrency = (value) => ({
  type: actionsTypes.SET_SELECTED_PAYMENT_CURRENCY,
  payload: {
    data: value,
  },
});

export const getLiqpayNewPaymentData = ({
  billingPlanName,
  currency,
  seedlistingEmailsAvailable,
  billingPlanType,
}) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!billingPlanName) {
    return toast.error(
      messages[locale][
        'payment.toast.get_liqpay_new_payment.billing_plan_name.error'
      ]
    );
  }
  if (!billingPlanType) {
    return toast.error(
      messages[locale][
        'payment.toast.get_liqpay_new_payment.billing_plan_type.error'
      ]
    );
  }
  if (!currency) {
    return toast.error(
      messages[locale]['payment.toast.get_liqpay_new_payment.currency.error']
    );
  }
  if (
    billingPlanName === BILLING_PLAN_NAME.ON_GO &&
    !seedlistingEmailsAvailable
  ) {
    return toast.error(
      messages[locale][
        'payment.toast.get_liqpay_new_payment.number_of_checks.error'
      ]
    );
  }

  dispatch(handleLiqpayNewPaymentDataIsLoading(true));

  Api.getLiqpayNewPaymentData({
    billingPlanName,
    currency,
    seedlistingEmailsAvailable,
    billingPlanType,
  })
    .then((data) => {
      dispatch(setLiqpayNewPaymentData(data));
      dispatch(handleLiqpayNewPaymentDataIsLoading(false));
    })
    .catch((err) => {
      dispatch(handleLiqpayNewPaymentDataIsLoading(false));
      return toast.error(err);
    });
};

export const setExchangeData = (value) => ({
  type: actionsTypes.SET_EXCHANGE_DATA,
  payload: {
    data: value,
  },
});

export const handleExchangeDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_EXCHANGE_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const getExchangeData = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  try {
    dispatch(handleExchangeDataIsLoading(true));

    const { data } = await axios.get(
      'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11'
    );
    dispatch(setExchangeData(data));
    dispatch(handleExchangeDataIsLoading(false));
  } catch (err) {
    dispatch(handleExchangeDataIsLoading(false));
    toast.error(
      messages[locale]['payment.toast.get_exchange_data.exchange_data.error']
    );
    console.error('err', err);
  }
};

export const setOnGoPaymentParams = (value) => ({
  type: actionsTypes.SET_ON_GO_PAYMENT_PARAMS,
  payload: {
    data: value,
  },
});

export const handleOnGoPaymentParamsIsEdit = (value) => ({
  type: actionsTypes.HANDLE_ON_GO_PAYMENT_PARAMS_IS_EDIT,
  payload: {
    data: value,
  },
});

export const handleShowWarningPopup = (value) => ({
  type: actionsTypes.HANDLE_SHOW_WARNING_POPUP,
  payload: {
    data: value,
  },
});

export const handleShowSuccessPopup = (value) => ({
  type: actionsTypes.HANDLE_SHOW_SUCCESS_POPUP,
  payload: {
    data: value,
  },
});

export const handleWireTransferPaymentDataIsSend = (value) => ({
  type: actionsTypes.HANDLE_WIRE_TRANSFER_PAYMENT_DATA_IS_SEND,
  payload: {
    data: value,
  },
});

export const handleWireTransferPaymentDataIsSended = (value) => ({
  type: actionsTypes.HANDLE_WIRE_TRANSFER_PAYMENT_DATA_IS_SENDED,
  payload: {
    data: value,
  },
});

export const sendWireTransferNewPaymentData = ({
  billingPlanName,
  currency,
  seedlistingEmailsAvailable,
  billingPlanType,
}) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!billingPlanName) {
    return toast.error(
      messages[locale][
        'payment.toast.wire-transfer_new_payment.billing_plan_name.error'
      ]
    );
  }
  if (!billingPlanType) {
    return toast.error(
      messages[locale][
        'payment.toast.get_liqpay_new_payment.billing_plan_type.error'
      ]
    );
  }
  if (!currency) {
    return toast.error(
      messages[locale]['payment.toast.wire-transfer_new_payment.currency.error']
    );
  }
  if (
    billingPlanName === BILLING_PLAN_NAME.ON_GO &&
    !seedlistingEmailsAvailable
  ) {
    return toast.error(
      messages[locale][
        'payment.toast.wire-transfer_new_payment.number_of_checks.error'
      ]
    );
  }

  dispatch(handleWireTransferPaymentDataIsSend(true));

  Api.addWireTransferNewPaymentData(
    billingPlanName === BILLING_PLAN_NAME.ON_GO
      ? {
          seedlisting_emails_available: seedlistingEmailsAvailable,
          billing_plan_name: billingPlanName,
          currency,
          billing_plan_type: billingPlanType,
        }
      : {
          billing_plan_name: billingPlanName,
          billing_plan_type: billingPlanType,
          currency,
        }
  )
    .then((data) => {
      dispatch(handleWireTransferPaymentDataIsSended(true));
      if (data) {
        toast.success(
          `${messages[locale]['payment.toast.wire-transfer_send_new_payment.success']}`,
          {
            autoClose: false,
          }
        );
        return dispatch(handleWireTransferPaymentDataIsSend(false));
      }
      throw new Error();
    })
    .catch((err) => {
      dispatch(handleWireTransferPaymentDataIsSended(true));
      dispatch(handleWireTransferPaymentDataIsSend(false));
      return toast.error(err);
    });
};

export const handleDeliverabilityConsultingDataIsSending = (value) => ({
  type: actionsTypes.HANDLE_DELIVERABILITY_CONSULTING_DATA_IS_SENDING,
  payload: {
    data: value,
  },
});

export const sendDeliverabilityConsultingData = (data) => (
  dispatch,
  getState
) => {
  const state = getState();
  const locale = localeCurrentSelector(state);
  let counts = getAllCounts(); // чтение количества запросов из localstorage

  if (!data?.email) {
    return toast.error(
      messages[locale][
        'payment.toast.set_deliverability_consulting.data.email.error'
      ]
    );
  }

  // если количество запросов больше установленного
  if (
    counts?.count[DELIVERABILITY_CONSULTING_TYPE] >=
    DELIVERABILITY_CONSULTING_MAX_REQUEST
  ) {
    const currentDate = Date.now();
    const timeStart = counts.timeStart[DELIVERABILITY_CONSULTING_TYPE];
    // проверим дату на сутки
    if (currentDate - timeStart < COUNTS_PERIOD_REQUEST) {
      return toast.error(
        messages[locale][
          'payment.toast.set_deliverability_consulting.req_processed.error'
        ]
      );
    }
  }

  // если запросов еще не было
  if (!counts) {
    // создать объект в localstorage
    counts = setCounts({
      type: DELIVERABILITY_CONSULTING_TYPE,
      savedData: null,
      maxRequest: DELIVERABILITY_CONSULTING_MAX_REQUEST,
    });
  }

  dispatch(handleDeliverabilityConsultingDataIsSending(true));

  Api.addDeliverabilityConsulting(data)
    .then((data) => {
      if (data) {
        setCounts({
          type: DELIVERABILITY_CONSULTING_TYPE,
          savedData: counts,
          maxRequest: DELIVERABILITY_CONSULTING_MAX_REQUEST,
        });
        dispatch(handleShowSuccessPopup(true));
        return dispatch(handleDeliverabilityConsultingDataIsSending(false));
      }
      throw new Error();
    })
    .catch((err) => {
      dispatch(handleDeliverabilityConsultingDataIsSending(false));
      return toast.error(err);
    });
};
