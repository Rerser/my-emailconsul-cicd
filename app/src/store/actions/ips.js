import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
  ipsDataSelector,
  ipsDataPaginationSelector,
  ipsDataFilterSelector,
  ipsDataSortingSelector,
  ipsNewIpsFileSelector,
  netmasksDataSelector,
} from 'store/selectors';
import {
  validateIpAddress,
  deepCompare,
  getStatusError,
} from 'utils/helpers';
import messages from 'utils/messages';
import {
  IP_STATUS,
  NOT_FOUND_ERROR_STATUS,
  ONE_MEGA_BYTE,
} from 'utils/constants';
import {
  getNetmasksData,
  setNetmasksData,
} from './netmasks';

export const setIpsErrors = (value) => ({
  type: actionsTypes.SET_IPS_ERRORS,
  payload: {
    data: value,
  },
});

export const handleIpsDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_IPS_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setIpsDataPagination = (value) => ({
  type: actionsTypes.SET_IPS_DATA_PAGINATION,
  payload: {
    data: value,
  },
});

export const handleIpsDataFilter = (value) => ({
  type: actionsTypes.HANDLE_IPS_DATA_FILTER,
  payload: {
    data: value,
  },
});

export const handleIpsDataSorting = (value) => ({
  type: actionsTypes.HANDLE_IPS_DATA_SORTING,
  payload: {
    data: value,
  },
});

export const setIpsData = (value) => ({
  type: actionsTypes.SET_IPS_DATA,
  payload: {
    data: value,
  },
});

export const getIpsData = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  if (withLoader) {
    dispatch(handleIpsDataIsLoading(true));
  }

  /*
    get params
  */
  const state = getState();
  const ipsData = ipsDataSelector(state);
  const paginationData = ipsDataPaginationSelector(state);

  const { netmask, ip } = ipsDataFilterSelector(state);
  const { field, order } = ipsDataSortingSelector(state);
  const { offset, limit } = paginationData;

  const sort = order === 'desc' ? `-${field}` : field;

  Api.getIpsData({
    offset,
    limit,
    netmask,
    ip,
    sort,
  })
    .then((data) => {
      const { data: ips = [], meta: pagination = {} } = data;

      if (
        !deepCompare(ips, ipsData) ||
        !deepCompare(pagination, paginationData)
      ) {
        // update data and pagination data at once
        dispatch(setIpsData(data));
      }

      dispatch(handleIpsDataIsLoading(false));
    })
    .catch((err) => {
      const status = getStatusError(err);

      if (status === NOT_FOUND_ERROR_STATUS) {
        dispatch(
          setIpsErrors({
            data: {
              message: err,
              status,
            },
          })
        );
      }
      dispatch(handleIpsDataIsLoading(false));

      return console.error('err', err);
    });
};

export const handleIpsAvailableCountIsLoading = (value) => ({
  type: actionsTypes.HANDLE_IPS_AVAILABLE_COUNT_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setIpsAvailableCount = (value) => ({
  type: actionsTypes.SET_IPS_AVAILABLE_COUNT,
  payload: {
    data: value,
  },
});

export const getIpsAvailableCount = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  dispatch(handleIpsAvailableCountIsLoading(true));

  try {
    const data = await Api.getIpsAvailableCount();
    dispatch(handleIpsAvailableCountIsLoading(false));
    dispatch(setIpsAvailableCount(data));
  } catch (err) {
    const status = getStatusError(err);
    dispatch(handleIpsAvailableCountIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']}`
      );
    }
    return console.error('err', err);
  }
};

export const setEditedIp = (value) => ({
  type: actionsTypes.SET_EDITED_IP,
  payload: {
    data: value,
  },
});

export const handleEditedIp = (value) => ({
  type: actionsTypes.HANDLE_EDITED_IP,
  payload: {
    data: value,
  },
});

export const handleEditedIpIsLoading = (value) => ({
  type: actionsTypes.HANDLE_EDITED_IP_IS_LOADING,
  payload: {
    data: value,
  },
});

// outdated
export const saveEditedIp = (data) => (dispatch, getState) => {
  const { id, ip } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  const requestData = {
    id,
    ip,
  };

  if (!ip) {
    return toast.error(messages[locale]['ips.toast.save_edited_ip.ip.error']);
  }
  if (!validateIpAddress(ip)) {
    return toast.error(
      messages[locale]['ips.toast.save_edited_ip.ip.valid.error']
    );
  }

  let method = '';
  let message = '';
  if (id) {
    method = 'updateIp';
    message = messages[locale]['ips.toast.save_edited_ip.updated'];
  } else {
    method = 'addIp';
    message = messages[locale]['ips.toast.save_edited_ip.added'];
  }

  dispatch(handleEditedIpIsLoading(true));

  Api[method](requestData)
    .then((data) => {
      dispatch(handleEditedIpIsLoading(false));
      dispatch(setEditedIp(null));
      dispatch(getIpsData());
      if (method === 'addIp') {
        dispatch(getIpsAvailableCount());
      }
      return toast.success(message);
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleEditedIpIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return toast.error(err);
    });
};

export const deleteIp = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(messages[locale]['ips.toast.delete_ip.id.error']);
  }

  Api.deleteIp({ id })
    .then((data) => {
      dispatch(getIpsData({ withLoader: false }));
      dispatch(getIpsAvailableCount());
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return toast.error(err);
    });
};

export const deleteIpsFromFilters = () => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const { netmask, ip: filterIp } = ipsDataFilterSelector(state); 
  const netmasksData = netmasksDataSelector(state);

  Api.deleteIpsFromFilters({ filterIp, netmask })
    .then((data) => {
      if (netmask || filterIp) {
        dispatch(handleIpsDataFilter({ field: 'ip', value: '' }));
        dispatch(handleIpsDataFilter({ field: 'netmask', value: '' }));
      }
      if (netmask && netmasksData.length) {
        const foundNetmaskIndex = netmasksData.indexOf(netmask);

        if (foundNetmaskIndex > -1) {
          netmasksData.splice(foundNetmaskIndex, 1);
          dispatch(setNetmasksData(netmasksData));
        }
      }
      dispatch(getIpsData({ withLoader: false }));
      dispatch(getIpsAvailableCount());
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return toast.error(err);
    });
};

const setCurrentIpStatus = (value) => ({
  type: actionsTypes.SET_CURRENT_IP_STATUS,
  payload: {
    data: value,
  },
});

export const refreshIp = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(messages[locale]['ips.toast.refresh_ip.id.error']);
  }

  const foundIpIndex = ipsDataSelector(state).findIndex(ipData => ipData.id === id);

  if (foundIpIndex > -1) {
    dispatch(setCurrentIpStatus({
      index: foundIpIndex,
      status: IP_STATUS.WAITING
    }));
  }

  Api.refreshIp({ id })
    .then((data) => {
      dispatch(getIpsData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return toast.error(err);
    });
};

export const setIsOnNewIp = (value) => ({
  type: actionsTypes.SET_IS_ON_NEW_IP,
  payload: {
    data: value,
  },
});

export const handleNewIps = (value) => ({
  type: actionsTypes.HANDLE_NEW_IPS,
  payload: {
    data: value,
  },
});

export const handleNewNetmask = (value) => ({
  type: actionsTypes.HANDLE_NEW_NETMASK,
  payload: {
    data: value,
  },
});

export const handleNewIpMode = (value) => ({
  type: actionsTypes.HANDLE_NEW_IP_MODE,
  payload: {
    data: value,
  },
});

export const removeNewIp = (value) => ({
  type: actionsTypes.REMOVE_NEW_IP,
  payload: {
    data: value,
  },
});

export const handleNewIpIsLoading = (value) => ({
  type: actionsTypes.HANDLE_NEW_IP_IS_LOADING,
  payload: {
    data: value,
  },
});

export const saveNewNetmask = (data) => (dispatch, getState) => {
  const { netmask } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  const trimmedValue = netmask.trim();

  let requestData = {};

  if (!trimmedValue) {
    return toast.error(messages[locale]['netmask is required']);
  }

  // netmask validate only on backend with specific library
  requestData = { netmask: trimmedValue };

  dispatch(handleNewIpIsLoading(true));

  Api.addNetmask(requestData)
    .then((data) => {
      dispatch(handleNewIpIsLoading(false));
      dispatch(setIsOnNewIp(false));
      dispatch(getIpsData());
      dispatch(getNetmasksData({ withLoader: false }));
      dispatch(getIpsAvailableCount());
      return toast.success(
        messages[locale]['ips.toast.save_netmask.netmask.added.success']
      );
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewIpIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return console.error('err', err);
    });
};

export const saveNewIps = ({ newIps: newIpsData }) => (dispatch, getState) => {
  const newIps = new Set(newIpsData.map(ip => ip.trim()));

  const state = getState();
  const locale = localeCurrentSelector(state);

  let validationErrorMessage = null;

  newIps.forEach((ip) => {
    if (!ip) {
      validationErrorMessage =
        messages[locale]['ips.toast.save_newips.ip.empty'];
    }
    if (!validateIpAddress(ip)) {
      validationErrorMessage = `Ip ${ip} ${messages[locale]['ips.toast.save_newips.not_valid']}`;
    }
  });

  if (validationErrorMessage) {
    return toast.error(validationErrorMessage);
  }

  dispatch(handleNewIpIsLoading(true));

  Api.addIp({ ips: Array.from(newIps) })
    .then((data) => {
      dispatch(handleNewIpIsLoading(false));
      if (data) {
        dispatch(setIsOnNewIp(false));
        dispatch(getIpsData());
        dispatch(getIpsAvailableCount());
        return toast.success(
          messages[locale]['ips.toast.save_newips.ip.added.success']
        );
      }
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewIpIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      console.error('err', err);
      return toast.error(err);
    });
};

export const deleteNetmask = (netmask) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!netmask) {
    return toast.error(
      messages[locale]['ips.toast.delete_netmask.netmask.error']
    );
  }

  Api.deleteNetmask(netmask)
    .then((data) => {
      dispatch(handleIpsDataFilter({ field: 'netmask', value: null })); // -> getIpsData will dispatch automatically after ffilter field changes
      dispatch(getNetmasksData({ withLoader: false }));
      dispatch(getIpsAvailableCount());
      return toast.success(
        `${messages[locale]['ips.toast.delete_netmask.netmask.delete.sucess.1']} ${netmask} ${messages[locale]['ips.toast.delete_netmask.netmask.delete.sucess.2']}`
      );
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return console.error('err', err);
    });
};

export const setNewIpsFile = (value) => ({
  type: actionsTypes.SET_NEW_IPS_FILE,
  payload: {
    data: value,
  },
});

export const handleNewIpsFileIsUploading = (value) => ({
  type: actionsTypes.HANDLE_NEW_IPS_FILE_IS_UPLOADING,
  payload: {
    data: value,
  },
});

export const handelSetNewIpsFile = (file) =>  (dispatch, getState) =>{  
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!file) {
    return dispatch(setNewIpsFile(null));
  }
  
  if (file.type !== 'text/csv') {
    return toast.error(messages[locale]['ips.toast.upload_csv.file.type.error']);
  }
  if (file.size > ONE_MEGA_BYTE) {
    return toast.error(messages[locale]['ips.toast.upload_csv.file.size.error']);
  }

  dispatch(setNewIpsFile(file));
};

export const setResponseNewIpsUploadedFile = (value) => ({
  type: actionsTypes.SET_RESPONSE_NEW_IPS_UPLOADED_FILE,
  payload: {
    data: value,
  },
});

export const handelUploadNewIpsFile = () => async (dispatch, getState) =>{
  const state = getState();
  const locale = localeCurrentSelector(state);
  const currentIpsFile = ipsNewIpsFileSelector(state);

  if (!currentIpsFile) {
    return toast.error(messages[locale]['ips.toast.upload_csv.file.error']);
  }
  
  const formData = new FormData();

  formData.append('file', currentIpsFile);

  try {
    dispatch(handleNewIpsFileIsUploading(true));

    const data = await Api.importIpsFromCSV(formData);

    dispatch(handleNewIpsFileIsUploading(false));
    dispatch(handelSetNewIpsFile(null));
    dispatch(setResponseNewIpsUploadedFile(data)); 
    dispatch(getIpsAvailableCount());
    dispatch(getIpsData({ withLoader: false }));
    return toast.success(messages[locale]['ips.toast.save_newips.ip.added.success']);
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleNewIpsFileIsUploading(false));
    dispatch(handelSetNewIpsFile(null));
    dispatch(setResponseNewIpsUploadedFile(null)); 
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

// rbl-logs

export const setIpRblChangesLogsPeriodFrom = (value) => ({
  type: actionsTypes.SET_IP_RBL_CHANGES_LOGS_PERIOD_FROM,
  payload: {
    data: value,
  },
});

export const setIpRblChangesLogsPeriodTo = (value) => ({
  type: actionsTypes.SET_IP_RBL_CHANGES_LOGS_PERIOD_TO,
  payload: {
    data: value,
  },
});

export const handleIpRblChangesLogsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_IP_RBL_CHANGES_LOGS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setIpRblChangesLogs = (value) => ({
  type: actionsTypes.SET_IP_RBL_CHANGES_LOGS,
  payload: {
    data: value,
  },
});

export const setIpRblChangesLogsErrors = (value) => ({
  type: actionsTypes.SET_IP_RBL_CHANGES_LOGS_ERRORS,
  payload: {
    data: value,
  },
});

export const getIpRblChangesLogs = ({ id }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(messages[locale]['ips.toast.chosen_ip.id.error']);
  }

  try {
    dispatch(handleIpRblChangesLogsIsLoading(true));

    const data = await Api.getIpRblChangesLogs({ id });

    dispatch(setIpRblChangesLogs(data));
    dispatch(handleIpRblChangesLogsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setIpRblChangesLogsErrors({
          ipRblChangesLogs: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleIpRblChangesLogsIsLoading(false));
    return console.error('err', err);
  }
};
