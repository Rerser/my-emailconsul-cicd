import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
} from 'store/selectors';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import { getStatusError } from 'utils/helpers';
import messages from 'utils/messages';

export const handleDmarcDataIsLoading = (data) => ({
  type: actionsTypes.HANDLE_DMARC_DATA_IS_LOADING,
  payload: {
    data,
  },
});

export const setDmarcData = (data) => ({
  type: actionsTypes.SET_DMARC_DATA,
  payload: {
    data,
  },
});

export const setDmarcDataError = (data) => ({
  type: actionsTypes.SET_DMARC_DATA_ERROR,
  payload: {
    data,
  },
});

export const getDmarcData = (domain) => async (dispatch, getState) => {
  try {
    const state = getState();
    const locale = localeCurrentSelector(state);

    if (!domain) {
      return toast.error(messages[locale]['dmarc.toast.domain.error']);
    }
    dispatch(handleDmarcDataIsLoading(true));

    const data = await Api.getAuthenticationDMARC(domain);
    dispatch(setDmarcData(data));

    dispatch(handleDmarcDataIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setDmarcDataError({
          message: err.message,
          status,
        })
      );
    }
    dispatch(handleDmarcDataIsLoading(false));
    return console.error('err', err);
  }
};