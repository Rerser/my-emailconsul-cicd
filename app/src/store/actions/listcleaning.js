import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import messages from 'utils/messages';
import { localeCurrentSelector } from 'store/selectors';

export const hanleListcleaningDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_LISTCLEANING_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const getListcleaningData = ({ withLoader = true } = {}) => (
  dispatch
) => {
  if (withLoader) {
    dispatch(hanleListcleaningDataIsLoading(true));
  }

  Api.getListcleaningData()
    .then((data) => {
      dispatch({
        type: actionsTypes.SET_LISTCLEANING_DATA,
        payload: {
          data,
        },
      });

      if (withLoader) {
        dispatch(hanleListcleaningDataIsLoading(false));
      }
    })
    .catch((err) => {
      if (withLoader) {
        dispatch(hanleListcleaningDataIsLoading(false));
      }
      return toast.error(err);
    });
};

// export const setFilesSort = (value) => (dispatch) => {
//   dispatch({
//     type: actionsTypes.SET_FILES_SORT,
//     payload: {
//       data: value,
//     }
//   })
// }

export const setNewListCleaning = (value) => ({
  type: actionsTypes.SET_NEW_LISTCLEANING,
  payload: {
    data: value,
  },
});

export const handleNewListCleaningFile = (value) => ({
  type: actionsTypes.HANDLE_NEW_LISTCLEANING_FILE,
  payload: {
    data: value,
  },
});

export const handleNewListCleaningFileIsUploaging = (value) => ({
  type: actionsTypes.HANDLE_NEW_LISTCLEANING_FILE_IS_UPLOADING,
  payload: {
    data: value,
  },
});

export const runListcleaning = (data) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!data.file) {
    return toast.error(
      messages[locale]['listcleaning.toast.run_listcleaning.file.error']
    );
  }
  if (data.file.type !== 'text/csv') {
    return toast.error(
      messages[locale]['listcleaning.toast.run_listcleaning.type.error']
    );
  }

  const requestData = new FormData();
  requestData.append('file', data.file);

  toast.warning(
    messages[locale]['listcleaning.toast.run_listcleaning.window.warning'],
    {
      autoClose: false,
    }
  );
  dispatch(handleNewListCleaningFileIsUploaging(true));

  Api.runListcleaning(requestData)
    .then((data) => {
      dispatch(getListcleaningData());
      dispatch(setNewListCleaning(null));
      dispatch(handleNewListCleaningFileIsUploaging(false));
      toast.success(
        messages[locale][
          'listcleaning.toast.run_listcleaning.listcleaning.start.success'
        ],
        {
          autoClose: false,
        }
      );
    })
    .catch((err) => {
      dispatch(handleNewListCleaningFileIsUploaging(false));
      return toast.error(err);
    });
};

export const terminateListCleaning = (data) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!data || !data.id) {
    return toast.error(
      messages[locale]['listcleaning.toast.terminate_listcleaning.data.error']
    );
  }

  Api.terminateListCleaning({ listcleaning_id: data.id })
    .then((data) => {
      dispatch(getListcleaningData());
    })
    .catch((err) => toast.error(err));
};
