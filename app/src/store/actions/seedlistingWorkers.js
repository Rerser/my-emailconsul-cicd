import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { validateEmail, deepCompare, getStatusError } from 'utils/helpers';
import {
  SEEDLISTING_WORKER_STATUS,
  NOT_FOUND_ERROR_STATUS,
} from 'utils/constants';
import messages from 'utils/messages';
import {
  localeCurrentSelector,
  seedlistingWorkersDataSelector,
  seedlistingWorkersDataPaginationSelector,
  seedlistingWorkersSortingSelector,
} from 'store/selectors';

export const setSeedlistingWorkersErrors = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_WORKERS_ERRORS,
  payload: {
    data: value,
  },
});

export const handleSeedlistingWorkersDataIsLoading = (data) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_WORKERS_DATA_IS_LOADING,
  payload: {
    data,
  },
});

export const setSeedlistingWorkersData = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_WORKERS_DATA,
  payload: {
    data,
  },
});

export const setSeedlistingWorkersDataPagination = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_WORKERS_DATA_PAGINATION,
  payload: {
    data: value,
  },
});

export const setSeedlistingWorkersSorting = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_WORKERS_SORTING,
  payload: {
    data,
  },
});

export const setSeedlistingWorkersHosts = (data) => ({
  type: actionsTypes.SET_SEEDLISTING_WORKERS_HOSTS,
  payload: {
    data,
  },
});

export const handleSeedlistingWorkersHostsIsLoading = (data) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_WORKERS_HOSTS_IS_LOADING,
  payload: {
    data,
  },
});

export const getSeedlistingWorkersHosts = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  try {
    dispatch(handleSeedlistingWorkersHostsIsLoading(true));

    const hosts = await Api.getSeedlistingWorkersHosts();

    dispatch(setSeedlistingWorkersHosts(hosts));
    dispatch(handleSeedlistingWorkersHostsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleSeedlistingWorkersHostsIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']} - getSeedlistingWorkerHosts`
      );
    }
    return console.error(err);
  }
};

export const getSeedlistingWorkersData = ({ withLoader = true } = {}) => async (
  dispatch,
  getState
) => {
  const state = getState();

  try {
    const seedlistingWorkersData = seedlistingWorkersDataSelector(state);
    const paginationData = seedlistingWorkersDataPaginationSelector(state);

    if (withLoader) {
      dispatch(handleSeedlistingWorkersDataIsLoading(true));
    }

    const { field, order } = seedlistingWorkersSortingSelector(state);
    const { offset, limit } = paginationData;

    const sort = order === 'desc' ? `-${field}` : field;

    const data = await Api.getSeedlistingWorkersData({ offset, limit, sort });
    const { data: seedlistingWorkers, meta: pagination = {} } = data;

    if (
      !deepCompare(seedlistingWorkers, seedlistingWorkersData) ||
      !deepCompare(pagination, paginationData)
    ) {
      dispatch(setSeedlistingWorkersData(data));
    }

    dispatch(handleSeedlistingWorkersDataIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setSeedlistingWorkersErrors({
          data: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleSeedlistingWorkersDataIsLoading(false));
    return console.error(err);
  }
};

export const setNewSeedlistingWorker = (data) => ({
  type: actionsTypes.SET_NEW_SEEDLISTING_WORKER,
  payload: {
    data,
  },
});

export const handleNewSeedlistingWorker = (data) => ({
  type: actionsTypes.HANDLE_NEW_SEEDLISTING_WORKER,
  payload: {
    data,
  },
});

export const handleNewSeedlistingWorkerSavingIsLoading = (data) => ({
  type: actionsTypes.HANDLE_NEW_SEEDLISTING_WORKER_SAVING_IS_LOADING,
  payload: {
    data,
  },
});

export const addSeedlistingWorker = (data) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);
  const { email, password, host, worker_container } = data;

  if (!email) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.email.error'
      ]
    );
  }
  if (!validateEmail(email)) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.email.valid.error'
      ]
    );
  }
  if (!password) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.password.error'
      ]
    );
  }
  if (!host) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.host.error'
      ]
    );
  }
  if (!worker_container) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.worker_container.error'
      ]
    );
  }

  dispatch(handleNewSeedlistingWorkerSavingIsLoading(true));

  Api.addSeedlistingWorker(data)
    .then((data) => {
      dispatch(handleNewSeedlistingWorkerSavingIsLoading(false));
      dispatch(setNewSeedlistingWorker(null));
      dispatch(getSeedlistingWorkersData());
      return toast.success(
        messages[locale][
          'seedlisting_workers.toast.add_seedlisting_worker.worker.created.success'
        ]
      );
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewSeedlistingWorkerSavingIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - addSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};

export const deleteSeedlistingWorker = (data) => (dispatch, getState) => {
  const { id } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.delete_seedlisting_worker.id.error'
      ]
    );
  }

  Api.deleteSeedlistingWorker({ id })
    .then((data) => {
      dispatch(getSeedlistingWorkersData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - deleteSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};

export const restartSeedlistingWorker = (data) => (dispatch, getState) => {
  const { id } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.restart_seedlisting_worker.id.error'
      ]
    );
  }

  Api.updateSeedlistingWorker({
    id,
    data: { status: SEEDLISTING_WORKER_STATUS.STARTING },
  })
    .then((data) => {
      dispatch(getSeedlistingWorkersData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - restartSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};

export const terminateSeedlistingWorker = (data) => (dispatch, getState) => {
  const { id } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.terminate_seedlisting_worker.id.error'
      ]
    );
  }

  Api.updateSeedlistingWorker({
    id,
    data: { status: SEEDLISTING_WORKER_STATUS.TERMINATED },
  })
    .then((data) => {
      dispatch(getSeedlistingWorkersData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - terminateSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};

export const updateSeedlistingWorker = (data) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);
  const { id, email, password, host, worker_container } = data;

  if (!id) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.update_seedlisting_worker.id.error'
      ]
    );
  }
  if (!email) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.email.error'
      ]
    );
  }
  if (!validateEmail(email)) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.email.valid.error'
      ]
    );
  }
  if (!password) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.password.error'
      ]
    );
  }
  if (!host) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.host.error'
      ]
    );
  }
  if (!worker_container) {
    return toast.error(
      messages[locale][
        'seedlisting_workers.toast.add_update_seedlisting_worker.worker_container.error'
      ]
    );
  }

  dispatch(handleNewSeedlistingWorkerSavingIsLoading(true));

  Api.updateSeedlistingWorker({
    id,
    data: { email, password, status: SEEDLISTING_WORKER_STATUS.STARTING },
  })
    .then((data) => {
      dispatch(handleNewSeedlistingWorkerSavingIsLoading(false));
      dispatch(setNewSeedlistingWorker(null));
      dispatch(getSeedlistingWorkersData());
      return toast.success(
        messages[locale][
          'seedlisting_workers.toast.update_seedlisting_worker.updated.success'
        ]
      );
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewSeedlistingWorkerSavingIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - updateSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};

export const restartAllSeedlistingWorkers = () => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  Api.restartAllSeedlistingWorkers()
    .then((data) => {
      dispatch(getSeedlistingWorkersData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - restartSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};

export const terminateAllSeedlistingWorkers = () => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  Api.terminateAllSeedlistingWorkers()
    .then((data) => {
      dispatch(getSeedlistingWorkersData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - terminateAllSeedlistingWorker`
        );
      }
      return toast.error(err);
    });
};
