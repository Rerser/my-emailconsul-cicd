import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { validateEmail } from 'utils/helpers';
import { TIMEZONES } from 'utils/constants';
import { usersMeSelector, localeCurrentSelector } from 'store/selectors';
import messages from 'utils/messages';

export const handleIpsReportDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_IPS_REPORT_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setIpsReportData = (value) => ({
  type: actionsTypes.SET_IPS_REPORT_DATA,
  payload: {
    data: value,
  },
});

export const handleIpsReportData = (value) => ({
  type: actionsTypes.HANDLE_IPS_REPORT_DATA,
  payload: {
    data: value,
  },
});

export const handleIpsReportIsOnEdit = (value) => ({
  type: actionsTypes.HANDLE_IPS_REPORT_IS_ON_EDIT,
  payload: {
    data: value,
  },
});

export const getIpsReportData = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  /*
    get usersMe
  */
  const state = getState();
  const usersMe = usersMeSelector(state);

  if (withLoader) {
    dispatch(handleIpsReportDataIsLoading(true));
  }

  Api.getIpsReportData()
    .then((data) => {
      if (data?.front_end_stored_data) {
        const parsedEmailReportFrontEndStoredData = JSON.parse(
          data.front_end_stored_data
        );
        data.hours = parsedEmailReportFrontEndStoredData.hours;
        data.timezone_index =
          parsedEmailReportFrontEndStoredData.timezone_index;
      }
      // set current user email by default
      if ((!data?.emails || !data?.emails[0]) && usersMe?.email) {
        data.emails = [usersMe.email];
      }

      dispatch(setIpsReportData(data));

      if (withLoader) {
        dispatch(handleIpsReportDataIsLoading(false));
      }
    })
    .catch((err) => {
      if (withLoader) {
        dispatch(handleIpsReportDataIsLoading(false));
      }
      return console.error('err', err);
    });
};

export const saveIpsReportData = (data) => (dispatch, getState) => {
  const { is_active, hours, timezone_index, emails, isNew } = data;

  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!emails || !emails.length) {
    return toast.error(
      messages[locale]['ipsreport.toast.save_ips_report.emails.error']
    );
  }

  for (const email of emails) {
    if (!validateEmail(email)) {
      return toast.error(
        `${messages[locale]['ipsreport.toast.save_ips_report.email.valid.error.1']} ${email} ${messages[locale]['ipsreport.toast.save_ips_report.email.valid.error.2']}`
      );
    }
  }

  const requestData = {
    is_active,
    emails,
  };

  if (is_active) {
    if (!hours) {
      return toast.error(
        messages[locale]['ipsreport.toast.save_ips_report.hours.error']
      );
    }
    if (!timezone_index) {
      return toast.error(
        messages[locale]['ipsreport.toast.save_ips_report.ofset.error']
      );
    }

    /*
      get hours and minutes in utc
    */
    const timezone = TIMEZONES.find((item) => item.index === +timezone_index);

    const d = new Date();

    d.setHours(+hours);
    d.setMinutes(0);

    const now = Date.parse(d);
    const tt = new Date(now - timezone.utc_offset_sec * 1000);
    const ttHours = tt.getHours();
    const ttMinutes = tt.getMinutes();

    const frontEndStoredData = {
      hours,
      timezone_index,
    };

    requestData.utc_hours = ttHours;
    requestData.utc_minutes = ttMinutes;
    requestData.front_end_stored_data = JSON.stringify(frontEndStoredData);
  }

  let method = '';
  let message = '';
  if (!isNew) {
    method = 'updateIpsReport';
    message = messages[locale]['ipsreport.toast.save_ips_report.updated'];
  } else {
    method = 'addIpsReport';
    message = messages[locale]['ipsreport.toast.save_ips_report.added'];
  }

  dispatch(handleIpsReportDataIsLoading(true));

  Api[method](requestData)
    .then((data) => {
      dispatch(handleIpsReportDataIsLoading(false));
      dispatch(handleIpsReportIsOnEdit(false));
      if (isNew) {
        dispatch(
          handleIpsReportData({
            field: 'isNew',
            value: false,
          })
        );
      }
      return toast.success(message);
    })
    .catch((err) => {
      dispatch(handleIpsReportDataIsLoading(false));
      return toast.error(err);
    });
};
