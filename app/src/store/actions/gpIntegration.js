import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { localeCurrentSelector } from 'store/selectors';
import { getStatusError, gotoExternalLink } from 'utils/helpers';
import messages from 'utils/messages';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';

export const setGpUserIntegrationErrors = (value) => ({
  type: actionsTypes.SET_GP_USER_INTEGRATION_ERRORS,
  payload: {
    data: value,
  },
});

// check user integration

export const handleGpUserIntegratedIsLoading = (value) => ({
  type: actionsTypes.HANDLE_GP_USER_INTEGRATED_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setGpUserIntegrated = (value) => ({
  type: actionsTypes.SET_GP_USER_INTEGRATED,
  payload: {
    data: value,
  },
});

export const checkGpUserCredentials = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  try {
    dispatch(handleGpUserIntegratedIsLoading(true));

    const data = await Api.checkGpUserCredentials();
    dispatch(setGpUserIntegrated(data.status));

    dispatch(handleGpUserIntegratedIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleGpUserIntegratedIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setGpUserIntegrationErrors({
          userIntegration: {
            message: err,
            status,
          },
        })
      );
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

// integrate with gp

export const setGpSecretsIsShowSlider = (value) => ({
  type: actionsTypes.SET_GP_SECRETS_IS_SHOW_SLIDER,
  payload: {
    data: value,
  },
});

export const setGpSecrets = (value) => ({
  type: actionsTypes.SET_GP_SECRETS,
  payload: {
    data: value,
  },
});

export const handleGpSecretsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_GP_SECRETS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const sendGpSecrets = ({ creds }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!creds) {
    return toast.error(messages[locale]['gp_integration.save_creds.no-creds.error']);
  }

  try {
    dispatch(handleGpSecretsIsLoading(true));

    const data = await Api.sendGpSecrets({ creds });

    if (data) {
      const link = await Api.getGPAuthUrl();

      if (link) {
        gotoExternalLink(link);
      }
    }

    dispatch(handleGpSecretsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleGpSecretsIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

// run gp domains with check date

export const setGpDateRunIsShowSlider = (value) => ({
  type: actionsTypes.SET_GP_DATE_RUN_IS_SHOW_SLIDER,
  payload: {
    data: value,
  },
});

export const setGpDateRun = (value) => ({
  type: actionsTypes.SET_GP_DATE_RUN,
  payload: {
    data: value,
  },
});

export const handleGpDateRunIsLoading = (value) => ({
  type: actionsTypes.HANDLE_GP_DATE_RUN_IS_LOADING,
  payload: {
    data: value,
  },
});

export const sendGpDateRun = ({ date }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!date) {
    return toast.error(messages[locale]['gp_integration.check_date.no-date.error']);
  }

  try {
    dispatch(handleGpDateRunIsLoading(true));

    const data = await Api.sendGpDateRun({ date });

    dispatch(handleGpDateRunIsLoading(false));
    if (data) {
      dispatch(setGpDateRunIsShowSlider(false));
      dispatch(setGpDomainsHistoryIsShowSlider(true));
    }
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleGpDateRunIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

// history

export const setGpDomainsHistoryIsShowSlider = (value) => ({
  type: actionsTypes.SET_GP_DOMAINS_HISTORY_IS_SHOW_SLIDER,
  payload: {
    data: value,
  },
});

export const handleGpDomainsHistoryIsLoading = (value) => ({
  type: actionsTypes.HANDLE_GP_DOMAINS_HISTORY_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleGpDomainsHistoryIsLoaded = (value) => ({
  type: actionsTypes.HANDLE_GP_DOMAINS_HISTORY_IS_LOADED,
  payload: {
    data: value,
  },
});

export const setGpDomainsHistory = (value) => ({
  type: actionsTypes.SET_GP_DOMAINS_HISTORY,
  payload: {
    data: value,
  },
});

export const getGpDomainsHistory = ({ withLoader = true, limit, offset }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  try {
    if (withLoader) {
      dispatch(handleGpDomainsHistoryIsLoading(true));
    }
    dispatch(handleGpDomainsHistoryIsLoaded(false));

    const data = await Api.getGpDomainsHistory({ limit, offset });
    dispatch(setGpDomainsHistory(data));

    dispatch(handleGpDomainsHistoryIsLoaded(true));
    dispatch(handleGpDomainsHistoryIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleGpDomainsHistoryIsLoaded(true));
    dispatch(handleGpDomainsHistoryIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setGpUserIntegrationErrors({
          gpDomainsHistory: {
            message: err,
            status,
          },
        })
      );
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

// clear all data
export const clearGpIntegrationAllData = () => ({
  type: actionsTypes.CLEAR_GP_INTEGRATION_ALL_DATA,
});
