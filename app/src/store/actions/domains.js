import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
  domainsDataSelector,
  domainsDataPaginationSelector,
  domainsDataFilterSelector,
  domainsDataSortingSelector,
  domainsNewDomainsFileSelector,
} from 'store/selectors';
import { deepCompare, getStatusError } from 'utils/helpers';
import messages from 'utils/messages';
import { DOMAIN_STATUS, NOT_FOUND_ERROR_STATUS, ONE_MEGA_BYTE } from 'utils/constants';

export const setDomainsErrors = (value) => ({
  type: actionsTypes.SET_DOMAINS_ERRORS,
  payload: {
    data: value,
  },
});

export const handleDomainsDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_DOMAINS_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setDomainsDataPagination = (value) => ({
  type: actionsTypes.SET_DOMAINS_DATA_PAGINATION,
  payload: {
    data: value,
  },
});

export const handleDomainsDataFilter = (value) => ({
  type: actionsTypes.HANDLE_DOMAINS_DATA_FILTER,
  payload: {
    data: value,
  },
});

export const handleDomainsDataSorting = (value) => ({
  type: actionsTypes.HANDLE_DOMAINS_DATA_SORTING,
  payload: {
    data: value,
  },
});

export const setDomainsData = (value) => ({
  type: actionsTypes.SET_DOMAINS_DATA,
  payload: {
    data: value,
  },
});

export const getDomainsData = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  if (withLoader) {
    dispatch(handleDomainsDataIsLoading(true));
  }

  /*
    get params
  */
  const state = getState();
  const domainsData = domainsDataSelector(state);
  const paginationData = domainsDataPaginationSelector(state);

  const { domain } = domainsDataFilterSelector(state);
  const { field, order } = domainsDataSortingSelector(state);
  const { offset, limit } = paginationData;

  const sort = order === 'desc' ? `-${field}` : field;

  Api.getDomainsData({
    offset,
    limit,
    domain,
    sort,
  })
    .then((data) => {
      const { data: domains = [], meta: pagination = {} } = data;

      if (
        !deepCompare(domains, domainsData) ||
        !deepCompare(pagination, paginationData)
      ) {
        // update data and pagination data at once
        dispatch(setDomainsData(data));
      }

      dispatch(handleDomainsDataIsLoading(false));
    })
    .catch((err) => {
      const status = getStatusError(err);

      if (status === NOT_FOUND_ERROR_STATUS) {
        dispatch(
          setDomainsErrors({
            data: {
              message: err,
              status,
            },
          })
        );
      }
      dispatch(handleDomainsDataIsLoading(false));

      return console.error('err', err);
    });
};

export const handleDomainsAvailableCountIsLoading = (value) => ({
  type: actionsTypes.HANDLE_DOMAINS_AVAILABLE_COUNT_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setDomainsAvailableCount = (value) => ({
  type: actionsTypes.SET_DOMAINS_AVAILABLE_COUNT,
  payload: {
    data: value,
  },
});

export const getDomainsAvailableCount = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  dispatch(handleDomainsAvailableCountIsLoading(true));

  try {
    const data = await Api.getDomainsAvailableCount();
    dispatch(handleDomainsAvailableCountIsLoading(false));
    dispatch(setDomainsAvailableCount(data));
  } catch (err) {
    const status = getStatusError(err);
    dispatch(handleDomainsAvailableCountIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

export const deleteDomain = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['domains.toast.delete_domain.id.error']
    );
  }

  Api.deleteDomain({ id })
    .then((data) => {
      dispatch(getDomainsData({ withLoader: false }));
      dispatch(getDomainsAvailableCount());
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(messages[locale]['error404.toast.description.text']);
      }
      return toast.error(err);
    });
};

export const deleteDomainsFromFilters = () => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const { domain: filterDomain } = domainsDataFilterSelector(state);

  Api.deleteDomainsFromFilters({ filterDomain })
    .then((data) => {
      if (filterDomain) {
        dispatch(handleDomainsDataFilter({ field: 'domain', value: '' }));
      }
      dispatch(getDomainsData({ withLoader: false }));
      dispatch(getDomainsAvailableCount());
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(messages[locale]['error404.toast.description.text']);
      }
      return toast.error(err);
    });
};

const setCurrentDomainStatus = (value) => ({
  type: actionsTypes.SET_CURRENT_DOMAIN_STATUS,
  payload: {
    data: value,
  },
});

export const refreshDomain = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['domains.toast.refresh_domain.id.error']
    );
  }

  const foundIpIndex = domainsDataSelector(state).findIndex(ipData => ipData.id === id);

  if (foundIpIndex > -1) {
    dispatch(setCurrentDomainStatus({
      index: foundIpIndex,
      status: DOMAIN_STATUS.WAITING
    }));
  }

  Api.refreshDomain({ id })
    .then((data) => {
      dispatch(getDomainsData({ withLoader: false }));
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(messages[locale]['error404.toast.description.text']);
      }
      return toast.error(err);
    });
};

export const setIsOnNewDomain = (value) => ({
  type: actionsTypes.SET_IS_ON_NEW_DOMAIN,
  payload: {
    data: value,
  },
});

export const handleNewDomains = (value) => ({
  type: actionsTypes.HANDLE_NEW_DOMAINS,
  payload: {
    data: value,
  },
});

export const removeNewDomain = (value) => ({
  type: actionsTypes.REMOVE_NEW_DOMAIN,
  payload: {
    data: value,
  },
});

export const handleNewDomainIsLoading = (value) => ({
  type: actionsTypes.HANDLE_NEW_DOMAIN_IS_LOADING,
  payload: {
    data: value,
  },
});

export const saveNewDomains = ({ newDomains: newDomainsData }) => (
  dispatch,
  getState
) => {
  const newDomains = new Set(newDomainsData.map(domain => domain.trim().toLowerCase()));

  const state = getState();
  const locale = localeCurrentSelector(state);

  let validationErrorMessage = null;

  newDomains.forEach((domain) => {
    if (!domain) {
      validationErrorMessage =
        messages[locale]['domains.toast.save_newdomains.domain.empty'];
    }
  });

  if (validationErrorMessage) {
    return toast.error(validationErrorMessage);
  }

  dispatch(handleNewDomainIsLoading(true));

  Api.addDomain({ domains: Array.from(newDomains) })
    .then((data) => {
      dispatch(handleNewDomainIsLoading(false));
      if (data) {
        dispatch(setIsOnNewDomain(false));
        dispatch(getDomainsData());
        dispatch(getDomainsAvailableCount());
        return toast.success(
          messages[locale]['domains.toast.save_newdomains.domain.added.success']
        );
      }
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewDomainIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(messages[locale]['error404.toast.description.text']);
      }
      return console.error('err', err);
    });
};

export const handleNewDomainMode = (value) => ({
  type: actionsTypes.HANDLE_NEW_DOMAIN_MODE,
  payload: {
    data: value,
  },
});

export const setNewDomainsFile = (value) => ({
  type: actionsTypes.SET_NEW_DOMAINS_FILE,
  payload: {
    data: value,
  },
});

export const handleNewDomainsFileIsUploading = (value) => ({
  type: actionsTypes.HANDLE_NEW_DOMAINS_FILE_IS_UPLOADING,
  payload: {
    data: value,
  },
})

export const handelSetNewDomainsFile = (file) =>  (dispatch, getState) =>{  
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!file) {
    return dispatch(setNewDomainsFile(null));
  }
  
  if (file.type !== 'text/csv') {
    return toast.error(messages[locale]['domains.toast.upload_csv.file.type.error']);
  }
  if (file.size > ONE_MEGA_BYTE) {
    return toast.error(messages[locale]['domains.toast.upload_csv.file.size.error']);
  }

  dispatch(setNewDomainsFile(file));
};

export const setResponseNewDomainsUploadedFile = (value) => ({
  type: actionsTypes.SET_RESPONSE_NEW_DOMAINS_UPLOADED_FILE,
  payload: {
    data: value,
  },
})

export const handelUploadNewDomainsFile = () => async (dispatch, getState) =>{
  const state = getState();
  const locale = localeCurrentSelector(state);
  const currentDomainsFile = domainsNewDomainsFileSelector(state);

  if (!currentDomainsFile) {
    return toast.error(messages[locale]['domains.toast.upload_csv.file.error']);
  }
  
  const formData = new FormData();

  formData.append('file', currentDomainsFile);

  try {
    dispatch(handleNewDomainsFileIsUploading(true));

    const data = await Api.importDomainsFromCSV(formData);

    dispatch(handleNewDomainsFileIsUploading(false));
    dispatch(handelSetNewDomainsFile(null));
    dispatch(setResponseNewDomainsUploadedFile(data)); 
    dispatch(getDomainsAvailableCount());
    dispatch(getDomainsData({ withLoader: false }));
    return toast.success(messages[locale]['domains.toast.save_newdomains.domain.added.success']);
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleNewDomainsFileIsUploading(false));
    dispatch(handelSetNewDomainsFile(null));
    dispatch(setResponseNewDomainsUploadedFile(null)); 
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
}

// rbl logs

export const setDomainRblChangesLogsPeriodFrom = (value) => ({
  type: actionsTypes.SET_DOMAIN_RBL_CHANGES_LOGS_PERIOD_FROM,
  payload: {
    data: value,
  },
});

export const setDomainRblChangesLogsPeriodTo = (value) => ({
  type: actionsTypes.SET_DOMAIN_RBL_CHANGES_LOGS_PERIOD_TO,
  payload: {
    data: value,
  },
});

export const handleDomainRblChangesLogsIsLoading = (value) => ({
  type: actionsTypes.HANDLE_DOMAIN_RBL_CHANGES_LOGS_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setDomainRblChangesLogs = (value) => ({
  type: actionsTypes.SET_DOMAIN_RBL_CHANGES_LOGS,
  payload: {
    data: value,
  },
});

export const setDomainRblChangesLogsErrors = (value) => ({
  type: actionsTypes.SET_DOMAINS_RBL_CHANGES_LOGS_ERRORS,
  payload: {
    data: value,
  },
});

export const getDomainRblChangesLogs = ({ id }) => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(messages[locale]['domains.toast.chosen_ip.id.error']);
  }

  try {
    dispatch(handleDomainRblChangesLogsIsLoading(true));

    const data = await Api.getDomainRblChangesLogs({ id });

    dispatch(setDomainRblChangesLogs(data));
    dispatch(handleDomainRblChangesLogsIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setDomainRblChangesLogsErrors({
          domainRblChangesLogs: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleDomainRblChangesLogsIsLoading(false));
    return console.error('err', err);
  }
};