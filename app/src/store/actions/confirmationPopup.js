import { toast } from 'react-toastify';
import actionsTypes from 'store/actionsTypes';
import messages from 'utils/messages';
import { localeCurrentSelector } from 'store/selectors';

export const handleConfirmationPopupShow = (value) => ({
  type: actionsTypes.HANDLE_CONFIRMATION_POPUP_SHOW,
  payload: {
    data: value,
  },
});

export const setConfirmationPopupTitle = (value) => ({
  type: actionsTypes.SET_CONFIRMATION_POPUP_TITLE,
  payload: {
    data: value,
  },
});

export const setConfirmationPopupCallback = (value) => ({
  type: actionsTypes.SET_CONFIRMATION_POPUP_CALLBACK,
  payload: {
    data: value,
  },
});

// THUNKS
// callback
export const setCallbackConfirmationPopup = (cb = null) => (
  dispatch,
  getState
) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!cb) {
    return toast.error(
      messages[locale]['confirmation_popup-redux.no.callback.text']
    );
  }

  dispatch(handleConfirmationPopupShow(true));
  dispatch(setConfirmationPopupCallback(cb));
};
