import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import { validateEmail, getStatusError, deepCompare } from 'utils/helpers';
import messages from 'utils/messages';
import {
  localeCurrentSelector,
  usersPaginationSelector,
  usersSortingSelector,
  usersDataSelector,
  authSwitchedUserSelector,
  authIsSwitchedUserModeSelector,
} from 'store/selectors';
import history from 'utils/history';
import { NOT_FOUND_ERROR_STATUS } from 'utils/constants';
import { setMyUserBillingPlanData } from './myUserBillingPlan';
import { handleIsSwitchedUserMode } from './auth';

export const setUsersErrors = (value) => ({
  type: actionsTypes.SET_USERS_ERRORS,
  payload: {
    data: value,
  },
});

export const usersSetMe = (data) => (dispatch) => {
  dispatch({
    type: actionsTypes.USERS_SET_ME,
    payload: {
      data,
    },
  });
};

export const usersHandleMeIsLoading = (data) => (dispatch) => {
  dispatch({
    type: actionsTypes.USERS_HANDLE_ME_IS_LOADING,
    payload: {
      data,
    },
  });
};

export const usersGetMe = () => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  dispatch(usersHandleMeIsLoading(true));

  Api.usersGetMe()
    .then((data) => {
      dispatch(usersSetMe(data));
      dispatch(usersHandleMeIsLoading(false));
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(usersHandleMeIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}userGetMe`
        );
      }
      return toast.error(err);
    });
};

export const setUsersSorting = (value) => ({
  type: actionsTypes.SET_USERS_SORTING,
  payload: {
    data: value,
  },
});

export const handleUsersDataIsLoading = (data) => ({
  type: actionsTypes.HANDLE_USERS_DATA_IS_LOADING,
  payload: {
    data,
  },
});

export const setUsersDataPagination = (value) => ({
  type: actionsTypes.SET_USERS_DATA_PAGINATION,
  payload: {
    data: value,
  },
});

export const getUsersData = ({ withLoader = true } = {}) => async (
  dispatch,
  getState
) => {
  const state = getState();

  try {
    if (withLoader) {
      dispatch(handleUsersDataIsLoading(true));
    }

    const usersData = usersDataSelector(state);
    const { field, order } = usersSortingSelector(state);
    const paginationData = usersPaginationSelector(state);
    const { offset, limit } = paginationData;

    const sort = order === 'desc' ? `-${field}` : field;

    const data = await Api.getUsersData({ offset, limit, sort });
    const { data: users, meta: pagination = {} } = data;

    if (
      !deepCompare(users, usersData) ||
      !deepCompare(pagination, paginationData)
    ) {
      dispatch({
        type: actionsTypes.SET_USERS_DATA,
        payload: {
          data,
        },
      });
    }
    dispatch(handleUsersDataIsLoading(false));
  } catch (err) {
    const status = getStatusError(err);

    if (status === NOT_FOUND_ERROR_STATUS) {
      dispatch(
        setUsersErrors({
          data: {
            message: err,
            status,
          },
        })
      );
    }
    dispatch(handleUsersDataIsLoading(false));
    return console.error(err);
  }
};

export const setEditedUser = (data) => ({
  type: actionsTypes.SET_EDITED_USER,
  payload: {
    data,
  },
});

export const handleEditedUser = (data) => ({
  type: actionsTypes.HANDLE_EDITED_USER,
  payload: {
    data,
  },
});

export const handleEditedUserSavingIsLoading = (data) => ({
  type: actionsTypes.HANDLE_EDITED_USER_SAVING_IS_LOADING,
  payload: {
    data,
  },
});

export const saveEditedUser = (data) => (dispatch, getState) => {
  const { id, email, password, role, name, is_active } = data;
  const state = getState();
  const locale = localeCurrentSelector(state);

  const requestData = {
    role,
    name,
    is_active,
  };

  // if there is no id, it means that we create a new user
  if (!id) {
    if (!email) {
      return toast.error(
        messages[locale]['users.toast.save_edited_user.email.error']
      );
    }
    if (!validateEmail(email)) {
      return toast.error(
        messages[locale]['users.toast.save_edited_user.email.valid.error']
      );
    }
    if (!password) {
      return toast.error(
        messages[locale]['users.toast.save_edited_user.password.error']
      );
    }
    if (password.length < 6) {
      return toast.error(
        messages[locale][
          'users.toast.save_edited_user.password.too_simple.error'
        ]
      );
    }
    if (!role) {
      return toast.error(
        messages[locale]['users.toast.save_edited_user.role.error']
      );
    }

    requestData.email = email;
    requestData.password = password;
    requestData.role = role;
  } else {
    requestData.id = id;
  }

  let method = '';
  let message = '';
  if (id) {
    method = 'updateUser';
    message = messages[locale]['users.toast.save_edited_user.updated'];
  } else {
    method = 'addUser';
    message = messages[locale]['users.toast.save_edited_user.added'];
  }

  dispatch(handleEditedUserSavingIsLoading(true));

  Api[method](requestData)
    .then((data) => {
      dispatch(handleEditedUserSavingIsLoading(false));
      dispatch(setEditedUser(null));
      dispatch(handleUsersDataIsLoading(true));
      dispatch(getUsersData({ withLoader: false }));
      return toast.success(message);
    })
    .catch((err) => {
      const status = getStatusError(err);

      dispatch(handleEditedUserSavingIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}saveEditedUser`
        );
      }
      return toast.error(err);
    });
};

export const refreshUserMainData = () => async (dispatch, getState) => {
  const state = getState();

  try {
    const myUserBillingPlan = await Api.getMyUserBillingPlan();
    const me = await Api.usersGetMe();
    dispatch(setMyUserBillingPlanData(myUserBillingPlan));
    dispatch(usersSetMe(me));

    const switchedUser = authSwitchedUserSelector(state);
    const IsSwitchedUserMode = authIsSwitchedUserModeSelector(state);
    const isReturnToAdmin = !switchedUser && IsSwitchedUserMode;

    if (isReturnToAdmin) {
      history.push('/users');
      dispatch(handleIsSwitchedUserMode(false));
    }
  } catch (err) {
    return console.error(err);
  }
};
