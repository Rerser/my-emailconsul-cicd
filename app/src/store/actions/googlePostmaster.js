import { toast } from 'react-toastify';
import Api from 'api';
import actionsTypes from 'store/actionsTypes';
import {
  localeCurrentSelector,
  googlePostmasterDataSelector,
  googlePostmasterDataFilterSelector,
  googlePostmasterDateFromSelector,
  googlePostmasterDateToSelector,
  googlePostmasterNewDomainsFileSelector,
} from 'store/selectors';
import { deepCompare, getStatusError } from 'utils/helpers';
import messages from 'utils/messages';
import { NOT_FOUND_ERROR_STATUS, ONE_MEGA_BYTE } from 'utils/constants';

export const setGpDomainsErrors = (value) => ({
  type: actionsTypes.SET_GP_ERRORS,
  payload: {
    data: value,
  },
});

export const handleGpDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_GP_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const handleGpDataIsLoaded = (value) => ({
  type: actionsTypes.HANDLE_GP_DATA_IS_LOADED,
  payload: {
    data: value,
  },
});

export const handleGpDataFilter = (value) => ({
  type: actionsTypes.HANDLE_GP_DATA_FILTER,
  payload: {
    data: value,
  },
});

export const setGpData = (value) => ({
  type: actionsTypes.SET_GP_DATA,
  payload: {
    data: value,
  },
});

export const getGpDomainsData = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  if (withLoader) {
    dispatch(handleGpDataIsLoading(true));
  }

  /*
    get params
  */
  const state = getState();
  const gpDomainsData = googlePostmasterDataSelector(state);
  const googlePostmasterDateFrom = googlePostmasterDateFromSelector(state);
  const googlePostmasterDateTo = googlePostmasterDateToSelector(state);

  const { domain } = googlePostmasterDataFilterSelector(state);

  dispatch(handleGpDataIsLoaded(false));
  Api.getGpDomainsData({ from: googlePostmasterDateFrom, to: googlePostmasterDateTo, domain })
    .then((data) => {
      const { data: domains = [] } = data;

      if (!deepCompare(domains, gpDomainsData)) {
        // update data and pagination data at once
        dispatch(setGpData(data));
      }
      dispatch(handleGpDataIsLoading(false));
      dispatch(handleGpDataIsLoaded(true));
    })
    .catch((err) => {
      const status = getStatusError(err);

      if (status === NOT_FOUND_ERROR_STATUS) {
        dispatch(
          setGpDomainsErrors({
            data: {
              message: err,
              status,
            },
          })
        );
      }
      dispatch(handleGpDataIsLoading(false));
      dispatch(handleGpDataIsLoaded(true));
      return console.error('err', err);
    });
};

export const deleteGpDomain = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['gpdomains.toast.delete_domain.id.error']
    );
  }

  Api.deleteGpDomain({ id })
    .then((data) => {
      dispatch(getGpDomainsData({ withLoader: false }));
      dispatch(getGpDomainsAvailableCount());
    })
    .catch((err) => {
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(messages[locale]['error404.toast.description.text']);
      }
      return toast.error(err);
    });
};

export const setIsOnNewGpDomain = (value) => ({
  type: actionsTypes.SET_IS_ON_NEW_GP_DOMAIN,
  payload: {
    data: value,
  },
});

export const handleNewGpDomains = (value) => ({
  type: actionsTypes.HANDLE_NEW_GP_DOMAINS,
  payload: {
    data: value,
  },
});

export const removeNewGpDomain = (value) => ({
  type: actionsTypes.REMOVE_NEW_GP_DOMAIN,
  payload: {
    data: value,
  },
});

export const handleNewGpDomainIsLoading = (value) => ({
  type: actionsTypes.HANDLE_NEW_DOMAIN_IS_LOADING,
  payload: {
    data: value,
  },
});

export const saveNewGpDomains = ({ newGPDomains: newDomainsData }) => (
  dispatch,
  getState
) => {
  const newDomains = new Set(newDomainsData.map(domain => domain.trim().toLowerCase()));

  const state = getState();
  const locale = localeCurrentSelector(state);

  let validationErrorMessage = null;

  newDomains.forEach((domain) => {
    if (!domain) {
      validationErrorMessage =
        messages[locale]['gpdomains.toast.save_newdomains.domain.empty'];
    }
  });

  if (validationErrorMessage) {
    return toast.error(validationErrorMessage);
  }

  dispatch(handleNewGpDomainIsLoading(true));

  Api.addGPDomain({ domains: Array.from(newDomains) })
    .then((data) => {
      dispatch(handleNewGpDomainIsLoading(false));
      if (data) {
        dispatch(setIsOnNewGpDomain(false));
        dispatch(getGpDomainsData());
        dispatch(getGpDomainsAvailableCount());
        return toast.success(
          messages[locale]['gpdomains.toast.save_newdomains.domain.added.success']
        );
      }
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewGpDomainIsLoading(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(messages[locale]['error404.toast.description.text']);
      }
      return console.error('err', err);
    });
};

export const setGpDateFrom = (value) => ({
  type: actionsTypes.SET_GP_DATE_FROM,
  payload: {
    data: value,
  },
});

export const setGpDateTo = (value) => ({
  type: actionsTypes.SET_GP_DATE_TO,
  payload: {
    data: value,
  },
});

export const setGpChosenData = (value) => ({
  type: actionsTypes.SET_GP_CHOSEN_DATA,
  payload: {
    data: value,
  }
});

export const handleGpDomainsAvailableCountIsLoading = (value) => ({
  type: actionsTypes.HANDLE_GP_DOMAINS_AVAILABLE_COUNT_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setGpDomainsAvailableCount = (value) => ({
  type: actionsTypes.SET_GP_DOMAINS_AVAILABLE_COUNT,
  payload: {
    data: value,
  },
});

export const getGpDomainsAvailableCount = () => async (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  dispatch(handleGpDomainsAvailableCountIsLoading(true));

  try {
    const data = await Api.getGpDomainsAvailableCount();
    dispatch(handleGpDomainsAvailableCountIsLoading(false));
    dispatch(setGpDomainsAvailableCount(data));
  } catch (err) {
    const status = getStatusError(err);
    dispatch(handleGpDomainsAvailableCountIsLoading(false));
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
};

export const handleNewGpDomainMode = (value) => ({
  type: actionsTypes.HANDLE_NEW_GP_DOMAIN_MODE,
  payload: {
    data: value,
  },
});

export const setNewGpDomainsFile = (value) => ({
  type: actionsTypes.SET_NEW_GP_DOMAINS_FILE,
  payload: {
    data: value,
  },
});

export const handleNewGpDomainsFileIsUploading = (value) => ({
  type: actionsTypes.HANDLE_NEW_GP_DOMAINS_FILE_IS_UPLOADING,
  payload: {
    data: value,
  },
})

export const handelSetNewGpDomainsFile = (file) =>  (dispatch, getState) =>{  
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!file) {
    return dispatch(setNewGpDomainsFile(null));
  }
  
  if (file.type !== 'text/csv') {
    return toast.error(messages[locale]['gpdomains.toast.upload_csv.file.type.error']);
  }
  if (file.size > ONE_MEGA_BYTE) {
    return toast.error(messages[locale]['gpdomains.toast.upload_csv.file.size.error']);
  }

  dispatch(setNewGpDomainsFile(file));
};

export const setResponseNewGpDomainsUploadedFile = (value) => ({
  type: actionsTypes.SET_RESPONSE_NEW_GP_DOMAINS_UPLOADED_FILE,
  payload: {
    data: value,
  },
});

export const handelUploadNewGpDomainsFile = () => async (dispatch, getState) =>{
  const state = getState();
  const locale = localeCurrentSelector(state);
  const currentDomainsFile = googlePostmasterNewDomainsFileSelector(state);

  if (!currentDomainsFile) {
    return toast.error(messages[locale]['gpdomains.toast.upload_csv.file.error']);
  }
  
  const formData = new FormData();

  formData.append('file', currentDomainsFile);

  try {
    dispatch(handleNewGpDomainsFileIsUploading(true));

    const data = await Api.importGPDomainsFromCSV(formData);

    dispatch(handleNewGpDomainsFileIsUploading(false));
    dispatch(handelSetNewGpDomainsFile(null));
    dispatch(setResponseNewGpDomainsUploadedFile(data)); 
    dispatch(getGpDomainsAvailableCount());
    dispatch(getGpDomainsData({ withLoader: false }));
    return toast.success(messages[locale]['gpdomains.toast.save_newdomains.domain.added.success']);
  } catch (err) {
    const status = getStatusError(err);

    dispatch(handleNewGpDomainsFileIsUploading(false));
    dispatch(handelSetNewGpDomainsFile(null));
    dispatch(setResponseNewGpDomainsUploadedFile(null)); 
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(messages[locale]['error404.toast.description.text']);
    }
    return console.error('err', err);
  }
}

export const handleGpShowTableMode = (value) => ({
  type: actionsTypes.HANDLE_GP_SHOW_TABLE_MODE,
  payload: {
    data: value,
  },
});

export const clearGpAllData = (value) => ({
  type: actionsTypes.CLEAR_GP_ALL_DATA,
});
