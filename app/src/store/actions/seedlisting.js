import { toast } from 'react-toastify';
import Api, { apiSetToken } from 'api';
import ApiMock from 'api/apiMock';
import actionsTypes from 'store/actionsTypes';
import { getMyUserBillingPlan } from 'store/actions/myUserBillingPlan';
import {
  authIsSwitchedUserModeSelector,
  authTokenSelector,
  newSeedlistingSelector,
  usersMeSelector,
  localeCurrentSelector,
  seedlistingDataSelector,
  seedlistingDataPaginationSelector,
} from 'store/selectors';
import {
  DEMO_USER_EMAIL,
  LOCAL_STORAGE_HOSTS_SETTINGS,
  LOCAL_STORAGE_TOKEN_KEY,
  NOT_FOUND_ERROR_STATUS,
  USER_ROLE,
} from 'utils/constants';
import messages from 'utils/messages';

import { deepCompare, getStatusError } from 'utils/helpers';

export const setSeedlistingErrors = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_ERRORS,
  payload: {
    data: value,
  },
});

export const handleSeedlistingDataIsLoading = (value) => ({
  type: actionsTypes.HANDLE_SEEDLISTING_DATA_IS_LOADING,
  payload: {
    data: value,
  },
});

export const setSeedlistingDataPagination = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_DATA_PAGINATION,
  payload: {
    data: value,
  },
});

export const setSeedlistingData = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_DATA,
  payload: {
    data: value,
  },
});

export const getSeedlistingData = ({ withLoader = true } = {}) => (
  dispatch,
  getState
) => {
  const state = getState();
  const seedlistingListData = seedlistingDataSelector(state);

  const me = usersMeSelector(state);
  const { email: meEmail } = me;

  if (withLoader) {
    dispatch(handleSeedlistingDataIsLoading(true));
  }

  // const { field, order } = seedlistingWorkersSortingSelector(state);
  const paginationData = seedlistingDataPaginationSelector(state);
  const { offset, limit } = paginationData;

  // const sort = order === 'desc' ? `-${field}` : field;

  if (meEmail === DEMO_USER_EMAIL) {
    return ApiMock.getSeedlistingData()
      .then((data) => {
        const { data: seedlistingList = [], meta: pagination = {} } = data;

        if (
          !deepCompare(seedlistingList, seedlistingListData) ||
          !deepCompare(pagination, paginationData)
        ) {
          dispatch(setSeedlistingData(data));
        }

        dispatch(handleSeedlistingDataIsLoading(false));
      });
  }

  Api.getSeedlistingData({ offset, limit /* , sort */ })
    .then((data) => {
      const { data: seedlistingList = [], meta: pagination = {} } = data;

      if (
        !deepCompare(seedlistingList, seedlistingListData) ||
        !deepCompare(pagination, paginationData)
      ) {
        dispatch(setSeedlistingData(data));
      }

      dispatch(handleSeedlistingDataIsLoading(false));
    })
    .catch((err) => {
      const status = getStatusError(err);

      if (status === NOT_FOUND_ERROR_STATUS) {
        dispatch(
          setSeedlistingErrors({
            data: {
              message: err,
              status,
            },
          })
        );
      }
      dispatch(handleSeedlistingDataIsLoading(false));
      return console.error('err', err);
    });
};

export const setNewSeedlisting = (value) => ({
  type: actionsTypes.SET_NEW_SEEDLISTING,
  payload: {
    data: value,
  },
});

export const handleNewSeedlisting = (value) => ({
  type: actionsTypes.HANDLE_NEW_SEEDLISTING,
  payload: {
    data: value,
  },
});

export const handleNewSeedlistingIsCreating = (value) => ({
  type: actionsTypes.HANDLE_NEW_SEEDLISTING_IS_CREATING,
  payload: {
    data: value,
  },
});

export const setSeedlistingShowedInfo = (value) => ({
  type: actionsTypes.SET_SEEDLISTING_SHOWED_INFO,
  payload: {
    data: value,
  },
});

// сохранение данных из хостов в localStorage
export const saveHostsSettings = () => (_, getState) => {
  const state = getState();
  const hosts = newSeedlistingSelector(state)?.hosts;

  if (hosts) {
    window.localStorage.setItem(
      LOCAL_STORAGE_HOSTS_SETTINGS,
      JSON.stringify(hosts)
    );
  }
};

// обновление данных хостов в localStorage
export const updateHostsSettings = ({ avaiableNum, avaiableHosts } = {}) => (
  _,
  getState
) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!avaiableNum) {
    return toast.error(
      messages[locale]['seedlisting.toast.update_hosts.avaiable_num.error']
    );
  }
  if (!avaiableHosts) {
    return toast.error(
      messages[locale]['seedlisting.toast.update_hosts.avaiable_hosts.error']
    );
  }
  // возьмем настройки из localstorage
  const hostsSettings = JSON.parse(
    window.localStorage.getItem(LOCAL_STORAGE_HOSTS_SETTINGS)
  );

  if (!hostsSettings?.length) return;
  // сформируем новый hostsSettings
  const newHostSettings = hostsSettings.map((host) => {
    const { value = '', emails_per_iteration = 0 } = host;
    // если хост со значением value еще доступен
    if (avaiableHosts?.length && avaiableHosts.includes(value)) {
      // если количество доступных хостов уже меньше
      if (avaiableNum < emails_per_iteration) {
        // вернуть уменьшенное значение
        return {
          ...host,
          emails_per_iteration: avaiableNum,
          disabled: false,
        };
      }
      return {
        ...host,
        disabled: false,
      };
    }
    // если хост со значением value уже не доступен, "обнулим" данный хост
    return {
      ...host,
      emails_per_iteration: 0,
      selected: false,
      disabled: true,
    };
  });
  // запишем новый hostsSettings в localstorage
  window.localStorage.setItem(
    LOCAL_STORAGE_HOSTS_SETTINGS,
    JSON.stringify(newHostSettings)
  );
};

export const getSeedlistingShowedInfo = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['seedlisting.toast.get_seedlisting_info.id.error']
    );
  }

  dispatch(handleNewSeedlistingIsCreating(true));

  Api.getSeedlistingItemData({ id })
    .then((data) => {
      dispatch(handleNewSeedlistingIsCreating(false));
      dispatch(setSeedlistingShowedInfo(data));
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewSeedlistingIsCreating(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']}`
        );
      }
      return toast.error(err);
    });
};

export const runSeedlisting = ({ title, hosts, check_type } = {}) => (
  dispatch,
  getState
) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!title) {
    return toast.error(
      messages[locale]['seedlisting.toast.run_seedlisting.title.error']
    );
  }
  if (!hosts || !hosts.length) {
    return toast.error(
      messages[locale]['seedlisting.toast.run_seedlisting.hosts.error']
    );
  }
  if (!check_type) {
    return toast.error(
      messages[locale]['seedlisting.toast.run_seedlisting.check_type.error']
    );
  }

  dispatch(handleNewSeedlistingIsCreating(true));

  Api.runSeedlisting({ title, hosts, check_type })
    .then((data) => {
      toast.success(
        messages[locale][
          'seedlisting.toast.run_seedlisting.start_seedlisting.success'
        ]
      );

      dispatch(getSeedlistingData({ withLoader: false }));
      dispatch(handleNewSeedlistingIsCreating(false));

      dispatch(setNewSeedlisting(null));
      dispatch(setSeedlistingShowedInfo(data));

      dispatch(getMyUserBillingPlan());
    })
    .catch((err) => {
      const status = getStatusError(err);
      dispatch(handleNewSeedlistingIsCreating(false));
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - runSeedlisting`
        );
      }
      return toast.error(err);
    });
};

export const exportSeedlistingEmails = ({ id }) => (_, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['seedlisting.toast.export_seedlisting_emails.id.error']
    );
  }

  Api.exportSeedlistingEmails({ id }).catch((err) => {
    const status = getStatusError(err);
    if (status === NOT_FOUND_ERROR_STATUS) {
      return toast.error(
        `${messages[locale]['error404.toast.description.text']} - exportSeedlistingEmails`
      );
    }
    return toast.error(err);
  });
};

export const terminateSeedlisting = ({ id }) => (dispatch, getState) => {
  const state = getState();
  const locale = localeCurrentSelector(state);

  const usersMe = usersMeSelector(state);
  const isSwitchedUserMode = authIsSwitchedUserModeSelector(state);
  const currentTokenStore = authTokenSelector(state);

  if (!id) {
    return toast.error(
      messages[locale]['seedlisting.toast.terminate_seedlisting.id.error']
    );
  }

  if (usersMe.role === USER_ROLE.USER && isSwitchedUserMode) {
    const adminToken = window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY);
    apiSetToken(adminToken);
  }

  Api.terminateSeedlisting({ id })
    .then((data) => {
      if (usersMe.role === USER_ROLE.USER && isSwitchedUserMode) {
        apiSetToken(currentTokenStore);
      }
      toast.success(
        messages[locale][
          'seedlisting.toast.terminate_seedlisting.terminate.success'
        ]
      );
      dispatch(getSeedlistingData({ withLoader: false }));
    })
    .catch((err) => {
      if (usersMe.role === USER_ROLE.USER && isSwitchedUserMode) {
        apiSetToken(currentTokenStore);
      }
      const status = getStatusError(err);
      if (status === NOT_FOUND_ERROR_STATUS) {
        return toast.error(
          `${messages[locale]['error404.toast.description.text']} - terminateSeedlisting`
        );
      }
      return toast.error(err);
    });
};
