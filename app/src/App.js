import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { IntlProvider } from 'react-intl';
import { InjectIntlContext } from '@comparaonline/react-intl-hooks';
import Tooltip from 'components/ui/tooltip';

// import Listcleaning from 'components/sections/listcleaning';
import SeedlistingList from 'components/sections/seedlisting/seedlistingList';
import SeedlistingItem from 'components/sections/seedlisting/seedlistingItem';
import SeedlistingWorkers from 'components/sections/seedlistingWorkers';
import SeedlistingEmailInfo from 'components/sections/seedlisting/seedlistingItem/components/seedlistingEmailInfo';
import IpsList from 'components/sections/ips/IpsList';
import DomainRblChangesLogs from 'components/sections/domains/DomainsList/components/DomainRblChangesLogs';
import IpRblChangesLogs from 'components/sections/ips/IpsList/components/IpRblChangesLogs';
import DomainsList from 'components/sections/domains/DomainsList';
import Users from 'components/sections/users';
import UserLayout from 'components/sections/userLayout';
import Error404 from 'components/ui/error404';
import UserLogs from 'components/sections/users/components/userLogs';
import SPFPage from 'components/sections/spfPage';
import DKIMPage from 'components/sections/dkimPage';
import DMARCPage from 'components/sections/dmarc';
import SNDS from 'components/sections/snds';
// import AdminPostmaster from 'components/sections/adminPostmaster';
import GooglePostmaster from 'components/sections/googlePostmaster';
import {
  Login,
  SignUp,
  Activation,
  RecoverPassword,
} from 'components/sections/auth';
import { GuestRoute, ProtectedRoute } from 'components/routes';

import { authTokenSelector, localeCurrentSelector } from 'store/selectors';
import { refreshUserMainData } from 'store/actions/users';
import { apiSetToken } from 'api';
import { USER_ROLE } from 'utils/constants';
import history from 'utils/history';
import messages from 'utils/messages';

// import 'antd/lib/tooltip/style/index.css';
import 'react-toastify/dist/ReactToastify.min.css';
import 'stylesheets/Main.scss';

function App() {
  const dispatch = useDispatch();

  const authToken = useSelector(authTokenSelector);
  const localeCurrent = useSelector(localeCurrentSelector);

  useEffect(() => {
    apiSetToken(authToken);

    if (authToken) {
      dispatch(refreshUserMainData());
    }
  }, [authToken, dispatch]);

  return (
    <IntlProvider
      locale={localeCurrent}
      messages={messages[localeCurrent]}
      defaultLocale="en"
    >
      <InjectIntlContext>
        <main className="App">
          <Router basename="/" history={history}>
            <Switch>
              <GuestRoute path="/login" component={Login} />
              <GuestRoute path="/signup" component={SignUp} />
              <GuestRoute path="/activation" component={Activation} />
              <GuestRoute
                path="/recover_password"
                component={RecoverPassword}
              />

              <ProtectedRoute
                roles={[USER_ROLE.ADMIN]}
                path="/userlogs/:id"
                component={UserLogs}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN]}
                path="/users"
                component={Users}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN]}
                path="/seedlisting-workers"
                component={SeedlistingWorkers}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/seedlisting/:id/:emailId"
                component={SeedlistingEmailInfo}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/seedlisting/:id"
                component={SeedlistingItem}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/seedlisting"
                component={SeedlistingList}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/ips/:id/rbl-logs"
                component={IpRblChangesLogs}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/ips"
                component={IpsList}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/domains/:id/rbl-logs"
                component={DomainRblChangesLogs}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/domains"
                component={DomainsList}
              />
              {/* <ProtectedRoute roles={[USER_ROLE.ADMIN, USER_ROLE.USER]} path="/" component={Listcleaning}/> */}
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/user"
                exact={false}
                component={UserLayout}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/google-postmaster"
                component={GooglePostmaster}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/spf"
                component={SPFPage}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/dkim"
                component={DKIMPage}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/dmarc"
                component={DMARCPage}
              />
              <ProtectedRoute
                roles={[USER_ROLE.ADMIN, USER_ROLE.USER]}
                path="/snds"
                component={SNDS}
              />
              {/* <ProtectedRoute
                roles={[USER_ROLE.ADMIN]}
                path="/admin-google-postmaster"
                component={AdminPostmaster}
              /> */}
              <Route
                exact
                path="/"
                render={() => <Redirect to="/seedlisting" />}
              />

              <Route render={() => <Error404 withHomeButton />} />
            </Switch>
          </Router>

          <ToastContainer />
          <Tooltip />
        </main>
      </InjectIntlContext>
    </IntlProvider>
  );
}

export default App;
