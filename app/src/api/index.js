import axios from 'axios';
import { toast } from 'react-toastify';
import { LOCAL_STORAGE_TOKEN_KEY } from 'utils/constants';
import { usersSetMe } from 'store/actions/users';
import { authSetToken } from 'store/actions/auth';
import { getLocale } from 'utils/helpers';
import messages from 'utils/messages';
import { toastErrorConfig } from 'utils/components-configs';
import ApiReal from './apiReal';
import ApiMock from './apiMock';

// export const ADDR = 'http://localhost:8000/api/v1.0';
const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

export const apiSetToken = (token) =>
  api && (api.defaults.headers.common.authorization = `bearer ${token}`);

const handleAnswer = ({ data }) => {
  if (data.error) {
    const msg = data.error.message || false;
    console.error(msg);
    return Promise.reject(msg.toString());
  }
  if (data.hasOwnProperty('data')) {
    if (data.meta) {
      return Promise.resolve(data);
    }
    return Promise.resolve(data.data ?? true);
  }

  return Promise.reject('Api error');
};

const handleError = (error) => {
  const locale = getLocale();
  const t = messages[locale];
  const { response } = error;
  const config = toastErrorConfig || {};

  if (response != null) {
    const { status, data } = response;

    switch (true) {
      case status === 400:
        if (data?.error?.message) {
          toast.error(data.error.message, config);
        }
        break;

      case status === 401:
        window.localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY);
        apiSetToken(null);
        usersSetMe(null);
        authSetToken(null);
        toast.error(t['handle.error.unauthorized'], config);
        return (window.location.href = '/');

      case status >= 500:
        toast.error(t['handle.error.internal.server'], config);
        break;

      default:
        break;
    }
    console.error(
      `${t['handle.error.status.text']} ${status}. ${
        data?.error?.message
          ? t['handle.error.error.text'] + data.error.message
          : ''
      }`
    );

    if (data?.error?.message) {
      return Promise.reject(data.error.message);
    }
  }

  return Promise.reject(error.toString());
};

export default process.env.__TEST_ENV__
  ? ApiMock
  : ApiReal(api, handleAnswer, handleError);
