import { BILLING_PLAN_NAME, NOT_FOUND_ERROR_STATUS } from 'utils/constants';

export default function (api, handleAnswer, handleError) {
  return {
    // listcleaning
    getListcleaningData() {
      return api.get('/listcleaning').then(handleAnswer).catch(handleError);
    },

    runListcleaning(data) {
      return api
        .post('/listcleaning/upload', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    terminateListCleaning(data) {
      return api
        .post('/listcleaning/terminate', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    // auth
    authLogin(data) {
      return api
        .post('/auth/login', data)
        .then(handleAnswer)
        .catch(handleError);
    },
    authSignUp(data) {
      return api
        .post('/auth/sign_up', data)
        .then(handleAnswer)
        .catch(handleError);
    },
    authActivation(data) {
      return api
        .post('/auth/activation', data)
        .then(handleAnswer)
        .catch(handleError);
    },
    authDropPassword(data) {
      return api
        .post('/auth/drop_password', data)
        .then(handleAnswer)
        .catch(handleError);
    },
    authChangePassword(data) {
      return api
        .post('/auth/change_password', data)
        .then(handleAnswer)
        .catch(handleError);
    },
    authSwitchUser(data) {
      return api
        .post('/auth/switch_user', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    // users
    usersGetMe(data) {
      return api.get('/users/me').then(handleAnswer).catch(handleError);
    },

    getUsersData({ offset, limit, sort }) {
      const query = `limit=${limit}&offset=${offset}&sort=${sort}`;

      return api.get(`/users?${query}`).then(handleAnswer).catch(handleError);
    },

    addUser(data) {
      return api.post('/users', data).then(handleAnswer).catch(handleError);
    },

    updateUser(data) {
      return api
        .patch(`/users/${data.id}`, data)
        .then(handleAnswer)
        .catch(handleError);
    },

    // user logs
    getUserLogs(data) {
      const { user_id, offset, limit } = data;
      const query = `limit=${limit}&offset=${offset}`;
      return api
        .get(`/user_activity_log/${user_id}?${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // userBillingPlan
    getUserBillingPlan(data) {
      const { user_id } = data;
      return api
        .get(`/user_billing_plan/${user_id}`)
        .then(handleAnswer)
        .catch((err) => {
          // in this case if we gat 404, it means that user has no billing plan yet
          if (err?.response?.status === 404) {
            // return like we get a new one
            return handleAnswer({
              data: {
                data: {
                  user_id,
                  current_plan: null,
                  timestamp_to: null,
                  listcleaning: {
                    emails_available: 0,
                  },
                  seedlisting: {
                    emails_available: 0,
                  },
                },
              },
            });
          }

          return handleError(err);
        });
    },

    establishUserBillingPlan({ user_id, data }) {
      return api.post(`/user_billing_plan/establish/${user_id}`, data).then(handleAnswer).catch(handleError);
    },

    setNewLimitsUserBillingPlan({ user_id, data }) {
      return api.post(`/user_billing_plan/set_new_limits/${user_id}`, data).then(handleAnswer).catch(handleError);
    },

    discardUserBillingPlan({ user_id, data }) {
      return api.post(`/user_billing_plan/discard/${user_id}`, data).then(handleAnswer).catch(handleError);
    },

    getMyUserBillingPlan(data) {
      return api
        .get('/user_billing_plan')
        .then(handleAnswer)
        .catch((err) => {
          // in this case if we gat 404, it means that user has no billing plan yet
          if (err?.response?.status === NOT_FOUND_ERROR_STATUS) {
            // return like we get a new one
            return handleAnswer({
              data: {
                data: {
                  listcleaning: {
                    emails_available: 0,
                  },
                  is404Error: true, // флаг, что произошла ошибка 404
                },
              },
            });
          }

          return handleError(err);
        });
    },

    getUserBillingPlanGeneralByPlan() {
      return api
        .get('/user_billing_plan/general/plan')
        .then(handleAnswer)
        .catch(handleError);
    },

    getUserBillingPlanGeneral() {
      return api
        .get('/user_billing_plan/general/all')
        .then(handleAnswer)
        .catch(handleError);
    },

    // seedlisting
    getSeedlistingData({ offset, limit, sort }) {
      let query = `limit=${limit}&offset=${offset}`;

      if (sort) {
        query += `&sort=${sort}`;
      }

      return api
        .get(`/seedlisting?${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getSeedlistingItemData(data) {
      const { id } = data;
      return api
        .get(`/seedlisting/${id}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getSeedlistingItemEmailInfo(data) {
      const { id, seedlistingEmailId } = data;
      return api
        .get(`/seedlisting/${id}/seedlisting_email/${seedlistingEmailId}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    runSeedlisting(data) {
      return api
        .post('/seedlisting', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    exportSeedlistingEmails({ id }) {
      return api
        .get(`/seedlisting/${id}/export_emails`)
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', `${id}.csv`);
          document.body.appendChild(link);
          link.click();
          link.parentNode.removeChild(link);
        })
        .catch(handleError);
    },

    terminateSeedlisting(data) {
      const { id } = data;
      return api
        .post(`/seedlisting/${id}/terminate`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // seedlisting public reports
    getSeedlistingShareableLink(seedlistingId) {
      return api
        .get(`/seedlisting_public_reports/${seedlistingId}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    addSeedlistingShareableLink(data) {
      return api
        .post('/seedlisting_public_reports', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteSeedlistingShareableLink(linkId) {
      return api
        .delete(`/seedlisting_public_reports/${linkId}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // seedlistingWorkers
    getSeedlistingWorkersData({ offset, limit, sort }) {
      const query = `limit=${limit}&offset=${offset}&sort=${sort}`;

      return api
        .get(`/seedlisting_workers?${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    addSeedlistingWorker(data) {
      return api
        .post('/seedlisting_workers', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteSeedlistingWorker({ id }) {
      return api
        .delete(`/seedlisting_workers/${id}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    updateSeedlistingWorker({ id, data }) {
      return api
        .patch(`/seedlisting_workers/${id}`, {
          ...data,
        })
        .then(handleAnswer)
        .catch(handleError);
    },

    getSeedlistingWorkersHosts() {
      return api
        .get('/seedlisting_workers/hosts')
        .then(handleAnswer)
        .catch(handleError);
    },

    restartAllSeedlistingWorkers() {
      return api
        .post('/seedlisting_workers/restart')
        .then(handleAnswer)
        .catch(handleError);
    },

    terminateAllSeedlistingWorkers() {
      return api
        .post('/seedlisting_workers/terminate')
        .then(handleAnswer)
        .catch(handleError);
    },

    // payment
    getLiqpayNewPaymentData({
      billingPlanName,
      currency,
      seedlistingEmailsAvailable,
      billingPlanType,
    }) {
      let str = `/payment/liqpay/${billingPlanName}?currency=${currency}&billing_plan_type=${billingPlanType}`;
      if (billingPlanName === BILLING_PLAN_NAME.ON_GO) {
        str += `&seedlisting_emails_available=${seedlistingEmailsAvailable}`;
      }
      return api.get(str).then(handleAnswer).catch(handleError);
    },

    addWireTransferNewPaymentData(data) {
      return api
        .post('/payment/wire_transfer_request', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    addDeliverabilityConsulting(data) {
      return api
        .post('/contact_us/deliverability_consulting', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    // ips
    getIpsData({ offset, limit, netmask, ip, sort }) {
      let query = `limit=${limit}&offset=${offset}&sort=${sort}`;

      if (netmask) {
        query += `&netmask=${netmask}`;
      }
      if (ip) {
        query += `&ip=${ip}`;
      }

      return api.get(`/ips?${query}`).then(handleAnswer).catch(handleError);
    },

    addIp(data) {
      return api
        .post('/ips/add_bulk', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteIp({ id }) {
      return api
        .delete(`/ips/${id}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteIpsFromFilters({ filterIp, netmask }) {
      let ipQuery = '';
      let netmaskQuery = '';

      if (filterIp) {
        ipQuery = `ip=${filterIp}`;
      }

      if (netmask) {
        netmaskQuery = `netmask=${netmask}`;
      }

      const netmaskQueryThroughIpQuery = (ipQuery && netmaskQuery ? `&` : '') + netmaskQuery;
      
      const query = ipQuery || netmaskQuery 
        ? `?${ipQuery}${netmaskQueryThroughIpQuery}` 
        : '';

      return api
        .delete(`/ips${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    refreshIp({ id }) {
      return api
        .post(`/ips/${id}/refresh`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getIpsAvailableCount() {
      return api
        .get('/ips/available_count')
        .then(handleAnswer)
        .catch(handleError);
    },

    importIpsFromCSV(data) {
      return api
        .post('ips/import', data, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': 'text/csv'
          }
        })
        .then(handleAnswer)
        .catch(handleError);
    },

    getIpRblChangesLogs({ id }) {
      return api
        .get(`/ips/${id}/rbl_changes_log`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // domains
    getDomainsData({ offset, limit, domain, sort }) {
      let query = `limit=${limit}&offset=${offset}&sort=${sort}`;

      if (domain) {
        query += `&domain=${domain}`;
      }

      return api.get(`/domains?${query}`).then(handleAnswer).catch(handleError);
    },

    addDomain(data) {
      return api
        .post('/domains/add_bulk', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteDomain({ id }) {
      return api
        .delete(`/domains/${id}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteDomainsFromFilters({ filterDomain }) {
      const query = filterDomain ? `?domain=${filterDomain}` : '';

      return api
        .delete(`/domains${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    refreshDomain({ id }) {
      return api
        .post(`/domains/${id}/refresh`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getDomainsAvailableCount() {
      return api
        .get('/domains/available_count')
        .then(handleAnswer)
        .catch(handleError);
    },

    importDomainsFromCSV(data) {
      return api
        .post('domains/import', data, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': 'text/csv'
          }
        })
        .then(handleAnswer)
        .catch(handleError);
    },

    getDomainRblChangesLogs({ id }) {
      return api
        .get(`/domains/${id}/rbl_changes_log`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // ipsReport
    getIpsReportData() {
      return api
        .get('/ips_report')
        .then(handleAnswer)
        .catch((err) => {
          // in this case if we gat 404, it means that user has no ips report plan yet
          if (err?.response?.status === NOT_FOUND_ERROR_STATUS) {
            // return like we get a new one
            return handleAnswer({
              data: {
                data: {
                  is_active: false,
                  emails: [],
                  utc_hours: null,
                  utc_minutes: null,
                  isNew: true, // consider that it is a new ips report
                },
              },
            });
          }

          return handleError(err);
        });
    },

    addIpsReport(data) {
      return api
        .post('/ips_report', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    updateIpsReport(data) {
      return api
        .patch('/ips_report', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    // netmask
    addNetmask(data) {
      return api.post('/netmask', data).then(handleAnswer).catch(handleError);
    },

    deleteNetmask(netmask) {
      return api
        .delete(`/netmask?netmask=${netmask}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getNetmaskList() {
      return api.get('/netmask').then(handleAnswer).catch(handleError);
    },

    getEpcIp(ip) {
      return api.get(`/epc/ip/${ip}`).then(handleAnswer).catch(handleError);
    },

    getEpcDomain(domain) {
      return api
        .get(`/epc/domain/${domain}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getSpamAssasin({ seedlistingId, seedlistingEmailId }) {
      return api
        .get(
          `/seedlisting/${seedlistingId}/seedlisting_email/${seedlistingEmailId}/spamassassin`
        )
        .then(handleAnswer)
        .catch(handleError);
    },

    getAuthenticationSPF(domain) {
      return api
        .get(`/epc/spf/?domain=${domain}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getAuthenticationDKIMTextualRepresentation(domain, selector) {
      return api
        .post(`/epc/dkim/validate_textual_representation`, { domain, selector })
        .then(handleAnswer)
        .catch(handleError);
    },

    getAuthenticationDKIMValidateHeader(dkim_signature_header ) {
      return api
        .post(`/epc/dkim/validate_signature_header`, { dkim_signature_header })
        .then(handleAnswer)
        .catch(handleError);
    },

    getAuthenticationDMARC(domain) {
      return api
        .get(`/epc/dmarc/?domain=${domain}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // admin postmaster
    // getAdminPostmasterAuthUrl() {
    //   return api
    //     .get('/gpdomains/auth_url')
    //     .then(handleAnswer)
    //     .catch(handleError);
    // },

    //google postmaster
    getGpDomainsData({ from: fromDate, to: toDate, domain }) {
      let query = '';

      if (domain) {
        query += `&domain=${domain}`;
      }
      return api.get(`/gpdomains?stats_date_from=${fromDate}&stats_date_to=${toDate}${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    addGPDomain(data) {
      return api
        .post('/gpdomains/add_bulk', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteGpDomain({ id }) {
      return api.delete(`/gpdomains/${id}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getGpDomainsAvailableCount() {
      return api
        .get('/gpdomains/available_count')
        .then(handleAnswer)
        .catch(handleError);
    },

    importGPDomainsFromCSV(data) {
      return api
        .post('gpdomains/import', data, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': 'text/csv'
          }
        })
        .then(handleAnswer)
        .catch(handleError);
    },

    // gp user integration
    checkGpUserCredentials() {
      return api
        .get('/gpdomains/check_creds')
        .then(handleAnswer)
        .catch(handleError);
    },

    sendGpSecrets(data) {
      return api
        .post('/gpdomains/creds', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    getGPAuthUrl() {
      return api
        .get('/gpdomains/auth_url')
        .then(handleAnswer)
        .catch(handleError);
    },

    getGpDomainsHistory({ limit, offset }) {
      const query = `limit=${limit}&offset=${offset}`;

      return api
        .get(`/gpdomains/history?${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    sendGpDateRun(data) {
      return api
        .post('/gpdomains/run', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    // snds

    getSndsCredentials() {
      return api
        .get('/snds/creds')
        .then(handleAnswer)
        .catch(handleError);
    },

    sendSndsCredentials(data) {
      return api
        .post('/snds/creds', data)
        .then(handleAnswer)
        .catch(handleError);
    },

    deleteSndsCredentialKey(id) {
      return api
        .delete(`/snds/creds/${id}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    updateSndsCredentialKey({ id, status }) {
      return api
        .patch(`/snds/creds/${id}`, { status })
        .then(handleAnswer)
        .catch(handleError);
    },

    getSndsIpStatus({ fromDate, toDate, offset, limit, sort, ips }) {
      const queryDate = `period_from=${fromDate}&period_to=${toDate}`
      const queryPagination = `limit=${limit}&offset=${offset}&sort=${sort}`;
      let queryFilter = '';

      if (ips.length) {
        queryFilter = `&ips=${ips.join(',')}`;
      }

      return api.get(`/snds/ip_status?${queryDate}&${queryPagination}${queryFilter}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getSndsIpData({ fromDate, toDate, offset, limit, sort, ips, filterResult }) {
      const queryDate = `period_from=${fromDate}&period_to=${toDate}`
      const queryPagination = `limit=${limit}&offset=${offset}&sort=${sort}`;
      let queryFilter = '';

      if (ips.length) {
        queryFilter = `&ips=${ips.join(',')}`;
      }

      if (filterResult.length) {
        queryFilter += `&filter_result=${filterResult.join(',')}`
      }

      return api.get(`/snds/ip_data?${queryDate}&${queryPagination}${queryFilter}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getSndsUserIps(type) {
      return api.get(`/snds/user_ips?type=${type}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    getSndsDashboardsData({ fromDate, toDate }) {
      const queryDate = `period_from=${fromDate}&period_to=${toDate}`

      return api.get(`/snds/aggregated?${queryDate}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    // snds integration

    getSndsHistory({ limit, offset }) {
      const query = `limit=${limit}&offset=${offset}`;

      return api
        .get(`/snds/history?${query}`)
        .then(handleAnswer)
        .catch(handleError);
    },

    sendSndsDateRun(data) {
      return api
        .post('/snds/run', data)
        .then(handleAnswer)
        .catch(handleError);
    },
  };
}
